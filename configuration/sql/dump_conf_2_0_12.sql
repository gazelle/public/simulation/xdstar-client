--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: app_configuration; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE app_configuration (
    id integer NOT NULL,
    value character varying(255),
    variable character varying(255)
);


ALTER TABLE public.app_configuration OWNER TO gazelle;

--
-- Data for Name: app_configuration; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO app_configuration VALUES (1, '/opt/XDStarClient', 'epsosFolder');
INSERT INTO app_configuration VALUES (2, '/opt/XDStarClient/tmp', 'uploadFolder');
INSERT INTO app_configuration VALUES (4, 'gazelle', 'keystore_pass');
INSERT INTO app_configuration VALUES (6, 'gazelle', 'key_pass');
INSERT INTO app_configuration VALUES (7, 'cda.xsl', 'cda_xsl_path');
INSERT INTO app_configuration VALUES (8, 'XDR Test Pratician', 'default_pratician_id');
INSERT INTO app_configuration VALUES (13, 'UTC+01', 'application_time_zone');
INSERT INTO app_configuration VALUES (14, 'Eric Poiseau', 'application_admin_name');
INSERT INTO app_configuration VALUES (20, '/opt/XDStarClient/eDTemplate.xml', 'dispensation_template_path');
INSERT INTO app_configuration VALUES (21, '/opt/XDStarClient/eCTemplate.xml', 'consent_template_path');
INSERT INTO app_configuration VALUES (23, 'changeit', 'home_root_oid'); -- example : 1.3.6.1.4.1.12559.11.3.1
INSERT INTO app_configuration VALUES (25, 'changeit', 'ihe_source_root_oid'); -- example : 1.3.6.1.4.1.21367.2012.2.1.1
INSERT INTO app_configuration VALUES (16, '/opt/XDStarClient/uploadedFiles', 'registered_files_directory');
INSERT INTO app_configuration VALUES (11, 'XDStarClient', 'application_name');
INSERT INTO app_configuration VALUES (15, '/opt/truststore.epsos.jks', 'simu_jks_path');
INSERT INTO app_configuration VALUES (5, 'gazelle', 'key_alias');
INSERT INTO app_configuration VALUES (17, 'changeit', 'document_root_oid'); -- example : 1.3.6.1.4.1.12559.11.1.2.2.1.1.3.
INSERT INTO app_configuration VALUES (18, 'changeit', 'submission_set_root_oid'); -- example : 1.3.6.1.4.1.12559.11.1.2.2.1.1.4.
INSERT INTO app_configuration VALUES (3, 'changeit', 'epsos_source_root_oid'); -- example : 1.3.6.1.4.1.12559.11.1.2.2.1.3
INSERT INTO app_configuration VALUES (32, 'NOVAK', 'family');
INSERT INTO app_configuration VALUES (33, 'JAN', 'given');
INSERT INTO app_configuration VALUES (34, '19501201', 'birthdate');
INSERT INTO app_configuration VALUES (35, '', 'root');
INSERT INTO app_configuration VALUES (36, '', 'gender');
INSERT INTO app_configuration VALUES (37, '', 'extension');
INSERT INTO app_configuration VALUES (38, '', 'street');
INSERT INTO app_configuration VALUES (39, '', 'city');
INSERT INTO app_configuration VALUES (40, '', 'country');
INSERT INTO app_configuration VALUES (41, '', 'postalcode');
INSERT INTO app_configuration VALUES (42, '', 'maidenname');
INSERT INTO app_configuration VALUES (43, 'changeit', 'homeCommunityID'); -- example : 1.3.6.1.4.1.12559.11.1.2.2.2.1
INSERT INTO app_configuration VALUES (44, 'http://jumbo.irisa.fr:8080/SchematronValidator-SchematronValidator-ejb/GazelleObjectValidatorWS?wsdl', 'schematron_validator');
INSERT INTO app_configuration VALUES (19, '50000', 'connection_timeout');
INSERT INTO app_configuration VALUES (45, '/usr/bin/', 'dicom3tools_validator');
INSERT INTO app_configuration VALUES (26, '/opt/XDStarClient/xsd/IHE/XDSI.b_ImagingDocumentSource.xsd', 'xds_xsd');
INSERT INTO app_configuration VALUES (51, '/opt/XDStarClient/xsd/DSUB/soap-envelope.xsd', 'dsub_xsd');
INSERT INTO app_configuration VALUES (53, '/opt/XDStarClient/', 'server_local_path');
INSERT INTO app_configuration VALUES (9, 'abderrazek.boufahja@ihe-europe.net', 'application_admin_email');
INSERT INTO app_configuration VALUES (55, 'false', 'application_works_without_cas');
INSERT INTO app_configuration VALUES (56, '/opt/XDStarClient/attachments/', 'attachement_files_directory');
INSERT INTO app_configuration VALUES (57, 'true', 'display_epSOS_menu');
INSERT INTO app_configuration VALUES (58, 'true', 'display_IHE_ITI_menu');
INSERT INTO app_configuration VALUES (59, 'true', 'display_IHE_PHARM_menu');
INSERT INTO app_configuration VALUES (60, 'true', 'display_IHE_RAD_menu');
INSERT INTO app_configuration VALUES (28, 'https://gazelle.ihe.net/cas', 'cas_url');
INSERT INTO app_configuration VALUES (64, 'false', 'security-policies');
INSERT INTO app_configuration VALUES (65, 'SAMEORIGIN', 'X-Frame-Options');
INSERT INTO app_configuration VALUES (66, 'private, no-cache, no-store, must-revalidate, max-age=0', 'Cache-Control');
INSERT INTO app_configuration VALUES (67, 'max-age=31536000 ; includeSubDomains', 'Strict-Transport-Security');
INSERT INTO app_configuration VALUES (68, '', 'X-Content-Security-Policy');
INSERT INTO app_configuration VALUES (69, 'Use X-Content-Security-Policy values', 'X-WebKit-CSP');
INSERT INTO app_configuration VALUES (70, 'Use X-Content-Security-Policy-Report-Only values', 'X-WebKit-CSP-Report-Only');
INSERT INTO app_configuration VALUES (71, 'default-src ''self'' *.ihe.net www.epsos.eu; script-src ''self'' ''unsafe-eval'' ''unsafe-inline''; style-src ''self'' ''unsafe-inline'';', 'X-Content-Security-Policy-Report-Only');
INSERT INTO app_configuration VALUES (72, 'false', 'sql_injection_filter_switch');
INSERT INTO app_configuration VALUES (63, 'http://_HOSTANDPORT/XDStarClient-ejb/XDSDeletetWSService/XDSDeletetWS?wsdl', 'xdstar_delete_doc_endpoint');
INSERT INTO app_configuration VALUES (62, 'http://_HOSTANDPORT/XDStarClient-ejb/DSUBRecipientWSService/DSUBRecipientWS?wsdl', 'xdstar_dsub_recipient_endpoint');
INSERT INTO app_configuration VALUES (54, 'http://jumbo-3.irisa.fr:9080/epsos/services/rg', 'xdstool_xca_query_wsdl');
INSERT INTO app_configuration VALUES (24, 'https://gazelle.ihe.net/SVSSimulator/rest/RetrieveValueSetForSimulator', 'svs_repository_url');
INSERT INTO app_configuration VALUES (27, 'https://gazelle.ihe.net/xsl/xdsDetailedResult.xsl', 'validation_result_xslt');
INSERT INTO app_configuration VALUES (29, 'https://gazelle.ihe.net/content/xdstarclient', 'app_documentation');
INSERT INTO app_configuration VALUES (10, 'https://gazelle.ihe.net/jira/browse/XDSTAR', 'application_issue_tracker_url');
INSERT INTO app_configuration VALUES (12, 'https://gazelle.ihe.net/jira/browse/XDSTAR#selectedTab=com.atlassian.jira.plugin.system.project%3Achangelog-panel', 'application_release_notes_url');
INSERT INTO app_configuration VALUES (22, 'http://_HOSTANDPORT/XDStarClient/', 'application_url');
INSERT INTO app_configuration VALUES (61, 'https://gazelle.ihe.net/AssertionManagerGui', 'assertion_manager_url');
INSERT INTO app_configuration VALUES (46, 'https://gazelle.ihe.net/CDAGenerator-CDAGenerator-ejb/CDAValidatorWS?wsdl', 'cda_mbv_wsdl');
INSERT INTO app_configuration VALUES (48, 'https://gazelle.ihe.net/CDAGenerator-CDAGenerator-ejb/CDAValidatorWS?wsdl', 'cda_mbv_wsdl');
INSERT INTO app_configuration VALUES (47, 'https://gazelle.ihe.net/xsl/mbcdaDetailedResult.xsl', 'cda_mbv_xslt');
INSERT INTO app_configuration VALUES (49, 'https://gazelle.ihe.net/xsl/mbcdaDetailedResult.xsl', 'cda_mbv_xslt');
INSERT INTO app_configuration VALUES (50, 'https://gazelle.ihe.net/DDSWS/DemographicDataServerBean?wsdl', 'dds_web_service');
INSERT INTO app_configuration VALUES (52, 'https://gazelle.ihe.net/EVSClient/', 'evs_client_url');


--
-- Name: app_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY app_configuration
    ADD CONSTRAINT app_configuration_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--
