<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="//conceptList">
        <RetrieveValueSetResponse xmlns="urn:ihe:iti:2008">
            <ValueSet displayName="RoleCodeNL - zorgverlenertype (personen)" version="2016-04-01T00:00:00" id="2.16.840.1.113883.2.4.3.11.60.1.11.2">
                <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
                <xsl:attribute name="displayName"><xsl:value-of select="parent::node()/parent::node()/@shortName"/></xsl:attribute>
                <xsl:attribute name="version"><xsl:value-of select="parent::node()/@effectiveDate"/></xsl:attribute>
                <ConceptList xml:lang="en-US">
                    <xsl:for-each select="concept">
                        <Concept>
                            <xsl:attribute name="code"><xsl:value-of select="@code"/></xsl:attribute>
                            <xsl:attribute name="codeSystem"><xsl:value-of select="@codeSystem"/></xsl:attribute>
                            <xsl:attribute name="displayName"><xsl:value-of select="@displayName"/></xsl:attribute>
                        </Concept>
                    </xsl:for-each>
                </ConceptList>
            </ValueSet>
        </RetrieveValueSetResponse>
    </xsl:template>
</xsl:stylesheet>