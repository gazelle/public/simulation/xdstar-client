<xsl:stylesheet version="1.0" xmlns:t="http://www.test.com/" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xslFormatting="urn:xslFormatting" xmlns:ns2="metadata" xmlns:ns3="urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/ns2:AdhocQueryMetadata">
        <ns2:AdhocQueryMetadata>
        <ns2:uuid><xsl:value-of select="ns2:uuid"/></ns2:uuid>
        <ns2:name><xsl:value-of select="ns2:name"/></ns2:name>
        <ns2:displayAttributeName><xsl:value-of select="ns2:displayAttributeName"/></ns2:displayAttributeName>
        <xsl:for-each select="ns2:slot_optional">
            <ns2:slotDescriber>
                <slotMetadata>
                    <ns2:name><xsl:value-of select="ns2:name"/></ns2:name>
                    <ns2:multiple><xsl:value-of select="ns2:multiple"/></ns2:multiple>
                    <ns2:defaultValue><xsl:value-of select="ns2:defaultValue"/></ns2:defaultValue>
                    <ns2:attribute><xsl:value-of select="ns2:attribute"/></ns2:attribute>
                    <ns2:valueset><xsl:value-of select="ns2:valueset"/></ns2:valueset>
                    <ns2:isNumber><xsl:value-of select="ns2:isNumber"/></ns2:isNumber>
                </slotMetadata>
                <cardinality>
                    <xsl:if test="ns2:supportAndOr = 'false'">_01</xsl:if>
                    <xsl:if test="ns2:supportAndOr = 'true'">_0star</xsl:if>
                </cardinality>
            </ns2:slotDescriber>
        </xsl:for-each>
        <xsl:for-each select="ns2:slot_required">
            <ns2:slotDescriber>
                <slotMetadata>
                    <ns2:name><xsl:value-of select="ns2:name"/></ns2:name>
                    <ns2:multiple><xsl:value-of select="ns2:multiple"/></ns2:multiple>
                    <ns2:defaultValue><xsl:value-of select="ns2:defaultValue"/></ns2:defaultValue>
                    <ns2:attribute><xsl:value-of select="ns2:attribute"/></ns2:attribute>
                    <ns2:valueset><xsl:value-of select="ns2:valueset"/></ns2:valueset>
                    <ns2:isNumber><xsl:value-of select="ns2:isNumber"/></ns2:isNumber>
                </slotMetadata>
                <cardinality>
                    <xsl:if test="ns2:supportAndOr = 'false'">_11</xsl:if>
                    <xsl:if test="ns2:supportAndOr = 'true'">_1star</xsl:if>
                </cardinality>
            </ns2:slotDescriber>
        </xsl:for-each>
        <xsl:for-each select="ns2:returnTypes">
            <ns2:returnTypes><xsl:value-of select="."/></ns2:returnTypes>
        </xsl:for-each>
        </ns2:AdhocQueryMetadata>
    </xsl:template>
</xsl:stylesheet>