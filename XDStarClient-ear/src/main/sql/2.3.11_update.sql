-- Add new transaction type, those inserts will failed if the XCDR profile was already added in the tool manually  --
INSERT INTO tf_transaction (id, description, keyword, name) VALUES (nextval('tf_transaction_id_seq'), 'Cross-Gateway Document Provide', 'ITI-80', 'XCDR');
INSERT INTO affinity_domain (id, keyword, label_to_display, profile) VALUES (nextval('affinity_domain_id_seq'), 'IHE_XCDR', 'IHE (XCDR)', 'XCDR');
INSERT INTO affinity_domain_transactions (affinity_domain_id, transaction_id) VALUES ((SELECT id FROM affinity_domain WHERE keyword = 'IHE_XCDR'), (SELECT id FROM tf_transaction WHERE keyword = 'ITI-80'));
INSERT INTO usage_metadata (id, action, keyword, affinity_id, transaction_id) VALUES (47, 'Cross-Gateway Document Provide', 'XCDR', (SELECT id FROM affinity_domain WHERE keyword = 'IHE_XCDR'), (SELECT id FROM tf_transaction WHERE keyword = 'ITI-80'));
INSERT INTO registry_object_list_metadata (id, displayattributename, name) VALUES (7,'XCDR', 'XCDR');
INSERT INTO transaction_type (id, name) VALUES (nextval('transaction_type_id_seq'), 'XCDR');
INSERT INTO transaction_type_tf_transaction (transaction_type_id, transaction_id) VALUES ((SELECT id FROM transaction_type WHERE name = 'XCDR'), (SELECT id FROM tf_transaction WHERE keyword = 'ITI-80'));
------------------------------------------------------------------------------------------


-- Add XCDR validator (request & response) --
INSERT INTO validator_pack_class (id, extractnodeenum, hasnistvalidation, ispnr, isr, isxc, isxdr, namespaceenum, technicalframework, validatorname, version, disactivated) VALUES (nextval('validator_pack_class_id_seq'), 'PROVIDE_AND_REGISTER_DOCUMENT_SET_STRING', false, false, false, false, false, 'XDS_NAMESPACE', 'Cross-Community Document Reliable Interchange (XCDR), 3.80', 'IHE XCDR ITI-80 Cross-Gateway Document Provide - request', '1.00', false);
INSERT INTO validator_pack_class (id, extractnodeenum, hasnistvalidation, ispnr, isr, isxc, isxdr, namespaceenum, technicalframework, validatorname, version, disactivated) VALUES (nextval('validator_pack_class_id_seq'), 'REGISTRY_RESPONSE', false, false, false, false, false, 'RS_NAMESPACE', 'Cross-Community Document Reliable Interchange (XCDR), 3.80', 'IHE XCDR ITI-80 Cross-Gateway Document Provide - response', '1.00', false);
INSERT INTO validator_pack_class_usage (validator_pack_class_id, usages_id) VALUES ((SELECT id FROM validator_pack_class WHERE validatorname = 'IHE XCDR ITI-80 Cross-Gateway Document Provide - request'), (SELECT id FROM usage_metadata WHERE keyword = 'XCDR'));
INSERT INTO validator_pack_class_usage (validator_pack_class_id, usages_id) VALUES ((SELECT id FROM validator_pack_class WHERE validatorname = 'IHE XCDR ITI-80 Cross-Gateway Document Provide - response'), (SELECT id FROM usage_metadata WHERE keyword = 'XCDR'));
INSERT INTO validator_pack_class_pack_class (validator_pack_class_id, listpackclass_id) VALUES ((SELECT id FROM validator_pack_class WHERE validatorname = 'IHE XCDR ITI-80 Cross-Gateway Document Provide - response'), 3);
INSERT INTO validator_pack_class_pack_class (validator_pack_class_id, listpackclass_id) VALUES ((SELECT id FROM validator_pack_class WHERE validatorname = 'IHE XCDR ITI-80 Cross-Gateway Document Provide - response'), 4);
INSERT INTO validator_pack_class_pack_class (validator_pack_class_id, listpackclass_id) VALUES ((SELECT id FROM validator_pack_class WHERE validatorname = 'IHE XCDR ITI-80 Cross-Gateway Document Provide - request'), 19);
INSERT INTO validator_pack_class_pack_class (validator_pack_class_id, listpackclass_id) VALUES ((SELECT id FROM validator_pack_class WHERE validatorname = 'IHE XCDR ITI-80 Cross-Gateway Document Provide - request'), 20);
INSERT INTO validator_pack_class_nist_code (metadatacodes_id, id) VALUES (2, (SELECT id FROM validator_pack_class WHERE validatorname = 'IHE XCDR ITI-80 Cross-Gateway Document Provide - request'));
INSERT INTO validator_pack_class_nist_code (metadatacodes_id, id) VALUES (2, (SELECT id FROM validator_pack_class WHERE validatorname = 'IHE XCDR ITI-80 Cross-Gateway Document Provide - response'));
------------------------------------------------------------------------------------------