--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.7
-- Dumped by pg_dump version 9.6.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: adhoc_query_extra_constraints; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE adhoc_query_extra_constraints (
    registry_object_metadata_id integer NOT NULL,
    extraconstraintspecifications_id integer NOT NULL
);


ALTER TABLE adhoc_query_extra_constraints OWNER TO gazelle;

--
-- Name: adhoc_return_types; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE adhoc_return_types (
    id integer NOT NULL,
    return_type_val character varying(255)
);


ALTER TABLE adhoc_return_types OWNER TO gazelle;

--
-- Name: adhocquery_metadata_describer; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE adhocquery_metadata_describer (
    id integer NOT NULL,
    cardinality character varying(255),
    adhoc_query_id integer
);


ALTER TABLE adhocquery_metadata_describer OWNER TO gazelle;

--
-- Name: adhocquery_metadata_describer_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE adhocquery_metadata_describer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE adhocquery_metadata_describer_id_seq OWNER TO gazelle;

--
-- Name: affinity_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE affinity_domain (
    id integer NOT NULL,
    keyword character varying(255),
    label_to_display character varying(255),
    profile character varying(255)
);


ALTER TABLE affinity_domain OWNER TO gazelle;

--
-- Name: affinity_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE affinity_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE affinity_domain_id_seq OWNER TO gazelle;

--
-- Name: affinity_domain_transactions; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE affinity_domain_transactions (
    affinity_domain_id integer NOT NULL,
    transaction_id integer NOT NULL
);


ALTER TABLE affinity_domain_transactions OWNER TO gazelle;

--
-- Name: app_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE app_configuration (
    id integer NOT NULL,
    value character varying(255),
    variable character varying(255)
);


ALTER TABLE app_configuration OWNER TO gazelle;

--
-- Name: app_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE app_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE app_configuration_id_seq OWNER TO gazelle;

--
-- Name: attachment_file; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE attachment_file (
    id integer NOT NULL,
    contentid character varying(255),
    contenttransferencoding character varying(255),
    contenttype character varying(255),
    size character varying(255),
    metadata_message_id integer
);


ALTER TABLE attachment_file OWNER TO gazelle;

--
-- Name: attachment_file_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE attachment_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE attachment_file_id_seq OWNER TO gazelle;

--
-- Name: cfg_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_configuration (
    id integer NOT NULL,
    comment character varying(255),
    approved boolean NOT NULL,
    is_secured boolean,
    host_id integer
);


ALTER TABLE cfg_configuration OWNER TO gazelle;

--
-- Name: cfg_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_dicom_scp_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_dicom_scp_configuration (
    id integer NOT NULL,
    comments character varying(255),
    ae_title character varying(16) NOT NULL,
    modality_type character varying(255),
    port integer,
    port_secure integer,
    transfer_role character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer,
    sop_class_id integer NOT NULL
);


ALTER TABLE cfg_dicom_scp_configuration OWNER TO gazelle;

--
-- Name: cfg_dicom_scp_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_dicom_scp_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_dicom_scp_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_dicom_scu_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_dicom_scu_configuration (
    id integer NOT NULL,
    comments character varying(255),
    ae_title character varying(16) NOT NULL,
    modality_type character varying(255),
    port integer,
    port_secure integer,
    transfer_role character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer,
    sop_class_id integer NOT NULL
);


ALTER TABLE cfg_dicom_scu_configuration OWNER TO gazelle;

--
-- Name: cfg_dicom_scu_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_dicom_scu_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_dicom_scu_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_initiator_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_hl7_initiator_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE cfg_hl7_initiator_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_initiator_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_hl7_initiator_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_hl7_initiator_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_responder_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_hl7_responder_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    port integer,
    port_out integer,
    port_secure integer,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE cfg_hl7_responder_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_responder_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_hl7_responder_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_hl7_responder_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_initiator_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_hl7_v3_initiator_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE cfg_hl7_v3_initiator_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_initiator_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_hl7_v3_initiator_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_hl7_v3_initiator_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_responder_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_hl7_v3_responder_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    port integer,
    port_secured integer,
    url character varying(255) NOT NULL,
    usage character varying(255),
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE cfg_hl7_v3_responder_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_responder_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_hl7_v3_responder_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_hl7_v3_responder_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_host; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_host (
    id integer NOT NULL,
    alias character varying(255),
    comment character varying(255),
    hostname character varying(255) NOT NULL,
    ip character varying(255)
);


ALTER TABLE cfg_host OWNER TO gazelle;

--
-- Name: cfg_host_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_host_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_host_id_seq OWNER TO gazelle;

--
-- Name: cfg_sop_class; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_sop_class (
    id integer NOT NULL,
    keyword character varying(255) NOT NULL,
    name character varying(255)
);


ALTER TABLE cfg_sop_class OWNER TO gazelle;

--
-- Name: cfg_sop_class_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_sop_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_sop_class_id_seq OWNER TO gazelle;

--
-- Name: cfg_syslog_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_syslog_configuration (
    id integer NOT NULL,
    comments character varying(255),
    port integer,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE cfg_syslog_configuration OWNER TO gazelle;

--
-- Name: cfg_syslog_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_syslog_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_syslog_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_web_service_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_web_service_configuration (
    id integer NOT NULL,
    comments character varying(255),
    port integer,
    port_secured integer,
    url character varying(255) NOT NULL,
    usage character varying(255),
    assigning_authority character varying(255),
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE cfg_web_service_configuration OWNER TO gazelle;

--
-- Name: cfg_web_service_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_web_service_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_web_service_configuration_id_seq OWNER TO gazelle;

--
-- Name: classification_metadata_describer; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE classification_metadata_describer (
    id integer NOT NULL,
    cardinality character varying(255),
    classification_id integer
);


ALTER TABLE classification_metadata_describer OWNER TO gazelle;

--
-- Name: classification_metadata_describer_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE classification_metadata_describer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE classification_metadata_describer_id_seq OWNER TO gazelle;

--
-- Name: cmn_company_details; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_company_details (
    id integer NOT NULL,
    company_keyword character varying(255)
);


ALTER TABLE cmn_company_details OWNER TO gazelle;

--
-- Name: cmn_company_details_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_company_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_company_details_id_seq OWNER TO gazelle;

--
-- Name: cmn_home; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_home (
    id integer NOT NULL,
    home_title character varying(255),
    iso3_language character varying(255),
    main_content text
);


ALTER TABLE cmn_home OWNER TO gazelle;

--
-- Name: cmn_home_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_home_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_home_id_seq OWNER TO gazelle;

--
-- Name: cmn_ip_address; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_ip_address (
    id integer NOT NULL,
    added_by character varying(255),
    added_on timestamp without time zone,
    value character varying(255),
    company_details_id integer
);


ALTER TABLE cmn_ip_address OWNER TO gazelle;

--
-- Name: cmn_ip_address_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_ip_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_ip_address_id_seq OWNER TO gazelle;

--
-- Name: cmn_message_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_message_instance (
    id integer NOT NULL,
    content bytea,
    issuer character varying(255),
    type character varying(255),
    validation_detailed_result bytea,
    validation_status character varying(255),
    issuing_actor integer,
    issuer_ip_address character varying(255)
);


ALTER TABLE cmn_message_instance OWNER TO gazelle;

--
-- Name: cmn_message_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_message_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_message_instance_id_seq OWNER TO gazelle;

--
-- Name: cmn_message_instance_metadata; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_message_instance_metadata (
    id integer NOT NULL,
    label character varying(255),
    value character varying(255),
    message_instance_id integer
);


ALTER TABLE cmn_message_instance_metadata OWNER TO gazelle;

--
-- Name: cmn_message_instance_metadata_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_message_instance_metadata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_message_instance_metadata_id_seq OWNER TO gazelle;

--
-- Name: cmn_receiver_console; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_receiver_console (
    id integer NOT NULL,
    code character varying(255),
    comment text,
    message_identifier character varying(255),
    message_type character varying(255),
    sut character varying(255),
    "timestamp" timestamp without time zone,
    domain_id integer,
    simulated_actor_id integer,
    transaction_id integer
);


ALTER TABLE cmn_receiver_console OWNER TO gazelle;

--
-- Name: cmn_receiver_console_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_receiver_console_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_receiver_console_id_seq OWNER TO gazelle;

--
-- Name: cmn_transaction_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_transaction_instance (
    id integer NOT NULL,
    "timestamp" timestamp without time zone,
    username character varying(255),
    is_visible boolean,
    domain_id integer,
    request_id integer,
    response_id integer,
    simulated_actor_id integer,
    transaction_id integer,
    standard integer,
    company_keyword character varying(255)
);


ALTER TABLE cmn_transaction_instance OWNER TO gazelle;

--
-- Name: cmn_transaction_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_transaction_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_transaction_instance_id_seq OWNER TO gazelle;

--
-- Name: cmn_validator_usage; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_validator_usage (
    id integer NOT NULL,
    caller character varying(255),
    date date,
    status character varying(255),
    type character varying(255)
);


ALTER TABLE cmn_validator_usage OWNER TO gazelle;

--
-- Name: cmn_validator_usage_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_validator_usage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_validator_usage_id_seq OWNER TO gazelle;

--
-- Name: cmn_value_set; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_value_set (
    id integer NOT NULL,
    accessible boolean,
    last_check timestamp without time zone,
    usage character varying(255),
    value_set_keyword character varying(255),
    value_set_name character varying(255),
    value_set_oid character varying(255)
);


ALTER TABLE cmn_value_set OWNER TO gazelle;

--
-- Name: cmn_value_set_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_value_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_value_set_id_seq OWNER TO gazelle;

--
-- Name: code; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE code (
    id integer NOT NULL,
    class_scheme character varying(255),
    code character varying(255),
    coding_scheme character varying(255),
    display text,
    transaction_id integer
);


ALTER TABLE code OWNER TO gazelle;

--
-- Name: code_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE code_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE code_id_seq OWNER TO gazelle;

--
-- Name: conf_type_usages; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE conf_type_usages (
    configuration_type_id integer NOT NULL,
    listusages_id integer NOT NULL
);


ALTER TABLE conf_type_usages OWNER TO gazelle;

--
-- Name: configuration_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE configuration_type (
    id integer NOT NULL,
    conf_type character varying(255)
);


ALTER TABLE configuration_type OWNER TO gazelle;

--
-- Name: configuration_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE configuration_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE configuration_type_id_seq OWNER TO gazelle;

--
-- Name: constraint_specification; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE constraint_specification (
    id integer NOT NULL,
    description character varying(800),
    kind character varying(255),
    xpath character varying(1500)
);


ALTER TABLE constraint_specification OWNER TO gazelle;

--
-- Name: constraint_specification_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE constraint_specification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE constraint_specification_id_seq OWNER TO gazelle;

--
-- Name: external_identifier_metadata_describer; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE external_identifier_metadata_describer (
    id integer NOT NULL,
    cardinality character varying(255),
    external_identifier_id integer
);


ALTER TABLE external_identifier_metadata_describer OWNER TO gazelle;

--
-- Name: external_identifier_metadata_describer_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE external_identifier_metadata_describer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE external_identifier_metadata_describer_id_seq OWNER TO gazelle;

--
-- Name: extrinsic_object_extra_constraints; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE extrinsic_object_extra_constraints (
    registry_object_metadata_id integer NOT NULL,
    extraconstraintspecifications_id integer NOT NULL
);


ALTER TABLE extrinsic_object_extra_constraints OWNER TO gazelle;

--
-- Name: extrinsic_object_metadata_describer; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE extrinsic_object_metadata_describer (
    id integer NOT NULL,
    cardinality character varying(255),
    extrinsic_object_id integer
);


ALTER TABLE extrinsic_object_metadata_describer OWNER TO gazelle;

--
-- Name: extrinsic_object_metadata_describer_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE extrinsic_object_metadata_describer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE extrinsic_object_metadata_describer_id_seq OWNER TO gazelle;

--
-- Name: gs_contextual_information; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE gs_contextual_information (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    value character varying(255),
    path integer NOT NULL
);


ALTER TABLE gs_contextual_information OWNER TO gazelle;

--
-- Name: gs_contextual_information_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE gs_contextual_information_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gs_contextual_information_id_seq OWNER TO gazelle;

--
-- Name: gs_contextual_information_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE gs_contextual_information_instance (
    id integer NOT NULL,
    form character varying(255),
    value character varying(255),
    contextual_information_id integer NOT NULL,
    test_steps_instance_id integer NOT NULL
);


ALTER TABLE gs_contextual_information_instance OWNER TO gazelle;

--
-- Name: gs_contextual_information_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE gs_contextual_information_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gs_contextual_information_instance_id_seq OWNER TO gazelle;

--
-- Name: gs_message; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE gs_message (
    id integer NOT NULL,
    message_content bytea,
    message_type_id character varying(255),
    time_stamp timestamp without time zone,
    test_instance_participants_receiver_id integer,
    test_instance_participants_sender_id integer,
    transaction_id integer
);


ALTER TABLE gs_message OWNER TO gazelle;

--
-- Name: gs_message_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE gs_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gs_message_id_seq OWNER TO gazelle;

--
-- Name: gs_system; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE gs_system (
    id integer NOT NULL,
    institution_keyword character varying(255),
    keyword character varying(255),
    system_owner character varying(255)
);


ALTER TABLE gs_system OWNER TO gazelle;

--
-- Name: gs_system_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE gs_system_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gs_system_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE gs_test_instance (
    id integer NOT NULL,
    server_test_instance_id character varying(255) NOT NULL,
    test_instance_status_id integer
);


ALTER TABLE gs_test_instance OWNER TO gazelle;

--
-- Name: gs_test_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE gs_test_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gs_test_instance_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance_oid; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE gs_test_instance_oid (
    test_instance_id integer NOT NULL,
    oid_configuration_id integer NOT NULL
);


ALTER TABLE gs_test_instance_oid OWNER TO gazelle;

--
-- Name: gs_test_instance_participants; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE gs_test_instance_participants (
    id integer NOT NULL,
    server_aipo_id character varying(255) NOT NULL,
    server_test_instance_participants_id character varying(255) NOT NULL,
    aipo_id integer,
    system_id integer
);


ALTER TABLE gs_test_instance_participants OWNER TO gazelle;

--
-- Name: gs_test_instance_participants_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE gs_test_instance_participants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gs_test_instance_participants_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance_status; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE gs_test_instance_status (
    id integer NOT NULL,
    description character varying(255),
    keyword character varying(255),
    label_to_display character varying(255)
);


ALTER TABLE gs_test_instance_status OWNER TO gazelle;

--
-- Name: gs_test_instance_status_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE gs_test_instance_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gs_test_instance_status_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance_test_instance_participants; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE gs_test_instance_test_instance_participants (
    test_instance_id integer NOT NULL,
    test_instance_participants_id integer NOT NULL
);


ALTER TABLE gs_test_instance_test_instance_participants OWNER TO gazelle;

--
-- Name: gs_test_steps_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE gs_test_steps_instance (
    id integer NOT NULL,
    dicom_scp_config_id integer,
    dicom_scu_config_id integer,
    hl7v2_initiator_config_id integer,
    hl7v2_responder_config_id integer,
    hl7v3_initiator_config_id integer,
    hl7v3_responder_config_id integer,
    syslog_config_id integer,
    testinstance_id integer,
    web_service_config_id integer
);


ALTER TABLE gs_test_steps_instance OWNER TO gazelle;

--
-- Name: gs_test_steps_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE gs_test_steps_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gs_test_steps_instance_id_seq OWNER TO gazelle;

--
-- Name: mbv_assertion; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE mbv_assertion (
    id integer NOT NULL,
    assertionid character varying(255),
    idscheme character varying(255),
    constraint_id integer
);


ALTER TABLE mbv_assertion OWNER TO gazelle;

--
-- Name: mbv_assertion_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE mbv_assertion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mbv_assertion_id_seq OWNER TO gazelle;

--
-- Name: mbv_class_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE mbv_class_type (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    name character varying(255),
    parent_identifier character varying(255),
    xml_name character varying(255),
    path character varying(255),
    templateid character varying(255),
    constraintadvanced text,
    documentation_spec_id integer,
    package_id integer
);


ALTER TABLE mbv_class_type OWNER TO gazelle;

--
-- Name: mbv_class_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE mbv_class_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mbv_class_type_id_seq OWNER TO gazelle;

--
-- Name: mbv_constraint; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE mbv_constraint (
    id integer NOT NULL,
    author character varying(255),
    datecreation character varying(255),
    description text,
    history text,
    lastchange character varying(255),
    name character varying(255),
    ocl text,
    svsref bytea,
    type character varying(255),
    classtype_id integer,
    kind character varying(255)
);


ALTER TABLE mbv_constraint OWNER TO gazelle;

--
-- Name: mbv_constraint_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE mbv_constraint_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mbv_constraint_id_seq OWNER TO gazelle;

--
-- Name: mbv_documentation_spec; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE mbv_documentation_spec (
    id integer NOT NULL,
    description text,
    document character varying(255),
    name character varying(255),
    paragraph character varying(255)
);


ALTER TABLE mbv_documentation_spec OWNER TO gazelle;

--
-- Name: mbv_documentation_spec_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE mbv_documentation_spec_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mbv_documentation_spec_id_seq OWNER TO gazelle;

--
-- Name: mbv_package; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE mbv_package (
    id integer NOT NULL,
    description character varying(255),
    name character varying(255),
    package_name character varying(255),
    namespace character varying(255)
);


ALTER TABLE mbv_package OWNER TO gazelle;

--
-- Name: mbv_package_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE mbv_package_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mbv_package_id_seq OWNER TO gazelle;

--
-- Name: mbv_standards; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE mbv_standards (
    package_id integer NOT NULL,
    standards character varying(255)
);


ALTER TABLE mbv_standards OWNER TO gazelle;

--
-- Name: message_registeres_files; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE message_registeres_files (
    message_id integer NOT NULL,
    registered_file_id integer NOT NULL
);


ALTER TABLE message_registeres_files OWNER TO gazelle;

--
-- Name: message_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE message_type (
    id integer NOT NULL,
    action character varying(255),
    name character varying(255),
    uuid character varying(255),
    transaction_id integer
);


ALTER TABLE message_type OWNER TO gazelle;

--
-- Name: message_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE message_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE message_type_id_seq OWNER TO gazelle;

--
-- Name: message_type_optional_parameters; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE message_type_optional_parameters (
    message_type_id integer NOT NULL,
    parameter_id integer NOT NULL
);


ALTER TABLE message_type_optional_parameters OWNER TO gazelle;

--
-- Name: message_type_required_parameters; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE message_type_required_parameters (
    message_type_id integer NOT NULL,
    parameter_id integer NOT NULL
);


ALTER TABLE message_type_required_parameters OWNER TO gazelle;

--
-- Name: metadata; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE metadata (
    id integer NOT NULL,
    generated boolean,
    name character varying(255),
    required boolean,
    type character varying(255),
    uuid character varying(255)
);


ALTER TABLE metadata OWNER TO gazelle;

--
-- Name: metadata_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE metadata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE metadata_id_seq OWNER TO gazelle;

--
-- Name: metadata_message; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE metadata_message (
    id integer NOT NULL,
    http_header text,
    message_content text,
    message_type_id integer,
    validation_details text,
    validation_result boolean
);


ALTER TABLE metadata_message OWNER TO gazelle;

--
-- Name: metadata_message_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE metadata_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE metadata_message_id_seq OWNER TO gazelle;

--
-- Name: nist_code; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE nist_code (
    id integer NOT NULL,
    nistcodename character varying(255),
    url character varying(255)
);


ALTER TABLE nist_code OWNER TO gazelle;

--
-- Name: nist_code_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE nist_code_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE nist_code_id_seq OWNER TO gazelle;

--
-- Name: oid; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE oid (
    id integer NOT NULL
);


ALTER TABLE oid OWNER TO gazelle;

--
-- Name: oid_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE oid_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE oid_id_seq OWNER TO gazelle;

--
-- Name: pack_class; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE pack_class (
    id integer NOT NULL,
    packclass character varying(255)
);


ALTER TABLE pack_class OWNER TO gazelle;

--
-- Name: pack_class_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE pack_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pack_class_id_seq OWNER TO gazelle;

--
-- Name: parameter; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE parameter (
    id integer NOT NULL,
    attribute character varying(255),
    default_value character varying(255),
    multiple boolean,
    name character varying(255),
    supports_and_or boolean,
    uuid character varying(255)
);


ALTER TABLE parameter OWNER TO gazelle;

--
-- Name: parameter_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE parameter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE parameter_id_seq OWNER TO gazelle;

--
-- Name: registered_file; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE registered_file (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    name character varying(255),
    path character varying(255),
    type character varying(255)
);


ALTER TABLE registered_file OWNER TO gazelle;

--
-- Name: registered_file_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE registered_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE registered_file_id_seq OWNER TO gazelle;

--
-- Name: registry_object_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE registry_object_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE registry_object_id_seq OWNER TO gazelle;

--
-- Name: registry_object_list_extra_constraints; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE registry_object_list_extra_constraints (
    registry_object_list_id integer NOT NULL,
    extra_constraint_specifications_id integer NOT NULL
);


ALTER TABLE registry_object_list_extra_constraints OWNER TO gazelle;

--
-- Name: registry_object_list_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE registry_object_list_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE registry_object_list_id_seq OWNER TO gazelle;

--
-- Name: registry_object_list_metadata; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE registry_object_list_metadata (
    id integer NOT NULL,
    displayattributename character varying(255),
    name character varying(255)
);


ALTER TABLE registry_object_list_metadata OWNER TO gazelle;

--
-- Name: registry_object_list_metadata_extrinsicobject; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE registry_object_list_metadata_extrinsicobject (
    registry_object_list_metadata_id integer NOT NULL,
    extrinsicobject_id integer NOT NULL
);


ALTER TABLE registry_object_list_metadata_extrinsicobject OWNER TO gazelle;

--
-- Name: registry_object_list_metadata_registrypackage; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE registry_object_list_metadata_registrypackage (
    registry_object_list_metadata_id integer NOT NULL,
    registrypackage_id integer NOT NULL
);


ALTER TABLE registry_object_list_metadata_registrypackage OWNER TO gazelle;

--
-- Name: registry_object_list_metadata_usage; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE registry_object_list_metadata_usage (
    registry_object_list_metadata_id integer NOT NULL,
    usages_id integer NOT NULL
);


ALTER TABLE registry_object_list_metadata_usage OWNER TO gazelle;

--
-- Name: registry_object_metadata; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE registry_object_metadata (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    displayattributename character varying(255),
    name character varying(255),
    uuid character varying(255),
    value character varying(255),
    valueset character varying(500)
);


ALTER TABLE registry_object_metadata OWNER TO gazelle;

--
-- Name: registry_object_metadata_classificationdescriber; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE registry_object_metadata_classificationdescriber (
    registry_object_metadata_id integer NOT NULL,
    classificationdescriber_id integer NOT NULL
);


ALTER TABLE registry_object_metadata_classificationdescriber OWNER TO gazelle;

--
-- Name: registry_object_metadata_describer; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE registry_object_metadata_describer (
    id integer NOT NULL,
    cardinality character varying(255),
    registry_object_id integer
);


ALTER TABLE registry_object_metadata_describer OWNER TO gazelle;

--
-- Name: registry_object_metadata_describer_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE registry_object_metadata_describer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE registry_object_metadata_describer_id_seq OWNER TO gazelle;

--
-- Name: registry_object_metadata_ext_identifierdescriber; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE registry_object_metadata_ext_identifierdescriber (
    registry_object_metadata_id integer NOT NULL,
    ext_identifierdescriber_id integer NOT NULL
);


ALTER TABLE registry_object_metadata_ext_identifierdescriber OWNER TO gazelle;

--
-- Name: registry_object_metadata_slotdescriber; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE registry_object_metadata_slotdescriber (
    registry_object_metadata_id integer NOT NULL,
    slotdescriber_id integer NOT NULL
);


ALTER TABLE registry_object_metadata_slotdescriber OWNER TO gazelle;

--
-- Name: registry_object_metadata_usage; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE registry_object_metadata_usage (
    registry_object_metadata_id integer NOT NULL,
    usages_id integer NOT NULL
);


ALTER TABLE registry_object_metadata_usage OWNER TO gazelle;

--
-- Name: registry_package_extra_constraints; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE registry_package_extra_constraints (
    registry_object_metadata_id integer NOT NULL,
    extraconstraintspecifications_id integer NOT NULL
);


ALTER TABLE registry_package_extra_constraints OWNER TO gazelle;

--
-- Name: registry_package_metadata_describer; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE registry_package_metadata_describer (
    id integer NOT NULL,
    cardinality character varying(255),
    registry_package_id integer
);


ALTER TABLE registry_package_metadata_describer OWNER TO gazelle;

--
-- Name: registry_package_metadata_describer_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE registry_package_metadata_describer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE registry_package_metadata_describer_id_seq OWNER TO gazelle;

--
-- Name: slot_metadata; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE slot_metadata (
    id integer NOT NULL,
    attribute character varying(255),
    defaultvalue character varying(255),
    isnumber boolean,
    multiple boolean,
    name character varying(255),
    regex character varying(255),
    supportandor boolean,
    valueset character varying(500),
    iscodeonly boolean
);


ALTER TABLE slot_metadata OWNER TO gazelle;

--
-- Name: slot_metadata_describer; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE slot_metadata_describer (
    id integer NOT NULL,
    cardinality character varying(255),
    slot_id integer
);


ALTER TABLE slot_metadata_describer OWNER TO gazelle;

--
-- Name: slot_metadata_describer_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE slot_metadata_describer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE slot_metadata_describer_id_seq OWNER TO gazelle;

--
-- Name: slot_metadata_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE slot_metadata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE slot_metadata_id_seq OWNER TO gazelle;

--
-- Name: sys_conf_type_usages; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE sys_conf_type_usages (
    system_configuration_id integer NOT NULL,
    listusages_id integer NOT NULL
);


ALTER TABLE sys_conf_type_usages OWNER TO gazelle;

--
-- Name: system_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE system_configuration (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    is_available boolean,
    is_public boolean,
    name character varying(255),
    owner character varying(255),
    system_name character varying(255),
    url character varying(255) NOT NULL,
    repositoryuniqueid character varying(255),
    homecommunityid character varying(255),
    owner_company character varying(255)
);


ALTER TABLE system_configuration OWNER TO gazelle;

--
-- Name: system_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE system_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE system_configuration_id_seq OWNER TO gazelle;

--
-- Name: tf_actor; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tf_actor (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128),
    can_act_as_responder boolean
);


ALTER TABLE tf_actor OWNER TO gazelle;

--
-- Name: tf_actor_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tf_actor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tf_actor_id_seq OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tf_actor_integration_profile (
    id integer NOT NULL,
    actor_id integer,
    integration_profile_id integer
);


ALTER TABLE tf_actor_integration_profile OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tf_actor_integration_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tf_actor_integration_profile_id_seq OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tf_actor_integration_profile_option (
    id integer NOT NULL,
    actor_integration_profile_id integer NOT NULL,
    integration_profile_option_id integer
);


ALTER TABLE tf_actor_integration_profile_option OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_option_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tf_actor_integration_profile_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tf_actor_integration_profile_option_id_seq OWNER TO gazelle;

--
-- Name: tf_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tf_domain (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE tf_domain OWNER TO gazelle;

--
-- Name: tf_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tf_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tf_domain_id_seq OWNER TO gazelle;

--
-- Name: tf_domain_integration_profiles; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tf_domain_integration_profiles (
    domain_id integer NOT NULL,
    integration_profile_id integer NOT NULL
);


ALTER TABLE tf_domain_integration_profiles OWNER TO gazelle;

--
-- Name: tf_integration_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tf_integration_profile (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE tf_integration_profile OWNER TO gazelle;

--
-- Name: tf_integration_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tf_integration_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tf_integration_profile_id_seq OWNER TO gazelle;

--
-- Name: tf_integration_profile_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tf_integration_profile_option (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE tf_integration_profile_option OWNER TO gazelle;

--
-- Name: tf_integration_profile_option_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tf_integration_profile_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tf_integration_profile_option_id_seq OWNER TO gazelle;

--
-- Name: tf_transaction; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tf_transaction (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE tf_transaction OWNER TO gazelle;

--
-- Name: tf_transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tf_transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tf_transaction_id_seq OWNER TO gazelle;

--
-- Name: tm_oid; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tm_oid (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    oid character varying(255) NOT NULL,
    system_id integer
);


ALTER TABLE tm_oid OWNER TO gazelle;

--
-- Name: tm_oid_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tm_oid_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tm_oid_id_seq OWNER TO gazelle;

--
-- Name: tm_path; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tm_path (
    id integer NOT NULL,
    description character varying(255) NOT NULL,
    keyword character varying(255) NOT NULL,
    type character varying(255)
);


ALTER TABLE tm_path OWNER TO gazelle;

--
-- Name: tm_path_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tm_path_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tm_path_id_seq OWNER TO gazelle;

--
-- Name: tp_global_input_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tp_global_input_instance (
    id integer NOT NULL,
    value character varying(255),
    global_input_id integer
);


ALTER TABLE tp_global_input_instance OWNER TO gazelle;

--
-- Name: tp_global_input_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tp_global_input_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tp_global_input_instance_id_seq OWNER TO gazelle;

--
-- Name: tp_global_input_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tp_global_input_type (
    id integer NOT NULL,
    default_value character varying(255),
    description character varying(255) NOT NULL,
    tag character varying(255) NOT NULL
);


ALTER TABLE tp_global_input_type OWNER TO gazelle;

--
-- Name: tp_global_input_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tp_global_input_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tp_global_input_type_id_seq OWNER TO gazelle;

--
-- Name: tp_input_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tp_input_type (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE tp_input_type OWNER TO gazelle;

--
-- Name: tp_input_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tp_input_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tp_input_type_id_seq OWNER TO gazelle;

--
-- Name: tp_rule_result; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tp_rule_result (
    id integer NOT NULL,
    result integer,
    rule_id integer,
    test_step_instance_id integer
);


ALTER TABLE tp_rule_result OWNER TO gazelle;

--
-- Name: tp_rule_result_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tp_rule_result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tp_rule_result_id_seq OWNER TO gazelle;

--
-- Name: tp_rule_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tp_rule_type (
    id integer NOT NULL,
    description character varying(255) NOT NULL,
    xpath character varying(255) NOT NULL
);


ALTER TABLE tp_rule_type OWNER TO gazelle;

--
-- Name: tp_rule_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tp_rule_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tp_rule_type_id_seq OWNER TO gazelle;

--
-- Name: tp_test_plan; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tp_test_plan (
    id integer NOT NULL,
    description text NOT NULL,
    test_name character varying(255) NOT NULL
);


ALTER TABLE tp_test_plan OWNER TO gazelle;

--
-- Name: tp_test_plan_global_input; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tp_test_plan_global_input (
    tp_test_plan_id integer NOT NULL,
    globalinput_id integer NOT NULL
);


ALTER TABLE tp_test_plan_global_input OWNER TO gazelle;

--
-- Name: tp_test_plan_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tp_test_plan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tp_test_plan_id_seq OWNER TO gazelle;

--
-- Name: tp_test_plan_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tp_test_plan_instance (
    id integer NOT NULL,
    execution_date timestamp without time zone,
    result character varying(255),
    test_plan_id integer
);


ALTER TABLE tp_test_plan_instance OWNER TO gazelle;

--
-- Name: tp_test_plan_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tp_test_plan_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tp_test_plan_instance_id_seq OWNER TO gazelle;

--
-- Name: tp_test_plan_instance_tp_global_input_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tp_test_plan_instance_tp_global_input_instance (
    tp_test_plan_instance_id integer NOT NULL,
    globalinputinstance_id integer NOT NULL
);


ALTER TABLE tp_test_plan_instance_tp_global_input_instance OWNER TO gazelle;

--
-- Name: tp_test_plan_test_step; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tp_test_plan_test_step (
    tp_test_plan_id integer NOT NULL,
    teststep_id integer NOT NULL
);


ALTER TABLE tp_test_plan_test_step OWNER TO gazelle;

--
-- Name: tp_test_step; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tp_test_step (
    id integer NOT NULL,
    affinity_domain_keyword character varying(255),
    description character varying(255),
    identifier integer NOT NULL,
    message_type character varying(255),
    previous_ts_identifier integer,
    seq_id integer NOT NULL,
    template_name character varying(255),
    transaction_keyword character varying(255),
    transaction_type character varying(255) NOT NULL
);


ALTER TABLE tp_test_step OWNER TO gazelle;

--
-- Name: tp_test_step_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tp_test_step_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tp_test_step_id_seq OWNER TO gazelle;

--
-- Name: tp_test_step_input; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tp_test_step_input (
    tp_test_step_id integer NOT NULL,
    input_id integer NOT NULL
);


ALTER TABLE tp_test_step_input OWNER TO gazelle;

--
-- Name: tp_test_step_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tp_test_step_instance (
    id integer NOT NULL,
    result character varying(255),
    status character varying(255),
    test_step_id integer,
    system_configuration_id integer,
    test_plan_instance_id integer
);


ALTER TABLE tp_test_step_instance OWNER TO gazelle;

--
-- Name: tp_test_step_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tp_test_step_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tp_test_step_instance_id_seq OWNER TO gazelle;

--
-- Name: tp_test_step_instance_tp_input_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tp_test_step_instance_tp_input_type (
    tp_test_step_instance_id integer NOT NULL,
    inputs_id integer NOT NULL
);


ALTER TABLE tp_test_step_instance_tp_input_type OWNER TO gazelle;

--
-- Name: tp_test_step_instance_xdstar_message; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tp_test_step_instance_xdstar_message (
    tp_test_step_instance_id integer NOT NULL,
    abstractmessage_id integer NOT NULL
);


ALTER TABLE tp_test_step_instance_xdstar_message OWNER TO gazelle;

--
-- Name: tp_test_step_rule; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tp_test_step_rule (
    tp_test_step_id integer NOT NULL,
    rule_id integer NOT NULL
);


ALTER TABLE tp_test_step_rule OWNER TO gazelle;

--
-- Name: tp_transaction_template; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tp_transaction_template (
    id integer NOT NULL,
    http_header character varying(1000),
    name character varying(255),
    request_type character varying(255),
    response_type character varying(255),
    template text
);


ALTER TABLE tp_transaction_template OWNER TO gazelle;

--
-- Name: tp_transaction_template_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tp_transaction_template_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tp_transaction_template_id_seq OWNER TO gazelle;

--
-- Name: transaction_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE transaction_type (
    id integer NOT NULL,
    name character varying(255)
);


ALTER TABLE transaction_type OWNER TO gazelle;

--
-- Name: transaction_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE transaction_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE transaction_type_id_seq OWNER TO gazelle;

--
-- Name: transaction_type_tf_transaction; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE transaction_type_tf_transaction (
    transaction_type_id integer NOT NULL,
    transaction_id integer NOT NULL
);


ALTER TABLE transaction_type_tf_transaction OWNER TO gazelle;

--
-- Name: transfer_syntax_uid; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE transfer_syntax_uid (
    id integer NOT NULL,
    comment character varying(255),
    name character varying(255),
    uid character varying(255) NOT NULL
);


ALTER TABLE transfer_syntax_uid OWNER TO gazelle;

--
-- Name: transfer_syntax_uid_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE transfer_syntax_uid_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE transfer_syntax_uid_id_seq OWNER TO gazelle;

--
-- Name: usage_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE usage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usage_id_seq OWNER TO gazelle;

--
-- Name: usage_metadata; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE usage_metadata (
    id integer NOT NULL,
    action character varying(255),
    keyword character varying(255),
    affinity_id integer,
    transaction_id integer
);


ALTER TABLE usage_metadata OWNER TO gazelle;

--
-- Name: ut_test_file_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE ut_test_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ut_test_file_id_seq OWNER TO gazelle;

--
-- Name: ut_unit_test; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE ut_unit_test (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    active boolean NOT NULL,
    comment text,
    expected_result integer NOT NULL,
    keyword character varying(255) NOT NULL,
    last_modifier character varying(255),
    last_result boolean,
    last_run_date timestamp without time zone,
    last_success_date timestamp without time zone,
    last_tested_version character varying(255),
    reason_for_failure text,
    detailedresult text,
    testedfilepath character varying(255),
    validator_pack_class_id integer
);


ALTER TABLE ut_unit_test OWNER TO gazelle;

--
-- Name: ut_unit_test_file; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE ut_unit_test_file (
    id integer NOT NULL,
    comment text,
    filepath character varying(255),
    input_keyword character varying(255),
    input_type integer,
    unit_test_id integer
);


ALTER TABLE ut_unit_test_file OWNER TO gazelle;

--
-- Name: ut_unit_test_file_log; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE ut_unit_test_file_log (
    log_id integer NOT NULL,
    file_id integer NOT NULL
);


ALTER TABLE ut_unit_test_file_log OWNER TO gazelle;

--
-- Name: ut_unit_test_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE ut_unit_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ut_unit_test_id_seq OWNER TO gazelle;

--
-- Name: ut_unit_test_log; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE ut_unit_test_log (
    id integer NOT NULL,
    additional_info text,
    effective_result integer,
    reason_for_failure text,
    run_date timestamp without time zone,
    success boolean NOT NULL,
    tested_version character varying(255),
    unit_test_id integer
);


ALTER TABLE ut_unit_test_log OWNER TO gazelle;

--
-- Name: ut_unit_test_log_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE ut_unit_test_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ut_unit_test_log_id_seq OWNER TO gazelle;

--
-- Name: validator_pack_class; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE validator_pack_class (
    id integer NOT NULL,
    extractnodeenum character varying(255),
    hasnistvalidation boolean,
    ispnr boolean,
    isr boolean,
    isxc boolean,
    isxdr boolean,
    namespaceenum character varying(255),
    technicalframework character varying(255),
    validatorname character varying(255),
    version character varying(255),
    disactivated boolean
);


ALTER TABLE validator_pack_class OWNER TO gazelle;

--
-- Name: validator_pack_class_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE validator_pack_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE validator_pack_class_id_seq OWNER TO gazelle;

--
-- Name: validator_pack_class_nist_code; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE validator_pack_class_nist_code (
    metadatacodes_id integer,
    id integer NOT NULL
);


ALTER TABLE validator_pack_class_nist_code OWNER TO gazelle;

--
-- Name: validator_pack_class_pack_class; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE validator_pack_class_pack_class (
    validator_pack_class_id integer NOT NULL,
    listpackclass_id integer NOT NULL
);


ALTER TABLE validator_pack_class_pack_class OWNER TO gazelle;

--
-- Name: validator_pack_class_registry_object_metadata; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE validator_pack_class_registry_object_metadata (
    validator_pack_class_id integer NOT NULL,
    listregistryobjectmetadata_id integer NOT NULL
);


ALTER TABLE validator_pack_class_registry_object_metadata OWNER TO gazelle;

--
-- Name: validator_pack_class_usage; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE validator_pack_class_usage (
    validator_pack_class_id integer NOT NULL,
    usages_id integer NOT NULL
);


ALTER TABLE validator_pack_class_usage OWNER TO gazelle;

--
-- Name: validator_packclass_extra_constraints; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE validator_packclass_extra_constraints (
    validator_pack_class_id integer NOT NULL,
    extra_constraint_specifications_id integer NOT NULL
);


ALTER TABLE validator_packclass_extra_constraints OWNER TO gazelle;

--
-- Name: validator_registry_object_list_metadata_rol; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE validator_registry_object_list_metadata_rol (
    registryobjectlistmetadata_id integer,
    id integer NOT NULL
);


ALTER TABLE validator_registry_object_list_metadata_rol OWNER TO gazelle;

--
-- Name: xcpd_home_community; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE xcpd_home_community (
    id integer NOT NULL,
    country_code character varying(255),
    description character varying(255)
);


ALTER TABLE xcpd_home_community OWNER TO gazelle;

--
-- Name: xcpd_home_community_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE xcpd_home_community_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE xcpd_home_community_id_seq OWNER TO gazelle;

--
-- Name: xdstar_message; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE xdstar_message (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    content_type_id integer,
    message_type character varying(255),
    time_stamp timestamp without time zone,
    is_gazelle_driven boolean,
    response_code integer,
    wado_downloaded_file_id bytea,
    affinity_domain_id integer,
    received_message_content_id integer,
    sent_message_content_id integer,
    transaction_id integer,
    configuration_id integer
);


ALTER TABLE xdstar_message OWNER TO gazelle;

--
-- Name: xdstar_message_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE xdstar_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE xdstar_message_id_seq OWNER TO gazelle;

--
-- Name: xdstar_message_resp; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE xdstar_message_resp (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    content_type_id integer,
    message_type character varying(255),
    time_stamp timestamp without time zone,
    ip_address character varying(255),
    wsdl_endpoint character varying(255),
    wsdl_interface character varying(255),
    wsdl_operation character varying(255),
    wsdl_service character varying(255),
    affinity_domain_id integer,
    received_message_content_id integer,
    sent_message_content_id integer,
    transaction_id integer
);


ALTER TABLE xdstar_message_resp OWNER TO gazelle;

--
-- Name: xdstar_message_resp_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE xdstar_message_resp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE xdstar_message_resp_id_seq OWNER TO gazelle;

--
-- Name: adhocquery_metadata_describer adhocquery_metadata_describer_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY adhocquery_metadata_describer
    ADD CONSTRAINT adhocquery_metadata_describer_pkey PRIMARY KEY (id);


--
-- Name: affinity_domain affinity_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY affinity_domain
    ADD CONSTRAINT affinity_domain_pkey PRIMARY KEY (id);


--
-- Name: app_configuration app_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY app_configuration
    ADD CONSTRAINT app_configuration_pkey PRIMARY KEY (id);


--
-- Name: attachment_file attachment_file_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY attachment_file
    ADD CONSTRAINT attachment_file_pkey PRIMARY KEY (id);


--
-- Name: cfg_configuration cfg_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_configuration
    ADD CONSTRAINT cfg_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_dicom_scp_configuration cfg_dicom_scp_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_dicom_scp_configuration
    ADD CONSTRAINT cfg_dicom_scp_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_dicom_scu_configuration cfg_dicom_scu_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_dicom_scu_configuration
    ADD CONSTRAINT cfg_dicom_scu_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_initiator_configuration cfg_hl7_initiator_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_initiator_configuration
    ADD CONSTRAINT cfg_hl7_initiator_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_responder_configuration cfg_hl7_responder_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_responder_configuration
    ADD CONSTRAINT cfg_hl7_responder_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_v3_initiator_configuration cfg_hl7_v3_initiator_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_v3_initiator_configuration
    ADD CONSTRAINT cfg_hl7_v3_initiator_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_v3_responder_configuration cfg_hl7_v3_responder_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_v3_responder_configuration
    ADD CONSTRAINT cfg_hl7_v3_responder_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_host cfg_host_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_host
    ADD CONSTRAINT cfg_host_pkey PRIMARY KEY (id);


--
-- Name: cfg_sop_class cfg_sop_class_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_sop_class
    ADD CONSTRAINT cfg_sop_class_pkey PRIMARY KEY (id);


--
-- Name: cfg_syslog_configuration cfg_syslog_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_syslog_configuration
    ADD CONSTRAINT cfg_syslog_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_web_service_configuration cfg_web_service_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_web_service_configuration
    ADD CONSTRAINT cfg_web_service_configuration_pkey PRIMARY KEY (id);


--
-- Name: classification_metadata_describer classification_metadata_describer_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY classification_metadata_describer
    ADD CONSTRAINT classification_metadata_describer_pkey PRIMARY KEY (id);


--
-- Name: cmn_company_details cmn_company_details_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_company_details
    ADD CONSTRAINT cmn_company_details_pkey PRIMARY KEY (id);


--
-- Name: cmn_home cmn_home_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_home
    ADD CONSTRAINT cmn_home_pkey PRIMARY KEY (id);


--
-- Name: cmn_ip_address cmn_ip_address_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_ip_address
    ADD CONSTRAINT cmn_ip_address_pkey PRIMARY KEY (id);


--
-- Name: cmn_message_instance_metadata cmn_message_instance_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_message_instance_metadata
    ADD CONSTRAINT cmn_message_instance_metadata_pkey PRIMARY KEY (id);


--
-- Name: cmn_message_instance cmn_message_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_message_instance
    ADD CONSTRAINT cmn_message_instance_pkey PRIMARY KEY (id);


--
-- Name: cmn_receiver_console cmn_receiver_console_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_receiver_console
    ADD CONSTRAINT cmn_receiver_console_pkey PRIMARY KEY (id);


--
-- Name: cmn_transaction_instance cmn_transaction_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_transaction_instance
    ADD CONSTRAINT cmn_transaction_instance_pkey PRIMARY KEY (id);


--
-- Name: cmn_validator_usage cmn_validator_usage_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_validator_usage
    ADD CONSTRAINT cmn_validator_usage_pkey PRIMARY KEY (id);


--
-- Name: cmn_value_set cmn_value_set_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_value_set
    ADD CONSTRAINT cmn_value_set_pkey PRIMARY KEY (id);


--
-- Name: code code_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY code
    ADD CONSTRAINT code_pkey PRIMARY KEY (id);


--
-- Name: configuration_type configuration_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY configuration_type
    ADD CONSTRAINT configuration_type_pkey PRIMARY KEY (id);


--
-- Name: constraint_specification constraint_specification_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY constraint_specification
    ADD CONSTRAINT constraint_specification_pkey PRIMARY KEY (id);


--
-- Name: external_identifier_metadata_describer external_identifier_metadata_describer_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY external_identifier_metadata_describer
    ADD CONSTRAINT external_identifier_metadata_describer_pkey PRIMARY KEY (id);


--
-- Name: extrinsic_object_metadata_describer extrinsic_object_metadata_describer_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY extrinsic_object_metadata_describer
    ADD CONSTRAINT extrinsic_object_metadata_describer_pkey PRIMARY KEY (id);


--
-- Name: gs_contextual_information_instance gs_contextual_information_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_contextual_information_instance
    ADD CONSTRAINT gs_contextual_information_instance_pkey PRIMARY KEY (id);


--
-- Name: gs_contextual_information gs_contextual_information_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_contextual_information
    ADD CONSTRAINT gs_contextual_information_pkey PRIMARY KEY (id);


--
-- Name: gs_message gs_message_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_message
    ADD CONSTRAINT gs_message_pkey PRIMARY KEY (id);


--
-- Name: gs_system gs_system_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_system
    ADD CONSTRAINT gs_system_pkey PRIMARY KEY (id);


--
-- Name: gs_test_instance_participants gs_test_instance_participants_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance_participants
    ADD CONSTRAINT gs_test_instance_participants_pkey PRIMARY KEY (id);


--
-- Name: gs_test_instance_participants gs_test_instance_participants_server_aipo_id_server_test_in_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance_participants
    ADD CONSTRAINT gs_test_instance_participants_server_aipo_id_server_test_in_key UNIQUE (server_aipo_id, server_test_instance_participants_id);


--
-- Name: gs_test_instance gs_test_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance
    ADD CONSTRAINT gs_test_instance_pkey PRIMARY KEY (id);


--
-- Name: gs_test_instance_status gs_test_instance_status_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance_status
    ADD CONSTRAINT gs_test_instance_status_pkey PRIMARY KEY (id);


--
-- Name: gs_test_steps_instance gs_test_steps_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_steps_instance
    ADD CONSTRAINT gs_test_steps_instance_pkey PRIMARY KEY (id);


--
-- Name: affinity_domain_transactions key1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY affinity_domain_transactions
    ADD CONSTRAINT key1 UNIQUE (affinity_domain_id, transaction_id);


--
-- Name: message_registeres_files key2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY message_registeres_files
    ADD CONSTRAINT key2 UNIQUE (message_id, registered_file_id);


--
-- Name: affinity_domain keyword_; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY affinity_domain
    ADD CONSTRAINT keyword_ UNIQUE (keyword);


--
-- Name: mbv_assertion mbv_assertion_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_assertion
    ADD CONSTRAINT mbv_assertion_pkey PRIMARY KEY (id);


--
-- Name: mbv_class_type mbv_class_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_class_type
    ADD CONSTRAINT mbv_class_type_pkey PRIMARY KEY (id);


--
-- Name: mbv_constraint mbv_constraint_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_constraint
    ADD CONSTRAINT mbv_constraint_pkey PRIMARY KEY (id);


--
-- Name: mbv_documentation_spec mbv_documentation_spec_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_documentation_spec
    ADD CONSTRAINT mbv_documentation_spec_pkey PRIMARY KEY (id);


--
-- Name: mbv_package mbv_package_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_package
    ADD CONSTRAINT mbv_package_pkey PRIMARY KEY (id);


--
-- Name: message_type_optional_parameters message_type_optional_paramete_message_type_id_parameter_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY message_type_optional_parameters
    ADD CONSTRAINT message_type_optional_paramete_message_type_id_parameter_id_key UNIQUE (message_type_id, parameter_id);


--
-- Name: message_type message_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY message_type
    ADD CONSTRAINT message_type_pkey PRIMARY KEY (id);


--
-- Name: message_type_required_parameters message_type_required_paramete_message_type_id_parameter_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY message_type_required_parameters
    ADD CONSTRAINT message_type_required_paramete_message_type_id_parameter_id_key UNIQUE (message_type_id, parameter_id);


--
-- Name: metadata_message metadata_message_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY metadata_message
    ADD CONSTRAINT metadata_message_pkey PRIMARY KEY (id);


--
-- Name: metadata metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY metadata
    ADD CONSTRAINT metadata_pkey PRIMARY KEY (id);


--
-- Name: tp_transaction_template name_; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_transaction_template
    ADD CONSTRAINT name_ UNIQUE (name);


--
-- Name: nist_code nist_code_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY nist_code
    ADD CONSTRAINT nist_code_pkey PRIMARY KEY (id);


--
-- Name: oid oid_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY oid
    ADD CONSTRAINT oid_pkey PRIMARY KEY (id);


--
-- Name: pack_class pack_class_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY pack_class
    ADD CONSTRAINT pack_class_pkey PRIMARY KEY (id);


--
-- Name: parameter parameter_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY parameter
    ADD CONSTRAINT parameter_pkey PRIMARY KEY (id);


--
-- Name: registered_file registered_file_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registered_file
    ADD CONSTRAINT registered_file_pkey PRIMARY KEY (id);


--
-- Name: registry_object_list_metadata registry_object_list_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_list_metadata
    ADD CONSTRAINT registry_object_list_metadata_pkey PRIMARY KEY (id);


--
-- Name: registry_object_metadata_describer registry_object_metadata_describer_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_metadata_describer
    ADD CONSTRAINT registry_object_metadata_describer_pkey PRIMARY KEY (id);


--
-- Name: registry_object_metadata registry_object_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_metadata
    ADD CONSTRAINT registry_object_metadata_pkey PRIMARY KEY (id);


--
-- Name: registry_package_metadata_describer registry_package_metadata_describer_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_package_metadata_describer
    ADD CONSTRAINT registry_package_metadata_describer_pkey PRIMARY KEY (id);


--
-- Name: gs_test_instance server_test_instance_id_; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance
    ADD CONSTRAINT server_test_instance_id_ UNIQUE (server_test_instance_id);


--
-- Name: gs_test_instance_participants server_test_instance_participants_id_; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance_participants
    ADD CONSTRAINT server_test_instance_participants_id_ UNIQUE (server_test_instance_participants_id);


--
-- Name: slot_metadata_describer slot_metadata_describer_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY slot_metadata_describer
    ADD CONSTRAINT slot_metadata_describer_pkey PRIMARY KEY (id);


--
-- Name: slot_metadata slot_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY slot_metadata
    ADD CONSTRAINT slot_metadata_pkey PRIMARY KEY (id);


--
-- Name: system_configuration system_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY system_configuration
    ADD CONSTRAINT system_configuration_pkey PRIMARY KEY (id);


--
-- Name: tf_actor_integration_profile_option tf_actor_integration_profile__actor_integration_profile_id__key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor_integration_profile_option
    ADD CONSTRAINT tf_actor_integration_profile__actor_integration_profile_id__key UNIQUE (actor_integration_profile_id, integration_profile_option_id);


--
-- Name: tf_actor_integration_profile tf_actor_integration_profile_actor_id_integration_profile_i_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor_integration_profile
    ADD CONSTRAINT tf_actor_integration_profile_actor_id_integration_profile_i_key UNIQUE (actor_id, integration_profile_id);


--
-- Name: tf_actor_integration_profile_option tf_actor_integration_profile_option_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor_integration_profile_option
    ADD CONSTRAINT tf_actor_integration_profile_option_pkey PRIMARY KEY (id);


--
-- Name: tf_actor_integration_profile tf_actor_integration_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor_integration_profile
    ADD CONSTRAINT tf_actor_integration_profile_pkey PRIMARY KEY (id);


--
-- Name: tf_actor tf_actor_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor
    ADD CONSTRAINT tf_actor_keyword_key UNIQUE (keyword);


--
-- Name: tf_actor tf_actor_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor
    ADD CONSTRAINT tf_actor_pkey PRIMARY KEY (id);


--
-- Name: tf_domain_integration_profiles tf_domain_integration_profile_integration_profile_id_domain_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_domain_integration_profiles
    ADD CONSTRAINT tf_domain_integration_profile_integration_profile_id_domain_key UNIQUE (integration_profile_id, domain_id);


--
-- Name: tf_domain tf_domain_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_domain
    ADD CONSTRAINT tf_domain_keyword_key UNIQUE (keyword);


--
-- Name: tf_domain tf_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_domain
    ADD CONSTRAINT tf_domain_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile tf_integration_profile_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_integration_profile
    ADD CONSTRAINT tf_integration_profile_keyword_key UNIQUE (keyword);


--
-- Name: tf_integration_profile_option tf_integration_profile_option_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_integration_profile_option
    ADD CONSTRAINT tf_integration_profile_option_keyword_key UNIQUE (keyword);


--
-- Name: tf_integration_profile_option tf_integration_profile_option_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_integration_profile_option
    ADD CONSTRAINT tf_integration_profile_option_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile tf_integration_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_integration_profile
    ADD CONSTRAINT tf_integration_profile_pkey PRIMARY KEY (id);


--
-- Name: tf_transaction tf_transaction_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_transaction
    ADD CONSTRAINT tf_transaction_keyword_key UNIQUE (keyword);


--
-- Name: tf_transaction tf_transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_transaction
    ADD CONSTRAINT tf_transaction_pkey PRIMARY KEY (id);


--
-- Name: tm_oid tm_oid_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tm_oid
    ADD CONSTRAINT tm_oid_pkey PRIMARY KEY (id);


--
-- Name: tm_path tm_path_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tm_path
    ADD CONSTRAINT tm_path_keyword_key UNIQUE (keyword);


--
-- Name: tm_path tm_path_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tm_path
    ADD CONSTRAINT tm_path_pkey PRIMARY KEY (id);


--
-- Name: tp_global_input_instance tp_global_input_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_global_input_instance
    ADD CONSTRAINT tp_global_input_instance_pkey PRIMARY KEY (id);


--
-- Name: tp_global_input_type tp_global_input_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_global_input_type
    ADD CONSTRAINT tp_global_input_type_pkey PRIMARY KEY (id);


--
-- Name: tp_input_type tp_input_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_input_type
    ADD CONSTRAINT tp_input_type_pkey PRIMARY KEY (id);


--
-- Name: tp_rule_result tp_rule_result_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_rule_result
    ADD CONSTRAINT tp_rule_result_pkey PRIMARY KEY (id);


--
-- Name: tp_rule_type tp_rule_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_rule_type
    ADD CONSTRAINT tp_rule_type_pkey PRIMARY KEY (id);


--
-- Name: tp_test_plan_instance tp_test_plan_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_plan_instance
    ADD CONSTRAINT tp_test_plan_instance_pkey PRIMARY KEY (id);


--
-- Name: tp_test_plan tp_test_plan_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_plan
    ADD CONSTRAINT tp_test_plan_pkey PRIMARY KEY (id);


--
-- Name: tp_test_step_instance tp_test_step_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_step_instance
    ADD CONSTRAINT tp_test_step_instance_pkey PRIMARY KEY (id);


--
-- Name: tp_test_step tp_test_step_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_step
    ADD CONSTRAINT tp_test_step_pkey PRIMARY KEY (id);


--
-- Name: tp_transaction_template tp_transaction_template_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_transaction_template
    ADD CONSTRAINT tp_transaction_template_pkey PRIMARY KEY (id);


--
-- Name: transaction_type transaction_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY transaction_type
    ADD CONSTRAINT transaction_type_pkey PRIMARY KEY (id);


--
-- Name: transaction_type_tf_transaction transaction_type_tf_transacti_transaction_type_id_transacti_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY transaction_type_tf_transaction
    ADD CONSTRAINT transaction_type_tf_transacti_transaction_type_id_transacti_key UNIQUE (transaction_type_id, transaction_id);


--
-- Name: transfer_syntax_uid transfer_syntax_uid_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY transfer_syntax_uid
    ADD CONSTRAINT transfer_syntax_uid_pkey PRIMARY KEY (id);


--
-- Name: transfer_syntax_uid uid_; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY transfer_syntax_uid
    ADD CONSTRAINT uid_ UNIQUE (uid);


--
-- Name: registry_object_metadata_slotdescriber uk436ac237b4687a51; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_metadata_slotdescriber
    ADD CONSTRAINT uk436ac237b4687a51 UNIQUE (slotdescriber_id);


--
-- Name: registry_package_extra_constraints uk4892df4c1023e19d; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_package_extra_constraints
    ADD CONSTRAINT uk4892df4c1023e19d UNIQUE (extraconstraintspecifications_id);


--
-- Name: extrinsic_object_extra_constraints uk6123f50f1023e19d; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY extrinsic_object_extra_constraints
    ADD CONSTRAINT uk6123f50f1023e19d UNIQUE (extraconstraintspecifications_id);


--
-- Name: tp_test_step_instance_tp_input_type uk68e1d81165cc1b31; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_step_instance_tp_input_type
    ADD CONSTRAINT uk68e1d81165cc1b31 UNIQUE (inputs_id);


--
-- Name: tp_test_step_instance_xdstar_message uk77b54c0769374df5; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_step_instance_xdstar_message
    ADD CONSTRAINT uk77b54c0769374df5 UNIQUE (abstractmessage_id);


--
-- Name: tp_test_step_input uk7c5b46a11c0e8db0; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_step_input
    ADD CONSTRAINT uk7c5b46a11c0e8db0 UNIQUE (input_id);


--
-- Name: message_type_required_parameters uk_1itee57vt485bxcrdrgtsahhx; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY message_type_required_parameters
    ADD CONSTRAINT uk_1itee57vt485bxcrdrgtsahhx UNIQUE (message_type_id, parameter_id);


--
-- Name: transaction_type_tf_transaction uk_1xp6dnm8x0wv250c8qxlnj806; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY transaction_type_tf_transaction
    ADD CONSTRAINT uk_1xp6dnm8x0wv250c8qxlnj806 UNIQUE (transaction_type_id, transaction_id);


--
-- Name: tp_test_step_rule uk_2vwfsanwfe5khuqifx82uax0c; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_step_rule
    ADD CONSTRAINT uk_2vwfsanwfe5khuqifx82uax0c UNIQUE (rule_id);


--
-- Name: tp_test_step_instance_tp_input_type uk_3h0hwgmsoakud47lp5bikl7bw; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_step_instance_tp_input_type
    ADD CONSTRAINT uk_3h0hwgmsoakud47lp5bikl7bw UNIQUE (inputs_id);


--
-- Name: tf_domain uk_436tct1jl8811q2xgd8bjth9q; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_domain
    ADD CONSTRAINT uk_436tct1jl8811q2xgd8bjth9q UNIQUE (keyword);


--
-- Name: message_type_optional_parameters uk_4dncey7hvnl9ehpihgoh9hk3c; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY message_type_optional_parameters
    ADD CONSTRAINT uk_4dncey7hvnl9ehpihgoh9hk3c UNIQUE (message_type_id, parameter_id);


--
-- Name: tf_transaction uk_6nt35dm69gbrrj0aegkkqalr2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_transaction
    ADD CONSTRAINT uk_6nt35dm69gbrrj0aegkkqalr2 UNIQUE (keyword);


--
-- Name: tp_test_step_instance_xdstar_message uk_6uho8a84trh869w2ldu932wex; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_step_instance_xdstar_message
    ADD CONSTRAINT uk_6uho8a84trh869w2ldu932wex UNIQUE (abstractmessage_id);


--
-- Name: gs_test_instance_participants uk_7rrch4mgjwqsbug2p4fgu9nwm; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance_participants
    ADD CONSTRAINT uk_7rrch4mgjwqsbug2p4fgu9nwm UNIQUE (server_aipo_id, server_test_instance_participants_id);


--
-- Name: ut_unit_test uk_89jnp3ftpf9x1fabhp0ld3g6y; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY ut_unit_test
    ADD CONSTRAINT uk_89jnp3ftpf9x1fabhp0ld3g6y UNIQUE (keyword);


--
-- Name: tf_actor_integration_profile_option uk_8b4tb6bcp3eh7mtsucokgh7fx; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor_integration_profile_option
    ADD CONSTRAINT uk_8b4tb6bcp3eh7mtsucokgh7fx UNIQUE (actor_integration_profile_id, integration_profile_option_id);


--
-- Name: registry_package_extra_constraints uk_8wmqvjjg70hb7di5gq0je52rg; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_package_extra_constraints
    ADD CONSTRAINT uk_8wmqvjjg70hb7di5gq0je52rg UNIQUE (extraconstraintspecifications_id);


--
-- Name: tp_test_plan_instance_tp_global_input_instance uk_atw3xp6pgkigmq5fq1p1tq04h; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_plan_instance_tp_global_input_instance
    ADD CONSTRAINT uk_atw3xp6pgkigmq5fq1p1tq04h UNIQUE (globalinputinstance_id);


--
-- Name: tf_actor_integration_profile uk_b6ejd87o8v27xinqw27nn1hss; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor_integration_profile
    ADD CONSTRAINT uk_b6ejd87o8v27xinqw27nn1hss UNIQUE (actor_id, integration_profile_id);


--
-- Name: adhoc_query_extra_constraints uk_d33disreibesex7evxbmq3jgi; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY adhoc_query_extra_constraints
    ADD CONSTRAINT uk_d33disreibesex7evxbmq3jgi UNIQUE (extraconstraintspecifications_id);


--
-- Name: tp_test_plan_test_step uk_d89vhplr24irwwtksd94mqwrc; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_plan_test_step
    ADD CONSTRAINT uk_d89vhplr24irwwtksd94mqwrc UNIQUE (teststep_id);


--
-- Name: extrinsic_object_extra_constraints uk_dba900iu33qmao5yy27en4381; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY extrinsic_object_extra_constraints
    ADD CONSTRAINT uk_dba900iu33qmao5yy27en4381 UNIQUE (extraconstraintspecifications_id);


--
-- Name: tp_test_plan_global_input uk_ffcl8hce5swa0dsymmmf49l01; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_plan_global_input
    ADD CONSTRAINT uk_ffcl8hce5swa0dsymmmf49l01 UNIQUE (globalinput_id);


--
-- Name: tf_actor uk_fpb6vpa9bnw6cjsb1pucuuivw; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor
    ADD CONSTRAINT uk_fpb6vpa9bnw6cjsb1pucuuivw UNIQUE (keyword);


--
-- Name: message_registeres_files uk_ifs40myhegcl7tpwveb1erb18; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY message_registeres_files
    ADD CONSTRAINT uk_ifs40myhegcl7tpwveb1erb18 UNIQUE (message_id, registered_file_id);


--
-- Name: tf_domain_integration_profiles uk_iw9qgddyj9xsshmek3gr6u588; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_domain_integration_profiles
    ADD CONSTRAINT uk_iw9qgddyj9xsshmek3gr6u588 UNIQUE (integration_profile_id, domain_id);


--
-- Name: cmn_home uk_mv2quil5gwcd8bxyc76v4vyy1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_home
    ADD CONSTRAINT uk_mv2quil5gwcd8bxyc76v4vyy1 UNIQUE (iso3_language);


--
-- Name: validator_packclass_extra_constraints uk_n34udbdv37fpveti0egdfg97w; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY validator_packclass_extra_constraints
    ADD CONSTRAINT uk_n34udbdv37fpveti0egdfg97w UNIQUE (extra_constraint_specifications_id);


--
-- Name: tf_integration_profile uk_ny4glgtyovt2w045nwct19arx; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_integration_profile
    ADD CONSTRAINT uk_ny4glgtyovt2w045nwct19arx UNIQUE (keyword);


--
-- Name: registry_object_metadata_slotdescriber uk_orsbgmsq1xunrb4n3pq4bjy5u; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_metadata_slotdescriber
    ADD CONSTRAINT uk_orsbgmsq1xunrb4n3pq4bjy5u UNIQUE (slotdescriber_id);


--
-- Name: ut_unit_test_file_log uk_p5f447j78nqr5q5njfp5kri80; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY ut_unit_test_file_log
    ADD CONSTRAINT uk_p5f447j78nqr5q5njfp5kri80 UNIQUE (file_id, log_id);


--
-- Name: registry_object_list_extra_constraints uk_q6edfsdx870poq91ufm6uj0t2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_list_extra_constraints
    ADD CONSTRAINT uk_q6edfsdx870poq91ufm6uj0t2 UNIQUE (extra_constraint_specifications_id);


--
-- Name: extrinsic_object_extra_constraints uk_r40lmlxm3o7ucurmacfpk3o3q; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY extrinsic_object_extra_constraints
    ADD CONSTRAINT uk_r40lmlxm3o7ucurmacfpk3o3q UNIQUE (extraconstraintspecifications_id);


--
-- Name: affinity_domain_transactions uk_r4g9ud7n9b2gx7tia9engpipe; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY affinity_domain_transactions
    ADD CONSTRAINT uk_r4g9ud7n9b2gx7tia9engpipe UNIQUE (affinity_domain_id, transaction_id);


--
-- Name: cmn_company_details uk_r6osh086b3nqbuxpswcp1hcc2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_company_details
    ADD CONSTRAINT uk_r6osh086b3nqbuxpswcp1hcc2 UNIQUE (company_keyword);


--
-- Name: tp_test_step_input uk_rvl3r6gg7294rmue3syx05snc; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_step_input
    ADD CONSTRAINT uk_rvl3r6gg7294rmue3syx05snc UNIQUE (input_id);


--
-- Name: tf_integration_profile_option uk_thqc1vykug4qhn8jdij04d1bh; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_integration_profile_option
    ADD CONSTRAINT uk_thqc1vykug4qhn8jdij04d1bh UNIQUE (keyword);


--
-- Name: adhoc_query_extra_constraints uka9d27bca1023e19d; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY adhoc_query_extra_constraints
    ADD CONSTRAINT uka9d27bca1023e19d UNIQUE (extraconstraintspecifications_id);


--
-- Name: tp_test_plan_test_step ukabf5a5cd14833a9c; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_plan_test_step
    ADD CONSTRAINT ukabf5a5cd14833a9c UNIQUE (teststep_id);


--
-- Name: tp_test_plan_global_input ukc969743add73e53; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_plan_global_input
    ADD CONSTRAINT ukc969743add73e53 UNIQUE (globalinput_id);


--
-- Name: tp_test_plan_instance_tp_global_input_instance ukcfcfe110e63c9e; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_plan_instance_tp_global_input_instance
    ADD CONSTRAINT ukcfcfe110e63c9e UNIQUE (globalinputinstance_id);


--
-- Name: tp_test_step_rule ukeb40f1455c4ea93e; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_step_rule
    ADD CONSTRAINT ukeb40f1455c4ea93e UNIQUE (rule_id);


--
-- Name: usage_metadata usage_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY usage_metadata
    ADD CONSTRAINT usage_metadata_pkey PRIMARY KEY (id);


--
-- Name: ut_unit_test_file ut_unit_test_file_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY ut_unit_test_file
    ADD CONSTRAINT ut_unit_test_file_pkey PRIMARY KEY (id);


--
-- Name: ut_unit_test_log ut_unit_test_log_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY ut_unit_test_log
    ADD CONSTRAINT ut_unit_test_log_pkey PRIMARY KEY (id);


--
-- Name: ut_unit_test ut_unit_test_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY ut_unit_test
    ADD CONSTRAINT ut_unit_test_pkey PRIMARY KEY (id);


--
-- Name: validator_pack_class_nist_code validator_pack_class_nist_code_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY validator_pack_class_nist_code
    ADD CONSTRAINT validator_pack_class_nist_code_pkey PRIMARY KEY (id);


--
-- Name: validator_pack_class validator_pack_class_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY validator_pack_class
    ADD CONSTRAINT validator_pack_class_pkey PRIMARY KEY (id);


--
-- Name: validator_registry_object_list_metadata_rol validator_registry_object_list_metadata_rol_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY validator_registry_object_list_metadata_rol
    ADD CONSTRAINT validator_registry_object_list_metadata_rol_pkey PRIMARY KEY (id);


--
-- Name: xcpd_home_community xcpd_home_community_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xcpd_home_community
    ADD CONSTRAINT xcpd_home_community_pkey PRIMARY KEY (id);


--
-- Name: xdstar_message xdstar_message_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xdstar_message
    ADD CONSTRAINT xdstar_message_pkey PRIMARY KEY (id);


--
-- Name: xdstar_message_resp xdstar_message_resp_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xdstar_message_resp
    ADD CONSTRAINT xdstar_message_resp_pkey PRIMARY KEY (id);


--
-- Name: registry_object_metadata_usage fk158df70f822d9af7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_metadata_usage
    ADD CONSTRAINT fk158df70f822d9af7 FOREIGN KEY (usages_id) REFERENCES usage_metadata(id);


--
-- Name: registry_object_metadata_usage fk158df70ff85ee775; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_metadata_usage
    ADD CONSTRAINT fk158df70ff85ee775 FOREIGN KEY (registry_object_metadata_id) REFERENCES registry_object_metadata(id);


--
-- Name: mbv_assertion fk16ce89c47778d25e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_assertion
    ADD CONSTRAINT fk16ce89c47778d25e FOREIGN KEY (constraint_id) REFERENCES mbv_constraint(id);


--
-- Name: slot_metadata_describer fk1c0819d875341666; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY slot_metadata_describer
    ADD CONSTRAINT fk1c0819d875341666 FOREIGN KEY (slot_id) REFERENCES slot_metadata(id);


--
-- Name: gs_test_instance_oid fk200bd51aadaef596; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance_oid
    ADD CONSTRAINT fk200bd51aadaef596 FOREIGN KEY (test_instance_id) REFERENCES gs_test_instance(id);


--
-- Name: gs_test_instance_oid fk200bd51afb5a2c1c; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance_oid
    ADD CONSTRAINT fk200bd51afb5a2c1c FOREIGN KEY (oid_configuration_id) REFERENCES tm_oid(id);


--
-- Name: ut_unit_test_file_log fk20baed3342c51221; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY ut_unit_test_file_log
    ADD CONSTRAINT fk20baed3342c51221 FOREIGN KEY (file_id) REFERENCES ut_unit_test_file(id);


--
-- Name: ut_unit_test_file_log fk20baed33cb6c073; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY ut_unit_test_file_log
    ADD CONSTRAINT fk20baed33cb6c073 FOREIGN KEY (log_id) REFERENCES ut_unit_test_log(id);


--
-- Name: ut_unit_test_file fk2332a1aee42bb174; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY ut_unit_test_file
    ADD CONSTRAINT fk2332a1aee42bb174 FOREIGN KEY (unit_test_id) REFERENCES ut_unit_test(id);


--
-- Name: cfg_web_service_configuration fk23f4a6263927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_web_service_configuration
    ADD CONSTRAINT fk23f4a6263927e7e FOREIGN KEY (gs_system_id) REFERENCES gs_system(id);


--
-- Name: cfg_web_service_configuration fk23f4a626511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_web_service_configuration
    ADD CONSTRAINT fk23f4a626511b8deb FOREIGN KEY (configuration_id) REFERENCES cfg_configuration(id);


--
-- Name: validator_pack_class_usage fk23fc18415a32764d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY validator_pack_class_usage
    ADD CONSTRAINT fk23fc18415a32764d FOREIGN KEY (validator_pack_class_id) REFERENCES validator_pack_class(id);


--
-- Name: validator_pack_class_usage fk23fc1841822d9af7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY validator_pack_class_usage
    ADD CONSTRAINT fk23fc1841822d9af7 FOREIGN KEY (usages_id) REFERENCES usage_metadata(id);


--
-- Name: cfg_hl7_initiator_configuration fk2891f8ff3927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_initiator_configuration
    ADD CONSTRAINT fk2891f8ff3927e7e FOREIGN KEY (gs_system_id) REFERENCES gs_system(id);


--
-- Name: cfg_hl7_initiator_configuration fk2891f8ff511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_initiator_configuration
    ADD CONSTRAINT fk2891f8ff511b8deb FOREIGN KEY (configuration_id) REFERENCES cfg_configuration(id);


--
-- Name: tf_domain_integration_profiles fk2c03ea431b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_domain_integration_profiles
    ADD CONSTRAINT fk2c03ea431b781a49 FOREIGN KEY (domain_id) REFERENCES tf_domain(id);


--
-- Name: tf_domain_integration_profiles fk2c03ea43866df480; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_domain_integration_profiles
    ADD CONSTRAINT fk2c03ea43866df480 FOREIGN KEY (integration_profile_id) REFERENCES tf_integration_profile(id);


--
-- Name: code fk2eadedbd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY code
    ADD CONSTRAINT fk2eadedbd491f4b FOREIGN KEY (transaction_id) REFERENCES tf_transaction(id);


--
-- Name: mbv_class_type fk350aaf1f8e9bb0b6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_class_type
    ADD CONSTRAINT fk350aaf1f8e9bb0b6 FOREIGN KEY (package_id) REFERENCES mbv_package(id);


--
-- Name: mbv_class_type fk350aaf1f9ba1deeb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_class_type
    ADD CONSTRAINT fk350aaf1f9ba1deeb FOREIGN KEY (documentation_spec_id) REFERENCES mbv_documentation_spec(id);


--
-- Name: mbv_constraint fk3afefb5bc668fe56; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_constraint
    ADD CONSTRAINT fk3afefb5bc668fe56 FOREIGN KEY (classtype_id) REFERENCES mbv_class_type(id);


--
-- Name: tp_global_input_instance fk3d2aec03d8fb89a1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_global_input_instance
    ADD CONSTRAINT fk3d2aec03d8fb89a1 FOREIGN KEY (global_input_id) REFERENCES tp_global_input_type(id);


--
-- Name: transaction_type_tf_transaction fk416aad559fe81658; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY transaction_type_tf_transaction
    ADD CONSTRAINT fk416aad559fe81658 FOREIGN KEY (transaction_type_id) REFERENCES transaction_type(id);


--
-- Name: transaction_type_tf_transaction fk416aad55bd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY transaction_type_tf_transaction
    ADD CONSTRAINT fk416aad55bd491f4b FOREIGN KEY (transaction_id) REFERENCES tf_transaction(id);


--
-- Name: tp_test_plan_instance fk41adb6a1d5846765; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_plan_instance
    ADD CONSTRAINT fk41adb6a1d5846765 FOREIGN KEY (test_plan_id) REFERENCES tp_test_plan(id);


--
-- Name: mbv_standards fk41dd2c188e9bb0b6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_standards
    ADD CONSTRAINT fk41dd2c188e9bb0b6 FOREIGN KEY (package_id) REFERENCES mbv_package(id);


--
-- Name: registry_object_metadata_slotdescriber fk436ac2371be729a7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_metadata_slotdescriber
    ADD CONSTRAINT fk436ac2371be729a7 FOREIGN KEY (slotdescriber_id) REFERENCES slot_metadata_describer(id);


--
-- Name: registry_object_metadata_slotdescriber fk436ac237f85ee775; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_metadata_slotdescriber
    ADD CONSTRAINT fk436ac237f85ee775 FOREIGN KEY (registry_object_metadata_id) REFERENCES registry_object_metadata(id);


--
-- Name: classification_metadata_describer fk45bf76f062c33946; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY classification_metadata_describer
    ADD CONSTRAINT fk45bf76f062c33946 FOREIGN KEY (classification_id) REFERENCES registry_object_metadata(id);


--
-- Name: cfg_hl7_v3_responder_configuration fk484bb2fe3927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_v3_responder_configuration
    ADD CONSTRAINT fk484bb2fe3927e7e FOREIGN KEY (gs_system_id) REFERENCES gs_system(id);


--
-- Name: cfg_hl7_v3_responder_configuration fk484bb2fe511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_v3_responder_configuration
    ADD CONSTRAINT fk484bb2fe511b8deb FOREIGN KEY (configuration_id) REFERENCES cfg_configuration(id);


--
-- Name: registry_package_extra_constraints fk4892df4c78dfdc8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_package_extra_constraints
    ADD CONSTRAINT fk4892df4c78dfdc8 FOREIGN KEY (registry_object_metadata_id) REFERENCES registry_object_metadata(id);


--
-- Name: registry_package_extra_constraints fk4892df4c890db86; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_package_extra_constraints
    ADD CONSTRAINT fk4892df4c890db86 FOREIGN KEY (extraconstraintspecifications_id) REFERENCES constraint_specification(id);


--
-- Name: gs_contextual_information fk4a7365f13b128c1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_contextual_information
    ADD CONSTRAINT fk4a7365f13b128c1 FOREIGN KEY (path) REFERENCES tm_path(id);


--
-- Name: registry_object_metadata_describer fk4bf73575321281a1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_metadata_describer
    ADD CONSTRAINT fk4bf73575321281a1 FOREIGN KEY (registry_object_id) REFERENCES registry_object_metadata(id);


--
-- Name: xdstar_message fk535b480611b2a759; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xdstar_message
    ADD CONSTRAINT fk535b480611b2a759 FOREIGN KEY (configuration_id) REFERENCES system_configuration(id);


--
-- Name: xdstar_message fk535b480672a3043a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xdstar_message
    ADD CONSTRAINT fk535b480672a3043a FOREIGN KEY (affinity_domain_id) REFERENCES affinity_domain(id);


--
-- Name: xdstar_message fk535b4806bd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xdstar_message
    ADD CONSTRAINT fk535b4806bd491f4b FOREIGN KEY (transaction_id) REFERENCES tf_transaction(id);


--
-- Name: xdstar_message fk535b4806c0bbfe10; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xdstar_message
    ADD CONSTRAINT fk535b4806c0bbfe10 FOREIGN KEY (received_message_content_id) REFERENCES metadata_message(id);


--
-- Name: xdstar_message fk535b4806d72636f9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xdstar_message
    ADD CONSTRAINT fk535b4806d72636f9 FOREIGN KEY (sent_message_content_id) REFERENCES metadata_message(id);


--
-- Name: conf_type_usages fk59b166fc6d662ac1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY conf_type_usages
    ADD CONSTRAINT fk59b166fc6d662ac1 FOREIGN KEY (configuration_type_id) REFERENCES configuration_type(id);


--
-- Name: conf_type_usages fk59b166fcdd54bc19; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY conf_type_usages
    ADD CONSTRAINT fk59b166fcdd54bc19 FOREIGN KEY (listusages_id) REFERENCES usage_metadata(id);


--
-- Name: gs_test_instance_participants fk5c650a50611bae11; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance_participants
    ADD CONSTRAINT fk5c650a50611bae11 FOREIGN KEY (system_id) REFERENCES gs_system(id);


--
-- Name: gs_test_instance_participants fk5c650a5083369963; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance_participants
    ADD CONSTRAINT fk5c650a5083369963 FOREIGN KEY (aipo_id) REFERENCES tf_actor_integration_profile_option(id);


--
-- Name: extrinsic_object_extra_constraints fk6123f50f890db86; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY extrinsic_object_extra_constraints
    ADD CONSTRAINT fk6123f50f890db86 FOREIGN KEY (extraconstraintspecifications_id) REFERENCES constraint_specification(id);


--
-- Name: extrinsic_object_extra_constraints fk6123f50fa0cf8355; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY extrinsic_object_extra_constraints
    ADD CONSTRAINT fk6123f50fa0cf8355 FOREIGN KEY (registry_object_metadata_id) REFERENCES registry_object_metadata(id);


--
-- Name: cfg_dicom_scu_configuration fk62aeccde3927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_dicom_scu_configuration
    ADD CONSTRAINT fk62aeccde3927e7e FOREIGN KEY (gs_system_id) REFERENCES gs_system(id);


--
-- Name: cfg_dicom_scu_configuration fk62aeccde4f3bdb32; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_dicom_scu_configuration
    ADD CONSTRAINT fk62aeccde4f3bdb32 FOREIGN KEY (sop_class_id) REFERENCES cfg_sop_class(id);


--
-- Name: cfg_dicom_scu_configuration fk62aeccde511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_dicom_scu_configuration
    ADD CONSTRAINT fk62aeccde511b8deb FOREIGN KEY (configuration_id) REFERENCES cfg_configuration(id);


--
-- Name: tp_test_step_instance_tp_input_type fk68e1d8114b6481e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_step_instance_tp_input_type
    ADD CONSTRAINT fk68e1d8114b6481e FOREIGN KEY (tp_test_step_instance_id) REFERENCES tp_test_step_instance(id);


--
-- Name: tp_test_step_instance_tp_input_type fk68e1d811c95f5d69; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_step_instance_tp_input_type
    ADD CONSTRAINT fk68e1d811c95f5d69 FOREIGN KEY (inputs_id) REFERENCES tp_input_type(id);


--
-- Name: validator_registry_object_list_metadata_rol fk70f4e9d528c7e2d7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY validator_registry_object_list_metadata_rol
    ADD CONSTRAINT fk70f4e9d528c7e2d7 FOREIGN KEY (registryobjectlistmetadata_id) REFERENCES registry_object_list_metadata(id);


--
-- Name: validator_registry_object_list_metadata_rol fk70f4e9d5b9410ced; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY validator_registry_object_list_metadata_rol
    ADD CONSTRAINT fk70f4e9d5b9410ced FOREIGN KEY (id) REFERENCES validator_pack_class(id);


--
-- Name: tp_rule_result fk729a2d7d26a5b700; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_rule_result
    ADD CONSTRAINT fk729a2d7d26a5b700 FOREIGN KEY (rule_id) REFERENCES tp_rule_type(id);


--
-- Name: tp_rule_result fk729a2d7d72ae3da1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_rule_result
    ADD CONSTRAINT fk729a2d7d72ae3da1 FOREIGN KEY (test_step_instance_id) REFERENCES tp_test_step_instance(id);


--
-- Name: ut_unit_test_log fk74bfa8d2e42bb174; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY ut_unit_test_log
    ADD CONSTRAINT fk74bfa8d2e42bb174 FOREIGN KEY (unit_test_id) REFERENCES ut_unit_test(id);


--
-- Name: message_type_optional_parameters fk771f04dc47adb392; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY message_type_optional_parameters
    ADD CONSTRAINT fk771f04dc47adb392 FOREIGN KEY (message_type_id) REFERENCES message_type(id);


--
-- Name: message_type_optional_parameters fk771f04dc7ef1b703; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY message_type_optional_parameters
    ADD CONSTRAINT fk771f04dc7ef1b703 FOREIGN KEY (parameter_id) REFERENCES parameter(id);


--
-- Name: tp_test_step_instance_xdstar_message fk77b54c074b6481e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_step_instance_xdstar_message
    ADD CONSTRAINT fk77b54c074b6481e FOREIGN KEY (tp_test_step_instance_id) REFERENCES tp_test_step_instance(id);


--
-- Name: tp_test_step_instance_xdstar_message fk77b54c07d4cd2ebb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_step_instance_xdstar_message
    ADD CONSTRAINT fk77b54c07d4cd2ebb FOREIGN KEY (abstractmessage_id) REFERENCES xdstar_message(id);


--
-- Name: tf_actor_integration_profile_option fk78115a4d698ea7bd; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor_integration_profile_option
    ADD CONSTRAINT fk78115a4d698ea7bd FOREIGN KEY (integration_profile_option_id) REFERENCES tf_integration_profile_option(id);


--
-- Name: tf_actor_integration_profile_option fk78115a4d72619921; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor_integration_profile_option
    ADD CONSTRAINT fk78115a4d72619921 FOREIGN KEY (actor_integration_profile_id) REFERENCES tf_actor_integration_profile(id);


--
-- Name: xdstar_message_resp fk7a1ee06972a3043a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xdstar_message_resp
    ADD CONSTRAINT fk7a1ee06972a3043a FOREIGN KEY (affinity_domain_id) REFERENCES affinity_domain(id);


--
-- Name: xdstar_message_resp fk7a1ee069bd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xdstar_message_resp
    ADD CONSTRAINT fk7a1ee069bd491f4b FOREIGN KEY (transaction_id) REFERENCES tf_transaction(id);


--
-- Name: xdstar_message_resp fk7a1ee069c0bbfe10; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xdstar_message_resp
    ADD CONSTRAINT fk7a1ee069c0bbfe10 FOREIGN KEY (received_message_content_id) REFERENCES metadata_message(id);


--
-- Name: xdstar_message_resp fk7a1ee069d72636f9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xdstar_message_resp
    ADD CONSTRAINT fk7a1ee069d72636f9 FOREIGN KEY (sent_message_content_id) REFERENCES metadata_message(id);


--
-- Name: tp_test_step_input fk7c5b46a172efdbe8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_step_input
    ADD CONSTRAINT fk7c5b46a172efdbe8 FOREIGN KEY (tp_test_step_id) REFERENCES tp_test_step(id);


--
-- Name: tp_test_step_input fk7c5b46a17fa1cfe8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_step_input
    ADD CONSTRAINT fk7c5b46a17fa1cfe8 FOREIGN KEY (input_id) REFERENCES tp_input_type(id);


--
-- Name: usage_metadata fk7d18f40d7007d3ad; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY usage_metadata
    ADD CONSTRAINT fk7d18f40d7007d3ad FOREIGN KEY (affinity_id) REFERENCES affinity_domain(id);


--
-- Name: usage_metadata fk7d18f40dbd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY usage_metadata
    ADD CONSTRAINT fk7d18f40dbd491f4b FOREIGN KEY (transaction_id) REFERENCES tf_transaction(id);


--
-- Name: cfg_configuration fk7d98485b9a8e05a9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_configuration
    ADD CONSTRAINT fk7d98485b9a8e05a9 FOREIGN KEY (host_id) REFERENCES cfg_host(id);


--
-- Name: tp_test_step_instance fk86e9a25e5e9a5b69; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_step_instance
    ADD CONSTRAINT fk86e9a25e5e9a5b69 FOREIGN KEY (system_configuration_id) REFERENCES system_configuration(id);


--
-- Name: tp_test_step_instance fk86e9a25e67dc1705; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_step_instance
    ADD CONSTRAINT fk86e9a25e67dc1705 FOREIGN KEY (test_step_id) REFERENCES tp_test_step(id);


--
-- Name: tp_test_step_instance fk86e9a25ee2b6f5db; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_step_instance
    ADD CONSTRAINT fk86e9a25ee2b6f5db FOREIGN KEY (test_plan_instance_id) REFERENCES tp_test_plan_instance(id);


--
-- Name: ut_unit_test fk86ec04cd5a32764d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY ut_unit_test
    ADD CONSTRAINT fk86ec04cd5a32764d FOREIGN KEY (validator_pack_class_id) REFERENCES validator_pack_class(id);


--
-- Name: cfg_hl7_v3_initiator_configuration fk94b8666b3927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_v3_initiator_configuration
    ADD CONSTRAINT fk94b8666b3927e7e FOREIGN KEY (gs_system_id) REFERENCES gs_system(id);


--
-- Name: cfg_hl7_v3_initiator_configuration fk94b8666b511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_v3_initiator_configuration
    ADD CONSTRAINT fk94b8666b511b8deb FOREIGN KEY (configuration_id) REFERENCES cfg_configuration(id);


--
-- Name: registry_object_list_metadata_extrinsicobject fk95266d09807a730a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_list_metadata_extrinsicobject
    ADD CONSTRAINT fk95266d09807a730a FOREIGN KEY (extrinsicobject_id) REFERENCES extrinsic_object_metadata_describer(id);


--
-- Name: registry_object_list_metadata_extrinsicobject fk95266d099cfc084e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_list_metadata_extrinsicobject
    ADD CONSTRAINT fk95266d099cfc084e FOREIGN KEY (registry_object_list_metadata_id) REFERENCES registry_object_list_metadata(id);


--
-- Name: registry_object_list_metadata_usage fk9658e94822d9af7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_list_metadata_usage
    ADD CONSTRAINT fk9658e94822d9af7 FOREIGN KEY (usages_id) REFERENCES usage_metadata(id);


--
-- Name: registry_object_list_metadata_usage fk9658e949cfc084e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_list_metadata_usage
    ADD CONSTRAINT fk9658e949cfc084e FOREIGN KEY (registry_object_list_metadata_id) REFERENCES registry_object_list_metadata(id);


--
-- Name: cmn_message_instance_metadata fk_6pxpnca029j7ewvr7dus8q3sf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_message_instance_metadata
    ADD CONSTRAINT fk_6pxpnca029j7ewvr7dus8q3sf FOREIGN KEY (message_instance_id) REFERENCES cmn_message_instance(id);


--
-- Name: cmn_receiver_console fk_6s2mr59uvd93xmp2dpkepfdvp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_receiver_console
    ADD CONSTRAINT fk_6s2mr59uvd93xmp2dpkepfdvp FOREIGN KEY (domain_id) REFERENCES tf_domain(id);


--
-- Name: cmn_ip_address fk_8b3pqht8rw1vyiu1o0dm1ewtt; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_ip_address
    ADD CONSTRAINT fk_8b3pqht8rw1vyiu1o0dm1ewtt FOREIGN KEY (company_details_id) REFERENCES cmn_company_details(id);


--
-- Name: validator_packclass_extra_constraints fk_cblr6qidhomg5m0gp7ikssx05; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY validator_packclass_extra_constraints
    ADD CONSTRAINT fk_cblr6qidhomg5m0gp7ikssx05 FOREIGN KEY (validator_pack_class_id) REFERENCES validator_pack_class(id);


--
-- Name: cmn_receiver_console fk_dvjns4jx634sntdq34baohlny; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_receiver_console
    ADD CONSTRAINT fk_dvjns4jx634sntdq34baohlny FOREIGN KEY (transaction_id) REFERENCES tf_transaction(id);


--
-- Name: registry_object_list_extra_constraints fk_hl29tswqkaqvolxgtiire32il; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_list_extra_constraints
    ADD CONSTRAINT fk_hl29tswqkaqvolxgtiire32il FOREIGN KEY (registry_object_list_id) REFERENCES registry_object_list_metadata(id);


--
-- Name: validator_packclass_extra_constraints fk_n34udbdv37fpveti0egdfg97w; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY validator_packclass_extra_constraints
    ADD CONSTRAINT fk_n34udbdv37fpveti0egdfg97w FOREIGN KEY (extra_constraint_specifications_id) REFERENCES constraint_specification(id);


--
-- Name: registry_object_list_extra_constraints fk_q6edfsdx870poq91ufm6uj0t2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_list_extra_constraints
    ADD CONSTRAINT fk_q6edfsdx870poq91ufm6uj0t2 FOREIGN KEY (extra_constraint_specifications_id) REFERENCES constraint_specification(id);


--
-- Name: cmn_receiver_console fk_tnagd28ej30pkampt9gjs28d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_receiver_console
    ADD CONSTRAINT fk_tnagd28ej30pkampt9gjs28d FOREIGN KEY (simulated_actor_id) REFERENCES tf_actor(id);


--
-- Name: cfg_dicom_scp_configuration fka165b9993927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_dicom_scp_configuration
    ADD CONSTRAINT fka165b9993927e7e FOREIGN KEY (gs_system_id) REFERENCES gs_system(id);


--
-- Name: cfg_dicom_scp_configuration fka165b9994f3bdb32; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_dicom_scp_configuration
    ADD CONSTRAINT fka165b9994f3bdb32 FOREIGN KEY (sop_class_id) REFERENCES cfg_sop_class(id);


--
-- Name: cfg_dicom_scp_configuration fka165b999511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_dicom_scp_configuration
    ADD CONSTRAINT fka165b999511b8deb FOREIGN KEY (configuration_id) REFERENCES cfg_configuration(id);


--
-- Name: cfg_syslog_configuration fka36479093927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_syslog_configuration
    ADD CONSTRAINT fka36479093927e7e FOREIGN KEY (gs_system_id) REFERENCES gs_system(id);


--
-- Name: cfg_syslog_configuration fka3647909511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_syslog_configuration
    ADD CONSTRAINT fka3647909511b8deb FOREIGN KEY (configuration_id) REFERENCES cfg_configuration(id);


--
-- Name: registry_object_list_metadata_registrypackage fka6140a7c8088d764; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_list_metadata_registrypackage
    ADD CONSTRAINT fka6140a7c8088d764 FOREIGN KEY (registrypackage_id) REFERENCES registry_package_metadata_describer(id);


--
-- Name: registry_object_list_metadata_registrypackage fka6140a7c9cfc084e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_list_metadata_registrypackage
    ADD CONSTRAINT fka6140a7c9cfc084e FOREIGN KEY (registry_object_list_metadata_id) REFERENCES registry_object_list_metadata(id);


--
-- Name: gs_message fka9c507b465385ec7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_message
    ADD CONSTRAINT fka9c507b465385ec7 FOREIGN KEY (test_instance_participants_receiver_id) REFERENCES gs_test_instance_participants(id);


--
-- Name: gs_message fka9c507b4bd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_message
    ADD CONSTRAINT fka9c507b4bd491f4b FOREIGN KEY (transaction_id) REFERENCES tf_transaction(id);


--
-- Name: gs_message fka9c507b4e0c3e041; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_message
    ADD CONSTRAINT fka9c507b4e0c3e041 FOREIGN KEY (test_instance_participants_sender_id) REFERENCES gs_test_instance_participants(id);


--
-- Name: adhoc_query_extra_constraints fka9d27bca890db86; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY adhoc_query_extra_constraints
    ADD CONSTRAINT fka9d27bca890db86 FOREIGN KEY (extraconstraintspecifications_id) REFERENCES constraint_specification(id);


--
-- Name: adhoc_query_extra_constraints fka9d27bcafd2db028; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY adhoc_query_extra_constraints
    ADD CONSTRAINT fka9d27bcafd2db028 FOREIGN KEY (registry_object_metadata_id) REFERENCES registry_object_metadata(id);


--
-- Name: tp_test_plan_test_step fkabf5a5cd39f3b0e0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_plan_test_step
    ADD CONSTRAINT fkabf5a5cd39f3b0e0 FOREIGN KEY (teststep_id) REFERENCES tp_test_step(id);


--
-- Name: tp_test_plan_test_step fkabf5a5cde0982c48; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_plan_test_step
    ADD CONSTRAINT fkabf5a5cde0982c48 FOREIGN KEY (tp_test_plan_id) REFERENCES tp_test_plan(id);


--
-- Name: message_type fkb3583472bd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY message_type
    ADD CONSTRAINT fkb3583472bd491f4b FOREIGN KEY (transaction_id) REFERENCES tf_transaction(id);


--
-- Name: adhoc_return_types fkb83a51f0988b02d6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY adhoc_return_types
    ADD CONSTRAINT fkb83a51f0988b02d6 FOREIGN KEY (id) REFERENCES registry_object_metadata(id);


--
-- Name: gs_test_instance_test_instance_participants fkbc3b31cd63e3d0fb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance_test_instance_participants
    ADD CONSTRAINT fkbc3b31cd63e3d0fb FOREIGN KEY (test_instance_participants_id) REFERENCES gs_test_instance_participants(id);


--
-- Name: gs_test_instance_test_instance_participants fkbc3b31cdadaef596; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance_test_instance_participants
    ADD CONSTRAINT fkbc3b31cdadaef596 FOREIGN KEY (test_instance_id) REFERENCES gs_test_instance(id);


--
-- Name: registry_object_metadata_classificationdescriber fkbf22bcfd9b9e7a7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_metadata_classificationdescriber
    ADD CONSTRAINT fkbf22bcfd9b9e7a7 FOREIGN KEY (classificationdescriber_id) REFERENCES classification_metadata_describer(id);


--
-- Name: registry_object_metadata_classificationdescriber fkbf22bcff85ee775; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_metadata_classificationdescriber
    ADD CONSTRAINT fkbf22bcff85ee775 FOREIGN KEY (registry_object_metadata_id) REFERENCES registry_object_metadata(id);


--
-- Name: gs_test_instance fkc783578f43e9dc7b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance
    ADD CONSTRAINT fkc783578f43e9dc7b FOREIGN KEY (test_instance_status_id) REFERENCES gs_test_instance_status(id);


--
-- Name: validator_pack_class_pack_class fkc942c1325a32764d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY validator_pack_class_pack_class
    ADD CONSTRAINT fkc942c1325a32764d FOREIGN KEY (validator_pack_class_id) REFERENCES validator_pack_class(id);


--
-- Name: validator_pack_class_pack_class fkc942c1326bfa2313; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY validator_pack_class_pack_class
    ADD CONSTRAINT fkc942c1326bfa2313 FOREIGN KEY (listpackclass_id) REFERENCES pack_class(id);


--
-- Name: tp_test_plan_global_input fkc969743a7e11a528; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_plan_global_input
    ADD CONSTRAINT fkc969743a7e11a528 FOREIGN KEY (globalinput_id) REFERENCES tp_global_input_type(id);


--
-- Name: tp_test_plan_global_input fkc969743ae0982c48; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_plan_global_input
    ADD CONSTRAINT fkc969743ae0982c48 FOREIGN KEY (tp_test_plan_id) REFERENCES tp_test_plan(id);


--
-- Name: tm_oid fkcc1f0704611bae11; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tm_oid
    ADD CONSTRAINT fkcc1f0704611bae11 FOREIGN KEY (system_id) REFERENCES gs_system(id);


--
-- Name: validator_pack_class_nist_code fkcd529190b9410ced; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY validator_pack_class_nist_code
    ADD CONSTRAINT fkcd529190b9410ced FOREIGN KEY (id) REFERENCES validator_pack_class(id);


--
-- Name: validator_pack_class_nist_code fkcd529190eabfbbd1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY validator_pack_class_nist_code
    ADD CONSTRAINT fkcd529190eabfbbd1 FOREIGN KEY (metadatacodes_id) REFERENCES nist_code(id);


--
-- Name: tp_test_plan_instance_tp_global_input_instance fkcfcfe16b2fbd8b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_plan_instance_tp_global_input_instance
    ADD CONSTRAINT fkcfcfe16b2fbd8b FOREIGN KEY (globalinputinstance_id) REFERENCES tp_global_input_instance(id);


--
-- Name: tp_test_plan_instance_tp_global_input_instance fkcfcfe174bf0058; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_plan_instance_tp_global_input_instance
    ADD CONSTRAINT fkcfcfe174bf0058 FOREIGN KEY (tp_test_plan_instance_id) REFERENCES tp_test_plan_instance(id);


--
-- Name: tf_actor_integration_profile fkd5a41967866df480; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor_integration_profile
    ADD CONSTRAINT fkd5a41967866df480 FOREIGN KEY (integration_profile_id) REFERENCES tf_integration_profile(id);


--
-- Name: tf_actor_integration_profile fkd5a419679d1084ab; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor_integration_profile
    ADD CONSTRAINT fkd5a419679d1084ab FOREIGN KEY (actor_id) REFERENCES tf_actor(id);


--
-- Name: cmn_transaction_instance fkd74918f11b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_transaction_instance
    ADD CONSTRAINT fkd74918f11b781a49 FOREIGN KEY (domain_id) REFERENCES tf_domain(id);


--
-- Name: cmn_transaction_instance fkd74918f11bc07e58; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_transaction_instance
    ADD CONSTRAINT fkd74918f11bc07e58 FOREIGN KEY (response_id) REFERENCES cmn_message_instance(id);


--
-- Name: cmn_transaction_instance fkd74918f172ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_transaction_instance
    ADD CONSTRAINT fkd74918f172ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES tf_actor(id);


--
-- Name: cmn_transaction_instance fkd74918f1afd7554a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_transaction_instance
    ADD CONSTRAINT fkd74918f1afd7554a FOREIGN KEY (request_id) REFERENCES cmn_message_instance(id);


--
-- Name: cmn_transaction_instance fkd74918f1bd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_transaction_instance
    ADD CONSTRAINT fkd74918f1bd491f4b FOREIGN KEY (transaction_id) REFERENCES tf_transaction(id);


--
-- Name: attachment_file fkd7dc93184efc347c; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY attachment_file
    ADD CONSTRAINT fkd7dc93184efc347c FOREIGN KEY (metadata_message_id) REFERENCES metadata_message(id);


--
-- Name: adhocquery_metadata_describer fkd92b69074059c9f3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY adhocquery_metadata_describer
    ADD CONSTRAINT fkd92b69074059c9f3 FOREIGN KEY (adhoc_query_id) REFERENCES registry_object_metadata(id);


--
-- Name: sys_conf_type_usages fkd97ede2e5e9a5b69; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY sys_conf_type_usages
    ADD CONSTRAINT fkd97ede2e5e9a5b69 FOREIGN KEY (system_configuration_id) REFERENCES system_configuration(id);


--
-- Name: sys_conf_type_usages fkd97ede2edd54bc19; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY sys_conf_type_usages
    ADD CONSTRAINT fkd97ede2edd54bc19 FOREIGN KEY (listusages_id) REFERENCES usage_metadata(id);


--
-- Name: cfg_hl7_responder_configuration fkdc2545923927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_responder_configuration
    ADD CONSTRAINT fkdc2545923927e7e FOREIGN KEY (gs_system_id) REFERENCES gs_system(id);


--
-- Name: cfg_hl7_responder_configuration fkdc254592511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_responder_configuration
    ADD CONSTRAINT fkdc254592511b8deb FOREIGN KEY (configuration_id) REFERENCES cfg_configuration(id);


--
-- Name: validator_pack_class_registry_object_metadata fkdf69cd8d5a32764d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY validator_pack_class_registry_object_metadata
    ADD CONSTRAINT fkdf69cd8d5a32764d FOREIGN KEY (validator_pack_class_id) REFERENCES validator_pack_class(id);


--
-- Name: validator_pack_class_registry_object_metadata fkdf69cd8dd562e7cc; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY validator_pack_class_registry_object_metadata
    ADD CONSTRAINT fkdf69cd8dd562e7cc FOREIGN KEY (listregistryobjectmetadata_id) REFERENCES registry_object_metadata(id);


--
-- Name: gs_contextual_information_instance fke02d7f235a577540; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_contextual_information_instance
    ADD CONSTRAINT fke02d7f235a577540 FOREIGN KEY (contextual_information_id) REFERENCES gs_contextual_information(id);


--
-- Name: gs_contextual_information_instance fke02d7f2383812bd3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_contextual_information_instance
    ADD CONSTRAINT fke02d7f2383812bd3 FOREIGN KEY (test_steps_instance_id) REFERENCES gs_test_steps_instance(id);


--
-- Name: affinity_domain_transactions fke0a98f5972a3043a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY affinity_domain_transactions
    ADD CONSTRAINT fke0a98f5972a3043a FOREIGN KEY (affinity_domain_id) REFERENCES affinity_domain(id);


--
-- Name: affinity_domain_transactions fke0a98f59bd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY affinity_domain_transactions
    ADD CONSTRAINT fke0a98f59bd491f4b FOREIGN KEY (transaction_id) REFERENCES tf_transaction(id);


--
-- Name: gs_test_steps_instance fke1c7ae27146029f1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae27146029f1 FOREIGN KEY (testinstance_id) REFERENCES gs_test_instance(id);


--
-- Name: gs_test_steps_instance fke1c7ae2724c9d386; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae2724c9d386 FOREIGN KEY (hl7v3_responder_config_id) REFERENCES cfg_hl7_v3_responder_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae273202260; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae273202260 FOREIGN KEY (syslog_config_id) REFERENCES cfg_syslog_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae2739f74269; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae2739f74269 FOREIGN KEY (web_service_config_id) REFERENCES cfg_web_service_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae27462961a4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae27462961a4 FOREIGN KEY (hl7v2_initiator_config_id) REFERENCES cfg_hl7_initiator_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae2753d7b3a7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae2753d7b3a7 FOREIGN KEY (dicom_scp_config_id) REFERENCES cfg_dicom_scp_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae279cf3f066; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae279cf3f066 FOREIGN KEY (hl7v3_initiator_config_id) REFERENCES cfg_hl7_v3_initiator_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae27a5a42187; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae27a5a42187 FOREIGN KEY (dicom_scu_config_id) REFERENCES cfg_dicom_scu_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae27cdff44c4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae27cdff44c4 FOREIGN KEY (hl7v2_responder_config_id) REFERENCES cfg_hl7_responder_configuration(id);


--
-- Name: tp_test_step_rule fkeb40f14526a5b700; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_step_rule
    ADD CONSTRAINT fkeb40f14526a5b700 FOREIGN KEY (rule_id) REFERENCES tp_rule_type(id);


--
-- Name: tp_test_step_rule fkeb40f14572efdbe8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tp_test_step_rule
    ADD CONSTRAINT fkeb40f14572efdbe8 FOREIGN KEY (tp_test_step_id) REFERENCES tp_test_step(id);


--
-- Name: extrinsic_object_metadata_describer fkf6f0466f9ca1627b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY extrinsic_object_metadata_describer
    ADD CONSTRAINT fkf6f0466f9ca1627b FOREIGN KEY (extrinsic_object_id) REFERENCES registry_object_metadata(id);


--
-- Name: message_type_required_parameters fkf783d73d47adb392; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY message_type_required_parameters
    ADD CONSTRAINT fkf783d73d47adb392 FOREIGN KEY (message_type_id) REFERENCES message_type(id);


--
-- Name: message_type_required_parameters fkf783d73d7ef1b703; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY message_type_required_parameters
    ADD CONSTRAINT fkf783d73d7ef1b703 FOREIGN KEY (parameter_id) REFERENCES parameter(id);


--
-- Name: registry_object_metadata_ext_identifierdescriber fkf88a9b4e808ba89a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_metadata_ext_identifierdescriber
    ADD CONSTRAINT fkf88a9b4e808ba89a FOREIGN KEY (ext_identifierdescriber_id) REFERENCES external_identifier_metadata_describer(id);


--
-- Name: registry_object_metadata_ext_identifierdescriber fkf88a9b4ef85ee775; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_object_metadata_ext_identifierdescriber
    ADD CONSTRAINT fkf88a9b4ef85ee775 FOREIGN KEY (registry_object_metadata_id) REFERENCES registry_object_metadata(id);


--
-- Name: external_identifier_metadata_describer fkf8d8e7d9c14dda7d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY external_identifier_metadata_describer
    ADD CONSTRAINT fkf8d8e7d9c14dda7d FOREIGN KEY (external_identifier_id) REFERENCES registry_object_metadata(id);


--
-- Name: registry_package_metadata_describer fkfd5ea3d291836051; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY registry_package_metadata_describer
    ADD CONSTRAINT fkfd5ea3d291836051 FOREIGN KEY (registry_package_id) REFERENCES registry_object_metadata(id);


--
-- Name: cmn_message_instance fkfeccef683360a4d2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_message_instance
    ADD CONSTRAINT fkfeccef683360a4d2 FOREIGN KEY (issuing_actor) REFERENCES tf_actor(id);


--
-- Name: message_registeres_files fkff0adc815eefd40a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY message_registeres_files
    ADD CONSTRAINT fkff0adc815eefd40a FOREIGN KEY (message_id) REFERENCES xdstar_message(id);


--
-- Name: message_registeres_files fkff0adc81888bdfc0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY message_registeres_files
    ADD CONSTRAINT fkff0adc81888bdfc0 FOREIGN KEY (message_id) REFERENCES xdstar_message(id);


--
-- Name: message_registeres_files fkff0adc81a4178d87; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY message_registeres_files
    ADD CONSTRAINT fkff0adc81a4178d87 FOREIGN KEY (registered_file_id) REFERENCES registered_file(id);


--
-- PostgreSQL database dump complete
--

