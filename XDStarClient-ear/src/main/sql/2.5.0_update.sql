INSERT INTO pack_class (id, packclass) VALUES (nextval('pack_class_id_seq'), 'net.ihe.gazelle.chxdsi.validator.chxdsi.CHXDSIPackValidator');

-- XDM-FR Validator
INSERT INTO pack_class (id, packclass) VALUES (nextval('pack_class_id_seq'), 'net.ihe.gazelle.ihexds.validator.xdmfr.XDMFRPackValidator');
INSERT INTO pack_class (id, packclass) VALUES (nextval('pack_class_id_seq'), 'net.ihe.gazelle.ihexds.validator.rimihefr.RIMIHEFRPackValidator');