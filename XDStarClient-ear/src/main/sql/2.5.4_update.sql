
-- Add MUERRORPackValidator in the pack class list
INSERT INTO pack_class (id, packclass) VALUES (nextval('pack_class_id_seq'), 'net.ihe.gazelle.ihexds.validator.muerror.MUERRORPackValidator');

UPDATE validator_pack_class set validatorname = 'IHE XDS.b ITI-57 Metadata Update – response' WHERE validatorname LIKE '%XDS.b ITI-57 PnR Set-b – response';

-- Remove the PNRERRORPackValidator in the validator Metadata Update response
DELETE from validator_pack_class_pack_class
WHERE validator_pack_class_id = (SELECT id FROM validator_pack_class WHERE validatorname LIKE '%XDS.b ITI-57 Metadata Update – response')
and listpackclass_id = (SELECT id FROM pack_class WHERE packclass LIKE 'net.ihe.gazelle.ihexds.validator.pnrerror.PNRERRORPackValidator');

-- Add the new validator MUERRORPackValidator in the validator Metadata Update response
INSERT INTO validator_pack_class_pack_class (validator_pack_class_id, listpackclass_id) VALUES (
(SELECT id FROM validator_pack_class WHERE validatorname LIKE '%XDS.b ITI-57 Metadata Update – response'),
(SELECT id FROM pack_class WHERE packclass LIKE 'net.ihe.gazelle.ihexds.validator.muerror.MUERRORPackValidator'));


-- Add Metadata Update Constraints
INSERT INTO mbv_package (id, description, name, package_name, namespace) VALUES (nextval('mbv_package_id_seq'), '', 'MUError', 'net.ihe.gazelle.ihexds.validator.MUError', '');
INSERT INTO mbv_standards (package_id, standards) VALUES ((SELECT id FROM mbv_package WHERE name LIKE 'MUError'), 'XDS');
INSERT INTO mbv_class_type (dtype, id, name, parent_identifier, xml_name, path, templateid, constraintadvanced, documentation_spec_id, package_id) VALUES ('ConstraintsSpec', nextval('mbv_class_type_id_seq'), 'MURegistryErrorSpec1', 'rs-RegistryErrorType', 'MURegistryErrorSpec1', NULL, NULL, NULL, NULL,
(SELECT id FROM mbv_package WHERE name LIKE 'MUError'));
INSERT INTO mbv_class_type (dtype, id, name, parent_identifier, xml_name, path, templateid, constraintadvanced, documentation_spec_id, package_id) VALUES ('ConstraintsSpec', nextval('mbv_class_type_id_seq'), 'MURegistryResponseSpec1', 'rs-RegistryResponseType', 'MURegistryResponseSpec1', NULL, NULL, NULL, NULL,
(SELECT id FROM mbv_package WHERE name LIKE 'MUError'));

INSERT INTO mbv_constraint (id, author, datecreation, description, history, lastchange, name, ocl, svsref, type, classtype_id, kind) VALUES (nextval('mbv_constraint_id_seq'), NULL, NULL, 'allowed error codes on Metadata Update transaction are XDSMetadataUpdateError, XDSPatientIDReconciliationError, XDSMetadataUpdateOperationError, XDSMetadataVersionError (IHE_ITI_TF_Rev1-11_Vol3_FT Table 4.2.4.1-2)', NULL, NULL, 'constraintRegistryError_codes_MU', '(not self.errorCode.oclIsUndefined()) and (
(self.errorCode=''XDSMetadataUpdateError'') or
(self.errorCode=''XDSPatientIDReconciliationError'') or
(self.errorCode=''XDSMetadataUpdateOperationError'') or
(self.errorCode=''XDSMetadataVersionError'')
)', NULL, 'ERROR', (SELECT id FROM mbv_class_type WHERE name LIKE 'MURegistryErrorSpec1'), NULL);
INSERT INTO mbv_constraint (id, author, datecreation, description, history, lastchange, name, ocl, svsref, type, classtype_id, kind) VALUES (nextval('mbv_constraint_id_seq'), NULL, NULL, 'accepted status on Registry Response are Failure or Success for Metadata Update transaction (TF_Rev8.0_Vol3_FT Table 4.1-12)', NULL, NULL, 'constraintRegistryResponseStatus_MU', '(not self.status.oclIsUndefined()) and (
self.status=''urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure'' or
self.status=''urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success''
)', NULL, 'ERROR', (SELECT id FROM mbv_class_type WHERE name LIKE 'MURegistryResponseSpec1'), NULL);




