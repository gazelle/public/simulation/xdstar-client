DELETE FROM app_configuration where variable='cas_url';
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), '20', 'NUMBER_OF_ITEMS_PER_PAGE');
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/SchematronValidator-ejb/GazelleObjectValidatorService/GazelleObjectValidator?wsdl', 'schematronValidator');
UPDATE app_configuration SET variable = 'truststore_path' WHERE variable = 'simu_jks_path' ;
INSERT INTO app_configuration VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/gazelle-documentation/XDStar-Client/user.html', 'documentation_url');