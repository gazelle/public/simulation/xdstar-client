
-- Those inserts will failed if the RMU profile was already added in the tool manually --
INSERT INTO affinity_domain (id, keyword, label_to_display, profile) VALUES (nextval('affinity_domain_id_seq'), 'IHE_RMU', 'IHE (RMU)', 'RMU');
INSERT INTO tf_transaction (id, description, keyword, name) VALUES (nextval('tf_transaction_id_seq'), 'Restricted Update Document Set', 'ITI-92', 'RMU');
INSERT INTO usage_metadata (id, action, keyword, affinity_id, transaction_id) VALUES (nextval('usage_metadata_id_seq'), 'Restricted Update Document Set', NULL, (SELECT id FROM affinity_domain WHERE keyword = 'IHE_RMU'), (SELECT id FROM tf_transaction WHERE keyword = 'ITI-92'));
INSERT INTO registry_object_list_metadata (id, displayattributename, name) VALUES (nextval('registry_object_list_metadata_id_seq'), 'RMU', 'RMU');
INSERT INTO transaction_type (id, name) VALUES (nextval('transaction_type_id_seq'), 'RMU');
------------------------------------------------------------------------------------------

INSERT INTO configuration_type (id, conf_type) VALUES (nextval('configuration_type_id_seq'), 'UPDATE_CONF');
INSERT INTO conf_type_usages (configuration_type_id, listusages_id) VALUES ((SELECT id FROM configuration_type WHERE conf_type = 'UPDATE_CONF'),(SELECT um.id FROM usage_metadata as um INNER JOIN tf_transaction as tt ON um.transaction_id = tt.id WHERE tt.keyword = 'ITI-92' ));