UPDATE xdstar_message SET dtype = 'RMU' WHERE dtype = 'XCMU' ;
UPDATE affinity_domain SET keyword = 'IHE_RMU', label_to_display = 'IHE (RMU)', profile ='RMU' WHERE keyword = 'IHE_XCMU' ;
UPDATE tf_transaction SET description = 'Restricted Update Document Set', keyword = 'ITI-X1', name = 'RMU' WHERE name = 'XCMU' ;
UPDATE usage_metadata SET action = 'Restricted Update Document Set' WHERE action = 'Cross Community Metadata Update' ;
UPDATE registry_object_list_metadata SET displayattributename = 'RMU', name = 'RMU' WHERE name = 'XCMU' ;
UPDATE transaction_type SET name = 'RMU' WHERE name = 'XCMU' ;

