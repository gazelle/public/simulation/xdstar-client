INSERT INTO app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'true', 'cas_enabled');
DELETE FROM app_configuration WHERE variable = 'application_works_without_cas';
INSERT INTO pack_class (id, packclass) VALUES (nextval('pack_class_id_seq'), 'net.ihe.gazelle.chxds.validator.chxdsdsdr.CHXDSDSDRPackValidator');
