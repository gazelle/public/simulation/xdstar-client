#!/bin/bash

CURRENT_PATH=`pwd`
sonar_host_url="http://gazelle.ihe.net/sonar"
sonar_jdbc_url="jdbc:postgresql://ovh2.ihe-europe.net/sonar"
sonar_jdbc_driverClassName="org.postgresql.Driver"
sonar_jdbc_username="gazelle"
sonar_jdbc_password="gazelle"

if [ "x$1" = "xlocal" ]; then 
	echo "executed on local sonar"
	CURRENT_PATH=`pwd`
	sonar_host_url="http://localhost:9000"
	sonar_jdbc_url="jdbc:postgresql://localhost/sonar?useUnicode=true&amp;characterEncoding=utf8"
	sonar_jdbc_driverClassName="org.postgresql.Driver"
	sonar_jdbc_username="gazelle"
	sonar_jdbc_password="gazelle"
fi

process_sonar(){
	echo "${VALIDATOR_NAME} - processing"
	if [ -d ${VALIDATOR_PATH} ]; then 
		echo "${VALIDATOR_NAME} - SVN update"
		cd ${VALIDATOR_PATH}
		svn up
		cd ${CURRENT_PATH}
	else 
		echo "${VALIDATOR_NAME} - SVN checkout"
		svn checkout --username anonsvn --password anonsvn ${VALIDATOR_FORGE_PATH} ${VALIDATOR_PATH}
	fi
	cd ${VALIDATOR_PATH}
	echo "${VALIDATOR_NAME} - sonar processing"
	mvn package
	mvn sonar:sonar -Dsonar.host.url=${sonar_host_url} -Dsonar.jdbc.url=${sonar_jdbc_url} -Dsonar.jdbc.driverClassName=${sonar_jdbc_driverClassName} -Dsonar.jdbc.username=${sonar_jdbc_username} -Dsonar.jdbc.password=${sonar_jdbc_password}
	echo "${VALIDATOR_NAME} - end processing"
	echo "=============================================================================================="
	cd ${CURRENT_PATH}
}

VALIDATOR_PATH="$CURRENT_PATH/dsub-validator-jar"
VALIDATOR_NAME="DSUB"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/gazelle/validators/src-gen/dsub/dsub-validator-jar/trunk"
process_sonar;

VALIDATOR_PATH="$CURRENT_PATH/dsub-code-jar"
VALIDATOR_NAME="DSUB CODE"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/gazelle/validators/src-gen/dsub/dsub-code-jar/trunk"
process_sonar;

VALIDATOR_PATH="$CURRENT_PATH/xds-validator-jar"
VALIDATOR_NAME="XDS VALIDATOR"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/gazelle/validators/src-gen/xds-validator-jar/trunk"
process_sonar;

VALIDATOR_PATH="$CURRENT_PATH/xdsi-validator-jar"
VALIDATOR_NAME="XDS-I VALIDATOR"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/gazelle/validators/src-gen/xdsi-validator-jar/trunk"
process_sonar;

VALIDATOR_PATH="$CURRENT_PATH/xds-code-jar"
VALIDATOR_NAME="XDS CODE"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/gazelle/validators/src-gen/xds-code-jar/trunk"
process_sonar;

VALIDATOR_PATH="$CURRENT_PATH/xdsi-code-jar"
VALIDATOR_NAME="XDS-I CODE"
VALIDATOR_FORGE_PATH="https://scm.gforge.inria.fr/svn/gazelle/validators/src-gen/xdsi-code-jar/trunk"
process_sonar;

