@startuml
[XDStarClient] #yellow
[assertion-provider-client]
[Schematron \nValidator] as SCHVal
[Demographic \nData Server] as DDSapp
[SVS \nSimulator] as SVSSimu
[Gazelle \nObject Checker] as GOCVal
interface "SOAP" as DDS
DDS - DDSapp
interface "SOAP" as SCH
SCH - SCHVal
interface "REST" as SVS
SVS - SVSSimu
interface "SOAP" as GOC
GOC - GOCVal
[XDStarClient] ..> DDS : generate patients
[XDStarClient] ..> SVS : get value sets
[XDStarClient] ..> SCH : Validate HL7 messages
[XDStarClient] ..> GOC : Validate FHIR messages
interface "SOAP" as STS
interface "XUA actors" as XUA #green
XUA - [XDStarClient] #green
STS - [assertion-provider-client]
XUA ..> STS #green: Get SAML assertions
@enduml