# XDStar Client

<!-- TOC -->
* [XDStar Client](#xdstar-client)
  * [Build and tests](#build-and-tests)
  * [Installation manual](#installation-manual)
    * [Configuration for sso-client-v7](#configuration-for-sso-client-v7)
  * [Administration Guide](#administration-guide)
  * [User manual](#user-manual)
  * [Release note](#release-note)
<!-- TOC -->

## Build and tests

The project must be build using JDK-8 and run on a JDK-7 environment.

```shell
mvn clean install
```

## Installation manual

The XDStar Client installation manual is available here:
https://gazelle.ihe.net/gazelle-documentation/XDStar-Client/installation.html

### Configuration for sso-client-v7

As XDStar Client depends on sso-client-v7, it needs to be configured properly to work. You can follow the README of sso-client-v7
available here: [sso-client-v7](https://gitlab.inria.fr/gazelle/library/sso-client-v7)

## Administration Guide

The XDStar Client administration guide is available here:
https://gazelle.ihe.net/gazelle-documentation/XDStar-Client/admin.html

## User manual

The XDStar Client user manual is available here:
https://gazelle.ihe.net/gazelle-documentation/XDStar-Client/user.html

## Release note

The XDStar Client release note is available here:
https://gazelle.ihe.net/gazelle-documentation/XDStar-Client/release-note.html