#!/bin/bash

DATABASE=$1
PACKAGE=$2
JENKINS_JOB=$3
EAR_FOLDER=$4

DEPLOY_PATH="/usr/local/jboss/server/gazelle/deploy/$PACKAGE"
BACKUP_PATH="packages_backup"

USER="$SUDO_USER"
EXECUTE_AS_USER="sudo -u $USER -H "

# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root" 1>&2
  exit 1
fi

wget http://gazelle.ihe.net/jenkins/job/"$JENKINS_JOB"/ws/"$EAR_FOLDER"/target/"$PACKAGE" -O /tmp/"$PACKAGE"

CHECK_DATABASE_EXISTS=`$EXECUTE_AS_USER psql -lqt | cut -d \| -f 1 |grep "$DATABASE"' '|wc -l`

echo "Check if $DATABASE database exists"
if [ "$CHECK_DATABASE_EXISTS" -eq 0 ]
then
  echo "Create $DATABASE database"
  $EXECUTE_AS_USER createdb -U gazelle -E UTF-8 -O gazelle "$DATABASE"
else
  echo "$DATABASE database already exists"
fi


if [ -f "$DEPLOY_PATH" ]
then
  if [ ! -d "$BACKUP_PATH" ]
  then
    echo "Creates backup folder: $BACKUP_PATH"
    $EXECUTE_AS_USER mkdir "$BACKUP_PATH"
  fi

  NOW=$(date +"%Y_%m_%d_%Hh%M")
  echo "Backup old package as $PACKAGE.$NOW.pre-install"

  $EXECUTE_AS_USER cp $DEPLOY_PATH $BACKUP_PATH/$PACKAGE.$NOW.pre-install
fi

echo "Deploy $JENKINS_JOB"
cp /tmp/"$PACKAGE" $DEPLOY_PATH

echo "$JENKINS_JOB deployed"

echo "Update files rights"

chgrp -R jboss-admin $DEPLOY_PATH
chown -R jboss $DEPLOY_PATH
chmod -R g+w $DEPLOY_PATH
