package net.ihe.gazelle.xdstar.testplan.action;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.testplan.InputType;
import net.ihe.gazelle.testplan.RuleType;
import net.ihe.gazelle.xdstar.common.model.AbstractMessage;
import net.ihe.gazelle.xdstar.common.model.ContentType;
import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.comon.util.XDSNamespaceContext;
import net.ihe.gazelle.xdstar.core.MessageSenderCommon;
import net.ihe.gazelle.xdstar.testplan.model.*;
import net.ihe.gazelle.xdstar.validator.MetadataValidator;
import net.ihe.gazelle.xdstar.xca.initgw.model.XCAMessage;
import net.ihe.gazelle.xdstar.xcpd.model.XCPDMessage;
import net.ihe.gazelle.xdstar.xdrsrc.model.XDRMessage;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@MessageDriven(name = "testStepInstanceMDB",
        activationConfig = {
                @ActivationConfigProperty(propertyName = "destination", propertyValue = "jms/TestStepInstanceQueue"),
                @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")})
//@DependsOn("jboss.mq.destination:service=Queue,name=TestStepInstanceQueue")
public class TestStepInstanceMDB implements MessageListener {


    private static Logger LOG = LoggerFactory.getLogger(TestStepInstanceMDB.class);

    @PersistenceContext(unitName = "XDStarClient-PersistenceUnit")
    private EntityManager entityManager;

    @Resource(mappedName = "java:/jms/TestPlanInstanceQueue")
    private Queue testPlanInstanceQueue;

    @Resource(mappedName = "java:/JmsXA")
    private ConnectionFactory connectionFactory;

    /**
     * Start all related transaction --> AbstractMessage
     * Save all started AbstractMessage
     */
    @Override
    public void onMessage(Message mess) {
        TestStepInstance tsi = null;
        try {
            if (mess instanceof ObjectMessage) {
                ObjectMessage om = (ObjectMessage) mess;
                if (om.getObject() instanceof TestStepInstance) {
                    tsi = (TestStepInstance) om.getObject();
                    tsi = entityManager.find(TestStepInstance.class, tsi.getId());
                    this.sendMessageToReceiver(tsi);
                    LOG.info("received message type : " + mess.getClass().getSimpleName());
                    tsi.setStatus(StatusType.ENDED);
                    this.validateTestStepInstance(tsi);
                    tsi = entityManager.merge(tsi);
                    entityManager.flush();
                    continueTestPlanInstance(tsi);
                }
            }
        } catch (Exception e) {
            LOG.info("exception on onMessage method.", e);
        }
    }

    private void validateTestStepInstance(TestStepInstance tsi) {
        if ((tsi != null) && (tsi.getParentTestStep() != null) && (tsi.getParentTestStep().getRule().size() > 0)) {
            for (RuleType rule : tsi.getParentTestStep().getRule()) {
                RuleResult rr = new RuleResult();
                rr.setParentRule(rule);
                rr.setTestStepInstance(tsi);
                rr.setResult(ResultType.UNKNOWN);
                tsi.getRuleResult().add(rr);
                if (rule.getXpath() != null && !rule.getXpath().equals("")) {
                    boolean eval = true;
                    for (AbstractMessage am : tsi.getAbstractMessage()) {
                        String soapResp = MetadataValidator.extractSoapEnvelope(am.getReceivedMessageContent().getMessageContent());
                        try {
                            eval = eval && net.ihe.gazelle.xdstar.comon.util.XpathUtils.evaluateByString(soapResp, rule.getXpath(),
                                    XDSNamespaceContext.getInstance());
                        } catch (Exception e) {
                            eval = false;
                            break;
                        }
                    }
                    if (!eval) {
                        rr.setResult(ResultType.FAILED);
                    } else {
                        rr.setResult(ResultType.PASSED);
                    }
                }
            }
            tsi.setResult(ResultType.PASSED);
            for (RuleResult rr : tsi.getRuleResult()) {
                if (rr.getResult().equals(ResultType.FAILED)) {
                    tsi.setResult(ResultType.FAILED);
                    break;
                }
            }
        }
    }

    private void sendMessageToReceiver(TestStepInstance tsi) {
        if (tsi == null) {
            return;
        }
        TransactionTemplate temp = null;
        if (tsi.getParentTestStep().getTemplateName() != null && !tsi.getParentTestStep().getTemplateName().equals("")) {
            temp = (TransactionTemplate) entityManager.createQuery("SELECT tt From transactionTemplate tt WHERE tt.name=:name")
                    .setParameter("name", tsi.getParentTestStep().getTemplateName())
                    .getSingleResult();
        }
        if (temp != null) {
            String header = temp.getHttpHeader();
            String body = temp.getTemplate();
            List<GlobalInputInstance> lg = tsi.getTestPlanInstance().getGlobalInputInstance();
            List<InputType> lip = tsi.getParentTestStep().getInput();
            tsi.setInputs(new ArrayList<InputType>());
            for (InputType inputType : lip) {
                InputType inputTypeStep = new InputType();
                inputTypeStep.setName(inputType.getName());
                inputTypeStep.setValue(inputType.getValue());
                if (inputTypeStep.getValue().matches("__.*__")) {
                    for (GlobalInputInstance ginputIns : lg) {
                        if (ginputIns.getGlobalInputType().getTag().equals(inputType.getValue())) {
                            inputTypeStep.setValue(ginputIns.getValue());
                        }
                    }
                }
                tsi.getInputs().add(inputTypeStep);
                body = body.replace("__" + inputTypeStep.getName() + "__", inputTypeStep.getValue());
            }
            body = body.replace("__configuration_url__", tsi.getSystemConfiguration().getUrl());
            body = replaceUUIDRandom(body);

            AbstractMessage message = null;
            switch (tsi.getParentTestStep().getTransactionType()) {
                case XQ:
                    message = new XCAMessage();
                    break;
                case XCPD:
                    message = new XCPDMessage();
                    break;
                case PNR:
                    message = new XDRMessage();
                    break;
                default:
                    break;
            }
            if (message == null) {
                message = new AbstractMessage();
            }
            message.setMessageType(tsi.getParentTestStep().getMessageType());
            message.setTransaction(Transaction.GetTransactionByKeyword(tsi.getParentTestStep().getTransactionKeyword(), entityManager));
            message.setAffinityDomain(AffinityDomain.getAffinityDomainByKeyword(tsi.getParentTestStep().getAffinityDomainKeyword(), entityManager));
            message.setContentType(ContentType.SOAP_MESSAGE);
            if (body.contains("mtom")) {
                message.setContentType(ContentType.MTOM_XOP);
            }
            message.setTimeStamp(new Date());
            message.setGazelleDriven(false);
            message.setConfiguration(tsi.getSystemConfiguration());

            MetadataMessage sent = new MetadataMessage();
            sent.setMessageContent(body);
            sent.setMessageType(temp.getRequestType());
            message.setSentMessageContent(sent);
            entityManager.persist(message);

            MessageSenderCommon sender = new MessageSenderCommon(tsi.getSystemConfiguration().getUrl(), message);
            try {
                byte[] bit = getBytesFromBody(body);
                String resp = sender.sendMessage(tsi.getSystemConfiguration().getUrl(), bit, message, header, this.entityManager).getObject2();
                MetadataMessage received = new MetadataMessage();
                received.setMessageContent(resp);
                received.setMessageType(temp.getResponseType());

                message.setReceivedMessageContent(received);

            } catch (Exception e) {
                LOG.info("exception on sending message.", e);
            }
            entityManager.merge(message);
            entityManager.flush();
            tsi.getAbstractMessage().add(message);
        }
    }

    private byte[] getBytesFromBody(String body) {
        String[] st = body.split("__MANIFEST_DOCUMENT__");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int i = 1;
        for (String string : st) {
            try {
                baos.write(string.getBytes(StandardCharsets.UTF_8));
                if (i < st.length) {
                    baos.write(getBytesFromManifest(ApplicationConfiguration.getValueOfVariable("ksa_manifest_file", entityManager)));
                }
                i++;
            } catch (IOException e) {
                LOG.error("" + e.getMessage());
                return null;
            }
        }
        return baos.toByteArray();
    }

    private byte[] getBytesFromManifest(String manifestPath) {
        FileInputStream fis = null;
        ByteArrayOutputStream baos = null;
        try {
            fis = new FileInputStream(manifestPath);
            baos = new ByteArrayOutputStream();
            IOUtils.copy(fis, baos);
            return baos.toByteArray();
        } catch (FileNotFoundException e) {
            LOG.error("" + e.getMessage());
        } catch (IOException e) {
            LOG.error("" + e.getMessage());
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    LOG.error("" + e.getMessage());
                }
            }
            if (baos != null) {
                try {
                    baos.close();
                } catch (IOException e) {
                    LOG.error("" + e.getMessage());
                }
            }
        }
        return null;
    }

    private String replaceUUIDRandom(String body) {
        Pattern pat = Pattern.compile("__UUID_RANDOM__");
        Matcher mat = pat.matcher(body);
        while (mat.find()) {
            body = body.replaceFirst("__UUID_RANDOM__", UUID.randomUUID().toString());
        }
        return body;
    }

    private void continueTestPlanInstance(TestStepInstance tsi) {
        Connection connection = null;
        Session session = null;
        MessageProducer producer = null;
        try {
            connection = connectionFactory.createConnection();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            producer = session.createProducer(testPlanInstanceQueue);
            MapMessage message = session.createMapMessage();
            message.setInt("tpi_id", tsi.getTestPlanInstance().getId());
            message.setInt("greaterThan", tsi.getParentTestStep().getSeqid());
            producer.send(message);
        } catch (JMSException e) {
            LOG.error("exception on continueTestPlanInstance method.", e);
        } finally {
            if (producer != null) {
                try {
                    producer.close();
                } catch (JMSException e) {
                    LOG.error("" + e.getMessage());
                }
            }
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException e) {
                    LOG.error("" + e.getMessage());
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException e) {
                    LOG.error("" + e.getMessage());
                }
            }
        }
    }

}
