package net.ihe.gazelle.xdstar.testplan.action;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.testplan.GlobalInputType;
import net.ihe.gazelle.testplan.TestPlanType;
import net.ihe.gazelle.testplan.TestStepType;
import net.ihe.gazelle.testplan.TransactionTypeType;
import net.ihe.gazelle.xdstar.common.model.AbstractMessage;
import net.ihe.gazelle.xdstar.conf.model.RepositoryConfiguration;
import net.ihe.gazelle.xdstar.testplan.model.*;
import net.ihe.gazelle.xdstar.xdrsrc.model.OID;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.context.FacesContext;
import javax.jms.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

@Stateful
@Name("KSAInitManager")
@Scope(ScopeType.SESSION)
@GenerateInterface(value = "KSAInitManagerLocal")
public class KSAInitManager implements KSAInitManagerLocal {

    private static Logger log = LoggerFactory.getLogger(KSAInitManager.class);

    private RepositoryConfiguration selectedRepositoryConfiguration;

    private TestPlanType selectedTestPlanType;

    private TestPlanInstance selectedTestPlanInstance;

    private boolean enabledRefresh = true;

    @PersistenceContext(unitName = "XDStarClient-PersistenceUnit")
    private EntityManager entityManager;

    @Resource(mappedName = "java:/jms/TestPlanInstanceQueue")
    private Queue testPlanInstanceQueue;

    @Resource(mappedName = "java:/JmsXA")
    private ConnectionFactory connectionFactory;

    public TestPlanInstance getSelectedTestPlanInstance() {
        return selectedTestPlanInstance;
    }

    public void setSelectedTestPlanInstance(
            TestPlanInstance selectedTestPlanInstance) {
        this.selectedTestPlanInstance = selectedTestPlanInstance;
    }

    public boolean isEnabledRefresh() {
        return enabledRefresh;
    }

    public void setEnabledRefresh(boolean enabledRefresh) {
        this.enabledRefresh = enabledRefresh;
    }

    public TestPlanType getSelectedTestPlanType() {
        return selectedTestPlanType;
    }

    public void setSelectedTestPlanType(TestPlanType selectedTestPlanType) {
        this.selectedTestPlanType = selectedTestPlanType;
    }

    public RepositoryConfiguration getSelectedRepositoryConfiguration() {
        return selectedRepositoryConfiguration;
    }

    public void setSelectedRepositoryConfiguration(
            RepositoryConfiguration selectedRepositoryConfiguration) {
        this.selectedRepositoryConfiguration = selectedRepositoryConfiguration;
    }

    public void initTestPlanInstance() {
        if (this.selectedTestPlanType == null) {
            return;
        }
        this.selectedTestPlanInstance = new TestPlanInstance();
        this.selectedTestPlanInstance.setParentTestPlan(this.selectedTestPlanType);
        this.selectedTestPlanInstance.setGlobalInputInstance(new ArrayList<GlobalInputInstance>());
        this.selectedTestPlanInstance.setResult(ResultType.UNKNOWN);
        for (GlobalInputType gi : this.selectedTestPlanType.getGlobalInput()) {
            GlobalInputInstance gii = new GlobalInputInstance();
            gii.setGlobalInputType(gi);
            gii.setValue(gi.getDefaultValue());
            this.selectedTestPlanInstance.getGlobalInputInstance().add(gii);
        }
        this.selectedTestPlanInstance.setTestStepInstance(new ArrayList<TestStepInstance>());
        for (TestStepType tst : this.selectedTestPlanType.getTestStep()) {
            TestStepInstance tsi = new TestStepInstance();
            tsi.setTestPlanInstance(selectedTestPlanInstance);
            tsi.setParentTestStep(tst);
            tsi.setStatus(StatusType.CREATED);
            tsi.setResult(ResultType.UNKNOWN);
            tsi.setTestPlanInstance(selectedTestPlanInstance);
            tsi.setAbstractMessage(new ArrayList<AbstractMessage>());
            this.selectedTestPlanInstance.getTestStepInstance().add(tsi);
        }
        this.enabledRefresh = true;
        initTestPlanInstanceForKSA();
    }

    private void initTestPlanInstanceForKSA() {
        for (GlobalInputInstance gii : this.selectedTestPlanInstance.getGlobalInputInstance()) {
            if (gii.getGlobalInputType().getTag().equals("__UUID_REG_PACK__")) {
                gii.setValue(UUID.randomUUID().toString());
            } else if (gii.getGlobalInputType().getTag().equals("__UUID_EXTRINSIC_OBJECT__")) {
                gii.setValue(UUID.randomUUID().toString());
            } else if (gii.getGlobalInputType().getTag().equals("__SUBMISSIONSET_UNIQUEID__")) {
                gii.setValue(OID.getNewSubmissionSetOid());
            } else if (gii.getGlobalInputType().getTag().equals("__DOCUMENTENTRY_UNIQUEID__")) {
                gii.setValue(OID.getNewDocumentOid());
            }
        }
    }

    public void startSelectedTestPlan() {
        if (this.selectedTestPlanInstance.getId() == null) {
            for (TestStepInstance tsi : this.selectedTestPlanInstance.getTestStepInstance()) {
                if (tsi.getParentTestStep().getTransactionType().equals(TransactionTypeType.PNR)) {
                    tsi.setSystemConfiguration(this.selectedRepositoryConfiguration);
                }
            }
            this.saveSelectedTestPlanInstance();
            this.luanchInitializationTestPlanInstance();
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "You have already started a Test Plan.");
        }
    }

    private void saveSelectedTestPlanInstance() {
        if (this.selectedTestPlanInstance == null) {
            return;
        }
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        em.persist(this.selectedTestPlanInstance);
        em.flush();
    }

    private void luanchInitializationTestPlanInstance() {
        Connection connection = null;
        Session session = null;
        MessageProducer producer = null;

        try {
            connection = connectionFactory.createConnection();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            producer = session.createProducer(testPlanInstanceQueue);
            MapMessage message = session.createMapMessage();
            message.setInt("tpi_id", this.selectedTestPlanInstance.getId());
            message.setInt("greaterThan", -1);
            producer.send(message);

        } catch (JMSException e) {
            log.error("KSAInitManager : not able to luanchInitializationTestPlanInstance", e);
        } finally {
            if (producer != null) {
                try {
                    producer.close();
                } catch (JMSException e) {
                    log.error("" + e.getMessage());
                }
            }
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException e) {
                    log.error("" + e.getMessage());
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException e) {
                    log.error("" + e.getMessage());
                }
            }
        }
    }

    @SuppressWarnings("unused")
    public void updateSelectedTestPlanInstance() {
        if (this.selectedTestPlanInstance != null && this.selectedTestPlanInstance.getId() != null) {
            this.selectedTestPlanInstance = entityManager.find(TestPlanInstance.class, this.selectedTestPlanInstance.getId());
            for (GlobalInputInstance gii : this.selectedTestPlanInstance.getGlobalInputInstance()) {
            }
            this.enabledRefresh = false;
            for (TestStepInstance sti : this.selectedTestPlanInstance.getTestStepInstance()) {
                if (sti.getStatus() != StatusType.ENDED) {
                    this.enabledRefresh = true;
                }
                for (AbstractMessage mess : sti.getAbstractMessage()) {
                }
            }
        }
    }

    @Override
    public void getTISequenceDiagram() {
        try {
            DiagramGenerator.getTestInstanceSequenceDiagram(getTestInstance());
        } catch (Exception e) {
            log.error("", e);
        }
    }

    private TestPlanInstance getTestInstance() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String pageId = params.get("id");

        TestPlanInstance currentTestInstance = null;
        if (pageId != null) {
            try {
                int id = Integer.valueOf(pageId).intValue();
                currentTestInstance = entityManager.find(TestPlanInstance.class, id);
            } catch (Exception e) {
                log.error("", e);
            }
        }
        return currentTestInstance;
    }

    public String getSequenceDiagramAsUrl(TestPlanInstance tpi) {
        return ApplicationConfiguration.getValueOfVariable("application_url") + "/testInstanceSequenceDiagram.seam?id=" + tpi.getId();
    }

    public String getPermanentLink(TestPlanInstance tsi) {
        if (tsi != null && tsi.getId() != null) {
            String res = ApplicationConfiguration.getValueOfVariable("application_url");
            res += "/testplan/testPlanInstance.seam?id=" + tsi.getId();
            return res;
        }
        return null;
    }

    // --- destroy ---------------
    @Destroy
    @Remove
    public void destroy() {
        log.info("destroy ncpAnalyzerManager..");
    }


}
