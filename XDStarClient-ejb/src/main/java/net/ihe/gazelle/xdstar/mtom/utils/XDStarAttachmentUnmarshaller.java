package net.ihe.gazelle.xdstar.mtom.utils;

import java.util.Map;

import javax.activation.DataHandler;
import javax.xml.bind.attachment.AttachmentUnmarshaller;

import com.sun.xml.ws.api.message.Attachment;

public class XDStarAttachmentUnmarshaller extends AttachmentUnmarshaller {

	Map<String, Attachment> attachments;

	public XDStarAttachmentUnmarshaller(){}

	public XDStarAttachmentUnmarshaller(Map<String, Attachment> parts){
		attachments = parts;
	}

	public boolean isXOPPackage() {
		return true;
	}

	public byte[] getAttachmentAsByteArray(String cid) {
		Attachment obj = attachments.get(cid.substring(4));
		return obj.asByteArray();
	}
	public DataHandler getAttachmentAsDataHandler(String cid) {
		Attachment obj = attachments.get(cid.substring(4));
		return obj.asDataHandler();
	}
}
