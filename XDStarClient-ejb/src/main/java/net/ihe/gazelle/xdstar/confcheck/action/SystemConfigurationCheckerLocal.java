package net.ihe.gazelle.xdstar.confcheck.action;

import java.util.Collection;
import java.util.Date;

import javax.ejb.Local;
import javax.ejb.Timer;

import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;

@Local
public interface SystemConfigurationCheckerLocal {

	public void checkSelectedConfiguration(SystemConfiguration sysconf);
	
	public void checkAllConfigurations();
	
	public Collection<Timer> getRelatedTimers();
	
	public void createTimer(Date dateBegin, long period);
	
	public String getTimeRemaining(Timer currentTimer);
	
	public String getNextTimeout(Timer currentTimer);
	
	public void cancel(Timer currentTimer);

}