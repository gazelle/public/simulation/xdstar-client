package net.ihe.gazelle.xdstar.util;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import net.ihe.gazelle.metadata.model.RegistryObjectMetadata;


public class IHERegistryObjectMetadataSpecification {

	
	public static void saveRegistryObjectMetadataSpecification(RegistryObjectMetadata rom, OutputStream os) throws JAXBException{
		JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.metadata.model");
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.marshal(rom, os);
	}
	
	public static RegistryObjectMetadata load(InputStream is) throws JAXBException, UnsupportedEncodingException {
		JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.metadata.model");
		Unmarshaller u = jc.createUnmarshaller();
		Reader reader = new InputStreamReader(is, StandardCharsets.UTF_8);
		RegistryObjectMetadata mimi = (RegistryObjectMetadata) u.unmarshal(reader);
		return mimi;
	}
	
}
