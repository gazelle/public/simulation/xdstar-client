package net.ihe.gazelle.xdstar.patient.action;

import net.ihe.gazelle.demographic.ws.Patient;
import net.ihe.gazelle.demographic.ws.Race;
import net.ihe.gazelle.openempi.hb.PersonInfo;
import net.ihe.gazelle.openempi.hb.ReligionType;
import net.ihe.gazelle.openempi.hb.dao.PersonInfoDAO;
import net.ihe.gazelle.openempi.hb.dao.RaceDAO;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Abderrazek Boufahja > INRIA Rennes IHE development Project
 */
@Name("generatorManagerBean")
@Scope(ScopeType.PAGE)
public class GeneratorManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    @SuppressWarnings("unused")
    private static Logger log = LoggerFactory.getLogger(GeneratorManager.class);

    private String countryCode;

    private String firstNameLike;

    private String lastNameLike;

    private Integer numberPatient = 1;

    private List<Patient> list_Patients;

    public List<Patient> getList_Patients() {
        return list_Patients;
    }

    public void setList_Patients(List<Patient> list_Patients) {
        this.list_Patients = list_Patients;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getFirstNameLike() {
        return firstNameLike;
    }

    public void setFirstNameLike(String firstNameLike) {
        this.firstNameLike = firstNameLike;
    }

    public String getLastNameLike() {
        return lastNameLike;
    }

    public void setLastNameLike(String lastNameLike) {
        this.lastNameLike = lastNameLike;
    }

    public Integer getNumberPatient() {
        return numberPatient;
    }

    public void setNumberPatient(Integer numberPatient) {
        this.numberPatient = numberPatient;
    }

    public void generatePatient() {
        String targetEndpoint = ApplicationConfiguration.getValueOfVariable("dds_web_service");
        if (this.countryCode != null) {
            this.list_Patients = null;
            this.list_Patients = DDSClient.generatePatient(countryCode, firstNameLike, lastNameLike, numberPatient, targetEndpoint);
        } else {
            this.list_Patients = null;
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "You have to select a countryCode.");
        }
    }

    public String getPatientFirstName(Patient patient) {
        String res = null;
        if ((patient != null) && (patient.getPerson() != null) &&
                (patient.getPerson().getFirstNameSex() != null)) {
            res = patient.getPerson().getFirstNameSex().getFirstName();
        }
        return res;
    }

    public String getPatientLastName(Patient patient) {
        String res = null;
        if ((patient != null) && (patient.getPerson() != null) &&
                (patient.getPerson().getLastName() != null)) {
            res = patient.getPerson().getLastName().getValue();
        }
        return res;
    }

    public String getPatientMMName(Patient patient) {
        String res = null;
        if ((patient != null) && (patient.getPerson() != null) &&
                (patient.getPerson().getMotherMaidenName() != null)) {
            res = patient.getPerson().getMotherMaidenName().getValue();
        }
        return res;
    }

    public String getPatientBirthDate(Patient patient) {
        String res = "";
        try {
            final String DATE_FORMAT_NOW = "dd/MM/yyyy";
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
            res = sdf.format(patient.getPerson().getDateOfBirth().getTime());
        } catch (Exception e) {
        }
        return res;
    }

    public String getPatientGender(Patient patient) {
        if ((patient != null) &&
                (patient.getPerson() != null) &&
                (patient.getPerson().getFirstNameSex() != null) &&
                (patient.getPerson().getFirstNameSex().getGender() != null)) {
            return patient.getPerson().getFirstNameSex().getGender().getDescription();
        }
        return null;
    }

    public String getPatientRace(Patient patient) {
        if ((patient != null) &&
                (patient.getPerson() != null) &&
                (patient.getPerson().getRace() != null)) {
            return patient.getPerson().getRace().getDescription();
        }
        return null;
    }

    public String getPatientReligion(Patient patient) {
        if ((patient != null) && (patient.getPerson() != null) &&
                (patient.getPerson().getReligion() != null)) {
            return patient.getPerson().getReligion().getDescription();
        }
        return null;
    }

    private String getStreet(Patient patient) {
        String res = "";
        if (patient.getAddress().length > 0) {
            res = res + patient.getAddress()[0].getStreet().getValue();
        }
        return res;
    }

    private String getState(Patient patient) {
        String res = "";
        if (patient.getAddress().length > 0) {
            if (patient.getAddress()[0].getState() != null) {
                res = res + patient.getAddress()[0].getState().getName();
            }
        }
        return res;
    }

    private String getCity(Patient patient) {
        String res = "";
        if (patient.getAddress().length > 0) {
            if (patient.getAddress()[0].getCity() != null) {
                res = res + patient.getAddress()[0].getCity().getValue();
            }
        }
        return res;
    }

    private String getCountry(Patient patient) {
        String res = "";
        if (patient.getAddress().length > 0) {
            res = res + patient.getAddress()[0].getCountry().getIso();
        }
        return res;
    }

    private String getZipCode(Patient patient) {
        String res = "";
        if (patient.getAddress().length > 0) {
            res = res + patient.getAddress()[0].getPostalCode();
        }
        return res;
    }

    public String getPatientAddress(Patient patient) {
        String res = "";
        if ((patient.getAddress() != null) && (patient.getAddress().length > 0)) {
            res = res + this.getCountry(patient) + ", ";
            if (this.getState(patient) != null) {
                if (!this.getState(patient).equals("")) {
                    res = res + this.getState(patient) + ", ";
                }
            }
            res = res + this.getCity(patient) + ", ";
            res = res + this.getZipCode(patient) + ", ";
            res = res + this.getStreet(patient);

        }
        return res;
    }

    public void savePatients() {
        if (list_Patients != null) {
            for (Patient patient : list_Patients) {
                PersonInfo pi = this.toPersonInfo(patient);
                pi = PersonInfoMan.capitalizePersonInfo(pi);
                PersonInfoDAO.savePersonInfo(pi);
            }
        }
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Your patient(s) generated was(were) saved.");
    }

    public PersonInfo toPersonInfo(Patient patient) {
        PersonInfo res = PersonInfoMan.initializeNewPersonInfo();
        res.setAddress1(this.getStreet(patient));
        res.setCity(this.getCity(patient));
        res.setCountry(this.getCountry(patient));
        res.setDob(this.getPatientBirthDateAsDate(patient));
        res.setFname(this.getPatientFirstName(patient));
        res.setLname(this.getPatientLastName(patient));
        res.getDocumentHeader().getPerson().setMaidenLname(this.getPatientMMName(patient));
        res.setGender(this.getPatientGenderAsCharacter(patient));
        res.setStateProv(this.getState(patient));
        res.setZip(this.getZipCode(patient));
        res.setRace(getRaceAsRace(patient));
        res.setReligionType(getReligionAsReligionType(patient));
        res.setPiIdentifier(String.valueOf(PersonInfoDAO.getMaxValuePersonInfo() + 2));
        return res;
    }

    private Date getPatientBirthDateAsDate(Patient patient) {
        if (patient != null) {
            if (patient.getPerson() != null) {
                if (patient.getPerson().getDateOfBirth() != null) {
                    return patient.getPerson().getDateOfBirth().getTime();
                }
            }
        }
        return null;
    }

    public Character getPatientGenderAsCharacter(Patient patient) {
        if ((patient != null) &&
                (patient.getPerson() != null) &&
                (patient.getPerson().getFirstNameSex() != null) &&
                (patient.getPerson().getFirstNameSex().getGender() != null) &&
                (patient.getPerson().getFirstNameSex().getGender().getDescription() != null) &&
                (patient.getPerson().getFirstNameSex().getGender().getDescription().length() > 0)) {
            return patient.getPerson().getFirstNameSex().getGender().getDescription().charAt(0);
        }
        return null;
    }

    private net.ihe.gazelle.openempi.hb.Race getRaceAsRace(Patient patient) {
        net.ihe.gazelle.openempi.hb.Race result = null;
        String raceId = null;
        Race race = patient.getPerson().getRace();
        if (race != null) {
            String code = race.getCode();
            if (code.equals("1002-5")) {
                raceId = "AI";
            }
            if (code.equals("2028-9")) {
                raceId = "BL";
            }
            if (code.equals("2054-5")) {
                raceId = "BL";
            }
            if (code.equals("2076-8")) {
                raceId = "OR";
            }
            if (code.equals("2106-3")) {
                raceId = "WH";
            }
            if (code.equals("2131-1")) {
                raceId = "OR";
            }
        }
        if (raceId != null) {
            result = RaceDAO.getRaceByRaceCD(raceId);
        }
        return result;
    }

    private ReligionType getReligionAsReligionType(Patient patient) {
        ReligionType rt = null;
        if ((patient != null) && (patient.getPerson() != null) && (patient.getPerson().getReligion() != null)) {
            String religionTypeCD = patient.getPerson().getReligion().getCode();
            rt = net.ihe.gazelle.openempi.hb.dao.ReligionTypeDAO.getReligionTypeByReligionTypeCD(religionTypeCD);
        }
        return rt;
    }

    public List<String> getListCountryCode() {
        String targetEndpoint = ApplicationConfiguration.getValueOfVariable("dds_web_service");
        return DDSClient.getListCountryCode(targetEndpoint);
    }


}
