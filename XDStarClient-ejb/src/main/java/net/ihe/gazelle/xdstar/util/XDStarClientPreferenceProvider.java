package net.ihe.gazelle.xdstar.util;

import java.util.Date;

import net.ihe.gazelle.preferences.PreferenceProvider;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;

import org.apache.commons.lang.StringUtils;
import org.kohsuke.MetaInfServices;

@MetaInfServices(PreferenceProvider.class)
public class XDStarClientPreferenceProvider implements PreferenceProvider {

	@Override
	public String getString(String key) {
		if (key.equals("gazelle_home_path")){
			return ApplicationConfiguration.getValueOfVariable("server_local_path");
		}
		return ApplicationConfiguration.getValueOfVariable(key);
	}

	@Override
	public Integer getInteger(String key) {
		String stringValue = ApplicationConfiguration.getValueOfVariable(key);
		if (StringUtils.isNumeric(stringValue)) {
			return new Integer(stringValue);
		} else {
			return null;
		}
	}

	@Override
	public Boolean getBoolean(String key) {
		String stringValue = ApplicationConfiguration.getValueOfVariable(key);
		try{
			return Boolean.valueOf(stringValue);
		}
		catch(Exception e){
			return false;
		}
	}

	@Override
	public Date getDate(String key) {
		return null;
	}

	@Override
	public Object getObject(Object key) {
		return null;
	}

	@Override
	public void setObject(Object key, Object value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setString(String key, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setInteger(String key, Integer value) {

	}

	@Override
	public void setBoolean(String key, Boolean value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setDate(String key, Date value) {
		// TODO Auto-generated method stub

	}

	@Override
	public Integer getWeight() {
		return 1;
	}

	@Override
	public int compareTo(PreferenceProvider o) {
		return 1;
		//return getWeight().compareTo(o.getWeight());
	}

}
