package net.ihe.gazelle.xdstar.del.ws;

import net.ihe.gazelle.lcm.RemoveObjectsRequestType;
import net.ihe.gazelle.rs.RegistryResponseType;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.xdsar.dsub.ws.DSUBNotifyMessage;
import net.ihe.gazelle.xdstar.common.model.ContentType;
import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.namespace.QName;
import javax.xml.ws.Action;
import javax.xml.ws.BindingType;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.soap.Addressing;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import static org.apache.commons.lang.CharEncoding.UTF_8;

/**
 * @author aboufahj
 */
@Stateless
@Name("XDSDeletetWS")
@WebService(name = "XDSDeletetWS",
        serviceName = "XDSDeletetWSService",
        portName = "XDSDeletetWSSPort", targetNamespace = "urn:ihe:iti:xds-b:2010")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT,
        parameterStyle = SOAPBinding.ParameterStyle.BARE)
@BindingType(javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
@Addressing(enabled = true)
public class XDSDeletetWS implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(XDSDeletetWS.class);

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Resource
    WebServiceContext wsContext;

    @WebResult(name = "RegistryResponse",
            targetNamespace = "urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0",
            partName = "body")
    @Action(input = "urn:ihe:iti:2010:DeleteDocumentSet", output = "urn:ihe:iti:2010:DeleteDocumentSetResponse")
    @WebMethod(operationName = "DocumentRegistry_DeleteDocumentSet")
    public RegistryResponseType documentRegistryDeleteDocumentSet(
            @WebParam(partName = "body", name = "RemoveObjectsRequest", targetNamespace = "urn:oasis:names:tc:ebxml-regrep:xsd:lcm:3.0")
                    RemoveObjectsRequestType body
    ) {
        RegistryResponseType resp = new RegistryResponseType();
        resp.setStatus("urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success");

        XDSDeleteMessage ds = new XDSDeleteMessage();
        ds.setReceivedMessageContent(new MetadataMessage());
        ds.getReceivedMessageContent().setHttpHeader("Content-Type: application/soap+xml");
        ds.getReceivedMessageContent().setMessageType(MetadataMessageType.REMOVE_OBJECT_REQUEST);
        ds.getReceivedMessageContent().setMessageContent(getRemoveObjectsRequestTypeAsString(body));

        ds.setSentMessageContent(new MetadataMessage());
        ds.getSentMessageContent().setMessageType(MetadataMessageType.REGIQTRY_RESPONSE);
        ds.getSentMessageContent().setMessageContent(getRegistryResponseTypeAsString(resp));

        updateXDSDeleteMessage(ds);
        DSUBNotifyMessage.storeMessage(ds);

        treatDeleteDocumentSet(body);

        return resp;
    }

    private void treatDeleteDocumentSet(RemoveObjectsRequestType body) {
        // TODO delete the real document
    }

    private String getRegistryResponseTypeAsString(RegistryResponseType nt) {
        if (nt != null) {
            try {
                JAXBContext jj = JAXBContext.newInstance(RegistryResponseType.class);
                Marshaller m = jj.createMarshaller();
                m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                m.marshal(nt, baos);
                return baos.toString(UTF_8);
            } catch (PropertyException e) {
                LOG.error("" + e.getMessage());
            } catch (UnsupportedEncodingException e) {
                LOG.error("" + e.getMessage());
            } catch (JAXBException e) {
                LOG.error("" + e.getMessage());
            }
        }
        return null;
    }

    private String getRemoveObjectsRequestTypeAsString(RemoveObjectsRequestType nt) {
        if (nt != null) {
            try {
                JAXBContext jj = JAXBContext.newInstance(RemoveObjectsRequestType.class);
                Marshaller m = jj.createMarshaller();
                m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                m.marshal(nt, baos);
                return baos.toString(UTF_8);
            } catch (PropertyException e) {
                LOG.error("" + e.getMessage());
            } catch (UnsupportedEncodingException e) {
                LOG.error("" + e.getMessage());
            } catch (JAXBException e) {
                LOG.error("" + e.getMessage());
            }
        }
        return null;
    }

    private void updateXDSDeleteMessage(XDSDeleteMessage ds) {
        ds.setAffinityDomain(AffinityDomain.getAffinityDomainByKeyword("IHE_XDS-b"));
        ds.setContentType(ContentType.SOAP_MESSAGE);
        ds.setIp(getIpAddress());
        ds.setTimeStamp(new Date());
        ds.setTransaction(Transaction.GetTransactionByKeyword("ITI-62"));
        ds.setMessageType("DeleteDocumentSet");
        ds.setWsdlInterface(((QName) wsContext.getMessageContext().get(MessageContext.WSDL_INTERFACE)).toString());
        ds.setWsdlOperation(((QName) wsContext.getMessageContext().get(MessageContext.WSDL_OPERATION)).toString());
        ds.setWsdlService(((QName) wsContext.getMessageContext().get(MessageContext.WSDL_SERVICE)).toString());
        ds.setWsdlEndpoint(((HttpServletRequest) wsContext.getMessageContext().get(MessageContext.SERVLET_REQUEST)).getRequestURL().toString());
    }

    private String getIpAddress() {
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc
                .get(MessageContext.SERVLET_REQUEST);
        return req.getRemoteAddr();
    }
}


