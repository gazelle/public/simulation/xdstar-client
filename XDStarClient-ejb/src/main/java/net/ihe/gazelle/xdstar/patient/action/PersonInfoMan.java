package net.ihe.gazelle.xdstar.patient.action;

import java.math.BigDecimal;
import java.util.Date;

import net.ihe.gazelle.openempi.hb.DocumentHeader;
import net.ihe.gazelle.openempi.hb.Person;
import net.ihe.gazelle.openempi.hb.PersonInfo;

/**
 * 
 * @author abderrazek boufahja
 *
 */
public final class PersonInfoMan {
    
    private PersonInfoMan(){}
    
    public static PersonInfo initializeNewPersonInfo(){
        PersonInfo selectedPersonInfo = new PersonInfo();
        selectedPersonInfo.setAaNamespaceId("IHELOCAL");
        selectedPersonInfo.setAaUnivId("1.3.6.1.4.1.21367.2009.1.2.310");
        selectedPersonInfo.setAaUnivIdTypeCd("ISO");
        selectedPersonInfo.setAddress1(null);
        selectedPersonInfo.setAddress2(null);
        selectedPersonInfo.setAddressType(null);
        selectedPersonInfo.setAfNamespaceId("IHELOCAL");
        selectedPersonInfo.setAfUnivId("1.3.6.1.4.1.21367.2009.1.2.310");
        selectedPersonInfo.setAfUnivIdTypeCd("ISO");
        selectedPersonInfo.setAlias(BigDecimal.ZERO);
        selectedPersonInfo.setCity(null);
        selectedPersonInfo.setCorporateId(null);
        selectedPersonInfo.setCountry(null);
        selectedPersonInfo.setDegree(null);
        selectedPersonInfo.setDob(null);
        selectedPersonInfo.setDrvLicenseIssueDate(null);
        selectedPersonInfo.setDrvLicenseIssuingState(null);
        selectedPersonInfo.setDrvLicenseNum(null);
        selectedPersonInfo.setEmail(null);
        selectedPersonInfo.setEndDate(null);
        selectedPersonInfo.setEthnicGroup(null);
        selectedPersonInfo.setFname(null);
        selectedPersonInfo.setGender(null);
        selectedPersonInfo.setLname(null);
        selectedPersonInfo.setMaritalStatus(null);
        selectedPersonInfo.setNameTypeRepCd(null);
        selectedPersonInfo.setPaEndDt(null);
        selectedPersonInfo.setParishCd(null);
        selectedPersonInfo.setPaStartDt(null);
        selectedPersonInfo.setPatientConsent(BigDecimal.ONE);
        selectedPersonInfo.setPersonNameType(null);
        selectedPersonInfo.setPhoneAreaCd(null);
        selectedPersonInfo.setPhoneCountryCd(null);
        selectedPersonInfo.setPhoneExt(null);
        selectedPersonInfo.setPhoneNum(null);
        selectedPersonInfo.setPiEffectiveDate(null);
        selectedPersonInfo.setPiExpirationDate(null);
        selectedPersonInfo.setPiIdentifier(null);
        selectedPersonInfo.setPiIdentifierTypeCd(null);
        selectedPersonInfo.setPrefix(null);
        selectedPersonInfo.setRace(null);
        selectedPersonInfo.setSearchKey(null);
        selectedPersonInfo.setSsn(null);
        selectedPersonInfo.setStartDate(null);
        selectedPersonInfo.setStateProv(null);
        selectedPersonInfo.setTelecomUseCd(null);
        selectedPersonInfo.setUpdatedCorporateId(null);
        selectedPersonInfo.setZip(null);
        DocumentHeader dh = initializeDocumentHeader();
        selectedPersonInfo.setDocumentHeader(dh);
        return selectedPersonInfo;
    }
    
    private static DocumentHeader initializeDocumentHeader(){
        DocumentHeader dh = new DocumentHeader();
        dh.setMsgDate(new Date());
        dh.setRecApp("PAT_IDENTITY_X_REF_MGR_MISYS");
        dh.setRecFac("ALLSCRIPTS");
        dh.setMsgType("ADT^A04");
        Person pp = initializePerson();
        dh.setPerson(pp);
        return dh;
    }
    
    private static Person initializePerson(){
        Person pp = new Person();
        pp.setExpired(BigDecimal.ZERO);
        pp.setIsprovider(BigDecimal.ZERO);
        return pp;
    }
    
    public static PersonInfo capitalizePersonInfo(PersonInfo pi){
        if (pi != null){
            if (pi.getFname() != null){
                pi.setFname(PersonInfoMan.capitalizeString(pi.getFname()));
            }
            if (pi.getLname() != null){
                pi.setLname(PersonInfoMan.capitalizeString(pi.getLname()));
            }
        }
        return pi;
    }
    
    private static String capitalizeString(String input){
        if (input == null) {
        	return null;
        }
        return input.toUpperCase();
    }


}
