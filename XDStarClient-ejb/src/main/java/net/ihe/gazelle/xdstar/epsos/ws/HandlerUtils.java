package net.ihe.gazelle.xdstar.epsos.ws;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.Element;
import javax.xml.namespace.QName;
import javax.xml.ws.handler.MessageContext;
import java.util.List;
import java.util.Map;

public class HandlerUtils {
    private static final Logger LOG = LoggerFactory.getLogger(HandlerUtils.class);

    public static void printMessageContext(String whereFrom, Map<String, Object> propertyMap) {
        LOG.info("*** MessageContext from " + whereFrom + ":");
        printMessageContext(propertyMap);
    }

    @SuppressWarnings("unchecked")
    public static void printMessageContext(Map<String, Object> propertyMap) {
        outputObject("Message Outbound Property", (Boolean) propertyMap
                .get(MessageContext.MESSAGE_OUTBOUND_PROPERTY));
        // outputMap("HTTP Request Headers", (Map<String,List<String>>)
        //    propertyMap.get(MessageContext.HTTP_REQUEST_HEADERS));
        outputObject("HTTP Request Method", (String) propertyMap
                .get(MessageContext.HTTP_REQUEST_METHOD));
        outputObject("Path Info", (String) propertyMap.get(MessageContext.PATH_INFO));
        outputObject("Query String", (String) propertyMap.get(MessageContext.QUERY_STRING));
        // outputMap("HTTP Response Headers", (Map<String,List<String>>)
        // propertyMap.get(MessageContext.HTTP_RESPONSE_HEADERS));
        outputObject("HTTP Response Code", (Integer) propertyMap
                .get(MessageContext.HTTP_RESPONSE_CODE));
        outputArrayList("Reference Parameters", (List<Element>) propertyMap.get(MessageContext.REFERENCE_PARAMETERS));
        outputObject("WSDL Description", ((org.xml.sax.InputSource) propertyMap.get(MessageContext.WSDL_DESCRIPTION)));
        outputObject("WSDL Interface", (QName) propertyMap.get(MessageContext.WSDL_INTERFACE));
        outputObject("WSDL Operation", (QName) propertyMap.get(MessageContext.WSDL_OPERATION));
        outputObject("WSDL Port", (QName) propertyMap.get(MessageContext.WSDL_PORT));
        outputObject("WSDL Service", (QName) propertyMap.get(MessageContext.WSDL_SERVICE));
        outputObject("HTTP_REQUEST_HEADERS", propertyMap.get(MessageContext.HTTP_REQUEST_HEADERS));
        outputObject("HTTP_RESPONSE_HEADERS", propertyMap.get(MessageContext.HTTP_RESPONSE_HEADERS));
        outputObject("Servlet Context",
                propertyMap.get(MessageContext.SERVLET_CONTEXT));
        outputObject("Servlet Request",
                propertyMap.get(MessageContext.SERVLET_REQUEST));
        outputObject("Servlet Response",
                propertyMap.get(MessageContext.SERVLET_RESPONSE));

        HttpServletRequest test = (HttpServletRequest) propertyMap.get(MessageContext.SERVLET_REQUEST);
        StringBuilder msg = new StringBuilder();
        msg.append("---URI = ").append(test.getRequestURI());
        msg.append("---URI = ").append(test.getRequestURL());
        msg.append("---URI = ").append(test.getRequestedSessionId());
        msg.append("---URI = ").append(test.getLocalAddr());
        msg.append("---URI = ").append(test.getContextPath());
        msg.append("---URI = ").append(test.getLocalName());
        msg.append("---URI = ").append(test.getLocalPort());
        msg.append("---URI = ").append(test.getPathInfo());
        msg.append("---URI = ").append(test.getProtocol());
        msg.append("---URI = ").append(test.getQueryString());
        msg.append("---URI = ").append(test.getPathTranslated());
        msg.append("---URI = ").append(test.getRemoteHost());
        msg.append("---URI = ").append(test.getRemoteUser());
        msg.append("---URI = ").append(test.getScheme());
        msg.append("---URI = ").append(test.getServerName());
        msg.append("---URI = ").append(test.getServletPath());
        msg.append("---URI = ").append(test.getParameterMap());

        msg.append("#########################################");
        LOG.info(msg.toString());
    }

    private static void outputObject(String key, Object value) {
        LOG.info(key + " = " + ((value == null) ? "null" : value.toString()));

    }

    private static void outputArrayList(String key, List<Element> list) {
        LOG.info(key + ":" + ((list == null) ? "(null)" : ""));
        if (list != null) {
            for (Element e : list) {
                LOG.info("   " + e.toString());
            }
        }
    }
}