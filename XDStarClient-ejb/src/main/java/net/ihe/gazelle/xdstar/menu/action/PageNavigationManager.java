package net.ihe.gazelle.xdstar.menu.action;

import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.xdstar.menu.Page;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Name("pageNavigationManager")
@Scope(ScopeType.PAGE)
public class PageNavigationManager implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static Logger LOG = LoggerFactory
            .getLogger(PageNavigationManager.class);

    private static List<Page> listMenus = new ArrayList<Page>();

    static {
        try {
            JAXBContext jb = JAXBContext.newInstance(Page.class);
            Unmarshaller um = jb.createUnmarshaller();
            URL is = PageNavigationManager.class.getClassLoader().getResource("menus.xml");
            LOG.info(is.toString());
            Page home = (Page) um.unmarshal(is);
            listMenus.add(home);
            walkPagesAndUpdate(home);
        } catch (Exception e) {
            LOG.error("not able to upload menus.xml!" + e.getMessage());
        }

    }

    private Page selectedMenu;

    public Page getSelectedMenu() {
        return selectedMenu;
    }

    public void setSelectedMenu(Page selectedMenu) {
        this.selectedMenu = selectedMenu;
    }

    @PostConstruct
    public void initialize() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params != null && !params.isEmpty()) {
            String menuIdString = params.get("ID");
            if (menuIdString != null && !menuIdString.equals("")) {
                this.selectedMenu = null;
                this.updateSelectedMenu(menuIdString);

            }
        }
    }

    private static void walkPagesAndUpdate(Page page) {
        for (Page pagechild : page.getPage()) {
            listMenus.add(pagechild);
            walkPagesAndUpdate(pagechild);
        }
    }

    public void updateSelectedMenu(String ID) {
        for (Page pag : listMenus) {
            if (pag.getID().equals(ID)) {
                this.selectedMenu = pag;
                return;
            }
        }
        return;
    }

    public GazelleTreeNodeImpl<Object> getTreePage() {
        GazelleTreeNodeImpl<Object> rootNode = new GazelleTreeNodeImpl<Object>();
        populateTeeNode(rootNode, selectedMenu, 0);
        return rootNode;
    }

    private void populateTeeNode(GazelleTreeNodeImpl<Object> rootNode, Page page, int i) {
        GazelleTreeNodeImpl<Object> menuTreeNode = new GazelleTreeNodeImpl<Object>();
        menuTreeNode.setData(page);
        rootNode.addChild(i, menuTreeNode);
        i = 0;
        for (Page pag : page.getPage()) {
            populateTeeNode(menuTreeNode, pag, i);
            i++;
        }
    }

    public boolean getAdviseNodeOpened() {
        return true;
    }

    public static void main(String[] args) {
        LOG.info("eeeeeeee________" + listMenus.size());
        PageNavigationManager p = new PageNavigationManager();
        p.updateSelectedMenu("DISPINIT");
        LOG.info(p.selectedMenu.toString());
    }

}