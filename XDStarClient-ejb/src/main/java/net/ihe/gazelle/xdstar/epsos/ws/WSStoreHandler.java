package net.ihe.gazelle.xdstar.epsos.ws;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.ws.handler.LogicalMessageContext;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.xdstar.common.model.ContentType;
import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;
import net.ihe.gazelle.xdstar.util.DOM2String;

import org.jboss.ws.api.handler.GenericSOAPHandler;
//import org.jboss.ws.extensions.addressing.AddressingConstantsImpl;
//import org.jboss.wsf.common.handler.GenericSOAPHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WSStoreHandler extends GenericSOAPHandler<LogicalMessageContext>
{
    private Logger LOG = LoggerFactory.getLogger(WSStoreHandler.class);
    private static Set<QName> HEADERS = new HashSet<QName>();
    private String action;

    //TODO fix me
//    static {
//        HEADERS.add( new AddressingConstantsImpl().getActionQName());
//        HEADERS.add( new AddressingConstantsImpl().getReplyToQName());
//    }

    @Override
    public Set getHeaders()
    {
        return Collections.unmodifiableSet(HEADERS);
    }

    protected boolean handleOutbound(MessageContext msgContext) {
        return true;
    }

    protected boolean handleInbound(MessageContext msgContext) {
        try {
            SOAPMessage msg = ((SOAPMessageContext)msgContext).getMessage();
            SOAPHeader header = msg.getSOAPPart().getEnvelope().getHeader();
            SOAPHeaderElement elem = (SOAPHeaderElement)(header.getChildElements(new QName("http://www.w3.org/2005/08/addressing", "Action")).next());
            action = elem.getValue();
            LOG.info("Action is: " + action);
            storeMessage(msgContext, action);
        } catch (Exception e) {
            LOG.error("Error: ",e);
        }
        return true;
    }
    
    public void storeMessage(MessageContext mc, String action) {
		
		SOAPMessageContext smc = (SOAPMessageContext)mc;
		
		EntityManager em = HibernateUtilResponder.buildEntityManager();
		Boolean isoutput =  (Boolean) mc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		if (!isoutput){
			XGQResondingMessage respmsg = new XGQResondingMessage();
			respmsg.setAffinityDomain(AffinityDomain.getAffinityDomainByKeyword("epSOS-2", em));
			respmsg.setTransaction(Transaction.GetTransactionByKeyword("FetchDocumentService", em));
			respmsg.setContentType(ContentType.SOAP_MESSAGE);
			if (action != null && action.contains("trieve")){
				respmsg.setMessageType("FetchDataService:retrieve");
			}
			else{
				respmsg.setMessageType("FetchDataService:query");
			}
			respmsg.setTimeStamp(new Date());
			respmsg.setIp(this.getIpAdressRequest(mc));
			try{
				respmsg.setWsdlInterface(((QName) mc.get(MessageContext.WSDL_INTERFACE)).toString());
				respmsg.setWsdlOperation(((QName) mc.get(MessageContext.WSDL_OPERATION)).toString());
				respmsg.setWsdlService(((QName) mc.get(MessageContext.WSDL_SERVICE)).toString());
				respmsg.setWsdlEndpoint(((HttpServletRequest)mc.get(MessageContext.SERVLET_REQUEST)).getRequestURL().toString());
			}
			catch(Exception e){
				LOG.error("" + e.getMessage());
			}

			respmsg.setReceivedMessageContent(new MetadataMessage());
			respmsg.getReceivedMessageContent().setMessageContent(this.getSOAPMessage(smc));
			if (action != null && action.contains("trieve")){
				respmsg.getReceivedMessageContent().setMessageType(MetadataMessageType.RETRIEVE_DOCUMENT_SET_REQUEST);
			}
			else{
				respmsg.getReceivedMessageContent().setMessageType(MetadataMessageType.ADHOC_QUERY_REQUEST);
			}

			em.merge(respmsg);
			HibernateUtilResponder.releaseEntityManager(em);
		}
	}
    
    private String getIpAdressRequest(MessageContext mc) {
		HttpServletRequest request = (HttpServletRequest)mc.get(SOAPMessageContext.SERVLET_REQUEST);
		return request.getRemoteAddr();
	}
    
    private String getSOAPMessage(SOAPMessageContext mc){
		SOAPMessage sm = mc.getMessage();
		SOAPPart sh = sm.getSOAPPart();
		try {
			return DOM2String.getStringFromDocument(sh.getEnvelope());
		} catch (SOAPException e) {
			LOG.error("" + e.getMessage());
		}
		return null;
	}
}