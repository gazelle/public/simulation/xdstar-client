package net.ihe.gazelle.xdstar.dsub.validator;


public enum DSUBValidatorType {
	
	IHE_SUB_REQ("IHE - DSUB subscribe request"), IHE_SUB_RESP("IHE - DSUB subscribe response"), 
	IHE_UNSUB_REQ("IHE - DSUB unsubscribe request"), IHE_UNSUB_RESP("IHE - DSUB unsubscribe response"),
	IHE_NOTIF_REQ("IHE - DSUB notify request"), 
	IHE_PUB_REQ("IHE - DSUB publish request"),
	KSA_NOTIF_REQ("KSA - DSUB notify request");
	
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	private DSUBValidatorType(String value) {
		this.value = value;
	}
	
	public static DSUBValidatorType getValidatorFromValue(String value){
		for (DSUBValidatorType val : DSUBValidatorType.values()) {
			if (val.value.equals(value)){
				return val;
			}
		}
		return null;
	}

}
