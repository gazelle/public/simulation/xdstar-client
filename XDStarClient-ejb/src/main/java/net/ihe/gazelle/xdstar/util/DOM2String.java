
package net.ihe.gazelle.xdstar.util;

// stuff for domtostring
import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.jdom.input.DOMBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

public class DOM2String
{
    public static String getStringFromDocument(org.w3c.dom.Element el) {
        try {
            DOMSource domSource = new DOMSource(el);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(domSource, result);
            return writer.toString();
            
        } catch (TransformerException e) {
            //      log.debug( e.printStackTrace(). );
            return null;
        }
    }
    
    //method to convert Document to String
    public static String getStringFromDocument(org.w3c.dom.Document doc) {
	try {
	    DOMSource domSource = new DOMSource(doc);
	    StringWriter writer = new StringWriter();
	    StreamResult result = new StreamResult(writer);
	    TransformerFactory tf = TransformerFactory.newInstance();
	    Transformer transformer = tf.newTransformer();
	    transformer.transform(domSource, result);
	    return writer.toString();

	} catch (TransformerException e) {
	    //	    log.debug( e.printStackTrace(). );
	    return null;
	}
    }
    
    
    public static String getFormattedStringFromDocument(org.w3c.dom.Document doc) {
	DOMBuilder b = new DOMBuilder();
	// jdom
	org.jdom.Document d = b.build( doc );
	// just beautify the statistics element
	XMLOutputter out = new XMLOutputter( Format.getPrettyFormat() );
	return out.outputString( d );
    
    }


}
