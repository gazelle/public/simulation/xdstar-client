package net.ihe.gazelle.xdstar.validator.ws;

import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.exception.GazelleValidationException;
import net.ihe.gazelle.validation.model.ValidatorDescription;
import net.ihe.gazelle.validation.ws.AbstractModelBasedValidation;
import net.ihe.gazelle.xdstar.wado.validator.WADOValidator;
import net.ihe.gazelle.xdstar.wado.validator.WADOValidatorMethod;
import net.ihe.gazelle.xdstar.wado.validator.WADOValidatorType;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang.CharEncoding.UTF_8;


@Stateless
@Name("WADOModelBasedWS")
@WebService(name = "WADOModelBasedWS",
        serviceName = "WADOModelBasedWSService",
        portName = "WADOModelBasedWSPort",
        targetNamespace = "http://ws.mb.validator.gazelle.ihe.net")
public class WADOValidatorWS extends AbstractModelBasedValidation implements Serializable {


    private static Logger log = LoggerFactory.getLogger(WADOValidatorWS.class);

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    @WebMethod
    @WebResult(name = "DetailedResult")
    public String validateDocument(
            @WebParam(name = "document") String document,
            @WebParam(name = "validator") String validator)
            throws SOAPException {
        if (validator == null || validator.isEmpty()) {
            throw new SOAPException("A validator must be provided");
        }
        if (document == null || document.isEmpty()) {
            throw new SOAPException("A document must be provided");
        }
        WADOValidatorMethod wval = new WADOValidator();
        DetailedResult resD = wval.validateMessageByValidator(document, validator);
        String res = getDetailedResultAsString(resD);
        if (null == res) {
            throw new SOAPException("The Validator is not valid");
        }
        String status = res.contains("<ValidationTestResult>FAILED</ValidationTestResult>") ? "FAILED" : "PASSED";

        if (WADOValidatorType.getValidatorFromName(validator) != null) {
            this.addValidatorUsage(validator, status);
        }
        return res;
    }

    @Override
    protected String buildReportOnParsingFailure(GazelleValidationException e, ValidatorDescription validatorDescription) {
        return null;
    }

    @Override
    protected String executeValidation(String s, ValidatorDescription validatorDescription, boolean b) throws GazelleValidationException {
        return null;
    }

    @Override
    @WebMethod
    @WebResult(name = "about")
    public String about() {
        String res = "This webservice is developped by IHE-europe / gazelle team. The aim of this validator is to validate WADO messages soap using" +
                " model based validation.\n";
        res = res + "For more information please contact the manager of gazelle project eric.poiseau@inria.fr";
        return res;
    }

    @Override
    @WebMethod
    @WebResult(name = "Validators")
    public List<String> getListOfValidators(@WebParam(name = "descriminator") String descriminator)
            throws SOAPException {
        if (null == descriminator) {
            descriminator = "";
        }
        List<String> res = new ArrayList<String>();
        WADOValidatorType[] dd = WADOValidatorType.values();
        for (WADOValidatorType validators : dd) {
            if (validators.getName().toLowerCase().contains(descriminator.toLowerCase())) {
                res.add(validators.getName());
            }
        }
        return res;
    }

    private static String getDetailedResultAsString(DetailedResult dr) {
        if (dr != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                DetailedResultTransformer.save(baos, dr);
            } catch (JAXBException e) {
                log.error("JAXBException error in the method getDetailedResultAsString. ", e);
            }
            String res = null;
            try {
                res = baos.toString(UTF_8);
                if (res.contains("?>")) {
                    res = res.substring(res.indexOf("?>") + 2);
                }
                return res;
            } catch (UnsupportedEncodingException e) {
                log.error("" + e.getMessage());
            }
        }
        return null;
    }


    @Override
    protected ValidatorDescription getValidatorByOidOrName(String value) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected List<ValidatorDescription> getValidatorsForDescriminator(String descriminator) {
        // TODO Auto-generated method stub
        return null;
    }
}


