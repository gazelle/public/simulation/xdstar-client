package net.ihe.gazelle.xdstar.testplan.action;

import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.simulator.sut.model.UsageQuery;
import net.ihe.gazelle.testplan.TestPlanType;
import net.ihe.gazelle.testplan.editor.action.testplan.TESTPLANInitiator;
import net.ihe.gazelle.testplan.editor.action.testplan.TESTPLANTestPlanTypeEditorLocal;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.util.List;

/**
 * @author abderrazek boufahja
 */
@Name("testPlanManager")
@Scope(ScopeType.PAGE)
public class TestPlanManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static Logger log = LoggerFactory.getLogger(TestPlanManager.class);

    private TestPlanType selectedTestPlanType;

    private boolean editTestPlanType = false;

    private boolean viewExportedText = false;

    public boolean isViewExportedText() {
        return viewExportedText;
    }

    public void setViewExportedText(boolean viewExportedText) {
        this.viewExportedText = viewExportedText;
    }

    public boolean isEditTestPlanType() {
        return editTestPlanType;
    }

    public void setEditTestPlanType(boolean editTestPlanType) {
        this.editTestPlanType = editTestPlanType;
    }

    public TestPlanType getSelectedTestPlanType() {
        return selectedTestPlanType;
    }

    public void setSelectedTestPlanType(TestPlanType selectedTestPlanType) {
        this.selectedTestPlanType = selectedTestPlanType;
    }

    @SuppressWarnings("unchecked")
    public List<TestPlanType> getListTestPlanTypes() {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        return em.createQuery("Select tpt FROM TestPlanType tpt").getResultList();
    }

    public String generateTestPlan() {
        TESTPLANTestPlanTypeEditorLocal xx = (TESTPLANTestPlanTypeEditorLocal) (Component.getInstance("tESTPLANTestPlanTypeEditorManager"));
        xx.setSelectedTESTPLANTestPlanType(TESTPLANInitiator.initTestPlanType());
        return "/editor/testplan/TestPlanType.xhtml";
    }

    public String editNewTestPlanType() {
        this.selectedTestPlanType = TESTPLANInitiator.initTestPlanType();
//		int i = 1;
//		for (TestStepType ts : this.selectedTestPlanType.getTestStep()) {
//			ts.setIdentifier(i);
//			ts.setSeqid(i++);
//		}
//		EntityManager em = (EntityManager)Component.getInstance("entityManager");
//		em.persist(this.selectedTestPlanType);
        this.editTestPlanType = true;
        return this.editTestPlanOfSelectedTestPlanType();
    }

    public String editTestPlanType(TestPlanType tpd) {
        this.selectedTestPlanType = tpd;
        return this.editTestPlanOfSelectedTestPlanType();
    }

    public void saveTestPlan(TestPlanType tpt) {
        try {
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            em.merge(tpt);
            em.flush();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "TestPlan was saved.");
        } catch (PersistenceException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Fail to save TestPlan !");
            log.error("saveTestPlan : " + e.getMessage());
        }
    }

    public String editTestPlan(TestPlanType tp) {
        this.selectedTestPlanType = tp;
        TESTPLANTestPlanTypeEditorLocal xx = (TESTPLANTestPlanTypeEditorLocal) (Component.getInstance("tESTPLANTestPlanTypeEditorManager"));
        xx.setSelectedTESTPLANTestPlanType(this.selectedTestPlanType);
        return "/editor/testplan/TestPlanType.xhtml";
    }

    public List<Usage> getAllListUsages() {
        UsageQuery uq = new UsageQuery();
        return uq.getList();
    }

    public void saveSelectedTestPlanType() {
        if (this.selectedTestPlanType != null) {
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            this.selectedTestPlanType = em.merge(this.selectedTestPlanType);
            em.flush();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Test Plan was saved.");
        }
    }

    public void deleteSelectedTestPlanType() {
        if (this.selectedTestPlanType != null) {
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            em.remove(em.find(TestPlanType.class, this.selectedTestPlanType.getId()));
            em.flush();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Test Plan was deleted.");
        }
    }

    public String editTestPlanOfSelectedTestPlanType() {
        if (this.selectedTestPlanType != null) {
            TESTPLANTestPlanTypeEditorLocal xx = (TESTPLANTestPlanTypeEditorLocal) (Component.getInstance("tESTPLANTestPlanTypeEditorManager"));
            xx.setSelectedTESTPLANTestPlanType(this.selectedTestPlanType);
            return "/editor/testplan/TestPlanType.xhtml";
        }
        return null;
    }

    public void copyTestPlanType(TestPlanType tpt) {
        if (tpt != null) {
            TestPlanType newTPT = new TestPlanType(tpt);
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            em.persist(newTPT);
            em.flush();
        }
    }

    public void exportToXml(TestPlanType tpt) {
        if (tpt != null) {
            this.viewExportedText = true;
            this.selectedTestPlanType = tpt;
        }
    }

    @Destroy
    public void destroy() {
        log.info("destroy generatorManager..");
    }

}
