package net.ihe.gazelle.xdstar.kproject;

public enum TestPlanName {
	
	KSA_RADR("KSA-RADR"), KSA_LABR("KSA-LABR");
	
	private String value;
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	private TestPlanName(String value) {
		this.value = value;
	}

}
