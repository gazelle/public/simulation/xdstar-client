package net.ihe.gazelle.xdstar.integrationtesting;

import net.ihe.gazelle.common.LinkDataProvider;
import org.kohsuke.MetaInfServices;

import java.util.ArrayList;
import java.util.List;

@MetaInfServices(LinkDataProvider.class)
public class ValidatorIntegrationTestLinkDataProvider implements LinkDataProvider {

	public static final ValidatorIntegrationTestLinkDataProvider instance() {
		return new ValidatorIntegrationTestLinkDataProvider();
	}

	private static List<Class<?>> supportedClasses;

	static {
		supportedClasses = new ArrayList<Class<?>>();
		supportedClasses.add(ValidatorIntegrationTest.class);
	}

	@Override
	public List<Class<?>> getSupportedClasses() {
		return supportedClasses;
	}

	@Override
	public String getLabel(Object o, boolean detailed) {
		StringBuilder label = new StringBuilder();
		if (o instanceof ValidatorIntegrationTest) {
			ValidatorIntegrationTest vit = (ValidatorIntegrationTest) o;
			label.append(vit.getKeyword());
			if (detailed) {
				label.append(" (");
				label.append("1.00");
				label.append(')');
			}
		}
		return label.toString();
	}

	@Override
	public String getLink(Object o) {
		StringBuilder url = new StringBuilder();
		if (o instanceof ValidatorIntegrationTest) {
			ValidatorIntegrationTest vit = (ValidatorIntegrationTest) o;
			url.append("testing/validatorUnitTestEditor.seam");
			url.append("?unitTest=");
			url.append(vit.getKeyword());
		}
		return url.toString();
	}

	@Override
	public String getTooltip(Object o) {
		// TODO Auto-generated method stub
		return null;
	}

}
