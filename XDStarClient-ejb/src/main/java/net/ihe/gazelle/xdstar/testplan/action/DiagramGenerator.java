package net.ihe.gazelle.xdstar.testplan.action;

import java.io.OutputStream;
import java.util.Set;
import java.util.TreeSet;

import net.ihe.gazelle.common.filecache.FileCache;
import net.ihe.gazelle.common.filecache.FileCacheRenderer;
import net.ihe.gazelle.testplan.TestStepType;
import net.ihe.gazelle.xdstar.testplan.model.TestPlanInstance;
import net.ihe.gazelle.xdstar.testplan.model.TestStepInstance;
import net.sf.sdedit.config.Configuration;
import net.sf.sdedit.config.ConfigurationManager;
import net.sf.sdedit.diagram.Diagram;
import net.sf.sdedit.error.SemanticError;
import net.sf.sdedit.error.SyntaxError;
import net.sf.sdedit.server.Exporter;
import net.sf.sdedit.text.TextHandler;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class DiagramGenerator {
	
	private DiagramGenerator(){}

	private static Logger log = LoggerFactory.getLogger(DiagramGenerator.class);
	
	private static void addStart(StringBuilder sb, String keyword) {
		sb.append("#![").append(keyword).append("]\nuser:Actor\n");
	}
	
	private static void addActor(StringBuilder sb, String keyword, Set<String> actors) {
		sb.append(safeDiagramString(keyword));
		sb.append(":");
		sb.append(safeDiagramString(StringUtils.join(actors, '/')));
		sb.append("\n");
	}

	private static String safeDiagramString(String input) {
		String output = input;
		output = output.replace(".", "\\.");
		output = output.replace(":", "\\:");
		output = output.replace("=", "\\=");
		return output;
	}

	private static void addTestStep(StringBuilder sb, TestStepType testStep, String initiator, String responder) {
		if (testStep.getDescription().length() > 0) {
			sb.append("user:");
			sb.append(safeDiagramString(initiator));
			sb.append(".");

			String description = testStep.getDescription();
			String[] lines = description.split("\r\n|\r|\n");
			boolean first = true;
			for (String line : lines) {
				if (first) {
					first = false;
				} else {
					sb.append("\\\\n");
				}
				String newLine = WordUtils.wrap(line, 40);
				newLine = newLine.replace("\r\n", "\\\\n");
				newLine = newLine.replace("\r", "\\\\n");
				newLine = newLine.replace("\n", "\\\\n");
				sb.append(safeDiagramString(newLine));
			}
			sb.append("\n");
		}
		sb.append(safeDiagramString(initiator));
		sb.append(": =");
		sb.append(safeDiagramString(responder));
		sb.append(".");
		if (testStep.getTransactionKeyword() != null) {
			sb.append(testStep.getTransactionKeyword());
			sb.append(" : ");
			sb.append(testStep.getMessageType());
		}

		sb.append("\n");
		sb.append("\n");
	}

	public static StringBuilder getTestInstanceSequenceDiagramSD(TestPlanInstance testInstance) {
		StringBuilder sb = new StringBuilder();
		addStart(sb, testInstance.getParentTestPlan().getTestName());
		Set<String> actors2 = new TreeSet<String>();
		actors2.add("XDStarClient");
		addActor(sb, "XDStarClient", actors2);
		Set<String> actors = new TreeSet<String>();
		actors.add("SUT");
		addActor(sb, "NCP-A", actors);
		sb.append("\n");
		
		for (TestStepInstance tsi : testInstance.getTestStepInstance()) {
			TestStepType testStep = tsi.getParentTestStep();
			String initiator = "XDStarClient";
			String responder = "NCP-A";
			addTestStep(sb, testStep, initiator, responder);
		}

		return sb;
	}


	public static void getTestInstanceSequenceDiagram(TestPlanInstance testInstance) {
		StringBuilder sb = getTestInstanceSequenceDiagramSD(testInstance);
		generateDiagram(sb.toString(), "SequenceDiagram_TI_" + testInstance.getId());
	}

	private static void generateDiagram(String sequenceDiagramString, String cacheKey) {
		log.debug(sequenceDiagramString);
		try {
			try {
				FileCache.getFile(cacheKey, sequenceDiagramString, new FileCacheRenderer() {
					@Override
					public void render(OutputStream out, String value) throws Exception {
						generatePNG(out, value);
					}

					@Override
					public String getContentType() {
						return "image/png";
					}
				});
			} catch (SyntaxError se) {
				TextHandler th = (TextHandler) se.getProvider();
				String msg = "ERROR:syntax error in line " + th.getLineNumber() + ": " + se.getMessage();
				log.error(msg, se);
			} catch (SemanticError se) {
				TextHandler th = (TextHandler) se.getProvider();
				String msg = "ERROR:semantic error in line " + th.getLineNumber() + ": " + se.getMessage();
				log.error(msg, se);
			} catch (Throwable t) {
				log.error("ERROR:fatal error: " + t.getMessage(), t);
			}
		} catch (Throwable t) {
			log.error("", t);
		}
	}

	public static void generatePNG(OutputStream out, String value) throws SemanticError, SyntaxError {
		Exporter exporter = Exporter.getExporter("png", null, "A4", out);
		if (exporter == null) {
			throw new RuntimeException("FreeHEP library missing.");
		}
		Configuration conf = ConfigurationManager.createNewDefaultConfiguration().getDataObject();
		conf.setThreaded(false);
		Diagram diagram = new Diagram(conf, new TextHandler(value), exporter);
		diagram.generate();
		exporter.export();
	}

}
