package net.ihe.gazelle.xdstar.integrationtesting;

import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.validator.ut.action.UnitTestBrowser;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("validatorIntegrationTestBrowser")
@Scope(ScopeType.PAGE)
public class ValidatorIntegrationTestBrowser extends UnitTestBrowser<ValidatorIntegrationTest, ValidatorIntegrationTestQuery> {
	
	@Override
	protected ValidatorIntegrationTestQuery createNewQuery() {
		return new ValidatorIntegrationTestQuery();
	}
	
	@Override
	protected void appendPath(HQLCriterionsForFilter<ValidatorIntegrationTest> criteria,ValidatorIntegrationTestQuery query) {
		criteria.addPath("rule", query.testedValidator());
	}

}
