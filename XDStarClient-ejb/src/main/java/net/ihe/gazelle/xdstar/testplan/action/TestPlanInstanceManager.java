package net.ihe.gazelle.xdstar.testplan.action;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.xdstar.testplan.model.TestPlanInstance;
import net.ihe.gazelle.xdstar.testplan.model.TestStepInstance;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author abderrazek boufahja
 */
@Name("testPlanInstanceManager")
@Scope(ScopeType.PAGE)
public class TestPlanInstanceManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static Logger log = LoggerFactory.getLogger(TestPlanInstanceManager.class);

    private TestPlanInstance selectedTestPlanInstance;

    public TestPlanInstance getSelectedTestPlanInstance() {
        return selectedTestPlanInstance;
    }

    public void setSelectedTestPlanInstance(
            TestPlanInstance selectedTestPlanInstance) {
        this.selectedTestPlanInstance = selectedTestPlanInstance;
    }

    @Create
    public void initSelectedTestPlanInstance() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String pageId = params.get("id");
        try {
            Integer i = Integer.valueOf(pageId);
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            this.selectedTestPlanInstance = em.find(TestPlanInstance.class, i);
        } catch (Exception e) {
            this.selectedTestPlanInstance = null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<TestPlanInstance> getListTestPlanInstances() {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        return em.createQuery("Select tpt FROM TestPlanInstance tpt").getResultList();
    }

    public void deleteSelectedTestPlanInstance() {
        if (this.selectedTestPlanInstance != null) {
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            em.remove(em.find(TestPlanInstance.class, this.selectedTestPlanInstance.getId()));
            em.flush();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Test Plan Instance was deleted.");
        }
    }

    public String getSequenceDiagramAsUrl(TestPlanInstance tpi) {
        return ApplicationConfiguration.getValueOfVariable("application_url") + "/testInstanceSequenceDiagram.seam?id=" + tpi.getId();
    }

    public List<String> getListSystemConfigurations(TestPlanInstance tpi) {
        List<String> res = new ArrayList<String>();
        if (tpi != null) {
            for (TestStepInstance tsi : tpi.getTestStepInstance()) {
                if ((tsi.getSystemConfiguration() != null) && !res.contains(tsi.getSystemConfiguration().getName())) {
                    res.add(tsi.getSystemConfiguration().getName());
                }
            }
            return res;
        }
        return null;
    }

    public String getPermanentLink(TestPlanInstance tsi) {
        if (tsi != null && tsi.getId() != null) {
            String res = ApplicationConfiguration.getValueOfVariable("application_url");
            res += "/testplan/testPlanInstance.seam?id=" + tsi.getId();
            return res;
        }
        return null;
    }

    @Destroy
    public void destroy() {
        log.info("destroy generatorManager..");
    }

}
