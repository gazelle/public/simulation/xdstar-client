package net.ihe.gazelle.xdstar.ct;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.Date;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

/**
 * @author abderrazek boufahja
 */
@Name("CTManager")
@Scope(ScopeType.PAGE)
public class CTManager implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Boolean renderTestResult = false;
	
	private Date serverTime;
	
	
	public Boolean getRenderTestResult() {
		return renderTestResult;
	}

	public void setRenderTestResult(Boolean renderTestResult) {
		this.renderTestResult = renderTestResult;
	}

	public Date getServerTime() {
		return serverTime;
	}

	public void setServerTime(Date serverTime) {
		this.serverTime = serverTime;
	}

	public String getHostName(){
		try{
			return InetAddress.getLocalHost().getHostName();
		}
		catch(Exception e){
			return "unknown";
		}
	}
	
	public String getIpAdress(){
		try{
			return InetAddress.getLocalHost().getHostAddress();
		}
		catch(Exception e){
			return "unknown";
		}
	}
	
	public void testWithServer(){
		this.serverTime = new Date();
	}
}