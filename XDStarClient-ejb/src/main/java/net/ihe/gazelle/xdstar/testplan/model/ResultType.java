package net.ihe.gazelle.xdstar.testplan.model;

/**
 * 
 * @author abderrazek boufahja
 *
 */
public enum ResultType {
	
	FAILED("FAILED"), PASSED("PASSED"), SKIPPED("SKIPPED"), UNKNOWN("UNKNOWN");
	
	private String value;
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	private ResultType(String val) {
		this.value = val;
	}

}
