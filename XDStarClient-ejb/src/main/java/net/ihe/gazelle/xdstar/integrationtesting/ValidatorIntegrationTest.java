package net.ihe.gazelle.xdstar.integrationtesting;

import net.ihe.gazelle.validator.ut.model.UnitTest;
import net.ihe.gazelle.xdstar.validator.ValidatorPackClass;
import org.hibernate.annotations.Type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue("Validator_RULE_UT")
public class ValidatorIntegrationTest extends UnitTest {

	private static final long serialVersionUID = 904725100611305807L;

	@ManyToOne(targetEntity=ValidatorPackClass.class)
	@JoinColumn(name="validator_pack_class_id")
	private ValidatorPackClass testedValidator;
	
	private String testedFilePath;
	
	@Type(type = "text")
	private String detailedResult;

	public ValidatorIntegrationTest(){
		super();
	}

	public ValidatorIntegrationTest(String keyword){
		super();
		setKeyword(keyword);
	}

	public ValidatorIntegrationTest(ValidatorIntegrationTest test){
		super(test);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	public ValidatorPackClass getTestedValidator() {
		return testedValidator;
	}

	public void setTestedValidator(ValidatorPackClass testedValidator) {
		this.testedValidator = testedValidator;
	}

	public String getTestedFilePath() {
		return testedFilePath;
	}

	public void setTestedFilePath(String testedFilePath) {
		this.testedFilePath = testedFilePath;
	}

	public String getDetailedResult() {
		return detailedResult;
	}

	public void setDetailedResult(String detailedResult) {
		this.detailedResult = detailedResult;
	}

}
