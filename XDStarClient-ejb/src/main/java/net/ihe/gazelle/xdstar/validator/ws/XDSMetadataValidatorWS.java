package net.ihe.gazelle.xdstar.validator.ws;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.validation.exception.GazelleValidationException;
import net.ihe.gazelle.validation.model.ValidatorDescription;
import net.ihe.gazelle.validation.ws.AbstractModelBasedValidation;
import net.ihe.gazelle.xdstar.util.POMVersion;
import net.ihe.gazelle.xdstar.validator.GenericMetadataValidator;
import net.ihe.gazelle.xdstar.validator.ValidatorPackClass;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.soap.SOAPException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@Stateless
@Name("ModelBasedValidationWS")
@WebService(name = "ModelBasedValidationWS",
        serviceName = "ModelBasedValidationWSService",
        portName = "ModelBasedValidationWSPort",
        targetNamespace = "http://ws.mb.validator.gazelle.ihe.net")
public class XDSMetadataValidatorWS extends AbstractModelBasedValidation implements Serializable {


    private static Logger log = LoggerFactory.getLogger(XDSMetadataValidatorWS.class);


    /**
     *
     */
    private static final long serialVersionUID = 1L;


    @WebMethod
    @WebResult(name = "DetailedResult")
    public String validateDocument(
            @WebParam(name = "document") String document,
            @WebParam(name = "validator") String validator)
            throws SOAPException {
        if (null == document || document.isEmpty()) {
            throw new SOAPException("A document must be provided");
        }
        String res = XDSMetadataValidator.validateDocument(document, validator);
        res = res.replace("<ValidationServiceVersion>UNKNOWN</ValidationServiceVersion>",
                "<ValidationServiceVersion>" + POMVersion.getVersion() + "</ValidationServiceVersion>");
        String status = res.contains("<ValidationTestResult>FAILED</ValidationTestResult>") ? "FAILED" : "PASSED";
        if (GenericMetadataValidator.getValidatorPackClassFromValidatorName(validator) != null) {
            this.addValidatorUsage(validator, status);
        }
        return res;
    }

    @Override
    protected String buildReportOnParsingFailure(GazelleValidationException e, ValidatorDescription validatorDescription) {
        return null;
    }

    @Override
    protected String executeValidation(String s, ValidatorDescription validatorDescription, boolean b) throws GazelleValidationException {
        return null;
    }

    @WebMethod
    @WebResult(name = "DetailedResult")
    public String validateXDStarMetadataB64(@WebParam(name = "base64ObjectToValidate") String xdsDocumentB64, @WebParam(partName = "validator")
            String validator) throws SOAPException {
        if (null == xdsDocumentB64 || xdsDocumentB64.isEmpty()) {
            throw new SOAPException("A xdsDocumentB64 must be provided");
        }
        byte[] bb = Base64.decode(xdsDocumentB64);
        if (bb != null) {
            String xdsDocument = null;
            xdsDocument = new String(bb, StandardCharsets.UTF_8);
            return this.validateDocument(xdsDocument, validator);
        } else {
            return this.validateDocument(null, validator);
        }
    }

    @Override
    @WebMethod
    @WebResult(name = "about")
    public String about() {
        String res = "This webservice is developped by IHE-europe / gazelle team. The aim of this validator is to validate XDS metadata using model" +
                " based validation.\n";
        res = res + "For more information please contact the manager of gazelle project eric.poiseau@inria.fr";
        return res;
    }

    @Override
    @WebMethod
    @WebResult(name = "Validators")
    public List<String> getListOfValidators(@WebParam(name = "descriminator") String descriminator)
            throws SOAPException {
        if (null == descriminator) {
            descriminator = "";
        }
        List<String> res = new ArrayList<String>();
        HQLQueryBuilder<ValidatorPackClass> hql = new HQLQueryBuilder<ValidatorPackClass>(ValidatorPackClass.class);
        List<ValidatorPackClass> listValidatorPackClass = hql.getList();
        for (ValidatorPackClass validatorPackClass : listValidatorPackClass) {
            if (validatorPackClass.getValidatorName().contains(descriminator) && !validatorPackClass.disactivatedValue()) {
                res.add(validatorPackClass.getValidatorName());
            }
        }
        Collections.sort(res);
        return res;
    }


    @Override
    protected ValidatorDescription getValidatorByOidOrName(String value) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected List<ValidatorDescription> getValidatorsForDescriminator(String descriminator) {
        // TODO Auto-generated method stub
        return null;
    }
}


