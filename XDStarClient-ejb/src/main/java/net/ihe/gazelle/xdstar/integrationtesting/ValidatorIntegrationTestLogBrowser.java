package net.ihe.gazelle.xdstar.integrationtesting;

import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.validator.ut.action.UnitTestLogBrowser;
import net.ihe.gazelle.validator.ut.model.UnitTestLog;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("validatorIntegrationTestLogBrowser")
@Scope(ScopeType.PAGE)
public class ValidatorIntegrationTestLogBrowser extends UnitTestLogBrowser {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4644632965342106571L;

	@Override
	protected void addPathToCriteria(HQLCriterionsForFilter<UnitTestLog> criteria) {
	}
}
