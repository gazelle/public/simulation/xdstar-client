package net.ihe.gazelle.xdstar.testplan.action;

import net.ihe.gazelle.xdstar.testplan.model.ResultType;
import net.ihe.gazelle.xdstar.testplan.model.StatusType;
import net.ihe.gazelle.xdstar.testplan.model.TestPlanInstance;
import net.ihe.gazelle.xdstar.testplan.model.TestStepInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@MessageDriven(name = "testPlanInstanceMDB",
        activationConfig = {
                @ActivationConfigProperty(propertyName = "destination", propertyValue = "jms/TestPlanInstanceQueue"),
                @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")})
//@DependsOn("jboss.mq.destination:service=Queue,name=TestPlanInstanceQueue")
public class TestPlanInstanceMDB implements MessageListener {

    private static Logger log = LoggerFactory.getLogger(TestPlanInstanceMDB.class);

    @Resource(mappedName = "java:/jms/TestStepInstanceQueue")
    private Queue testStepInstanceQueue;

    @PersistenceContext(unitName = "XDStarClient-PersistenceUnit")
    private EntityManager entityManager;

    @Resource(mappedName = "java:/JmsXA")
    private ConnectionFactory connectionFactory;

    @Override
    public void onMessage(Message mess) {
        try {
            if (mess instanceof MapMessage) {
                MapMessage map = (MapMessage) mess;
                Integer tpiId = map.getInt("tpi_id");
                TestPlanInstance tpi = (TestPlanInstance) entityManager.find(TestPlanInstance.class, tpiId);
                Integer greaterThan = map.getInt("greaterThan");

                List<TestStepInstance> listCurrent = this.recuperatetestStepInstanceOnTheSequence(tpi, greaterThan);
                boolean finished = true;
                for (TestStepInstance testStepInstance : listCurrent) {
                    if (!testStepInstance.getStatus().equals(StatusType.ENDED)) {
                        finished = false;
                    }
                }

                if (finished) {
                    List<TestStepInstance> testStepInstancesFirst = this.recuperatetestStepInstanceAfterTheSequence(tpi, greaterThan);
                    for (TestStepInstance testStepInstance : testStepInstancesFirst) {
                        this.luanchTestStepsWithIdentifier(testStepInstance);
                    }
                    if (testStepInstancesFirst.size() == 0) {
                        tpi.setResult(ResultType.PASSED);
                        for (TestStepInstance testStepInstance : tpi.getTestStepInstance()) {
                            if (testStepInstance.getResult().equals(ResultType.FAILED)) {
                                tpi.setResult(ResultType.FAILED);
                                break;
                            }
                        }
                        entityManager.merge(tpi);
                        entityManager.flush();
                    }
                }

            }
        } catch (Exception e) {
            log.info("TestPlanInstanceMDB : not able to process message", e);
        }

    }

    private List<TestStepInstance> recuperatetestStepInstanceOnTheSequence(TestPlanInstance selectedTestPlanInstance, int seq) {
        List<TestStepInstance> res = new ArrayList<TestStepInstance>();
        for (TestStepInstance testStepInstance : selectedTestPlanInstance.getTestStepInstance()) {
            if (testStepInstance.getParentTestStep().getSeqid() == seq) {
                res.add(testStepInstance);
            }
        }
        return res;
    }

    private List<TestStepInstance> recuperatetestStepInstanceAfterTheSequence(TestPlanInstance selectedTestPlanInstance, int seq) {
        List<TestStepInstance> res = new ArrayList<TestStepInstance>();
        int nextseq = 100000000;
        for (TestStepInstance testStepInstance : selectedTestPlanInstance.getTestStepInstance()) {
            if (testStepInstance.getParentTestStep().getSeqid() < nextseq && testStepInstance.getParentTestStep().getSeqid() > seq) {
                nextseq = testStepInstance.getParentTestStep().getSeqid();
            }
        }
        for (TestStepInstance testStepInstance : selectedTestPlanInstance.getTestStepInstance()) {
            if (testStepInstance.getParentTestStep().getSeqid() == nextseq) {
                res.add(testStepInstance);
            }
        }
        return res;
    }

    private void luanchTestStepsWithIdentifier(TestStepInstance tsi) {
        tsi.setStatus(StatusType.STARTED);
        tsi = entityManager.merge(tsi);
        entityManager.flush();
        Connection connection = null;
        Session session = null;
        MessageProducer producer = null;
        try {
            connection = connectionFactory.createConnection();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            producer = session.createProducer(testStepInstanceQueue);
            ObjectMessage message = session.createObjectMessage();
            message.setObject(tsi);
            producer.send(message);
        } catch (JMSException e) {
            log.error("TestPlanInstanceMDB : not able to luanchTestStepsWithIdentifier", e);
        } finally {
            if (producer != null) {
                try {
                    producer.close();
                } catch (JMSException e) {
                    log.error("" + e.getMessage());
                }
            }
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException e) {
                    log.error("" + e.getMessage());
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException e) {
                    log.error("" + e.getMessage());
                }
            }
        }
    }

}
