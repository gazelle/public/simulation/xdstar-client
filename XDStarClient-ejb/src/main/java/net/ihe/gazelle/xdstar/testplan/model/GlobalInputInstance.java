package net.ihe.gazelle.xdstar.testplan.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import net.ihe.gazelle.testplan.GlobalInputType;

/**
 * 
 * @author abderrazek boufahja
 *
 */
@Entity
@Table(name="tp_global_input_instance", schema = "public")
@SequenceGenerator(name="tp_global_input_instance_sequence", sequenceName="tp_global_input_instance_id_seq", allocationSize=1)
public class GlobalInputInstance implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@NotNull
	@GeneratedValue(generator="tp_global_input_instance_sequence", strategy=GenerationType.SEQUENCE)
	@Column(name="id", nullable = false, unique = true)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="global_input_id")
	private GlobalInputType globalInputType;
	
	@Column(name="value")
	private String value;
	
	/**
	 * Constructor
	 * 
	 */
	public GlobalInputInstance()
	{
		
	}


	/*****************************************************************************
	 * 
	 * Getters and Setters
	 * 
	 ******************************************************************************/
	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public GlobalInputType getGlobalInputType() {
		return globalInputType;
	}


	public void setGlobalInputType(GlobalInputType globalInputType) {
		this.globalInputType = globalInputType;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


}
