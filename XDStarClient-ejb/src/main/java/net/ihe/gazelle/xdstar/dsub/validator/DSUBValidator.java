package net.ihe.gazelle.xdstar.dsub.validator;

import net.ihe.gazelle.common.validator.RIMValidator;
import net.ihe.gazelle.dsub.val.comnotifreq.COMNOTIFREQPackValidator;
import net.ihe.gazelle.dsub.val.dsubcommon.DSUBCOMMONPackValidator;
import net.ihe.gazelle.dsub.val.notifyreq.NOTIFYREQPackValidator;
import net.ihe.gazelle.dsub.val.publishreq.PUBLISHREQPackValidator;
import net.ihe.gazelle.dsub.val.subscribereq.SUBSCRIBEREQPackValidator;
import net.ihe.gazelle.dsub.val.subscriberesp.SUBSCRIBERESPPackValidator;
import net.ihe.gazelle.dsub.val.unsubscribereq.UNSUBSCRIBEREQPackValidator;
import net.ihe.gazelle.dsub.val.unsubscriberesp.UNSUBSCRIBERESPPackValidator;
import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.ihexds.validator.rdsihe.RDSIHEPackValidator;
import net.ihe.gazelle.ihexds.validator.rimihe.RIMIHEPackValidator;
import net.ihe.gazelle.ihexds.validator.xdsds.XDSDSPackValidator;
import net.ihe.gazelle.ksa.xdsb.kpnr.KPNRPackValidator;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.xdstar.validator.ws.DetailedResultTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang.CharEncoding.UTF_8;

public final class DSUBValidator implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    private static Logger log = LoggerFactory.getLogger(DSUBValidator.class);


    private DSUBValidator() {
    }

    public static String validateDocument(String document, String validator)
            throws SOAPException {
        DSUBValidatorType val = DSUBValidatorType.getValidatorFromValue(validator);
        DetailedResult dr = validateMessageByValidator(document, val);
        String res = getDetailedResultAsString(dr);
        if (null == res) {
            throw new SOAPException("The Validator is not valid");
        }
        if (res.contains("?>")) {
            res = res.substring(res.indexOf("?>") + 2);
        }
        return res;
    }

    public static DetailedResult validateMessageByValidator(String txdsString,
                                                            DSUBValidatorType val) {
        DetailedResult dr = null;
        if (val == null) {
            return null;
        }
        switch (val) {
            case IHE_PUB_REQ:
                dr = DSUBValidator.validateIHE_PUB_REQ(txdsString);
                break;
            case IHE_NOTIF_REQ:
                dr = DSUBValidator.validateIHE_NOTIF_REQ(txdsString);
                break;
            case IHE_SUB_REQ:
                dr = DSUBValidator.validateIHE_SUB_REQ(txdsString);
                break;
            case IHE_SUB_RESP:
                dr = DSUBValidator.validateIHE_SUB_RESP(txdsString);
                break;
            case IHE_UNSUB_REQ:
                dr = DSUBValidator.validateIHE_UNSUB_REQ(txdsString);
                break;
            case IHE_UNSUB_RESP:
                dr = DSUBValidator.validateIHE_UNSUB_RESP(txdsString);
                break;
            case KSA_NOTIF_REQ:
                dr = DSUBValidator.validateKSA_NOTIF_REQ(txdsString);
                break;
            default:
                break;
        }
        return dr;
    }

    public static DetailedResult validateKSA_NOTIF_REQ(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<ConstraintValidatorModule>();
        listConstraintValidatorModule.add(new NOTIFYREQPackValidator());
        listConstraintValidatorModule.add(new COMNOTIFREQPackValidator());
        listConstraintValidatorModule.add(new DSUBCOMMONPackValidator());
        listConstraintValidatorModule.add(new RDSIHEPackValidator());

        listConstraintValidatorModule.add(new XDSDSPackValidator());
        listConstraintValidatorModule.add(new KPNRPackValidator());
        listConstraintValidatorModule.add(new RIMIHEPackValidator());
        listConstraintValidatorModule.add(new RIMValidator());
        return SingletonDSUBValidator.validate(req, listConstraintValidatorModule, DSUBValidatorType.KSA_NOTIF_REQ);
    }

    public static DetailedResult validateIHE_NOTIF_REQ(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<ConstraintValidatorModule>();
        listConstraintValidatorModule.add(new NOTIFYREQPackValidator());
        listConstraintValidatorModule.add(new COMNOTIFREQPackValidator());
        listConstraintValidatorModule.add(new DSUBCOMMONPackValidator());
        listConstraintValidatorModule.add(new RIMIHEPackValidator());
        listConstraintValidatorModule.add(new RIMValidator());
        listConstraintValidatorModule.add(new RDSIHEPackValidator());
        return SingletonDSUBValidator.validate(req, listConstraintValidatorModule, DSUBValidatorType.IHE_NOTIF_REQ);
    }

    public static DetailedResult validateIHE_PUB_REQ(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<ConstraintValidatorModule>();
        listConstraintValidatorModule.add(new PUBLISHREQPackValidator());
        listConstraintValidatorModule.add(new COMNOTIFREQPackValidator());
        listConstraintValidatorModule.add(new DSUBCOMMONPackValidator());
        listConstraintValidatorModule.add(new RIMIHEPackValidator());
        listConstraintValidatorModule.add(new RIMValidator());
        listConstraintValidatorModule.add(new RDSIHEPackValidator());
        return SingletonDSUBValidator.validate(req, listConstraintValidatorModule, DSUBValidatorType.IHE_PUB_REQ);
    }

    public static DetailedResult validateIHE_SUB_REQ(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<ConstraintValidatorModule>();
        listConstraintValidatorModule.add(new SUBSCRIBEREQPackValidator());
        listConstraintValidatorModule.add(new DSUBCOMMONPackValidator());
        return SingletonDSUBValidator.validate(req, listConstraintValidatorModule, DSUBValidatorType.IHE_SUB_REQ);
    }

    public static DetailedResult validateIHE_SUB_RESP(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<ConstraintValidatorModule>();
        listConstraintValidatorModule.add(new SUBSCRIBERESPPackValidator());
        listConstraintValidatorModule.add(new DSUBCOMMONPackValidator());
        return SingletonDSUBValidator.validate(req, listConstraintValidatorModule, DSUBValidatorType.IHE_SUB_RESP);
    }

    public static DetailedResult validateIHE_UNSUB_REQ(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<ConstraintValidatorModule>();
        listConstraintValidatorModule.add(new UNSUBSCRIBEREQPackValidator());
        listConstraintValidatorModule.add(new DSUBCOMMONPackValidator());
        return SingletonDSUBValidator.validate(req, listConstraintValidatorModule, DSUBValidatorType.IHE_UNSUB_REQ);
    }

    public static DetailedResult validateIHE_UNSUB_RESP(String req) {
        List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<ConstraintValidatorModule>();
        listConstraintValidatorModule.add(new UNSUBSCRIBERESPPackValidator());
        listConstraintValidatorModule.add(new DSUBCOMMONPackValidator());
        return SingletonDSUBValidator.validate(req, listConstraintValidatorModule, DSUBValidatorType.IHE_UNSUB_RESP);
    }


    private static String getDetailedResultAsString(DetailedResult dr) {
        if (dr != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                DetailedResultTransformer.save(baos, dr);
                return baos.toString(UTF_8);
            } catch (JAXBException e) {
                log.error("not able to extract detailed result! : "+ e.getMessage());
            } catch (UnsupportedEncodingException e) {
                log.error("" + e.getMessage());
            }
        }
        return null;
    }

}


