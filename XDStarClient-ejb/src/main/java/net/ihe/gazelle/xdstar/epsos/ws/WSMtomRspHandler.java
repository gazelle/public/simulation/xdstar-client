package net.ihe.gazelle.xdstar.epsos.ws;
import java.util.Set;

import javax.activation.DataHandler;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.ws.handler.LogicalMessageContext;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import net.ihe.gazelle.xdstar.util.DOM2String;

import org.jboss.ws.core.soap.SOAPMessageImpl;
//import org.jboss.ws.extensions.xop.XOPContext;
//import org.jboss.wsf.common.handler.GenericSOAPHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author abderrazek boufahja
 *
 */
public class WSMtomRspHandler extends org.jboss.ws.api.handler.GenericSOAPHandler<LogicalMessageContext> {

	private Logger LOG = LoggerFactory.getLogger(WSMtomRspHandler.class);

	@Override
	public Set<QName> getHeaders() {
		return null;
	}

	@Override
	public void close(MessageContext mc) {
	}


	public boolean handleInbound(MessageContext mc) {
		return true;
	}

	protected boolean handleOutbound(MessageContext msgContext) {
		LOG.info("---- handleOutbound");
		try {
			SOAPMessage msg = ((SOAPMessageContext)msgContext).getMessage();
//			CommonMessageContext cmsgContext = (CommonMessageContext) msgContext;
			String content = this.getSOAPMessage((SOAPMessageContext)msgContext);
			if (content!= null && content.contains("RetrieveDocumentSetResponse") && msg instanceof SOAPMessageImpl ) {
				LOG.info("FORCE MTOM/XOP Response Message!");
				javax.mail.util.ByteArrayDataSource DUMMY_PLAIN_DATA_SOURCE = new javax.mail.util.ByteArrayDataSource("DUMMY", "text/plain");
				msg.addAttachmentPart(msg.createAttachmentPart(new DataHandler(DUMMY_PLAIN_DATA_SOURCE)));
				//TODO fixme
//				((SOAPMessageImpl)msg).setXOPMessage(true);
//				Scope currScope=cmsgContext.getCurrentScope();
//				cmsgContext.setCurrentScope(Scope.APPLICATION);
//				XOPContext.setMTOMEnabled(true);
//				cmsgContext.setCurrentScope(currScope);
			} else {
				LOG.warn("Can't force MTOM/XOP Response Message! msg is NOT SOAPMessageImpl!");
			}
		} catch (Exception e) {
			LOG.error("Can't force MTOM/XOP Response Message! Error:"+e,e);
		}
		return true;
	}

	private String getSOAPMessage(SOAPMessageContext mc){
		SOAPMessage sm = mc.getMessage();
		SOAPPart sh = sm.getSOAPPart();
		try {
			return DOM2String.getStringFromDocument(sh.getEnvelope());
		} catch (SOAPException e) {
			LOG.error("" + e.getMessage());
		}
		return null;
	}


}
