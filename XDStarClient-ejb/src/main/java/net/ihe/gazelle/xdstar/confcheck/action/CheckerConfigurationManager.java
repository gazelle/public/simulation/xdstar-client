package net.ihe.gazelle.xdstar.confcheck.action;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.security.RolesAllowed;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Name("checkerConfigurationManager")
@Scope(ScopeType.PAGE)
public class CheckerConfigurationManager implements Serializable {

    /**
     *
     */

    private static final Logger LOG = LoggerFactory.getLogger(CheckerConfigurationManager.class);

    private static final long serialVersionUID = 1L;

    private String timerPeriod = "3000";

    private String dateBegin;

    public String getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(String dateBegin) {
        this.dateBegin = dateBegin;
    }

    public String getTimerPeriod() {
        return timerPeriod;
    }

    public void setTimerPeriod(String timerPeriod) {
        this.timerPeriod = timerPeriod;
    }

    @RolesAllowed({"admin_role"})
    public void createTimer() {
        if (isValid()) {
            long period = -1;
            try {
                period = Long.parseLong(timerPeriod);
            } catch (NumberFormatException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The timer period specified is not a valid Long.");
                return;
            }
            Date bg = null;
            DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            try {
                bg = formatter.parse(dateBegin);
            } catch (ParseException e1) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The timer period specified is not a valid Long.");
                return;
            }

            if (period > 0 && bg != null) {
                try {
                    InitialContext xt = new InitialContext();

                    SystemConfigurationCheckerLocal systemConfigurationChecker =
                            (SystemConfigurationCheckerLocal) xt.lookup("java:app/XDStarClient-ejb/systemConfigurationChecker");
                    systemConfigurationChecker.createTimer(bg, period);
                } catch (NamingException e) {
                    LOG.error("" + e.getMessage());
                }

            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Initial expiration not well define, should be a date format.");
        }
    }

    public boolean isValid() {
        return this.timerPeriod != null && StringUtils.isNumeric(this.timerPeriod) &&
                this.dateBegin != null && this.dateBegin.matches("\\d{4}/\\d{2}/\\d{2} \\d{2}:\\d{2}:\\d{2}");
    }

    @RolesAllowed("admin_role")
    public void initNewSelectedTimer() {
        this.timerPeriod = "3000";
    }
}

