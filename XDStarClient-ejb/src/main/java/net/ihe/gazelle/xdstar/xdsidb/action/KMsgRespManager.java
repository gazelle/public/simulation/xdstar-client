package net.ihe.gazelle.xdstar.xdsidb.action;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.xdstar.comon.util.DocumentDownloadTool;
import net.ihe.gazelle.xdstar.comon.util.EVSClientValidator;
import net.ihe.gazelle.xdstar.validator.MetadataValidator;
import net.ihe.gazelle.xmltools.xsd.ValidationException;
import net.ihe.gazelle.xmltools.xsd.XMLValidator;
import org.dcm4chee.xdsib.retrieve.model.RespondingMessage;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

/**
 * @author abderrazek boufahja
 */
@Name("kMsgRespManager")
@Scope(ScopeType.PAGE)
public class KMsgRespManager implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(KMsgRespManager.class);

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    protected RespondingMessage selectedMessage;

    protected String stringRequestContent;
    protected String stringResponseContent;

    public RespondingMessage getSelectedMessage() {
        return selectedMessage;
    }

    public void setSelectedMessage(RespondingMessage selectedMessage) {
        this.selectedMessage = selectedMessage;
    }

    public String getStringRequestContent() {
        return stringRequestContent;
    }

    public void setStringRequestContent(String stringRequestContent) {
        this.stringRequestContent = stringRequestContent;
    }

    public String getStringResponseContent() {
        return stringResponseContent;
    }

    public void setStringResponseContent(String stringResponseContent) {
        this.stringResponseContent = stringResponseContent;
    }

    public void getMessageById() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params != null && !params.isEmpty()) {
            String messageIdString = params.get("id");
            Integer messageId = null;
            if (messageIdString != null) {
                try {
                    messageId = Integer.valueOf(messageIdString);
                } catch (NumberFormatException e) {
                    messageId = null;
                }
            }
            if (messageId != null) {
                EntityManager entityManager = HibernateUtilXDSIbDB.buildEntityManager();
                RespondingMessage messageToDisplay = entityManager.find(RespondingMessage.class, messageId);
                selectMessageToDisplay(messageToDisplay);
                HibernateUtilXDSIbDB.releaseEntityManager(entityManager);
            } else {
                selectMessageToDisplay(null);
            }
        }
    }

    public void selectMessageToDisplay(RespondingMessage inMessage) {
        if (inMessage != null) {
            selectedMessage = inMessage;

            if (selectedMessage.getSentMessageContent() != null &&
                    selectedMessage.getSentMessageContent().getMessageContent() != null &&
                    selectedMessage.getSentMessageContent().getMessageContent().length() > 0) {
                stringRequestContent = selectedMessage.getSentMessageContent().getMessageContent();
                stringRequestContent = this.formatString(stringRequestContent);
            } else {
                stringRequestContent = "No request stored";
            }

            if (selectedMessage.getReceivedMessageContent() != null &&
                    selectedMessage.getReceivedMessageContent().getMessageContent() != null &&
                    selectedMessage.getReceivedMessageContent().getMessageContent().length() > 0) {
                stringResponseContent = selectedMessage.getReceivedMessageContent().getMessageContent();
                stringResponseContent = this.formatString(stringResponseContent);
            } else {
                stringResponseContent = "No response stored";
            }
        } else {
            stringRequestContent = "No request stored";
            stringResponseContent = "No response stored";
            selectedMessage = null;
        }
    }

    private String formatString(String str) {
        String res = null;
        if (str != null) {
            if (str.indexOf("--MIMEBoundaryurn") == 0) {
                String soap = MetadataValidator.extractSoapEnvelope(str);
                String formattedSoap = this.formatXml(soap);
                res = str.replace(soap, formattedSoap);
            } else {
                res = this.formatXml(str);
            }
        }
        return res;
    }

    private String formatXml(String strxml) {
        try {
            Source xmlInput = new StreamSource(new StringReader(strxml));
            StringWriter stringWriter = new StringWriter();
            StreamResult xmlOutput = new StreamResult(stringWriter);
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            transformer.transform(xmlInput, xmlOutput);
            String res = xmlOutput.getWriter().toString();
            if (res.contains("?>")) {
                res = res.substring(res.indexOf("?>") + 2);
            }
            return res;
        } catch (Exception e) {
            return strxml;
        }
    }

    @Create
    public void initCreation() {
        this.getMessageById();
    }

    public String getFullPermanentLink(RespondingMessage message) {
        String baseUrl = ApplicationConfiguration.getValueOfVariable("application_url");
        return baseUrl + "/ksa/messageResponder.seam?id=" + message.getId();
    }

    public void validateWithEVSClient(String msg) {
        EVSClientValidator.validate(msg.getBytes(StandardCharsets.UTF_8));
    }

    public void downloadRequest() {
        String fileName = null;
        Integer id = this.selectedMessage.getId();
        if (this.stringIsXML(this.stringRequestContent)) {
            fileName = id + ".xml";
        } else {
            fileName = id + ".txt";
        }
        DocumentDownloadTool.showFile(new ByteArrayInputStream(this.stringRequestContent.getBytes(StandardCharsets.UTF_8)), fileName, true);
    }

    private boolean stringIsXML(String str) {
        boolean res = false;
        List<ValidationException> ttt = XMLValidator.validate(new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8)));
        if (ttt == null || ttt.size() == 0) {
            res = true;
        }
        return res;
    }

    public void downloadResponse() {
        String fileName = null;
        Integer id = this.selectedMessage.getId();

        if (this.stringIsXML(this.stringResponseContent)) {
            fileName = id + ".xml";
        } else {
            fileName = id + ".txt";
        }
        DocumentDownloadTool.showFile(new ByteArrayInputStream(this.stringResponseContent.getBytes(StandardCharsets.UTF_8)), fileName, true);
    }

}
