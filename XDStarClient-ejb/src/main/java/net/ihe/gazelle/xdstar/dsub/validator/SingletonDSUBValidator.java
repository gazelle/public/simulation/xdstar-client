package net.ihe.gazelle.xdstar.dsub.validator;

import net.ihe.gazelle.dsub.b2.*;
import net.ihe.gazelle.dsub.soapenv.Envelope;
import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.validation.*;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.xdstar.util.POMVersion;
import net.ihe.gazelle.xdstar.validator.ValidatorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class SingletonDSUBValidator {


    private static Logger log = LoggerFactory.getLogger(SingletonDSUBValidator.class);

    private SingletonDSUBValidator() {
    }

    static DetailedResult validate(String message, List<ConstraintValidatorModule> listConstraintValidatorModule, DSUBValidatorType type) {
        DetailedResult res = new DetailedResult();
        validateToSchema(res, message);
        try {
            if (message != null && SingletonDSUBValidator.containsSoapEnvelope(message)) {
                Envelope env = null;
                env = load(new ByteArrayInputStream(message.getBytes(StandardCharsets.UTF_8)));
                validateEnvelope(res, env, listConstraintValidatorModule);
            } else if (type == DSUBValidatorType.IHE_NOTIF_REQ || type == DSUBValidatorType.IHE_PUB_REQ ||
                    type == DSUBValidatorType.KSA_NOTIF_REQ) {
                NotifyType env = null;
                env = loadNotifyType(new ByteArrayInputStream(message.getBytes(StandardCharsets.UTF_8)));
                validateNotify(res, env, listConstraintValidatorModule);
            } else if (type == DSUBValidatorType.IHE_SUB_REQ) {
                SubscribeType env = null;
                env = loadSubscribeType(new ByteArrayInputStream(message.getBytes(StandardCharsets.UTF_8)));
                validateSubscribe(res, env, listConstraintValidatorModule);
            } else if (type == DSUBValidatorType.IHE_UNSUB_REQ) {
                UnsubscribeType env = null;
                env = loadUnsubscribeType(new ByteArrayInputStream(message.getBytes(StandardCharsets.UTF_8)));
                validateUnSubscribe(res, env, listConstraintValidatorModule);
            } else if (type == DSUBValidatorType.IHE_SUB_RESP) {
                SubscribeResponseType env = null;
                env = loadSubscribeResponseType(new ByteArrayInputStream(message.getBytes(StandardCharsets.UTF_8)));
                validateSubscribeResponse(res, env, listConstraintValidatorModule);
            } else if (type == DSUBValidatorType.IHE_UNSUB_RESP) {
                UnsubscribeResponseType env = null;
                env = loadUnsubscribeResponseType(new ByteArrayInputStream(message.getBytes(StandardCharsets.UTF_8)));
                validateUnSubscribeResponse(res, env, listConstraintValidatorModule);
            }
        } catch (ValidatorException vexp) {
            if (vexp.getDiagnostic() != null) {
                for (Notification notification : vexp.getDiagnostic()) {
                    if (res.getMDAValidation() == null) {
                        res.setMDAValidation(new MDAValidation());
                    }
                    res.getMDAValidation().getWarningOrErrorOrNote().add(notification);
                    res.getMDAValidation().setResult("FAILED");
                }
            }
        }
        summarizeMDAValidation(res.getMDAValidation());
        summarizeDetailedResult(res);
        return res;
    }

    static void summarizeMDAValidation(MDAValidation mda) {
        if (mda != null) {
            boolean containsErrors = false;
            for (Object obj : mda.getWarningOrErrorOrNote()) {
                if (obj instanceof Error) {
                    containsErrors = true;
                }
            }
            if (containsErrors) {
                mda.setResult("FAILED");
            } else {
                mda.setResult("PASSED");
            }
        }
    }

    public static boolean containsSoapEnvelope(String ss) {
        Pattern p = Pattern.compile("<([A-Za-z0-9_]*:)?Envelope.*?Envelope>", Pattern.MULTILINE | Pattern.DOTALL);
        Matcher mm = p.matcher(ss);
        if (mm.find()) {
            return true;
        }
        return false;
    }

    private static Envelope load(InputStream is) throws ValidatorException {
        ValidationEventHandlerDSUB handler = new ValidationEventHandlerDSUB();
        try {
            JAXBContext jc = JAXBContext.newInstance(Envelope.class);
            Unmarshaller u = jc.createUnmarshaller();
            u.setEventHandler(handler);
            Reader reader = new InputStreamReader(is, StandardCharsets.UTF_8);
            return (Envelope) u.unmarshal(reader);
        } catch (JAXBException e) {
            throw handler.getVexp();
        } catch (Exception e) {
            Notification not = new Error();
            not.setDescription("Errors when trying to parse the document, the kind of exception : " + e.getClass().getName() + ". " + e.getMessage());
            not.setLocation("all the document");
            not.setTest("parsing");
            handler.getVexp().getDiagnostic().add(not);
            throw handler.getVexp();
        }
    }

    private static NotifyType loadNotifyType(InputStream is) throws ValidatorException {
        ValidationEventHandlerDSUB handler = new ValidationEventHandlerDSUB();
        try {
            JAXBContext jc = JAXBContext.newInstance(NotifyType.class);
            Unmarshaller u = jc.createUnmarshaller();
            u.setEventHandler(handler);
            Reader reader = new InputStreamReader(is, StandardCharsets.UTF_8);
            return (NotifyType) u.unmarshal(reader);
        } catch (JAXBException e) {
            throw handler.getVexp();
        } catch (Exception e) {
            Notification not = new Error();
            not.setDescription("Errors when trying to parse the document, the kind of exception : " + e.getClass().getName() + ". " + e.getMessage());
            not.setLocation("all the document");
            not.setTest("parsing");
            handler.getVexp().getDiagnostic().add(not);
            throw handler.getVexp();
        }
    }

    private static SubscribeType loadSubscribeType(InputStream is) throws ValidatorException {
        ValidationEventHandlerDSUB handler = new ValidationEventHandlerDSUB();
        try {
            JAXBContext jc = JAXBContext.newInstance(SubscribeType.class);
            Unmarshaller u = jc.createUnmarshaller();
            u.setEventHandler(handler);
            Reader reader = new InputStreamReader(is, StandardCharsets.UTF_8);
            return (SubscribeType) u.unmarshal(reader);
        } catch (JAXBException e) {
            throw handler.getVexp();
        } catch (Exception e) {
            Notification not = new Error();
            not.setDescription("Errors when trying to parse the document, the kind of exception : " + e.getClass().getName() + ". " + e.getMessage());
            not.setLocation("all the document");
            not.setTest("parsing");
            handler.getVexp().getDiagnostic().add(not);
            throw handler.getVexp();
        }
    }

    private static SubscribeResponseType loadSubscribeResponseType(InputStream is) throws ValidatorException {
        ValidationEventHandlerDSUB handler = new ValidationEventHandlerDSUB();
        try {
            JAXBContext jc = JAXBContext.newInstance(SubscribeResponseType.class);
            Unmarshaller u = jc.createUnmarshaller();
            u.setEventHandler(handler);
            Reader reader = new InputStreamReader(is, StandardCharsets.UTF_8);
            return (SubscribeResponseType) u.unmarshal(reader);
        } catch (JAXBException e) {
            throw handler.getVexp();
        } catch (Exception e) {
            Notification not = new Error();
            not.setDescription("Errors when trying to parse the document, the kind of exception : " + e.getClass().getName() + ". " + e.getMessage());
            not.setLocation("all the document");
            not.setTest("parsing");
            handler.getVexp().getDiagnostic().add(not);
            throw handler.getVexp();
        }
    }

    private static UnsubscribeType loadUnsubscribeType(InputStream is) throws ValidatorException {
        ValidationEventHandlerDSUB handler = new ValidationEventHandlerDSUB();
        try {
            JAXBContext jc = JAXBContext.newInstance(UnsubscribeType.class);
            Unmarshaller u = jc.createUnmarshaller();
            u.setEventHandler(handler);
            Reader reader = new InputStreamReader(is, StandardCharsets.UTF_8);
            return (UnsubscribeType) u.unmarshal(reader);
        } catch (JAXBException e) {
            throw handler.getVexp();
        } catch (Exception e) {
            Notification not = new Error();
            not.setDescription("Errors when trying to parse the document, the kind of exception : " + e.getClass().getName() + ". " + e.getMessage());
            not.setLocation("all the document");
            not.setTest("parsing");
            handler.getVexp().getDiagnostic().add(not);
            throw handler.getVexp();
        }
    }

    private static UnsubscribeResponseType loadUnsubscribeResponseType(InputStream is) throws ValidatorException {
        ValidationEventHandlerDSUB handler = new ValidationEventHandlerDSUB();
        try {
            JAXBContext jc = JAXBContext.newInstance(UnsubscribeResponseType.class);
            Unmarshaller u = jc.createUnmarshaller();
            u.setEventHandler(handler);
            Reader reader = new InputStreamReader(is, StandardCharsets.UTF_8);
            return (UnsubscribeResponseType) u.unmarshal(reader);
        } catch (JAXBException e) {
            throw handler.getVexp();
        } catch (Exception e) {
            Notification not = new Error();
            not.setDescription("Errors when trying to parse the document, the kind of exception : " + e.getClass().getName() + ". " + e.getMessage());
            not.setLocation("all the document");
            not.setTest("parsing");
            handler.getVexp().getDiagnostic().add(not);
            throw handler.getVexp();
        }
    }


    static void summarizeDetailedResult(DetailedResult dr) {
        if (dr != null) {
            Date dd = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy, MM dd");
            DateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");
            dr.setValidationResultsOverview(new ValidationResultsOverview());
            dr.getValidationResultsOverview().setValidationDate(dateFormat.format(dd));
            dr.getValidationResultsOverview().setValidationTime(timeFormat.format(dd));
            dr.getValidationResultsOverview().setValidationServiceName("Gazelle DSUB Validation");
            dr.getValidationResultsOverview().setValidationTestResult("PASSED");
            if ((dr.getDocumentValidXSD() != null) && (dr.getDocumentValidXSD().getResult() != null) && (dr.getDocumentValidXSD().getResult()
                    .equals("FAILED"))) {
                dr.getValidationResultsOverview().setValidationTestResult("FAILED");
            }
            if ((dr.getDocumentWellFormed() != null) && (dr.getDocumentWellFormed().getResult() != null) && (dr.getDocumentWellFormed().getResult()
                    .equals("FAILED"))) {
                dr.getValidationResultsOverview().setValidationTestResult("FAILED");
            }
            if ((dr.getMDAValidation() != null) && (dr.getMDAValidation().getResult() != null) && (dr.getMDAValidation().getResult().equals
                    ("FAILED"))) {
                dr.getValidationResultsOverview().setValidationTestResult("FAILED");
            }
            if ((dr.getNistValidation() != null) && (dr.getNistValidation().getResult().contains("Summary: Errors were found"))) {
                dr.getValidationResultsOverview().setValidationTestResult("FAILED");
            }
            dr.getValidationResultsOverview().setValidationServiceVersion(POMVersion.getVersion());
        }
    }

    private static void validateEnvelope(DetailedResult res, Envelope env, List<ConstraintValidatorModule> listConstraintValidatorModule) {
        if (env == null) {
            return;
        }
        List<Notification> ln = new ArrayList<Notification>();
        for (ConstraintValidatorModule cvm : listConstraintValidatorModule) {
            Envelope.validateByModule(env, "/Envelope", cvm, ln);
        }
        if (res.getMDAValidation() == null) {
            res.setMDAValidation(new MDAValidation());
        }
        for (Notification notification : ln) {
            res.getMDAValidation().getWarningOrErrorOrNote().add(notification);
        }
    }

    private static void validateSubscribe(DetailedResult res, SubscribeType sub, List<ConstraintValidatorModule> listConstraintValidatorModule) {
        if (sub == null) {
            return;
        }
        List<Notification> ln = new ArrayList<Notification>();
        for (ConstraintValidatorModule cvm : listConstraintValidatorModule) {
            SubscribeType.validateByModule(sub, "/Subscribe", cvm, ln);
        }
        if (res.getMDAValidation() == null) {
            res.setMDAValidation(new MDAValidation());
        }
        for (Notification notification : ln) {
            res.getMDAValidation().getWarningOrErrorOrNote().add(notification);
        }
    }

    private static void validateSubscribeResponse(DetailedResult res, SubscribeResponseType subResp, List<ConstraintValidatorModule>
            listConstraintValidatorModule) {
        if (subResp == null) {
            return;
        }
        List<Notification> ln = new ArrayList<Notification>();
        for (ConstraintValidatorModule cvm : listConstraintValidatorModule) {
            SubscribeResponseType.validateByModule(subResp, "/SubscribeResponse", cvm, ln);
        }
        if (res.getMDAValidation() == null) {
            res.setMDAValidation(new MDAValidation());
        }
        for (Notification notification : ln) {
            res.getMDAValidation().getWarningOrErrorOrNote().add(notification);
        }
    }

    private static void validateUnSubscribe(DetailedResult res, UnsubscribeType unsub, List<ConstraintValidatorModule>
            listConstraintValidatorModule) {
        if (unsub == null) {
            return;
        }
        List<Notification> ln = new ArrayList<Notification>();
        for (ConstraintValidatorModule cvm : listConstraintValidatorModule) {
            UnsubscribeType.validateByModule(unsub, "/Unsubscribe", cvm, ln);
        }
        if (res.getMDAValidation() == null) {
            res.setMDAValidation(new MDAValidation());
        }
        for (Notification notification : ln) {
            res.getMDAValidation().getWarningOrErrorOrNote().add(notification);
        }
    }

    private static void validateUnSubscribeResponse(DetailedResult res, UnsubscribeResponseType unsubResp, List<ConstraintValidatorModule>
            listConstraintValidatorModule) {
        if (unsubResp == null) {
            return;
        }
        List<Notification> ln = new ArrayList<Notification>();
        for (ConstraintValidatorModule cvm : listConstraintValidatorModule) {
            UnsubscribeResponseType.validateByModule(unsubResp, "/UnsubscribeResponse", cvm, ln);
        }
        if (res.getMDAValidation() == null) {
            res.setMDAValidation(new MDAValidation());
        }
        for (Notification notification : ln) {
            res.getMDAValidation().getWarningOrErrorOrNote().add(notification);
        }
    }

    private static void validateNotify(DetailedResult res, NotifyType notif, List<ConstraintValidatorModule> listConstraintValidatorModule) {
        if (notif == null) {
            return;
        }
        List<Notification> ln = new ArrayList<Notification>();
        for (ConstraintValidatorModule cvm : listConstraintValidatorModule) {
            NotifyType.validateByModule(notif, "/Notify", cvm, ln);
        }
        if (res.getMDAValidation() == null) {
            res.setMDAValidation(new MDAValidation());
        }
        for (Notification notification : ln) {
            res.getMDAValidation().getWarningOrErrorOrNote().add(notification);
        }
    }

    private static void validateToSchema(DetailedResult res, String sub) {
        res.setDocumentWellFormed(DSUBXMLValidation.isXMLWellFormed(sub));
        try {
            res.setDocumentValidXSD(DSUBXMLValidation.isXDSValid(sub));
        } catch (Exception e) {
            log.info("error when validation to schema.", e);
        }
    }
}
