package net.ihe.gazelle.xdstar.util;

import javax.ejb.Stateless;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.Name;

@Stateless
@Name("PUProviderExtended")
@GenerateInterface(value="PUProviderExtendedLocal")
public class PUProviderExtended implements PUProviderExtendedLocal{



    @PersistenceUnit(unitName="XDStarClient-PersistenceUnit")
    private EntityManagerFactory entityManagerFactory;

    @Factory(scope=ScopeType.APPLICATION, value="entityManagerFactoryExtended")
    public EntityManagerFactory getFactory() {return entityManagerFactory;}



}