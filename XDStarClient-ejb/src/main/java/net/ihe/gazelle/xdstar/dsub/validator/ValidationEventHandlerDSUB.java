package net.ihe.gazelle.xdstar.dsub.validator;

import java.util.ArrayList;

import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.ValidationEventLocator;

import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.xdstar.validator.ValidatorException;


public class ValidationEventHandlerDSUB implements ValidationEventHandler {
	
	final private ValidatorException vexp = new ValidatorException("Errors when trying to parse the document", new ArrayList<Notification>());

	public ValidatorException getVexp() {
		return vexp;
	}

	@Override
	public boolean handleEvent(ValidationEvent event) {
		return ValidationEventHandlerDSUB.handleEvent(event, vexp);
	}
	
	private static boolean handleEvent(ValidationEvent event, ValidatorException vexp) {
		 if (event.getSeverity() != ValidationEvent.WARNING) {  
           ValidationEventLocator vel = event.getLocator();
           Notification not = new net.ihe.gazelle.validation.Error();
           not.setDescription("Line:Col[" + vel.getLineNumber() +  
                   ":" + vel.getColumnNumber() +  
                   "]:" +event.getMessage());
           not.setTest("message_parsing");
           not.setLocation("All the document");
           vexp.getDiagnostic().add(not);
       }  
       return true; 
	}

}
