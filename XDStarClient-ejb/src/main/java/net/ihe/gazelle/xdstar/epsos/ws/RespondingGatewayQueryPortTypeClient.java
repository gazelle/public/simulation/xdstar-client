
package net.ihe.gazelle.xdstar.epsos.ws;

/**
 * Please modify this class to meet your needs
 * This class is not complete
 */

import net.ihe.gazelle.query.AdhocQueryRequestType;
import net.ihe.gazelle.query.AdhocQueryResponseType;
import net.ihe.gazelle.xdstar.common.model.AbstractMessage;
import net.ihe.gazelle.xdstar.comon.util.XDSNamespacePrefixMapper;
import net.ihe.gazelle.xdstar.core.MessageSenderCommon;
import net.ihe.gazelle.xdstar.core.soap.SOAPRequestBuilder;
import net.ihe.gazelle.xdstar.validator.MetadataValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import static org.apache.commons.lang.CharEncoding.UTF_8;

/**
 * This class was generated by Apache CXF 2.7.6
 * 2013-09-11T10:33:05.914+02:00
 * Generated source version: 2.7.6
 */
public final class RespondingGatewayQueryPortTypeClient {

    private static final Logger LOG = LoggerFactory.getLogger(RespondingGatewayQueryPortTypeClient.class);

    public String send(AdhocQueryRequestType batchRequest, String wsdlLocation) throws Exception {
        MessageSenderCommon msc = new MessageSenderCommon();
        String messageToSend = this.getAdhocQueryAsString(batchRequest);
        if (messageToSend.contains("?>")) {
            messageToSend = messageToSend.substring(messageToSend.indexOf("?>") + 2);
        }
        messageToSend = SOAPRequestBuilder.createSOAPMessage(messageToSend, false, null, null, null,
                wsdlLocation, "urn:ihe:iti:2007:CrossGatewayQuery", false);
        AbstractMessage request = new AbstractMessage();
        String res = msc.sendMessage(wsdlLocation,
                messageToSend.getBytes(StandardCharsets.UTF_8), request, "application/soap+xml", null).getObject2();
        return res;

    }

    public AdhocQueryResponseType sendAndGetAdhocQueryResponse(AdhocQueryRequestType batchRequest, String wsdlLocation) throws Exception {
        String res = this.send(batchRequest, wsdlLocation);
        try {
            res = MetadataValidator.extractNode(res, "AdhocQueryResponse", "urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0");
        } catch (Exception e) {
            LOG.error("" + e.getMessage());
        }
        AdhocQueryResponseType aqr = this.getAdhocQueryResponseTypeFromString(res);
        return aqr;
    }

    protected AdhocQueryResponseType getAdhocQueryResponseTypeFromString(String aqr) {
        if (aqr != null) {
            try {
                JAXBContext jc = JAXBContext.newInstance(AdhocQueryResponseType.class);
                Unmarshaller m = jc.createUnmarshaller();
                byte[] val = aqr.getBytes(StandardCharsets.UTF_8);
                return (AdhocQueryResponseType) m.unmarshal(new ByteArrayInputStream(val));
            } catch (JAXBException e) {
                LOG.error("" + e.getMessage());
            }
        }
        return null;
    }

    protected String getAdhocQueryAsString(Object aqr) {
        try {
            JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.query");
            Marshaller m = jc.createMarshaller();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new XDSNamespacePrefixMapper());
            m.marshal(aqr, baos);
            return baos.toString(UTF_8);
        } catch (UnsupportedEncodingException e) {
            LOG.error("" + e.getMessage());
        } catch (JAXBException e) {
            LOG.error("" + e.getMessage());
        }
        return null;
    }
}
