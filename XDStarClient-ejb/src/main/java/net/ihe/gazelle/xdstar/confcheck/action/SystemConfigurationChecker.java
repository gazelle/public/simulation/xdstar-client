package net.ihe.gazelle.xdstar.confcheck.action;

import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.xdstar.core.MessageSenderCommon;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;


/**
 * @author abderrazek boufahja
 */
@NamedQuery(name = "SelectLC", query = "SELECT sc FROM commonSystemConfiguration sc")
@Stateless(name = "systemConfigurationChecker")
@Name("systemConfigurationChecker")
public class SystemConfigurationChecker implements SystemConfigurationCheckerLocal {


    private static Logger log = LoggerFactory.getLogger(SystemConfigurationChecker.class);

    @Resource
    private SessionContext sessionContext;

    @PersistenceContext(unitName = "XDStarClient-PersistenceUnit")
    private EntityManager em;

    //TODO FIXME
//	@RolesAllowed({"admin_role"})
    public void createTimer(Date dateBegin, long period) {
        TimerService ts = sessionContext.getTimerService();
        ts.createTimer(dateBegin, period, null);
    }

    public String getTimeRemaining(Timer currentTimer) {
        String res = null;
        if (currentTimer != null) {
            res = String.valueOf(currentTimer.getTimeRemaining());
        }
        return res;
    }

    public String getNextTimeout(Timer currentTimer) {
        String res = null;
        if (currentTimer != null) {
            res = String.valueOf(currentTimer.getNextTimeout());
        }
        return res;
    }

    public void cancel(Timer currentTimer) {
        if (currentTimer != null) {
            currentTimer.cancel();
        }
    }

    @SuppressWarnings({"rawtypes"})
    public Collection<Timer> getRelatedTimers() {
        List<Timer> lt = new ArrayList<Timer>();
        for (Object tim : sessionContext.getTimerService().getTimers()) {
            if (tim instanceof Timer) {
                lt.add((Timer) tim);
            } else if (tim instanceof java.util.HashSet) {
                for (Object timer : (java.util.HashSet) tim) {
                    if (timer instanceof Timer) {
                        lt.add((Timer) tim);
                    }
                }
            }
        }
        return lt;
    }

    public void checkSelectedConfiguration(SystemConfiguration sysconf) {
        (new MessageSenderCommon()).checkSelectedConfiguration(sysconf);
    }

    //TODO FIXME
//	@RolesAllowed({"admin_role"})
    public void checkAllConfigurations() {
        checkAllConfigurationsInTheTool();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "All systems' configuration are checked.");
    }

    @SuppressWarnings("unchecked")
    private void checkAllConfigurationsInTheTool() {
        List<SystemConfiguration> listSC = (List<SystemConfiguration>) em.createQuery("SELECT sc FROM commonSystemConfiguration sc").getResultList();
        for (SystemConfiguration systemConfiguration : listSC) {
            checkSelectedConfiguration(systemConfiguration);
        }
        FacesMessages.instance().clearGlobalMessages();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "All configurations checked !");
    }

    @Timeout
    public void periodicCheckOfAllConfigurations(Timer timer) {
        log.info("-- configurations were checked at " + new Date());
        this.checkAllConfigurationsInTheTool();
    }

}
