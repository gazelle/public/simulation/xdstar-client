package net.ihe.gazelle.xdstar.testplan.action;

import net.ihe.gazelle.testplan.TestPlanType;
import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;
import net.ihe.gazelle.xdstar.testplan.model.TransactionTemplate;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@Name("transTemplateManager")
@Scope(ScopeType.PAGE)
public class TransTemplateManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static Logger log = LoggerFactory.getLogger(TransTemplateManager.class);

    private TransactionTemplate selectedTransactionTemplate;

    private boolean editTransactionTemplate = false;

    public TransactionTemplate getSelectedTransactionTemplate() {
        return selectedTransactionTemplate;
    }

    public void setSelectedTransactionTemplate(
            TransactionTemplate selectedTransactionTemplate) {
        this.selectedTransactionTemplate = selectedTransactionTemplate;
    }

    public boolean isEditTransactionTemplate() {
        return editTransactionTemplate;
    }

    public void setEditTransactionTemplate(boolean editTransactionTemplate) {
        this.editTransactionTemplate = editTransactionTemplate;
    }

    @SuppressWarnings("unchecked")
    public List<TransactionTemplate> getTransactionTemplates() {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        return em.createQuery("Select tt FROM transactionTemplate tt").getResultList();
    }

    public void editNewTransactionTemplate() {
        this.selectedTransactionTemplate = new TransactionTemplate();
        this.editTransactionTemplate = true;
    }

    public void editTransactionTemplate(TransactionTemplate tpd) {
        this.selectedTransactionTemplate = tpd;
        this.editTransactionTemplate = true;
    }

    public void saveSelectedTransactionTemplate() {
        try {
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            em.merge(selectedTransactionTemplate);
            em.flush();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "TransactionTemplate was saved.");
        } catch (PersistenceException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Fail to save TransactionTemplate !");
            log.error("saveSelectedTransactionTemplate : " + e.getMessage());
        }
    }


    public void deleteSelectedTransactionTemplate() {
        if (this.selectedTransactionTemplate != null) {
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            em.remove(em.find(TransactionTemplate.class, this.selectedTransactionTemplate.getId()));
            em.flush();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Test Plan Domain was deleted.");
        }
    }

    public List<MetadataMessageType> getListMetadataMessageTypes() {
        return Arrays.asList(MetadataMessageType.values());
    }

    @Destroy
    public void destroy() {
        log.info("destroy generatorManager..");
    }

}
