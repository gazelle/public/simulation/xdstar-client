package net.ihe.gazelle.xdstar.patient.action;

import net.ihe.gazelle.demographic.ws.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author abderrazek boufahja
 */
public class DDSClient {

    private static final Logger LOG = LoggerFactory.getLogger(DDSClient.class);

    public DDSClient() {
    }

    public static List<String> getListCountryCode(String targetEndpoint) {
        GetListCountryCode glcc = new GetListCountryCode();
        List<CountryCode> lcc = null;
        try {
            DemographicDataServerServiceStub ddss = new DemographicDataServerServiceStub(targetEndpoint);
            ddss._getServiceClient().getOptions().setProperty(org.apache.axis2.Constants.Configuration.DISABLE_SOAP_ACTION, true);
            GetListCountryCodeE inGetListCountryCodeE = new GetListCountryCodeE();
            inGetListCountryCodeE.setGetListCountryCode(glcc);
            GetListCountryCodeResponseE inGetListCountryCodeResponseE = ddss.getListCountryCode(inGetListCountryCodeE);
            lcc = Arrays.asList(inGetListCountryCodeResponseE.getGetListCountryCodeResponse().get_return());
        } catch (Exception e) {
            LOG.error("" + e.getMessage());
        }
        List<String> lccs = new ArrayList<String>();
        if (lcc != null) {
            for (CountryCode cc : lcc) {
                lccs.add(cc.getPrintableName());
            }
        }
        return lccs;
    }

    //abe: firstNameNearby and lastNameNearby are no more available in DDS, remove parameters
    public static List<Patient> generatePatient(String countryCode, String firstNameLike, String lastNameLike, int numberPatient, String
            targetEndpoint) {
        List<Patient> list_Patients = new ArrayList<Patient>();
        if (countryCode != null) {
            int i = 0;
            Patient pat = null;
            ReturnPatient inReturnPatient = new ReturnPatient();
            inReturnPatient.setCountryCode(countryCode);
            inReturnPatient.setAddressOption(true);
            inReturnPatient.setBirthDayOption(true);
            inReturnPatient.setFirstNameLike(firstNameLike);
            inReturnPatient.setGenderDescription("");
            inReturnPatient.setLastNameLike(lastNameLike);
            inReturnPatient.setLastNameOption(true);
            inReturnPatient.setRaceOption(true);
            inReturnPatient.setReligionOption(true);
            inReturnPatient.setFirstNameOption(true);
            while (i < numberPatient) {
                try {
                    DemographicDataServerServiceStub ddss = new DemographicDataServerServiceStub(targetEndpoint);
                    ddss._getServiceClient().getOptions().setProperty(org.apache.axis2.Constants.Configuration.DISABLE_SOAP_ACTION, true);
                    ReturnPatientE inReturnPatientE = new ReturnPatientE();
                    inReturnPatientE.setReturnPatient(inReturnPatient);
                    ReturnPatientResponseE inReturnPatientResponseE = ddss.returnPatient(inReturnPatientE);
                    pat = inReturnPatientResponseE.getReturnPatientResponse().get_return();
                    LOG.info(inReturnPatientResponseE.getReturnPatientResponse().get_return().getPerson().getFirstNameSex().getFirstName());
                } catch (Exception e) {
                    LOG.error("" + e.getMessage());
                }
                if (pat != null) {
                    list_Patients.add(pat);
                    pat = null;
                }
                i++;
            }
        }
        return list_Patients;
    }

    public static void main(String[] args) {
        List<Patient> lp = DDSClient.generatePatient("FR", null, null, 2,
                "https://gazelle.ihe.net/DemographicDataServer-ejb/DemographicDataServerService/DemographicDataServer?wsdl");
        List<String> cclist = getListCountryCode("https://gazelle.ihe" +
                ".net/DemographicDataServer-ejb/DemographicDataServerService/DemographicDataServer?wsdl");
        LOG.info(String.valueOf(lp.size()));
        LOG.info(String.valueOf(cclist.size()));
        LOG.info(cclist.get(0));
    }


}
