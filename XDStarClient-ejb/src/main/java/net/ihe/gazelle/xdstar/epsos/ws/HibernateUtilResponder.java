package net.ihe.gazelle.xdstar.epsos.ws;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.hibernate.Session;
import org.hibernate.ejb.Ejb3Configuration;

/**
 * The only purpose of this class is to provide an EntityManager
 * 
 * @author Abderrrazek Boufahja
 */
public class HibernateUtilResponder {

    private static EntityManagerFactory entityManagerFactory;
    private static final Object emfSync = new Object();

    public static EntityManager buildEntityManager() {
        createEntityManagerFactory();

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        return entityManager;
    }

    private static void createEntityManagerFactory() {
        if (entityManagerFactory == null) {
            synchronized (emfSync) {
                if (entityManagerFactory == null) {
                    Ejb3Configuration cfg = new Ejb3Configuration();
                    cfg.configure("META-INF/responder.cfg.xml");
                    entityManagerFactory = cfg.createEntityManagerFactory();
                }
            }
        }
    }

    public static void releaseEntityManager(EntityManager entityManager) {
    	entityManager.flush();
        entityManager.getTransaction().commit();
        entityManager.clear();
        entityManager.close();
    }

    public static void releaseEntityManagerFactory() {
        if (entityManagerFactory != null) {
            synchronized (emfSync) {
                if (entityManagerFactory != null) {
                    entityManagerFactory.close();
                    entityManagerFactory = null;
                }
            }
        }
    }

    public static Session getSession(EntityManager entityManager) {
        if (entityManager != null) {
            Object delegate = entityManager.getDelegate();
            if (delegate instanceof Session) {
                Session session = (Session) delegate;
                return session;
            }
        }
        return null;
    }

    public static EntityManagerFactory getEntityManagerFactory() {
        createEntityManagerFactory();
        return entityManagerFactory;
    }

}
