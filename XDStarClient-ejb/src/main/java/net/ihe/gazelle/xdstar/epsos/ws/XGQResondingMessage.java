package net.ihe.gazelle.xdstar.epsos.ws;

import java.io.Serializable;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityManager;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.xdstar.common.model.RespondingMessage;

@Entity(name="epSOSxgqMessage")
@DiscriminatorValue("EPSOSXGQ")
public class XGQResondingMessage extends RespondingMessage implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static XGQResondingMessage getResondingMessageByIpWhereSentMessageContentIsNull(String ip, EntityManager em) {
		if (ip != null){
			HQLQueryBuilder<XGQResondingMessage> hh = new HQLQueryBuilder<XGQResondingMessage>(em, XGQResondingMessage.class);
			XGQResondingMessageQuery quer = new XGQResondingMessageQuery(hh);
			quer.ip().eq(ip);
			quer.timeStamp().order(false);
			quer.sentMessageContent().isNull();
			List<XGQResondingMessage> res = quer.getList();
			if (res == null){
				return null;
			}
			return res.get(0);
		}
		return null;
	}

}
