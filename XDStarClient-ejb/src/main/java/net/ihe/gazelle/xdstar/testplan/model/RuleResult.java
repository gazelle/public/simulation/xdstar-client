package net.ihe.gazelle.xdstar.testplan.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import net.ihe.gazelle.testplan.RuleType;

/**
 * 
 * @author abderrazek boufahja
 *
 */
@Entity
@Table(name="tp_rule_result", schema = "public")
@SequenceGenerator(name="tp_rule_result_sequence", sequenceName="tp_rule_result_id_seq", allocationSize=1)
public class RuleResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@NotNull
	@GeneratedValue(generator="tp_rule_result_sequence", strategy=GenerationType.SEQUENCE)
	@Column(name="id", nullable = false, unique = true)
	private Integer id;
	
	@ManyToOne(targetEntity=RuleType.class)
	@JoinColumn(name="rule_id")
	private RuleType parentRule;
	
	@Column(name="result")
	private ResultType result;
	
	@ManyToOne
	@JoinColumn(name="test_step_instance_id")
	private TestStepInstance testStepInstance;
	
	/**
	 * Constructor
	 * 
	 */
	public RuleResult()
	{
		
	}


	/*****************************************************************************
	 * 
	 * Getters and Setters
	 * 
	 ******************************************************************************/
	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}
	
	public TestStepInstance getTestStepInstance() {
		return testStepInstance;
	}


	public void setTestStepInstance(TestStepInstance testStepInstance) {
		this.testStepInstance = testStepInstance;
	}

	public RuleType getParentRule() {
		return parentRule;
	}


	public void setParentRule(RuleType parentRule) {
		this.parentRule = parentRule;
	}


	public ResultType getResult() {
		return result;
	}


	public void setResult(ResultType result) {
		this.result = result;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((this.result == null) ? 0 : this.result.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RuleResult other = (RuleResult) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (result != other.result)
			return false;
		return true;
	}

	
}
