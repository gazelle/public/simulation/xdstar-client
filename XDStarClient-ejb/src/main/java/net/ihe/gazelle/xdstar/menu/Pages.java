package net.ihe.gazelle.xdstar.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.Page;

public enum Pages implements Page {

    HOME("/home.seam", null, "XDStarClient", Authorizations.ALL),
    LIST_USAGES("/admin/listUsages.seam", null, "net.ihe.gazelle.xdstar.AffinityDomainTransactions", Authorizations.ADMIN),
    LIST_CLASSIFICATIONS("/admin/listClassifications.seam", null, "net.ihe.gazelle.xdstar.Classifications", Authorizations.ADMIN),
    LIST_EXTERNAL_IDENTIFIERS("/admin/listExternalIdentifiers.seam", null, "net.ihe.gazelle.xdstar.ExternalIdentifiers", Authorizations.ADMIN),
    LIST_EXTRINSIC_OBJECTS("/admin/listExtrinsicObjects.seam", null, "net.ihe.gazelle.xdstar.ExtrinsicObjects", Authorizations.ADMIN),
    LIST_REGISTRY_PACKAGES("/admin/listRegistryPackages.seam", null, "net.ihe.gazelle.xdstar.RegistryPackages", Authorizations.ADMIN),
    LIST_ADHOC_QUERIES("/admin/listAdhocQuerys.seam", null, "net.ihe.gazelle.xdstar.AdhocQueries", Authorizations.ADMIN),
    LIST_REGISTRY_OBJECT_LIST("/admin/listRegistryObjectList.seam", null, "RegistryObjectList", Authorizations.ADMIN),
    VALIDATOR_PACK_ASSOCIATION("/admin/ValidatorPackClass.seam", null, "Validator Pack association", Authorizations.ADMIN),
    BROWSE_UNIT_TESTS("/testing/browseUnitTests.seam", null, "Integration Tests", Authorizations.ADMIN),
    SUT_CONFIGURATIONS("/configuration/common/configurations.seam", null, "net.ihe.gazelle.xdstar.SUTConfigurations", Authorizations.ALL),
    DISPENSATION_SERVICE_INITIALIZE("/provide_register/xdrsrcSimulator.seam?affinityDomainKeyword=epSOS&amp;transactionKeyword=DispensationService:initialize()", null, "net.ihe.gazelle.xdstar.DispensationServiceinitialize", Authorizations.ALL),
    DISPENSATION_SERVICE_DISCARD("/discard/discard.seam?affinityDomainKeyword=epSOS&amp;transactionKeyword=DispensationService:Discard()", null, "DispensationService:Discard()", Authorizations.ALL),
    CONSENT_SERVICE_PUT("/provide_register/xdrsrcSimulator.seam?affinityDomainKeyword=epSOS&amp;transactionKeyword=ConsentService:put()", null, "net.ihe.gazelle.xdstar.ConsentServiceput", Authorizations.ALL),
    CONSENT_SERVICE_DISCARD("/discard/discard.seam?affinityDomainKeyword=epSOS&amp;transactionKeyword=ConsentService:Discard()", null, "ConsentService:Discard()", Authorizations.ALL),
    EPSOS1_TRANSACTION("/xca/epsos1Sim.seam", null, "net.ihe.gazelle.xdstar.EpSOS1OrderServicePatientService", Authorizations.ALL),
    EPSOS_IDENTIFICATION_SERVICE("/tools/xcpdRequest.seam?affinityDomainKeyword=epSOS&amp;transactionKeyword=IdentificationService", null, "net.ihe.gazelle.xdstar.IdentificationService", Authorizations.ALL),
    EPSOS2_FETCH_DOCUMENT_SERVICE("/xca/epsos2.seam", null, "Fetch Document Service", Authorizations.ALL),
    EPSOS2_PROVIDE_DATA_SERVICE("/epsos2/epsos2xdr.seam", null, "Provide Data Service", Authorizations.ALL),
    EPSOS2_PATIENT_IDENTIFICATION_AND_AUTHENTICATION_SERVICE("/epsos2/epsos2xcpd.seam", null, "Patient Identification and Authentication Service", Authorizations.ALL),
    NCP_A_ANALYZER("/epsos2/ncpanalyzer.seam", null, "NCP-A Analyzer", Authorizations.ALL),
    PROVIDE_AND_REGISTER_DOCUMENT_SET_EPSOS_AFFINITY_DOMAIN("/xdsb-epsos/xdsbepsos.seam", null, "ITI-41 [XDS.b - epSOS affinity]", Authorizations.ALL),
    PROVIDE_AND_REGISTER_SET_B_XDS_PROFILE("/provide_register/xdrsrcSimulator.seam?affinityDomainKeyword=IHE_XDS-b&amp;transactionKeyword=ITI-41", null, "net.ihe.gazelle.xdstar.ITI41ProvideAndRegisterSetb", Authorizations.ALL),
    REGISTER_STORED_QUERY_XDSB_PROFILE("/rsq/rsqSimulator.seam?affinityDomainKeyword=IHE_XDS-b&amp;transactionKeyword=ITI-18", null, "net.ihe.gazelle.xdstar.ITI18RegistryStoredQuery", Authorizations.ALL),
    RETRIEVE_DOCUMENT_SET_B_XDSB("/rds/rdsSimulator.seam?affinityDomainKeyword=IHE_XDS-b&amp;transactionKeyword=ITI-43", null, "net.ihe.gazelle.xdstar.ITI43RetrieveDocumentSet", Authorizations.ALL),
    REGISTER_ON_DEMAND_DOCUMENT_ENTRY_ITI_61_XDSB("/odd/iti61.seam", null, "ITI-61 [Register On-Demand Document Entry]", Authorizations.ALL),
    DELETE_DOCUMENT_SET_ITI_62("/discard/discard.seam?affinityDomainKeyword=IHE_XDS-b&amp;transactionKeyword=ITI-62", null, "ITI-62 [Delete Document Set]", Authorizations.ALL),
    CROSS_GATEWAY_QUERY_XCA_PROFILE("/xca/xcaQuery.seam?affinityDomainKeyword=IHE_XCA&amp;transactionKeyword=ITI-38", null, "net.ihe.gazelle.xdstar.ITI38CrossGatewayQuery", Authorizations.ALL),
    CROSS_GATEWAY_RETRIEVE_XCA("/xca/xcaRetrieve.seam?affinityDomainKeyword=IHE_XCA&amp;transactionKeyword=ITI-39", null, "net.ihe.gazelle.xdstar.ITI39CrossGatewayRetrieve", Authorizations.ALL),
    PROVIDE_AND_REGISTER_SET_B("/provide_register/xdrsrcSimulator.seam?affinityDomainKeyword=IHE_XDR&amp;transactionKeyword=ITI-41", null, "net.ihe.gazelle.xdstar.ITI41ProvideAndRegisterSetb", Authorizations.ALL),
    DELETE_DOCUMENT_SET("/discard/discard.seam?affinityDomainKeyword=IHE_XDR-b&amp;transactionKeyword=ITI-62", null, "ITI-62 [Delete Document Set]", Authorizations.ALL),
    PROVIDE_AND_REGISTER_SET_XDW_CONTEXT("/provide_register/xdrsrcSimulator.seam?affinityDomainKeyword=IHE_XDW_XDSb&amp;transactionKeyword=ITI-41", null, "net.ihe.gazelle.xdstar.ITI41ProvideAndRegisterSetb", Authorizations.ALL),
    REGISTER_STORED_QUERY_XDW_CONTEXT("/rsq/rsqSimulator.seam?affinityDomainKeyword=IHE_XDW_XDSb&amp;transactionKeyword=ITI-18", null, "net.ihe.gazelle.xdstar.ITI18RegistryStoredQuery", Authorizations.ALL),
    RETRIEVE_DOCUMENT_SET_B_XDW_CONTEXT("/rds/rdsSimulator.seam?affinityDomainKeyword=IHE_XDW_XDSb&amp;transactionKeyword=ITI-43", null, "net.ihe.gazelle.xdstar.ITI43RetrieveDocumentSet", Authorizations.ALL),
    CROSS_GATEWAY_FETCH("/xcftool/xgf.seam?affinityDomainKeyword=IHE_XCF&amp;transactionKeyword=ITI-63", null, "net.ihe.gazelle.xdstar.ITI63CrossGatewayFetch", Authorizations.ALL),
    MULTI_PATIENT_STORED_QUERY("/rsq/rsqSimulator.seam?affinityDomainKeyword=IHE_MPQ&amp;transactionKeyword=ITI-51", null, "ITI-51 [Multi-Patient Stored Query]", Authorizations.ALL),
    DOCUMENT_METADATA_SUBSCRIBE("/dsub/subscribe.seam", null, "ITI-52 [Document Metada Subscribe - Subscribe Request]", Authorizations.ALL),
    DOCUMENT_METADATA_SUBSCRIBE_UNSUBSCRIBE_TRANSACTION("/dsub/unsubscribe.seam", null, "ITI-52 [Document Metada Subscribe - UnSubscribe Request]", Authorizations.ALL),
    DOCUMENT_METADATA_NOTIFY("/dsub/notify.seam", null, "ITI-53 [Document Metada Notify]", Authorizations.ALL),
    DOCUMENT_METADATA_PUBLISH("/dsub/publish.seam", null, "ITI-54 [Document Metada Publish]", Authorizations.ALL),
    CROSS_GATEWAY_PATIENT_DISCOVERY("/tools/xcpdRequest.seam?affinityDomainKeyword=IHE_ITI&amp;transactionKeyword=ITI-55", null, "net.ihe.gazelle.xdstar.ITI55CrossGatewayPatientDiscovery", Authorizations.ALL),
    PATIENT_LOCATION_QUERY("/tools/xcpdRequest.seam?affinityDomainKeyword=IHE_ITI&amp;transactionKeyword=ITI-56", null, "net.ihe.gazelle.xdstar.ITI56PatientLocationQuery", Authorizations.ALL),
    CONSISTENT_TIME_TEST("/ct/consistentTime.seam", null, "CT", Authorizations.ALL),
    QUERY_PHARMACY_DOCUMENTS_PHARM_1("/pharma/pharm1.seam", null, "net.ihe.gazelle.xdstar.PHARM1QueryPharmacyDocuments", Authorizations.ALL),
    PROVIDE_AND_REGISTER_IMAGING_DOCUMENT_SET("/xdsi/xdsi68.seam", null, "RAD-68 [Provide and Register Imaging Document Set - MTOM/XOP]", Authorizations.ALL),
    RETRIEVE_IMAGING_DOCUMENT_SET("/rad69/rad69.seam", null, "RAD-69 [Retrieve Imaging Document Set]", Authorizations.ALL),
    RAD_55_WADO_RETRIEVE("/xdsi/rad55.seam", null, "RAD-55 [WADO Retrieve]", Authorizations.ALL),
    VALIDATE_XDS_I_B_RETRIEVE_IMAGING_DOCUMENT_SET_REQUEST("/rad69/rad69Val.seam", null, "Validate XDS-I.b RetrieveImagingDocumentSetRequest", Authorizations.ALL),
    RAD_69_IMAGING_DOCUMENT_CONSUMER_INITIATING_IMAGING_GATEWAY("/xcai/xcai69_1.seam", null, "RAD-69 [Imaging Document Consumer ==&gt; Initiating Imaging Gateway]", Authorizations.ALL),
    RAD_69_RESPONDING_IMAGING_GATEWAY_IMAGING_DOCUMENT_SOURCE("/xcai/xcai69_2.seam", null, "RAD-69 [Responding Imaging Gateway ==&gt; Imaging Document Source]", Authorizations.ALL),
    RAD_75_CROSS_GATEWAY_RETRIEVE_IMAGING_DOCUMENT_SET("/xcai/xcai75.seam", null, "RAD-75 [Cross Gateway Retrieve Imaging Document Set]", Authorizations.ALL),
    PROVIDE_AND_REGISTER_SET_B_KSA("/provide_register/xdrsrcSimulator.seam?affinityDomainKeyword=KSA_XDSb&amp;transactionKeyword=ITI-41", null, "net.ihe.gazelle.xdstar.ITI41ProvideAndRegisterSetb", Authorizations.ALL),
    REGISTRY_STORED_QUERY_KSA("/rsq/rsqSimulator.seam?affinityDomainKeyword=KSA_XDSb&amp;transactionKeyword=ITI-18", null, "net.ihe.gazelle.xdstar.ITI18RegistryStoredQuery", Authorizations.ALL),
    RETRIEVE_DOCUMENT_SET_KSA("/rds/rdsSimulator.seam?affinityDomainKeyword=KSA_XDSb&amp;transactionKeyword=ITI-43", null, "net.ihe.gazelle.xdstar.ITI43RetrieveDocumentSet", Authorizations.ALL),
    DELETE_DOCUMENT_SET_KSA("/discard/discard.seam?affinityDomainKeyword=KSA_XDSb&amp;transactionKeyword=ITI-62", null, "ITI-62 [Delete Document Set]", Authorizations.ALL),
    PROVIDE_AND_REGISTER_SET_B_CLR("/provide_register/xdrsrcSimulator.seam?affinityDomainKeyword=KSA_CLR&amp;transactionKeyword=ITI-41", null, "net.ihe.gazelle.xdstar.ITI41ProvideAndRegisterSetb", Authorizations.ALL),
    REGISTRY_STORED_QUERY_CLR("/rsq/rsqSimulator.seam?affinityDomainKeyword=KSA_CLR&amp;transactionKeyword=ITI-18", null, "net.ihe.gazelle.xdstar.ITI18RegistryStoredQuery", Authorizations.ALL),
    RETRIEVE_DOCUMENT_SET_CLR("/rds/rdsSimulator.seam?affinityDomainKeyword=KSA_CLR&amp;transactionKeyword=ITI-43", null, "net.ihe.gazelle.xdstar.ITI43RetrieveDocumentSet", Authorizations.ALL),
    DELETE_DOCUMENT_SET_CLR("/discard/discard.seam?affinityDomainKeyword=KSA_CLR&amp;transactionKeyword=ITI-62", null, "ITI-62 [Delete Document Set]", Authorizations.ALL),
    PROVIDE_AND_REGISTER_SET_B_CLO("/provide_register/xdrsrcSimulator.seam?affinityDomainKeyword=KSA_CLO&amp;transactionKeyword=ITI-41", null, "net.ihe.gazelle.xdstar.ITI41ProvideAndRegisterSetb", Authorizations.ALL),
    REGISTRY_STORED_QUERY_CLO("/rsq/rsqSimulator.seam?affinityDomainKeyword=KSA_CLO&amp;transactionKeyword=ITI-18", null, "net.ihe.gazelle.xdstar.ITI18RegistryStoredQuery", Authorizations.ALL),
    RETRIEVE_DOCUMENT_SET_CLO("/rds/rdsSimulator.seam?affinityDomainKeyword=KSA_CLO&amp;transactionKeyword=ITI-43", null, "net.ihe.gazelle.xdstar.ITI43RetrieveDocumentSet", Authorizations.ALL),
    DELETE_DOCUMENT_SET_CLO("/discard/discard.seam?affinityDomainKeyword=KSA_CLO&amp;transactionKeyword=ITI-62", null, "ITI-62 [Delete Document Set]", Authorizations.ALL),
    PROVIDE_AND_REGISTER_IMAGING_DOCUMENT_SET_SIR("/xdsi/xdsi68.seam?affinityDomainKeyword=KSA_SIR&amp;transactionKeyword=RAD-68", null, "RAD-68 [Provide and Register Imaging Document Set - MTOM/XOP]", Authorizations.ALL),
    RETRIEVE_IMAGING_DOCUMENT_SET_SIR("/rad69/rad69.seam?affinityDomainKeyword=KSA_SIR&amp;transactionKeyword=RAD-69", null, "RAD-69 [Retrieve Imaging Document Set]", Authorizations.ALL),
    RAD_55_WADO_RETRIEVE_SIR("/xdsi/rad55.seam?affinityDomainKeyword=KSA_SIR&amp;transactionKeyword=RAD-55", null, "RAD-55 [WADO Retrieve]", Authorizations.ALL),
    VALIDATE_XDS_I_B_RETRIEVE_IMAGING_DOCUMENT_SET_REQUEST_SIR("/rad69/rad69Val.seam", null, "Validate XDS-I.b RetrieveImagingDocumentSetRequest", Authorizations.ALL),
    ITI_62_DELETE_DOCUMENT_SET_ENDPOINT("/discard/responder.seam", null, "ITI-62 - Delete Document Set", Authorizations.ALL),
    ITI_53_DOCUMENT_METADATA_RECIPIENT_ENDPOINT("/dsub/recipient.seam", null, "ITI-53 - Document Metada Recipient Endpoint", Authorizations.ALL),
    LIST_OF_PATIENTS("/patients/listPatients.seam", null, "List Patients", Authorizations.ALL),
    SAVE_A_NEW_PATIENT("/patients/savePatient.seam", null, "Add Patient", Authorizations.ALL),
    GENERATE_PATIENTS("/patients/patientGeneration.seam", null, "Generate Patients", Authorizations.ALL),
    IMPORT_XDSTOOLS2_KSA_ENDPOINT("/ksa/importClones.seam", null, "Import XDSTOOLS2 KSA endpoint", Authorizations.ADMIN),
    XDS_TEMPLATES_DOCUMENTATION("/docum/templates/listTemplates.seam", null, "net.ihe.gazelle.xdstar.XDSTemplatesDocumentation", Authorizations.ALL),
    XDS_CLASSES_DOCUMENTATION("/docum/classes/listXDSClasses.seam", null, "XDS Classes Documentation", Authorizations.ALL),
    DSUB_CLASSES_DOCUMENTATION("/docum/classes/listDSUBClasses.seam", null, "DSUB Classes Documentation", Authorizations.ALL),
    CONSTRAINTS_SEARCH("/docum/constraints/constraints.seam", null, "Constraints Search", Authorizations.ALL),
    SIMULATOR_AS_INITIATOR("/messages/allMessages.seam", null, "Simulator as Initiator", Authorizations.ALL),
    SIMULATOR_AS_RESPONDER("/messages/allRespondingMessages.seam", null, "Simulator as Responder", Authorizations.ALL),
    TEST_REPORT_RESPONDER("/messages/messageResponder.seam", null, "Test Report Responder", Authorizations.ALL),
    TEST_REPORT_INITIATOR("/messages/message.seam", null, "Test Report Initiator", Authorizations.ALL),
    REGISTRED_XDS_I_B_TRANSACTIONS_FOR_KSA("/ksa/allRespondingMessages.seam", null, "Registred XDS-I.b Transactions For KSA", Authorizations.ALL),
    TEST_PLAN_RESULTS("/testplan/listTestPlanInstances.seam", null, "Test Plan Results", Authorizations.ALL),
    APPLICATION_CONFIGURATIONS("/administration/configure.seam", "gzl-icon-gears", "Application Configuration", Authorizations.ADMIN),
    XCPD_CONFIGURATIONS("/admin/configuration/xcpdConfiguration.seam", "gzl-icon-gears", "net.ihe.gazelle.xdstar.XCPDConfiguration", Authorizations.ADMIN),
    TRANSACTIONS_TYPES("/configuration/tt/transactionTypes.seam", "gzl-icon-files-o", "net.ihe.gazelle.xdstar.TransactionsTypes", Authorizations.ADMIN),
    HOME_COMMUNITY_ID_LIST("/admin/homeCommunity/HomeCommunityList.seam", "gzl-icon-files-o", "net.ihe.gazelle.xdstar.HommeCommunityIdList", Authorizations.ADMIN),
    CONFIGURATIONS_TYPES_LIST("/configuration/type/confTypes.seam", "gzl-icon-files-o", "net.ihe.gazelle.xdstar.ConfigurationsTypesList", Authorizations.ADMIN),
    TEST_PLANS_DEFINITION("/testplan/listTestPlans.seam", "gzl-icon-files-o", "Test Plans Definition", Authorizations.ADMIN),
    TRANSACTION_TEMPLATE("/testplan/listTransactionTemplates.seam", "gzl-icon-files-o", "Transaction-Template", Authorizations.ADMIN),
    CHECK_CONFIGURATION_SCHEDULER("/timer/timers.seam", "gzl-icon-files-o", "CheckConfiguration Scheduler", Authorizations.ADMIN),
    CONSTRAINTS_ADMIN("/docum/upload/uploadConstraints.seam", "gzl-icon-files-o", "Constraints-Admin", Authorizations.ADMIN),
    VALIDATION_STATISTICS("/administration/usageStatistics.seam", "gzl-icon-files-o", "Validation Statistics", Authorizations.ADMIN),
    CAS("/cas/home.seam", null, "gazelle.simulator.loginCAS", Authorizations.ALL),
    USAGE_EDITOR("/admin/editUsage.seam", null, "Usage Editor", Authorizations.ADMIN),
    CLASSIFICATION_METADATA_EDITOR("/admin/editClassificationMetadata.seam", null, "Classification MetaData Editor", Authorizations.ADMIN),
    EXTERNAL_IDENTIFIER_METADATA_EDITOR("/admin/editExternalIdentifierMetadata.seam", null, "ExternalIdentifier MetaData Editor", Authorizations.ADMIN),
    EXTRINSIC_OBJECT_METADATA_EDITOR("/admin/editExtrinsicObjectMetadata.seam", null, "ExtrinsicObject MetaData Editor", Authorizations.ADMIN),
    REGISTRY_PACKAGE_METADATA_EDITOR("/admin/editRegistryPackageMetadata.seam", null, "RegistryPackage MetaData Editor", Authorizations.ADMIN),
    ADHOCQUERY_METADATA_EDITOR("/admin/editAdhocQueryMetadata.seam", null, "AdhocQuery MetaData Editor", Authorizations.ADMIN),
    REGISTRY_OBJECT_LIST_EDITOR("/admin/editRegistryObjectListMetadata.seam", null, "Registry Object List Editor", Authorizations.ADMIN),
    VALIDATOR_PACK_CLASS_EDITOR("/admin/editValidatorPackClass.seam", null, "Validator Pack Class Editor", Authorizations.ADMIN),
    UNIT_TEST_DETAILS("/testing/validatorUnitTestEditor.seam", null, "Create Unit Test", Authorizations.ADMIN),
    UNIT_TEST_LOGS("/testing/validatorUnitTestLogs.seam", null, "Unit Test Logs", Authorizations.ADMIN),
    TEST_PLAN_INSATANCE("/testplan/testPlanInstance.seam", null, "Test Plan Instance", Authorizations.ALL),
    TEST_INSTANCE_SEQUENCE_DIAGRAM("/testInstanceSequenceDiagram.seam", null, "Test Instance Sequence Diagram", Authorizations.ALL),
    HOME_COMMUNITY_ID_EDITOR("/admin/homeCommunity/HomeCommunityEdit.seam", null, "Home CommunityId Editor", Authorizations.ADMIN),
    HOME_COMMUNITY_ID_DETAILS("/admin/homeCommunity/HomeCommunity.seam", null, "Home CommunityId Details", Authorizations.ADMIN),
    TEST_PLAN_TYPE_EDITOR("/editor/testplan/TestPlanType.seam", null, "Test Plan Type Editor", Authorizations.ADMIN),
    TEST_STEP_TYPE_EDITOR("/editor/testplan/TestStepType.seam", null, "Test Step Type Editor", Authorizations.ADMIN),
    INPUT_TYPE_EDITOR("/editor/testplan/InputType.seam", null, "Input Type Editor", Authorizations.ADMIN),
    RULE_TYPE_EDITOR("/editor/testplan/RuleType.seam", null, "Rule Type Editor", Authorizations.ADMIN),
    GLOBAL_INPUT_TYPE_EDITOR("/editor/testplan/GlobalInputType.seam", null, "Global Input Type Editor", Authorizations.ADMIN),
    LOGIN_REDIRECT("/login_redirect.seam", null, "User Not Logged In", Authorizations.ALL),
    HOME_COMMUNITY_SUMMARY("/admin/homeCommunity/HomeCommunitySummary.seam", null, "Home Community Summary", Authorizations.ADMIN),
    ERROR_EXPIRED("/errorExpired.seam", null, "Expired", Authorizations.ALL),
    UPDATE_DOCUMENT_SET("/update_document/update_document_set.seam", null, "Update Document Set", Authorizations.ALL),
    RMU("/update_document/rmu.seam", null, "Restricted Update Document Set", Authorizations.ALL);


    private String link;
    private String icon;
    private String label;
    private Authorization[] authorizations;

    Pages(String link, String icon, String label, Authorization... authorizations) {
        this.link = link;
        this.icon = icon;
        this.label = label;
        this.authorizations = authorizations;
    }

    @Override
    public String getId() {
        return name();
    }

    @Override
    public String getLabel() {
        return label;
    }

    /**
     * with .xhtml extension
     *
     * @return
     */
    @Override
    public String getLink() {
        return link.replace(".seam", ".xhtml");
    }

    @Override
    public String getIcon() {
        return icon;
    }

    @Override
    public Authorization[] getAuthorizations() {
        return authorizations;
    }

    @Override
    public String getMenuLink() {
        return link;
    }
}
