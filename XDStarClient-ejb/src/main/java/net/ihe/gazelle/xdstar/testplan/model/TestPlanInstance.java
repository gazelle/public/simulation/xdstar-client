package net.ihe.gazelle.xdstar.testplan.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import net.ihe.gazelle.testplan.TestPlanType;

/**
 * 
 * @author abderrazek boufahja
 *
 */
@Entity
@Table(name="tp_test_plan_instance", schema = "public")
@SequenceGenerator(name="tp_test_plan_instance_sequence", sequenceName="tp_test_plan_instance_id_seq", allocationSize=1)
public class TestPlanInstance implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@NotNull
	@GeneratedValue(generator="tp_test_plan_instance_sequence", strategy=GenerationType.SEQUENCE)
	@Column(name="id", nullable = false, unique = true)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="test_plan_id")
	private TestPlanType parentTestPlan;
	
	@Column(name="result")
	@Enumerated(EnumType.STRING)
	private ResultType result;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "execution_date")
	private Date executionDate;
	
	@OneToMany(targetEntity=GlobalInputInstance.class, cascade=CascadeType.ALL)
	private List<GlobalInputInstance> globalInputInstance;
	
	@OneToMany(targetEntity=TestStepInstance.class, cascade=CascadeType.ALL, mappedBy="testPlanInstance")
	private List<TestStepInstance> testStepInstance;
	
	/**
	 * Constructor
	 * 
	 */
	public TestPlanInstance()
	{
		
	}


	/*****************************************************************************
	 * 
	 * Getters and Setters
	 * 
	 ******************************************************************************/
	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public ResultType getResult() {
		return result;
	}


	public void setResult(ResultType result) {
		this.result = result;
	}


	public List<TestStepInstance> getTestStepInstance() {
		return testStepInstance;
	}


	public void setTestStepInstance(List<TestStepInstance> testStepInstance) {
		this.testStepInstance = testStepInstance;
	}

	public TestPlanType getParentTestPlan() {
		return parentTestPlan;
	}


	public void setParentTestPlan(TestPlanType parentTestPlan) {
		this.parentTestPlan = parentTestPlan;
	}


	public List<GlobalInputInstance> getGlobalInputInstance() {
		return globalInputInstance;
	}


	public void setGlobalInputInstance(List<GlobalInputInstance> globalInputInstance) {
		this.globalInputInstance = globalInputInstance;
	}


	public Date getExecutionDate() {
		return executionDate;
	}


	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}
	
	@PrePersist
	private void onPersist(){
		this.executionDate = new Date();
	}


}
