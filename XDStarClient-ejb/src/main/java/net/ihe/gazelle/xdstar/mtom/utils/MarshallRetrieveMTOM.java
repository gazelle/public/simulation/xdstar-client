package net.ihe.gazelle.xdstar.mtom.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import net.ihe.gazelle.edit.FileReadWrite;
import net.ihe.gazelle.xds.RetrieveDocumentSetResponseType;

import org.jboss.ws.core.binding.BindingException;
import org.jdom.JDOMException;

import com.sun.xml.ws.api.message.Attachment;
import com.sun.xml.ws.encoding.MimeMultipartParser;

public class MarshallRetrieveMTOM {

//	public static void main(String[] args) throws JAXBException, BindingException, IOException, JDOMException {
//
//		JAXBContext jc = JAXBContext.newInstance(RetrieveDocumentSetResponseType.class);
//		Marshaller marshaller = jc.createMarshaller();
//		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//		marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
//
//		LOG.info("------------------------");
//
//		String content = FileReadWrite.readDoc("src/test/resources/test.txt");
//		//LOG.info(content);
//
//		String el = extractRetrieveDocumentSetResponse(content);
//		LOG.info(el);
//		String cont = "multipart/related; boundary=MIMEBoundaryurn_uuid_828FE994AB5ACD41FA1364973394036; type=\"application/xop+xml\"; start=\"<0.urn:uuid:828FE994AB5ACD41FA1364973394037@apache.org>\"; " +
//				"start-info=\"application/soap+xml\"; action=\"urn:ihe:iti:2007:RetrieveDocumentSet\"";
//
//		RetrieveDocumentSetResponseType obj = transformMTOM2RetrieveDocumentResponse(
//				new ByteArrayInputStream(content.getBytes()), cont, new ByteArrayInputStream(el.getBytes(StandardCharsets.UTF_8)));
//		LOG.info(obj.getDocumentResponse().get(0).getDocument().getContentType());
//		//marshaller.setAttachmentMarshaller(new AttachmentMarshallerImpl());
//		marshaller.marshal(obj, System.out);
//	}
	
	public static RetrieveDocumentSetResponseType transformMTOM2RetrieveDocumentResponse(
			String contentMTOM, String contheader) throws JAXBException {
		String el = extractRetrieveDocumentSetResponse(contentMTOM);
		return transformMTOM2RetrieveDocumentResponse(new ByteArrayInputStream(contentMTOM.getBytes(StandardCharsets.UTF_8)), contheader,
				new ByteArrayInputStream(el.getBytes(StandardCharsets.UTF_8)));
	}

	private static RetrieveDocumentSetResponseType transformMTOM2RetrieveDocumentResponse(
			InputStream contentMTOM, String contheader, InputStream contentRetrieveXML)
			throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(RetrieveDocumentSetResponseType.class);
		//StreamingAttachmentFeature feature = BindingImpl.getDefaultBinding().getFeature(StreamingAttachmentFeature.class);
		MimeMultipartParser parser = new MimeMultipartParser(contentMTOM, contheader, null);
		Map<String, Attachment> parts = parser.getAttachmentParts();
		parser.getRootPart();
		XDStarAttachmentUnmarshaller myunm = new XDStarAttachmentUnmarshaller(parts);
		Unmarshaller unm = jc.createUnmarshaller();
		unm.setAttachmentUnmarshaller(myunm);
		RetrieveDocumentSetResponseType obj = (RetrieveDocumentSetResponseType) unm.unmarshal(contentRetrieveXML);
		return obj;
	}

	private static String extractRetrieveDocumentSetResponse(String ss){
		String res = "";
		Pattern p = Pattern.compile("<([A-Za-z0-9_]*:)?RetrieveDocumentSetResponse.*?RetrieveDocumentSetResponse>",Pattern.MULTILINE|Pattern.DOTALL);
		Matcher mm = p.matcher(ss);
		while (mm.find()){
			return mm.group();
		}
		return res;
	}

}
