package net.ihe.gazelle.xdstar.xdsidb.action;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.faces.context.FacesContext;

import net.ihe.gazelle.common.filter.FilterDataModel;

import org.dcm4chee.xdsib.retrieve.model.RespondingMessage;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Abderrazek Boufahja > INRIA Rennes IHE development Project
 *
 */
@Name("centralArchManager")
@Scope(ScopeType.PAGE)
public class CentralArchManager implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@SuppressWarnings("unused")
	private static Logger log = LoggerFactory.getLogger(CentralArchManager.class);
	
	private XDSIBRespFilter filter;

	private FilterDataModel<RespondingMessage> xdsidbDataModel;

	private Date startDate;

	private Date endDate;
	
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
		this.applyFilter();
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
		this.applyFilter();
	}

	public FilterDataModel<RespondingMessage> getXdsidbDataModel() {
		if (xdsidbDataModel == null){
			xdsidbDataModel = new FilterDataModel<RespondingMessage>(getFilter()){
                    @Override
        protected Object getId(RespondingMessage t) {
        // TODO Auto-generated method stub
        return t.getId();
        }
        };
		}
		return xdsidbDataModel;
	}
	
	public XDSIBRespFilter getFilter() {
		if (filter == null) {
			final FacesContext fc = FacesContext.getCurrentInstance();
			final Map<String, String> requestParameterMap = fc.getExternalContext().getRequestParameterMap();
			filter = new XDSIBRespFilter(requestParameterMap);
		}
		return filter;
	}

	public void setXdsidbDataModel(FilterDataModel<RespondingMessage> xdsidbDataModel) {
		this.xdsidbDataModel = xdsidbDataModel;
	}
	
	private void applyFilter(){
        this.getXdsidbDataModel().resetCache();
        this.getXdsidbDataModel().getFilter().modified();
    }
	
	public void resetFilter(){
		this.setEndDate(null);
		this.setStartDate(null);
		this.getXdsidbDataModel().getFilter().clear();
		this.getXdsidbDataModel().resetCache();
	}


}
