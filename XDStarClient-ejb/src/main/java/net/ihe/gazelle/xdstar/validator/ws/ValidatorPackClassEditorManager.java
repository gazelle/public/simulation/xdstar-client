package net.ihe.gazelle.xdstar.validator.ws;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.paths.HQLSafePathEntity;
import net.ihe.gazelle.metadata.model.AdhocQueryMetadata;
import net.ihe.gazelle.metadata.model.ConstraintSpecification;
import net.ihe.gazelle.metadata.model.RegistryObjectListMetadata;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.xdstar.core.DocumentFileUpload;
import net.ihe.gazelle.xdstar.filter.ValidatorPackClassDataModel;
import net.ihe.gazelle.xdstar.integrationtesting.ValidatorIntegrationTestQuery;
import net.ihe.gazelle.xdstar.validator.*;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.log.Log;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

import javax.ejb.AccessTimeout;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

import static org.apache.commons.lang.CharEncoding.UTF_8;

@Stateful
@Name("validatorPackClassEditorManager")
@Scope(ScopeType.SESSION)
@AccessTimeout(value = 10000)
@GenerateInterface(value = "ValidatorPackClassEditorLocal")
public class ValidatorPackClassEditorManager implements ValidatorPackClassEditorLocal {

    @Logger
    private static Log LOG;

    @PersistenceContext(unitName = "XDStarClient-PersistenceUnit")
    private EntityManager entityManager;

    private ValidatorPackClass selectedValidatorPackClass;

    private ConstraintSpecification selectedConstraintSpecification;

//	private List<ValidatorPackClass> listValidatorPackClass;

    private PackClass selectedPackClass;

    private NistCode selectedMetadataCodes;


    private AdhocQueryMetadata selectedRegistryObjectMetadata;

    private Usage selectedUsage;

    private File uploadedValidatorPackFile;

    private String uploadedFileContentType;

    private ValidatorPackClassDataModel listValidatorPackClass;

    public static void saveValidatorPackClassAsXML(ValidatorPackClass vp, OutputStream os) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(ValidatorPackClass.class);
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        m.marshal(vp, os);
    }

    public static ValidatorPackClass loadValidatorPackClassAsXML(InputStream is) throws JAXBException, UnsupportedEncodingException {
        JAXBContext jc = JAXBContext.newInstance(ValidatorPackClass.class);
        Unmarshaller u = jc.createUnmarshaller();
        Reader reader = new InputStreamReader(is, StandardCharsets.UTF_8);
        ValidatorPackClass mimi = (ValidatorPackClass) u.unmarshal(reader);
        return mimi;
    }

    public void resetFilter() {
        this.getListValidatorPackClass().getFilter().clear();
        this.getListValidatorPackClass().resetCache();
    }

    public ValidatorPackClassDataModel getListValidatorPackClass() {
        if (listValidatorPackClass == null) {
            try {
                HQLSafePathEntity.filterLabelMethodInit.remove(Transaction.class);
                HQLSafePathEntity.filterLabelMethodInit.put(Transaction.class, true);
                HQLSafePathEntity.filterLabelMethods.put(Transaction.class, Transaction.class.getMethod("getKeyword", null));
            } catch (SecurityException e) {
                LOG.info("", e);
            } catch (NoSuchMethodException e) {
                LOG.info("", e);
            }
            listValidatorPackClass = new ValidatorPackClassDataModel();
        }
        return listValidatorPackClass;
    }

    public void setListValidatorPackClass(ValidatorPackClassDataModel listValidatorPackClass) {
        this.listValidatorPackClass = listValidatorPackClass;
    }

    public String getUploadedFileContentType() {
        return uploadedFileContentType;
    }

    public void setUploadedFileContentType(String uploadedFileContentType) {
        this.uploadedFileContentType = uploadedFileContentType;
    }

    public File getUploadedValidatorPackFile() {
        return uploadedValidatorPackFile;
    }

    public void setUploadedValidatorPackFile(File uploadedValidatorPackFile) {
        this.uploadedValidatorPackFile = uploadedValidatorPackFile;
    }

    public void initSelectedValidatorPackClass() {
        this.selectedValidatorPackClass = new ValidatorPackClass();
    }

    public ValidatorPackClass getSelectedValidatorPackClass() {
        return selectedValidatorPackClass;
    }

    public void setSelectedValidatorPackClass(ValidatorPackClass selectedValidatorPackClass) {
        this.selectedValidatorPackClass = selectedValidatorPackClass;
    }

    public void reloadSelectedValidatorPackClass() {
        if ((this.selectedValidatorPackClass != null) && (this.selectedValidatorPackClass.getId() != null)) {
            this.selectedValidatorPackClass = entityManager.find(ValidatorPackClass.class, this.selectedValidatorPackClass.getId());
        }
    }

    public void saveSelectedValidatorPackClass() {

        if (this.selectedValidatorPackClass != null) {
            if (this.selectedValidatorPackClass.getValidatorName() == null) {
                FacesMessages.instance().add(Severity.ERROR, "The association between a validator and Pack Class shall have a name !");
            } else {
                this.selectedValidatorPackClass = entityManager.merge(this.selectedValidatorPackClass);
                entityManager.flush();
                FacesMessages.instance().add(Severity.INFO, "The current association between a validator and Pack Class was saved.");
            }
        }
    }

    public PackClass getSelectedPackClass() {
        return selectedPackClass;
    }

    public void setSelectedPackClass(PackClass selectedPackClass) {
        this.selectedPackClass = selectedPackClass;
    }

    public List<PackClass> getAvailablePackClass() {
        HQLQueryBuilder<PackClass> hh = new HQLQueryBuilder<PackClass>(PackClass.class);
        List<PackClass> listPackClass = hh.getList();
        Collections.sort(listPackClass);
        return listPackClass;

    }

    public void deleteSelectedValidatorPackClass() {
        if (this.selectedValidatorPackClass != null) {
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            ValidatorPackClass uu = em.find(ValidatorPackClass.class, this.selectedValidatorPackClass.getId());
            ValidatorIntegrationTestQuery validatorIntegrationTestQuery = new ValidatorIntegrationTestQuery();
            validatorIntegrationTestQuery.testedValidator().eq(this.selectedValidatorPackClass);

            if (validatorIntegrationTestQuery.getListNullIfEmpty() == null) {
                em.remove(uu);
            } else {
                FacesMessages.instance().add(Severity.ERROR, "The current validator pack class is used by UnitTest and cannot be deleted.");
            }
            this.selectedPackClass = null;
        }
    }

    public int countListValidatorPackClass() {
        return listValidatorPackClass.size();

    }

    public void initListValidatorPackClass() {
        //HQLQueryBuilder<ValidatorPackClass> hql = new HQLQueryBuilder<ValidatorPackClass>(ValidatorPackClass.class);
        this.listValidatorPackClass = new ValidatorPackClassDataModel();
    }

    // --- destroy ---------------
    @Destroy
    @Remove
    public void destroy() {
        LOG.info("destroy validatorPackClassEditorManager..");
    }

    public NamespaceEnum[] getAvailableNamespace() {

        return NamespaceEnum.values();
    }

    public List<NistCode> getAvailableMetadataCodes() {

        HQLQueryBuilder<NistCode> hh = new HQLQueryBuilder<NistCode>(NistCode.class);
        hh.addOrder("nistCodeName", true);
        return hh.getList();
    }

    public ExtractNodeAndTypeEnum[] getAvailableExtractNode() {

        return ExtractNodeAndTypeEnum.values();
    }

    public List<RegistryObjectListMetadata> getAvailableRegistryObjectListMetadatas() {
        HQLQueryBuilder<RegistryObjectListMetadata> hh = new HQLQueryBuilder<RegistryObjectListMetadata>(RegistryObjectListMetadata.class);
        hh.addOrder("displayAttributeName", true);
        return hh.getList();
    }

    public AdhocQueryMetadata getSelectedRegistryObjectMetadata() {
        return selectedRegistryObjectMetadata;
    }

    public void setSelectedRegistryObjectMetadata(AdhocQueryMetadata selectedRegistryObjectMetadata) {
        this.selectedRegistryObjectMetadata = selectedRegistryObjectMetadata;
    }

    public List<AdhocQueryMetadata> getAvailableRegistryObjectMetadatas() {
        HQLQueryBuilder<AdhocQueryMetadata> hh = new HQLQueryBuilder<AdhocQueryMetadata>(AdhocQueryMetadata.class);
        hh.addOrder("displayAttributeName", true);
        return hh.getList();
    }

    public Usage getSelectedUsage() {
        return selectedUsage;
    }

    public void setSelectedUsage(Usage selectedUsage) {
        this.selectedUsage = selectedUsage;
    }

    public void importValidatorPackClass() {
        if (this.uploadedValidatorPackFile != null) {
            try {
                if (uploadedFileContentType.contains("zip")) {
                    List<String> listUploaded = DocumentFileUpload.unzip(this.uploadedValidatorPackFile);
                    for (String string : listUploaded) {
                        ValidatorPackClass newVp = (ValidatorPackClass) loadValidatorPackClassAsXML(new ByteArrayInputStream(string.getBytes
                                (StandardCharsets.UTF_8)));
                        newVp.setListPackClass(ExtractExistingValidationObj.getExistingListPackClass(newVp.getListPackClass()));
                        newVp.setListRegistryObjectMetadata(ExtractExistingValidationObj.getExistingAdhocQueryMetadata(newVp
                                .getListRegistryObjectMetadata()));
                        newVp.setRegistryObjectListMetadata(ExtractExistingValidationObj.getExistingRegistryObjectListMetadata(newVp
                                .getRegistryObjectListMetadata()));
                        newVp.setUsages(ExtractExistingValidationObj.getExistingUsages(newVp.getUsages(), entityManager));
                        newVp.setMetadataCodes(ExtractExistingValidationObj.getExistingMetadataCodes(newVp.getMetadataCodes()));

                        EntityManager em = (EntityManager) Component.getInstance("entityManager");
                        em.merge(newVp);
                        em.flush();
                    }
                } else {
                    ValidatorPackClass newVp = (ValidatorPackClass) loadValidatorPackClassAsXML(new FileInputStream(uploadedValidatorPackFile));
                    newVp.setListPackClass(ExtractExistingValidationObj.getExistingListPackClass(newVp.getListPackClass()));
                    newVp.setListRegistryObjectMetadata(ExtractExistingValidationObj.getExistingAdhocQueryMetadata(newVp
                            .getListRegistryObjectMetadata()));
                    newVp.setRegistryObjectListMetadata(ExtractExistingValidationObj.getExistingRegistryObjectListMetadata(newVp
                            .getRegistryObjectListMetadata()));
                    newVp.setUsages(ExtractExistingValidationObj.getExistingUsages(newVp.getUsages(), entityManager));
                    newVp.setMetadataCodes(ExtractExistingValidationObj.getExistingMetadataCodes(newVp.getMetadataCodes()));

                    EntityManager em = (EntityManager) Component.getInstance("entityManager");
                    em.merge(newVp);
                    em.flush();
                }
            } catch (UnsupportedEncodingException e) {
                FacesMessages.instance().add(Severity.ERROR, "Unsupported encoding of the document," + e.getMessage());
                LOG.info("unsupported encoding : ", e);
            } catch (JAXBException e) {
                FacesMessages.instance().add(Severity.ERROR, "Not able to parse the specified xml : " + e.getMessage());
                LOG.info("jaxb exception : ", e);
            } catch (Exception e) {
                FacesMessages.instance().add(Severity.ERROR, "Problem occurs when parsing the specified xml: " + e.getMessage());
                LOG.info("exception : ", e);
            } finally {
                this.uploadedValidatorPackFile = null;
            }
        }
    }

    public void exportToXml(ValidatorPackClass selectedValidatorPackClass) {

        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        ValidatorPackClass res = em.find(ValidatorPackClass.class, selectedValidatorPackClass.getId());

        if (res != null) {

            JAXBContext jc;
            try {
                jc = JAXBContext.newInstance(ValidatorPackClass.class);
                Marshaller jaxbMarshaller = jc.createMarshaller();
                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                jaxbMarshaller.marshal(res, baos);
                DocumentFileUpload.showFile(new ByteArrayInputStream(baos.toByteArray()), "output.xml", "xml", true);
            } catch (JAXBException e) {
                LOG.error("" + e.getMessage());
            }
        }

    }

    private Map<Integer, String> getMapFilesContent() {
        Map<Integer, String> map = new HashMap<Integer, String>();
        for (ValidatorPackClass vp : this.getListValidatorPackClassSpecifications()) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                saveValidatorPackClassAsXML(vp, baos);
                map.put(vp.getId(), baos.toString(UTF_8));
            } catch (JAXBException e) {
                LOG.error("" + e.getMessage());
            } catch (UnsupportedEncodingException e) {
                LOG.error("" + e.getMessage());
            }
        }
        return map;
    }

    private List<ValidatorPackClass> getListValidatorPackClassSpecifications() {
        ValidatorPackClassQuery vq = new ValidatorPackClassQuery();
        List<ValidatorPackClass> res = vq.getList();
        if (res != null) {
            Collections.sort(res);
        }
        return res;
    }

    public void downloadAll() {
        Map<Integer, String> mapFiles = getMapFilesContent();
        ZipOutputStream zos = null;
        try {
            File zipFile = File.createTempFile("vp_", ".zip");
            zos = new ZipOutputStream(new FileOutputStream(zipFile));
            DocumentFileUpload.zip(mapFiles, zos);
            zos.close();
            DocumentFileUpload.showFile(zipFile, "zip", true);
        } catch (FileNotFoundException e) {
            LOG.error("" + e.getMessage());
        } catch (IOException e) {
            LOG.error("" + e.getMessage());
        } finally {
            if (zos != null) {
                try {
                    zos.close();
                } catch (IOException e) {
                    LOG.error("" + e.getMessage());
                }
            }
        }
    }

    public void uploadListener(FileUploadEvent event) {
        UploadedFile item = event.getUploadedFile();
        String uploadedFilename = item.getName();
        uploadedFileContentType = item.getContentType();
        FileOutputStream fos = null;
        try {
            uploadedValidatorPackFile = File.createTempFile("adq", uploadedFilename);
            fos = new FileOutputStream(uploadedValidatorPackFile);
            fos.write(item.getData());
        } catch (FileNotFoundException e) {
            LOG.error("" + e.getMessage());
        } catch (IOException e) {
            LOG.error("" + e.getMessage());
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    LOG.error("" + e.getMessage());
                }
            }
        }
    }

    public void initSelectedExtraConstraint() {
        this.selectedConstraintSpecification = new ConstraintSpecification();
    }

    public void saveSelectedExtraConstraint() {

        if (this.selectedConstraintSpecification.getId() != null) {
            return;
        } else {
            if (this.selectedValidatorPackClass != null) {
                this.selectedValidatorPackClass.getExtraConstraintSpecifications().add(selectedConstraintSpecification);
            }
        }
    }

    public ConstraintSpecification getSelectedConstraintSpecification() {
        return selectedConstraintSpecification;
    }

    public void setSelectedConstraintSpecification(ConstraintSpecification selectedConstraintSpecification) {
        this.selectedConstraintSpecification = selectedConstraintSpecification;
    }

    public NistCode getSelectedMetadataCodes() {
        return selectedMetadataCodes;
    }

    public void setSelectedMetadataCodes(NistCode selectedMetadataCodes) {
        this.selectedMetadataCodes = selectedMetadataCodes;
    }


}
