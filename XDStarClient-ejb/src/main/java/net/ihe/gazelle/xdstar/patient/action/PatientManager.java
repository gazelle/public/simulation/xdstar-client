package net.ihe.gazelle.xdstar.patient.action;

import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.openempi.hb.PersonInfo;
import net.ihe.gazelle.openempi.hb.Race;
import net.ihe.gazelle.openempi.hb.ReligionType;
import net.ihe.gazelle.openempi.hb.dao.PersonInfoDAO;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.List;

/**
 * @author Abderrazek Boufahja > INRIA Rennes IHE development Project
 */
@Name("patientManagerBean")
@Scope(ScopeType.PAGE)
public class PatientManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8734159233751607345L;

    @SuppressWarnings("unused")
    private static Logger log = LoggerFactory.getLogger(PatientManager.class);

    private PersonInfo selectedPersonInfo;

    public void setSelectedPersonInfo(PersonInfo selectedPersonInfo) {
        this.selectedPersonInfo = selectedPersonInfo;
    }

    public PersonInfo getSelectedPersonInfo() {
        return selectedPersonInfo;
    }

    public void deleteSelectedPersonInfo() {
        if (this.selectedPersonInfo != null) {
            PersonInfoDAO.deletePersonInfo(selectedPersonInfo);
        }
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "The person selected was deleted.");
    }

    public List<Race> getListRaces() {
        return net.ihe.gazelle.openempi.hb.dao.RaceDAO.getListAllRace();
    }

    public List<ReligionType> getListReligion() {
        return net.ihe.gazelle.openempi.hb.dao.ReligionTypeDAO.getListAllReligionType();
    }

    public GazelleListDataModel<PersonInfo> getAllPersonInfo() {
        List<PersonInfo> res = PersonInfoDAO.getListAllPersonInfo();
        GazelleListDataModel<PersonInfo> dm = new GazelleListDataModel<PersonInfo>(res);
        return dm;
    }

    public void saveSelectedPersonInfo() {
        if (this.selectedPersonInfo != null) {
            this.selectedPersonInfo.setFname(this.capitalizeString(this.selectedPersonInfo.getFname()));
            this.selectedPersonInfo.setLname(this.capitalizeString(this.selectedPersonInfo.getLname()));
        }
        PersonInfoDAO.savePersonInfo(selectedPersonInfo);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Your patient was saved.");
    }

    private String capitalizeString(String input) {
        if (input == null) {
            return null;
        }
        return input.toUpperCase();
    }

    public void initializeSelectedPersonInfo() {
        this.selectedPersonInfo = PersonInfoMan.initializeNewPersonInfo();
    }

}
