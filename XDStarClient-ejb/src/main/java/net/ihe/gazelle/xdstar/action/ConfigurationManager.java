package net.ihe.gazelle.xdstar.action;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.Serializable;

@Name("configurationManager")
@Scope(ScopeType.PAGE)
public class ConfigurationManager implements Serializable {

    public static final String HOME_COMMUNITY_ID = "homeCommunityID";
    public static final String SCHEMATRON_VALIDATOR = "schematronValidator";
    public static final String FAMILY = "family";
    public static final String ROOT = "root";
    public static final String STREET = "street";
    public static final String GIVEN = "given";
    public static final String EXTENSION = "extension";
    public static final String CITY = "city";
    public static final String BIRTHDATE = "birthdate";
    public static final String GENDER = "gender";
    public static final String COUNTRY = "country";
    public static final String POSTALCODE = "postalcode";
    public static final String MAIDENNAME = "maidenname";
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static Logger log = LoggerFactory.getLogger(ConfigurationManager.class);

    private ApplicationConfiguration homeCommunityID;
    private ApplicationConfiguration schematronValidator;
    private ApplicationConfiguration family;
    private ApplicationConfiguration root;
    private ApplicationConfiguration street;
    private ApplicationConfiguration given;
    private ApplicationConfiguration extension;
    private ApplicationConfiguration city;
    private ApplicationConfiguration birthdate;
    private ApplicationConfiguration gender;
    private ApplicationConfiguration country;
    private ApplicationConfiguration postalcode;
    private ApplicationConfiguration maidenname;

    public String getHomeCommunityID() {
        if (homeCommunityID == null) {
            this.homeCommunityID = ApplicationConfiguration.getApplicationConfigurationByVariableOrNew(HOME_COMMUNITY_ID);
        }
        return homeCommunityID.getValue();
    }

    public void setHomeCommunityID(String homeCommunityID) {
        this.homeCommunityID.setValue(homeCommunityID);
    }

    public String getSchematronValidator() {
        if (schematronValidator == null) {
            this.schematronValidator = ApplicationConfiguration.getApplicationConfigurationByVariableOrNew(SCHEMATRON_VALIDATOR);
        }
        return schematronValidator.getValue();
    }

    public void setSchematronValidator(String schematronValidator) {
        this.schematronValidator.setValue(schematronValidator);
    }

    public String getFamily() {
        if (family == null) {
            this.family = ApplicationConfiguration.getApplicationConfigurationByVariableOrNew(FAMILY);
        }
        return family.getValue();
    }

    public void setFamily(String family) {
        this.family.setValue(family);
    }

    public String getRoot() {
        if (root == null) {
            this.root = ApplicationConfiguration.getApplicationConfigurationByVariableOrNew(ROOT);
        }
        return root.getValue();
    }

    public void setRoot(String root) {
        this.root.setValue(root);
    }

    public String getStreet() {
        if (street == null) {
            this.street = ApplicationConfiguration.getApplicationConfigurationByVariableOrNew(STREET);
        }
        return street.getValue();
    }

    public void setStreet(String street) {
        this.street.setValue(street);
    }

    public String getGiven() {
        if (given == null) {
            this.given = ApplicationConfiguration.getApplicationConfigurationByVariableOrNew(GIVEN);
        }
        return given.getValue();
    }

    public void setGiven(String given) {
        this.given.setValue(given);
    }

    public String getExtension() {
        if (extension == null) {
            this.extension = ApplicationConfiguration.getApplicationConfigurationByVariableOrNew(EXTENSION);
        }
        return extension.getValue();
    }

    public void setExtension(String extension) {
        this.extension.setValue(extension);
    }

    public String getCity() {
        if (city == null) {
            this.city = ApplicationConfiguration.getApplicationConfigurationByVariableOrNew(CITY);
        }
        return city.getValue();
    }

    public void setCity(String city) {
        this.city.setValue(city);
    }

    public String getBirthdate() {
        if (birthdate == null) {
            this.birthdate = ApplicationConfiguration.getApplicationConfigurationByVariableOrNew(BIRTHDATE);
        }
        return birthdate.getValue();
    }

    public void setBirthdate(String birthdate) {
        this.birthdate.setValue(birthdate);
    }

    public String getGender() {
        if (gender == null) {
            this.gender = ApplicationConfiguration.getApplicationConfigurationByVariableOrNew(GENDER);
        }
        return gender.getValue();
    }

    public void setGender(String gender) {
        this.gender.setValue(gender);
    }

    public String getCountry() {
        if (country == null) {
            this.country = ApplicationConfiguration.getApplicationConfigurationByVariableOrNew(COUNTRY);
        }
        return country.getValue();
    }

    public void setCountry(String country) {
        this.country.setValue(country);
    }

    public String getPostalcode() {
        if (postalcode == null) {
            this.postalcode = ApplicationConfiguration.getApplicationConfigurationByVariableOrNew(POSTALCODE);
        }
        return postalcode.getValue();
    }

    public void setPostalcode(String postalcode) {
        this.postalcode.setValue(postalcode);
    }

    public String getMaidenname() {
        if (maidenname == null) {
            this.maidenname = ApplicationConfiguration.getApplicationConfigurationByVariableOrNew(MAIDENNAME);
        }
        return maidenname.getValue();
    }

    public void setMaidenname(String maidenname) {
        this.maidenname.setValue(maidenname);
    }

    public void saveVariables() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        homeCommunityID = entityManager.merge(homeCommunityID);
        schematronValidator = entityManager.merge(schematronValidator);
        family = entityManager.merge(family);
        root = entityManager.merge(root);
        street = entityManager.merge(street);
        given = entityManager.merge(given);
        extension = entityManager.merge(extension);
        city = entityManager.merge(city);
        birthdate = entityManager.merge(birthdate);
        gender = entityManager.merge(gender);
        country = entityManager.merge(country);
        postalcode = entityManager.merge(postalcode);
        maidenname = entityManager.merge(maidenname);
        entityManager.flush();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Configuration saved !");
    }
}
