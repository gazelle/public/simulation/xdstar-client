package net.ihe.gazelle.xdstar.kproject;

import gov.nist.registry.xdstools2.server.gazelle.actorConfig.IEntryFactory;

public class OidEntryFactory implements IEntryFactory {

	public OidEntry mkEntry(String line) {
		return new OidEntry(line);
	}
	
}
