package net.ihe.gazelle.xdstar.testplan.model;

/**
 * 
 * @author abderrazek boufahja
 *
 */
public enum StatusType {
	
	CREATED("CREATED"), STARTED("STARTED"), ENDED("ENDED");
	
	private String value;
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	private StatusType(String val) {
		this.value = val;
	}

}
