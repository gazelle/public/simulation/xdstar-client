package net.ihe.gazelle.xdstar.del.ws;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import net.ihe.gazelle.xdstar.common.model.RespondingMessage;

/**
 * 
 * @author aboufahj
 *
 */
@Entity(name="XDSDeleteMessage")
@DiscriminatorValue("XDSDEL")
public class XDSDeleteMessage extends RespondingMessage implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
