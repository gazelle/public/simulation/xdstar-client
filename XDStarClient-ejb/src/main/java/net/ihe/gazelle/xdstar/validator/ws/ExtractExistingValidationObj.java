package net.ihe.gazelle.xdstar.validator.ws;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import net.ihe.gazelle.metadata.model.AdhocQueryMetadata;
import net.ihe.gazelle.metadata.model.AdhocQueryMetadataQuery;
import net.ihe.gazelle.metadata.model.ExtrinsicObjectMetadata;
import net.ihe.gazelle.metadata.model.ExtrinsicObjectMetadataQuery;
import net.ihe.gazelle.metadata.model.RegistryObjectListMetadata;
import net.ihe.gazelle.metadata.model.RegistryObjectListMetadataQuery;
import net.ihe.gazelle.metadata.model.RegistryPackageMetadata;
import net.ihe.gazelle.metadata.model.RegistryPackageMetadataQuery;
import net.ihe.gazelle.metadata.model.describer.ExtrinsicObjectMetadataDescriber;
import net.ihe.gazelle.metadata.model.describer.RegistryPackageMetadataDescriber;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomainQuery;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.common.tf.model.TransactionQuery;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.simulator.sut.model.UsageQuery;
import net.ihe.gazelle.xdstar.validator.NistCode;
import net.ihe.gazelle.xdstar.validator.NistCodeQuery;
import net.ihe.gazelle.xdstar.validator.PackClass;
import net.ihe.gazelle.xdstar.validator.PackClassQuery;

public class ExtractExistingValidationObj {

    public static List<Usage> getExistingUsages(List<Usage> usages, EntityManager entityManager) {
	List<Usage> res = null;
	if(usages != null){
	    res = new ArrayList<Usage>();
	    for (Usage currentUsage : usages) {

		Boolean usageIsInDB = true;
		TransactionQuery tq = new TransactionQuery();
		if (currentUsage.getTransaction() != null) {
			tq.keyword().eq(currentUsage.getTransaction().getKeyword());
		}
		Transaction transactionBdd = tq.getUniqueResult();

		if(transactionBdd == null){
		    usageIsInDB = false;
		    entityManager.persist(currentUsage.getTransaction());
		    entityManager.flush();

		}else {
		    currentUsage.setTransaction(transactionBdd);
		}

		AffinityDomainQuery aq = new AffinityDomainQuery();
		if (currentUsage.getAffinity() != null) {
			aq.labelToDisplay().eq(currentUsage.getAffinity().getLabelToDisplay());
			aq.profile().eq(currentUsage.getAffinity().getProfile());
			aq.keyword().eq(currentUsage.getAffinity().getKeyword());
		}
		
		AffinityDomain affinityDomainBdd = aq.getUniqueResult();

		if(affinityDomainBdd == null){
		    usageIsInDB = false;
		    currentUsage.getAffinity().setSupportedTransactions(null);
		    entityManager.persist(currentUsage.getAffinity());
		    entityManager.flush();

		}else {
		    currentUsage.setAffinity(affinityDomainBdd);
		}


		if(usageIsInDB){
		    UsageQuery cca = new UsageQuery();
		    cca.transaction().eq(transactionBdd);
		    cca.affinity().eq(affinityDomainBdd);
		    Usage usageBdd = cca.getUniqueResult();
		    res.add(usageBdd);
		}else{
		    res.add(currentUsage);
		}
	    }
	}
	return res;
    }

    public static NistCode getExistingMetadataCodes(NistCode metadataCodes) {
	if (metadataCodes != null) {
	    NistCodeQuery ncq = new NistCodeQuery();
	    ncq.nistCodeName().eq(metadataCodes.getNistCodeName());
	    ncq.url().eq(metadataCodes.getUrl());
	    NistCode metadataCodesBB = ncq.getUniqueResult();
	    if (metadataCodesBB != null) {
		return metadataCodesBB;
	    }
	}
	return metadataCodes;
    }

    public static RegistryObjectListMetadata getExistingRegistryObjectListMetadata(RegistryObjectListMetadata registryObjectListMetadata) {

	if(registryObjectListMetadata != null){
	    RegistryObjectListMetadataQuery cca = new RegistryObjectListMetadataQuery();
	    cca.name().eq(registryObjectListMetadata.getName());
	    cca.displayAttributeName().eq(registryObjectListMetadata.getDisplayAttributeName());
	    RegistryObjectListMetadata bddResult = cca.getUniqueResult();
	    if(bddResult != null){
		return cca.getUniqueResult();
	    }
	    return registryObjectListMetadata;
	}
	return null;

    }

    public static List<AdhocQueryMetadata> getExistingAdhocQueryMetadata(List<AdhocQueryMetadata> listRegistryObjectMetadata) {
	List<AdhocQueryMetadata> res = null;
	if(listRegistryObjectMetadata != null){
	    res = new ArrayList<AdhocQueryMetadata>();
	    for (AdhocQueryMetadata currentAdhocQueryMetadata : listRegistryObjectMetadata) {

		AdhocQueryMetadataQuery cca = new AdhocQueryMetadataQuery();
		cca.uuid().eq(currentAdhocQueryMetadata.getUuid());
		cca.name().eq(currentAdhocQueryMetadata.getName());
		cca.displayAttributeName().eq(currentAdhocQueryMetadata.getDisplayAttributeName());
		AdhocQueryMetadata bddResult = cca.getUniqueResult();
		if(bddResult != null){
		    res.add(bddResult);
		}else{
		    res.add(currentAdhocQueryMetadata);
		}
	    }
	}
	return res;
    }

    public static List<PackClass> getExistingListPackClass(List<PackClass> listPackClass) {
	List<PackClass> res = new ArrayList<PackClass>();
	if(listPackClass != null){
	    for (PackClass currentPackClass : listPackClass) {			
		PackClassQuery cca = new PackClassQuery();
		cca.packClass().eq(currentPackClass.getPackClass());

		PackClass bddResult = cca.getUniqueResult();
		if(bddResult != null){
		    res.add(bddResult);
		}else{
		    res.add(currentPackClass);
		}
	    }
	}
	return res;
    }

    public static List<ExtrinsicObjectMetadataDescriber> getExistingExtrinsicObject(List<ExtrinsicObjectMetadataDescriber> extrinsicObject) {
	List<ExtrinsicObjectMetadataDescriber> res = new ArrayList<ExtrinsicObjectMetadataDescriber>();
	for (ExtrinsicObjectMetadataDescriber currentExtrinsicObjectDescriber : extrinsicObject) {

	    ExtrinsicObjectMetadataQuery cca = new ExtrinsicObjectMetadataQuery();
	    cca.uuid().eq(currentExtrinsicObjectDescriber.getEom().getUuid());
	    cca.name().eq(currentExtrinsicObjectDescriber.getEom().getName());
	    cca.displayAttributeName().eq(currentExtrinsicObjectDescriber.getEom().getDisplayAttributeName());

	    ExtrinsicObjectMetadata bddResult = cca.getUniqueResult();
	    if(bddResult != null){
		ExtrinsicObjectMetadataDescriber newExtrinsicObjectDescriber = new ExtrinsicObjectMetadataDescriber();
		newExtrinsicObjectDescriber.setCardinality(currentExtrinsicObjectDescriber.getCardinality());
		newExtrinsicObjectDescriber.setEom(bddResult);
		res.add(newExtrinsicObjectDescriber);
	    }else{
		res.add(currentExtrinsicObjectDescriber);
	    }
	}
	return res;
    }

    public static List<RegistryPackageMetadataDescriber> getExistingRegistryPackage(List<RegistryPackageMetadataDescriber> registryPackage) {
	List<RegistryPackageMetadataDescriber> res = new ArrayList<RegistryPackageMetadataDescriber>();
	for (RegistryPackageMetadataDescriber currentRegistryPackageDescriber : registryPackage) {

	    RegistryPackageMetadataQuery cca = new RegistryPackageMetadataQuery();
	    cca.uuid().eq(currentRegistryPackageDescriber.getRpm().getUuid());
	    cca.name().eq(currentRegistryPackageDescriber.getRpm().getName());
	    cca.displayAttributeName().eq(currentRegistryPackageDescriber.getRpm().getDisplayAttributeName());

	    RegistryPackageMetadata bddResult = cca.getUniqueResult();
	    if(bddResult != null){
		RegistryPackageMetadataDescriber newRegistryPackageDescriber = new RegistryPackageMetadataDescriber();
		newRegistryPackageDescriber.setCardinality(currentRegistryPackageDescriber.getCardinality());
		newRegistryPackageDescriber.setRpm(bddResult);
		res.add(newRegistryPackageDescriber);
	    }else{
		res.add(currentRegistryPackageDescriber);
	    }
	}
	return res;
    }
}
