package net.ihe.gazelle.xdstar.xdsidb.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import org.dcm4chee.xdsib.retrieve.model.RespondingMessage;
import org.dcm4chee.xdsib.retrieve.model.RespondingMessageQuery;

import java.util.Map;

public class XDSIBRespFilter extends Filter<RespondingMessage> implements QueryModifier<RespondingMessage> {

    /**
     *
     */
    private static final long serialVersionUID = -7984103972774534794L;

    public XDSIBRespFilter(Map<String, String> requestParameterMap) {
        super(getHQLCriterions(), requestParameterMap);
        queryModifiers.add(this);
    }

    private static HQLCriterionsForFilter<RespondingMessage> getHQLCriterions() {
        RespondingMessageQuery query = new RespondingMessageQuery();
        HQLCriterionsForFilter<RespondingMessage> result = query.getHQLCriterionsForFilter();

        result.addPath("affinityDomain", query.affinityDomain());
        result.addPath("transaction", query.transaction());
        result.addPath("messageType", query.messageType());
        result.addPath("validRequest", query.sentMessageContent().validationResult());
        result.addPath("validResponse", query.receivedMessageContent().validationResult());
        result.addPath("ip", query.ip());

        return result;
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<RespondingMessage> queryBuilder, Map<String, Object> filterValuesApplied) {

    }

}
