package net.ihe.gazelle.xdstar.testplan.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.testplan.InputType;
import net.ihe.gazelle.testplan.TestStepType;
import net.ihe.gazelle.xdstar.common.model.AbstractMessage;

/**
 * 
 * @author abderrazek boufahja
 *
 */
@Entity
@Table(name="tp_test_step_instance", schema = "public")
@SequenceGenerator(name="tp_test_step_instance_sequence", sequenceName="tp_test_step_instance_id_seq", allocationSize=1)
public class TestStepInstance implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@NotNull
	@GeneratedValue(generator="tp_test_step_instance_sequence", strategy=GenerationType.SEQUENCE)
	@Column(name="id", nullable = false, unique = true)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="test_step_id")
	private TestStepType parentTestStep;
	
	@Column(name="result")
	@Enumerated(EnumType.STRING)
	private ResultType result;
	
	@Column(name="status")
	@Enumerated(EnumType.STRING)
	private StatusType status;
	
	@OneToMany(targetEntity=AbstractMessage.class, cascade={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	private List<AbstractMessage> abstractMessage;	
	
	@OneToMany(targetEntity=RuleResult.class, mappedBy="testStepInstance", cascade=CascadeType.ALL)
	private List<RuleResult> ruleResult;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<InputType> inputs;
	
	@ManyToOne
	@JoinColumn(name="test_plan_instance_id")
	private TestPlanInstance testPlanInstance;
	
	@ManyToOne
	@JoinColumn(name="system_configuration_id")
	private SystemConfiguration systemConfiguration;

	/**
	 * Constructor
	 * 
	 */
	public TestStepInstance()
	{
		
	}


	/*****************************************************************************
	 * 
	 * Getters and Setters
	 * 
	 ******************************************************************************/
	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public SystemConfiguration getSystemConfiguration() {
		return systemConfiguration;
	}


	public void setSystemConfiguration(SystemConfiguration systemConfiguration) {
		this.systemConfiguration = systemConfiguration;
	}


	public StatusType getStatus() {
		return status;
	}


	public void setStatus(StatusType status) {
		this.status = status;
	}


	public TestStepType getParentTestStep() {
		return parentTestStep;
	}


	public void setParentTestStep(TestStepType parentTestStep) {
		this.parentTestStep = parentTestStep;
	}


	public ResultType getResult() {
		return result;
	}


	public void setResult(ResultType result) {
		this.result = result;
	}


	public List<AbstractMessage> getAbstractMessage() {
		if (abstractMessage == null){
			this.abstractMessage = new ArrayList<AbstractMessage>();
		}
		return abstractMessage;
	}


	public void setAbstractMessage(List<AbstractMessage> abstractMessage) {
		this.abstractMessage = abstractMessage;
	}

	public List<RuleResult> getRuleResult() {
		if (ruleResult == null){
			ruleResult = new ArrayList<RuleResult>();
		}
		return ruleResult;
	}


	public void setRuleResult(List<RuleResult> ruleResult) {
		this.ruleResult = ruleResult;
	}


	public TestPlanInstance getTestPlanInstance() {
		return testPlanInstance;
	}


	public void setTestPlanInstance(TestPlanInstance testPlanInstance) {
		this.testPlanInstance = testPlanInstance;
	}


	public List<InputType> getInputs() {
		return inputs;
	}


	public void setInputs(List<InputType> inputs) {
		this.inputs = inputs;
	}
	
	


}
