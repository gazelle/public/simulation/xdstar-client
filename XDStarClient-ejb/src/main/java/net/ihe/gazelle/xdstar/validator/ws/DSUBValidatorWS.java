package net.ihe.gazelle.xdstar.validator.ws;

import net.ihe.gazelle.validation.exception.GazelleValidationException;
import net.ihe.gazelle.validation.model.ValidatorDescription;
import net.ihe.gazelle.validation.ws.AbstractModelBasedValidation;
import net.ihe.gazelle.xdstar.dsub.validator.DSUBValidator;
import net.ihe.gazelle.xdstar.dsub.validator.DSUBValidatorType;
import org.jboss.seam.annotations.Name;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.soap.SOAPException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Stateless
@Name("DSUBModelBasedWS")
@WebService(name = "DSUBModelBasedWS", 
		serviceName = "DSUBModelBasedWSService", 
		portName = "DSUBModelBasedWSPort", 
		targetNamespace = "http://ws.mb.validator.gazelle.ihe.net")
public class DSUBValidatorWS extends AbstractModelBasedValidation implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	@WebMethod
	@WebResult(name = "DetailedResult")
	public String validateDocument(
			@WebParam(name = "document") String document,
			@WebParam(name = "validator") String validator)
			throws SOAPException {
		if (validator == null || validator.isEmpty()) {
			throw new SOAPException("A validator must be provided");
		}
		if (document == null || document.isEmpty()) {
			throw new SOAPException("A document must be provided");
		}
		String res = DSUBValidator.validateDocument(document, validator);
		String status = res.contains("<ValidationTestResult>FAILED</ValidationTestResult>")?"FAILED":"PASSED";
		if (DSUBValidatorType.getValidatorFromValue(validator) != null) {
			this.addValidatorUsage(validator, status);
		}
		return res;
	}

    @Override
    protected String buildReportOnParsingFailure(GazelleValidationException e, ValidatorDescription validatorDescription) {
        return null;
    }

    @Override
    protected String executeValidation(String s, ValidatorDescription validatorDescription, boolean b) throws GazelleValidationException {
        return null;
    }

    @Override
    @WebMethod
	@WebResult(name = "about")
	public String about(){
		String res = "This webservice is developped by IHE-europe / gazelle team. The aim of this validator is to validate DSUB messages soap using model based validation.\n";
		res = res + "For more information please contact the manager of gazelle project eric.poiseau@inria.fr";
		return res;
	}

	@Override
	@WebMethod
	@WebResult(name = "Validators")
	public List<String> getListOfValidators(@WebParam(name="descriminator") String descriminator)
			throws SOAPException {
		if (null == descriminator){
			descriminator = "";
		}
		List<String> res = new ArrayList<String>();
    	DSUBValidatorType[] dd = DSUBValidatorType.values();
    	for (DSUBValidatorType validators : dd) {
			if (validators.getValue().toLowerCase().contains(descriminator.toLowerCase())){
				res.add(validators.getValue());
			}
		}
		return res;
	}


	@Override
	protected ValidatorDescription getValidatorByOidOrName(String value) {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	protected List<ValidatorDescription> getValidatorsForDescriminator(String descriminator) {
	    // TODO Auto-generated method stub
	    return null;
	}
}


