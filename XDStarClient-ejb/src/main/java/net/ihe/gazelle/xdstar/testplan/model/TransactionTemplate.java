package net.ihe.gazelle.xdstar.testplan.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;

import org.hibernate.annotations.Type;

/**
 * 
 * @author abderrazek boufahja
 *
 */
@Entity(name="transactionTemplate")
@Table(name="tp_transaction_template", schema = "public")
@SequenceGenerator(name="tp_transaction_template_sequence", sequenceName="tp_transaction_template_id_seq", allocationSize=1)
public class TransactionTemplate implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@NotNull
	@GeneratedValue(generator="tp_transaction_template_sequence", strategy=GenerationType.SEQUENCE)
	@Column(name="id", nullable = false, unique = true)
	private Integer id;
	
	@Column(name="name", unique=true)
	private String name;
	
	@Column(name="http_header")
	@Size(max=1000)
	private String httpHeader;
	
	@Column(name="request_type")
	@Enumerated(EnumType.STRING)
	private MetadataMessageType requestType;
	
	@Column(name="response_type")
	@Enumerated(EnumType.STRING)
	private MetadataMessageType responseType;
	
	
	@Lob
	@Column(name="template")
	@Type(type = "text")
	private String template;


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public MetadataMessageType getRequestType() {
		return requestType;
	}


	public void setRequestType(MetadataMessageType requestType) {
		this.requestType = requestType;
	}


	public MetadataMessageType getResponseType() {
		return responseType;
	}


	public void setResponseType(MetadataMessageType responseType) {
		this.responseType = responseType;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getHttpHeader() {
		return httpHeader;
	}


	public void setHttpHeader(String httpHeader) {
		this.httpHeader = httpHeader;
	}


	public String getTemplate() {
		return template;
	}


	public void setTemplate(String template) {
		this.template = template;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TransactionTemplate other = (TransactionTemplate) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	

}
