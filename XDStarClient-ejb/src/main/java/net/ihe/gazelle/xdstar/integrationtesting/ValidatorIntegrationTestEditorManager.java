package net.ihe.gazelle.xdstar.integrationtesting;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.validator.ut.action.UnitTestEditorManager;
import net.ihe.gazelle.xdstar.core.DocumentFileUpload;
import net.ihe.gazelle.xdstar.validator.ValidatorPackClass;
import net.ihe.gazelle.xdstar.validator.ValidatorPackClassQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.log.Log;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.persistence.EntityManager;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

@Name("validatorIntegrationTestEditorManager")
@Scope(ScopeType.PAGE)
public class ValidatorIntegrationTestEditorManager extends UnitTestEditorManager<ValidatorIntegrationTest> {

    @Logger
    private static Log log;

    private File uploadedFile;

    private String uploadedFileContentType;

    private Boolean displayDetailedResult = false;


    @Override
    @Create
    public void init() {

        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

        if (urlParams.containsKey("unitTest")) {
            ValidatorIntegrationTestQuery vq = new ValidatorIntegrationTestQuery();
            vq.keyword().eq(urlParams.get("unitTest"));
            ValidatorIntegrationTest validatorIntegrationTest = vq.getUniqueResult();
            if (validatorIntegrationTest == null) {
                validatorIntegrationTest = new ValidatorIntegrationTest(urlParams.get("unitTest"));
            }
            setUnitTest(validatorIntegrationTest);
        } else {
            setUnitTest(new ValidatorIntegrationTest());
        }

    }

    @Override
    public void save() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.merge(getUnitTest());
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Unit test saved");
    }

    public List<ValidatorPackClass> getAvailableValidatorPackClass() {

        ValidatorPackClassQuery vq = new ValidatorPackClassQuery();
        List<ValidatorPackClass> res = vq.getList();
        Collections.sort(res, new Comparator<ValidatorPackClass>() {

            @Override
            public int compare(ValidatorPackClass o1, ValidatorPackClass o2) {
                if (o1 == null) {
                    return -1;
                }
                if (o2 == null) {
                    return 1;
                }
                if (o1.getValidatorName() == null) {
                    return -1;
                }
                return o1.getValidatorName().compareTo(o2.getValidatorName());
            }

        });
        return res;
    }


    public void uploadListener(FileUploadEvent event) {
        UploadedFile item = event.getUploadedFile();
        String uploadedFilename = item.getName();
        setUploadedFileContentType(item.getContentType());
        FileOutputStream fos = null;
        try {
            uploadedFile = File.createTempFile("test", uploadedFilename);
            fos = new FileOutputStream(uploadedFile);
            fos.write(item.getData());
        } catch (FileNotFoundException e) {
            log.error("" + e.getMessage());
        } catch (IOException e) {
            log.error("" + e.getMessage());
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    log.error("" + e.getMessage());
                }
            }
        }
    }

    public String getUploadedFileContentType() {
        return uploadedFileContentType;
    }

    public void setUploadedFileContentType(String uploadedFileContentType) {
        this.uploadedFileContentType = uploadedFileContentType;
    }

    public File getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(File uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public void importFile() {
        if (this.uploadedFile != null) {
            try {

                getUnitTest().setTestedFilePath(uploadedFile.getAbsolutePath());

            } catch (Exception e) {
                FacesMessages.instance().add(Severity.ERROR, "Problem occurs when uploading the specified xml: " + e.getMessage());
                log.info("exception : ", e);
            } finally {
                this.uploadedFile = null;
            }
        }
    }

    public void downloadFile() {

        String filePath = getUnitTest().getTestedFilePath();
        if (filePath != null) {
            try {
                File file = new File(filePath);
                DocumentFileUpload.showFile(file, "xml", true);
            } catch (Exception e) {
                FacesMessages.instance().add(Severity.ERROR, "Problem occurs when downloading the specified xml: " + e.getMessage());
                log.info("exception : ", e);
            } finally {
                this.uploadedFile = null;
            }
        }
    }

    public Boolean isDisplayDetailedResult() {
        return displayDetailedResult;
    }

    public void setDisplayDetailedResult(Boolean displayDetailedResult) {
        this.displayDetailedResult = displayDetailedResult;
    }

    public void actionListener(ActionEvent event) {
        displayDetailedResult = !displayDetailedResult;
    }

}
