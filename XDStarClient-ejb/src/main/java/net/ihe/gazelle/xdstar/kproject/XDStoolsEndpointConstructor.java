package net.ihe.gazelle.xdstar.kproject;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;

public class XDStoolsEndpointConstructor {
	
	public static String createPnRURLFromOID(String oid){
		String xdstoolsURL = ApplicationConfiguration.getValueOfVariable("ksa_xdstools_url");
		String res = xdstoolsURL + "/sim/" + oid + "/rep/prb";
		return res;
	}
	
	public static String createStoredQueryURLFromOID(String oid){
		String xdstoolsURL = ApplicationConfiguration.getValueOfVariable("ksa_xdstools_url");
		String res = xdstoolsURL + "/sim/" + oid + "/reg/sq";
		return res;
	}
	
	public static String createRetrieveURLFromOID(String oid){
		String xdstoolsURL = ApplicationConfiguration.getValueOfVariable("ksa_xdstools_url");
		String res = xdstoolsURL + "/sim/" + oid + "/rep/ret";
		return res;
	}
	
}
