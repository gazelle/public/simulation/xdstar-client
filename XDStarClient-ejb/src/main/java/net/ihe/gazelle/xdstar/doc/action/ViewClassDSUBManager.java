package net.ihe.gazelle.xdstar.doc.action;

import java.io.Serializable;

import net.ihe.gazelle.action.ViewClassManager;
import net.ihe.gazelle.metamodel.ClassesModel;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("viewClassDSUBManager")
@Scope(ScopeType.PAGE)
public class ViewClassDSUBManager extends ViewClassManager implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ClassesModel getAvailableClasses() {
		if (availableClasses == null){
			availableClasses = new ClassesModel();
			availableClasses.setStandard("DSUB");
		}
		return availableClasses;
	}

}
