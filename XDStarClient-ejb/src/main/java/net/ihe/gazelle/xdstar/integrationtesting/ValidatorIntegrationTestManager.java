package net.ihe.gazelle.xdstar.integrationtesting;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validator.ut.action.UnitTestManager;
import net.ihe.gazelle.validator.ut.model.UnitTest;
import net.ihe.gazelle.validator.ut.model.UnitTestLog;
import net.ihe.gazelle.validator.ut.model.UnitTestResult;
import net.ihe.gazelle.xdstar.core.DocumentFileUpload;
import net.ihe.gazelle.xdstar.validator.GenericMetadataValidator;
import net.ihe.gazelle.xdstar.validator.MetadataValidator;
import net.ihe.gazelle.xdstar.validator.XSLTransformator;
import org.apache.commons.io.FileUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

@Name("validatorIntegrationTestManager")
@Scope(ScopeType.PAGE)
public class ValidatorIntegrationTestManager extends UnitTestManager<ValidatorIntegrationTest> {

    private static final Logger LOG = LoggerFactory.getLogger(ValidatorIntegrationTestManager.class);

    public static final String UTF_8 = "UTF-8";
    private String drString;

    private ValidatorIntegrationTest testToDisable;

    public ValidatorIntegrationTest getTestToDisable() {
        return testToDisable;
    }

    public void setTestToDisable(ValidatorIntegrationTest testToDisable) {
        this.testToDisable = testToDisable;
    }

    public void disableTest() {
        deleteTest(testToDisable);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, testToDisable.getKeyword() + " successfully disabled !");
    }

    public String createUnitTest() {
        return "/testing/validatorUnitTestEditor.seam";
    }

    @Override
    public String editUnitTest(ValidatorIntegrationTest unitTest) {
        return "/testing/validatorUnitTestEditor.seam?unitTest=" + unitTest.getKeyword();
    }

    @Override
    public String displayUnitTestLogs(UnitTest unitTest) {
        return "/testing/validatorUnitTestLogs.seam?unitTest=" + unitTest.getId();
    }

    @Override
    public String listUnitTests() {
        return "/testing/browseUnitTests.xhtml";
    }

    @Override
    protected ValidatorIntegrationTest createCopyForTest(ValidatorIntegrationTest unitTest) {
        return new ValidatorIntegrationTest(unitTest);
    }

    @Override
    protected void save(ValidatorIntegrationTest unitTest) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.merge(unitTest);
        entityManager.flush();

    }

    private ValidatorIntegrationTest getValidatorIntegrationTestFromKeyWord(String keywordIn) {
        ValidatorIntegrationTestQuery vq = new ValidatorIntegrationTestQuery();
        vq.keyword().eq(keywordIn);
        return vq.getUniqueResult();
    }

    @Override
    protected void execute(ValidatorIntegrationTest inTest) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        String errorMessage = null;
        ValidatorIntegrationTest test;
        if (inTest.getId() == null) {
            test = getValidatorIntegrationTestFromKeyWord(inTest.getKeyword());
        } else {
            test = entityManager.find(ValidatorIntegrationTest.class, inTest.getId());
        }

        UnitTestLog testLog = new UnitTestLog(test);
        testLog.setTestedVersion(test.getTestedValidator().getVersion());

        String validatorName = test.getTestedValidator().getValidatorName();
        String document = convertFileToString(test.getTestedFilePath());

        DetailedResult detailedResult = GenericMetadataValidator.validate(document, validatorName);
        drString = MetadataValidator.getDetailedResultAsString(detailedResult);
        test.setDetailedResult(drString);
        boolean res = detailedResult.getValidationResultsOverview().getValidationTestResult().equals(UnitTestResult.PASSED.getValue());
        if (res) {
            testLog.setEffectiveResult(UnitTestResult.PASSED);
        } else {
            testLog.setEffectiveResult(UnitTestResult.FAILED);
        }
        testLog.setSuccess(test.getExpectedResult().equals(testLog.getEffectiveResult()));
        if (!testLog.isSuccess() && errorMessage == null) {
            errorMessage = "Returned <" + testLog.getEffectiveResult().getValue() + "> but <"
                    + test.getExpectedResult().getValue() + "> was expected";
        }
        testLog.setReasonForFailure(errorMessage);
        test.setLastResult(testLog.isSuccess());
        test.setLastRunDate(testLog.getRunDate());
        test.setLastTestedVersion(testLog.getTestedVersion());
        test.setReasonForFailure(testLog.getReasonForFailure());
        entityManager.merge(test);
        entityManager.merge(testLog);

        if (testLog.isSuccess()) {
            test.setLastSuccessDate(testLog.getRunDate());
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Unit test passed");
        }

    }

    private static String convertFileToString(String filePath) {
        File file = new File(filePath);
        String str = null;
        try {
            str = FileUtils.readFileToString(file, UTF_8);
        } catch (IOException e) {
            LOG.error("" + e.getMessage());
        }
        return str;
    }

    public String getDetailedResult(ValidatorIntegrationTest integrationTest) {

        String rawRes = integrationTest.getDetailedResult();
        String inXslPath = ApplicationConfiguration
                .getValueOfVariable("validation_result_xslt");
        HashMap<String, String> hash = new HashMap<String, String>();
        hash.put("index", "1");
        String res = XSLTransformator.resultTransformation(
                rawRes, inXslPath, hash);
        return res;
    }

    public void downloadDetailedResult(ValidatorIntegrationTest integrationTest) {

        String dr = integrationTest.getDetailedResult();
        String name = integrationTest.getKeyword();
        File exportFile = new File("detailedResult_" + name + ".xml");
        try {
            FileUtils.writeStringToFile(exportFile, dr);
            DocumentFileUpload.showFile(exportFile, "xml", true);
        } catch (IOException e) {
            LOG.error("" + e.getMessage());
        }
    }
}
