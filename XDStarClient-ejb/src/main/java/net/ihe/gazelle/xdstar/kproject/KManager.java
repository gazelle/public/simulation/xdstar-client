package net.ihe.gazelle.xdstar.kproject;

import gov.nist.registry.xdstools2.server.gazelle.actorConfig.CSVParser;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.simulator.sut.model.UsageQuery;
import net.ihe.gazelle.testplan.TestPlanType;
import net.ihe.gazelle.testplan.TestPlanTypeQuery;
import net.ihe.gazelle.xdstar.conf.model.RegistryConfiguration;
import net.ihe.gazelle.xdstar.conf.model.RegistryConfigurationQuery;
import net.ihe.gazelle.xdstar.conf.model.RepositoryConfiguration;
import net.ihe.gazelle.xdstar.conf.model.RepositoryConfigurationQuery;
import net.ihe.gazelle.xdstar.testplan.action.KSAInitManagerLocal;
import org.apache.commons.io.IOUtils;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author abderrazek boufahja
 */
@Name("kManager")
@Scope(ScopeType.PAGE)
public class KManager implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(KManager.class);

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String fileContent;

    public void uploadOIDSListener(FileUploadEvent event) {
        UploadedFile item = event.getUploadedFile();
        this.fileContent = new String(item.getData(), StandardCharsets.UTF_8);
    }

    public void deleteAndGenerate() {
        if (fileContent != null) {
            OidConfigs gconf = this.loadRegistryRepositoryFromGazelle(fileContent);
            Map<String, Boolean> listSys = new TreeMap<String, Boolean>();
            treatTheGenerationOfConfigurations(gconf, listSys);
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "the list of SUT was updated : " + listSys.keySet());
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to parse the uploaded file.");
        }
    }

    public void initClonesFromCSVFile() throws NamingException {
        if (fileContent != null) {
            OidConfigs gconf = this.loadRegistryRepositoryFromGazelle(fileContent);
            Map<String, Boolean> listSys = new TreeMap<String, Boolean>();
            treatTheGenerationOfConfigurations(gconf, listSys);
            for (String sysName : listSys.keySet()) {
                List<TestPlanType> listTestPlan = getListKSATestPlanType();
                for (TestPlanType testPlanType : listTestPlan) {
                    KSAInitManagerLocal kinitManager = (KSAInitManagerLocal) (new InitialContext()).lookup("XDStarClient/KSAInitManager/local");
                    kinitManager.setEnabledRefresh(true);
                    kinitManager.setSelectedRepositoryConfiguration(this.findPnRRepositoryConfigurationByName(sysName));
                    kinitManager.setSelectedTestPlanType(testPlanType);
                    kinitManager.initTestPlanInstance();
                    kinitManager.startSelectedTestPlan();
                }
            }
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "the list of SUT was updated : " + listSys.keySet());
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to parse the uploaded file.");
        }
    }

    private List<TestPlanType> getListKSATestPlanType() {
        List<String> listAffKeyword = new ArrayList<String>();
        listAffKeyword.add("KSA_CLO");
        listAffKeyword.add("KSA_CLR");
        listAffKeyword.add("KSA_XDSb");
        listAffKeyword.add("KSA_SIR");
        TestPlanTypeQuery qq = new TestPlanTypeQuery();
        qq.testStep().affinityDomainKeyword().in(listAffKeyword);
        return qq.getList();
    }

    private void treatTheGenerationOfConfigurations(OidConfigs gconf,
                                                    Map<String, Boolean> listSys) {
        for (int i = 0; i < gconf.size(); i++) {
            if (gconf.isXDStoolsOID(i)) {
                String name = gconf.getSystem(i);
                String oid = gconf.getValue(i);
                createAndUpdatePnRRepository(name, oid);
                createAndUpdateRetRepository(name, oid);
                createAndUpdateSQRegistry(name, oid);
                listSys.put(name, null);
            }
        }
    }

    private void createAndUpdateSQRegistry(String name, String oid) {
        RegistryConfiguration pnrrepconf = this.findSQRegistryConfigurationByName(name);
        if (pnrrepconf == null) {
            pnrrepconf = new RegistryConfiguration();
        }
        pnrrepconf.setName(name);
        pnrrepconf.setSystemName(name);
        pnrrepconf.setRepositoryUniqueId(oid);
        pnrrepconf.setUrl(XDStoolsEndpointConstructor.createStoredQueryURLFromOID(oid));
        pnrrepconf.setIsPublic(true);
        pnrrepconf.setIsAvailable(true);
        pnrrepconf.setListUsages(getUsageSQKSA());

        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        em.merge(pnrrepconf);
        em.flush();
    }

    private void createAndUpdatePnRRepository(String name, String oid) {
        RepositoryConfiguration pnrrepconf = this.findPnRRepositoryConfigurationByName(name);
        if (pnrrepconf == null) {
            pnrrepconf = new RepositoryConfiguration();
        }
        pnrrepconf.setName(name);
        pnrrepconf.setSystemName(name);
        pnrrepconf.setRepositoryUniqueId(oid);
        pnrrepconf.setUrl(XDStoolsEndpointConstructor.createPnRURLFromOID(oid));
        pnrrepconf.setIsPublic(true);
        pnrrepconf.setIsAvailable(true);
        pnrrepconf.setListUsages(getUsagePnrKSA());

        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        em.merge(pnrrepconf);
        em.flush();
    }

    private void createAndUpdateRetRepository(String name, String oid) {
        RepositoryConfiguration retrepconf = this.findRetRepositoryConfigurationByName(name);
        if (retrepconf == null) {
            retrepconf = new RepositoryConfiguration();
        }
        retrepconf.setName(name);
        retrepconf.setSystemName(name);
        retrepconf.setRepositoryUniqueId(oid);
        retrepconf.setUrl(XDStoolsEndpointConstructor.createRetrieveURLFromOID(oid));
        retrepconf.setIsPublic(true);
        retrepconf.setIsAvailable(true);
        retrepconf.setListUsages(getUsageRetKSA());

        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        em.merge(retrepconf);
        em.flush();
    }

    private List<Usage> getUsageRetKSA() {
        UsageQuery qq = new UsageQuery();
        List<String> listTRans = new ArrayList<String>();
        listTRans.add("ITI-43");
        qq.transaction().keyword().in(listTRans);
        return qq.getList();
    }

    private List<Usage> getUsageSQKSA() {
        UsageQuery qq = new UsageQuery();
        List<String> listTRans = new ArrayList<String>();
        listTRans.add("ITI-18");
        qq.transaction().keyword().in(listTRans);
        return qq.getList();
    }

    private List<Usage> getUsagePnrKSA() {
        UsageQuery qq = new UsageQuery();
        List<String> listTRans = new ArrayList<String>();
        listTRans.add("ITI-41");
        listTRans.add("	RAD-68");
        qq.transaction().keyword().in(listTRans);
        return qq.getList();
    }

    private RepositoryConfiguration findRetRepositoryConfigurationByName(String name) {
        RepositoryConfigurationQuery qq = new RepositoryConfigurationQuery();
        qq.name().eq(name);
        qq.listUsages().transaction().keyword().eq("ITI-43");
        return qq.getUniqueResult();
    }

    private RepositoryConfiguration findPnRRepositoryConfigurationByName(String name) {
        RepositoryConfigurationQuery qq = new RepositoryConfigurationQuery();
        qq.name().eq(name);
        qq.listUsages().transaction().keyword().eq("ITI-41");
        return qq.getUniqueResult();
    }

    private RegistryConfiguration findSQRegistryConfigurationByName(String name) {
        RegistryConfigurationQuery qq = new RegistryConfigurationQuery();
        qq.name().eq(name);
        qq.listUsages().transaction().keyword().eq("ITI-18");
        return qq.getUniqueResult();
    }

    private OidConfigs loadRegistryRepositoryFromGazelle(String content) {
        OidConfigs gConfigs = new OidConfigs();
        try {
            File fil = File.createTempFile("sut-tmp", "csv");
            IOUtils.copy(new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8)), new FileOutputStream(fil));
            new CSVParser(fil, gConfigs, new OidEntryFactory());
        } catch (FileNotFoundException e) {
            LOG.error("" + e.getMessage());
        } catch (IOException e) {
            LOG.error("" + e.getMessage());
        }
        return gConfigs;
    }

}