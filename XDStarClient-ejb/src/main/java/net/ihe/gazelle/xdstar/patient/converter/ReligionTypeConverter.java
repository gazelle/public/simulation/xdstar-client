/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.patient.converter;


import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.persistence.EntityManager;

import net.ihe.gazelle.openempi.hb.ReligionType;
import net.ihe.gazelle.openempi.hb.dao.HibernateUtilOpenempi;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

 

 
/**
 * 
 * @author abderrazek boufahja
 *
 */
@BypassInterceptors
@Name("religionTypeConverter")
@Converter(forClass= ReligionType.class)
public class ReligionTypeConverter implements javax.faces.convert.Converter, Serializable {
  
   /** Serial ID version of this object     */
   private static final long serialVersionUID = 12393988987567L;


   private static Logger log = LoggerFactory.getLogger(ReligionTypeConverter.class); 

   /**
    * 
    */
    
 
   public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) {
      if (value.getClass() == ReligionType.class  )
      {
    	  ReligionType religionType = (ReligionType) value;
         return religionType.getReligionTypeCd();
      }
      else
      {
         return null;
      }

   }


   @Transactional
   public Object getAsObject(FacesContext context,
		   UIComponent component, String value) throws ConverterException {
	   if (value != null)
	   {
		   try
		   {
			   String id = value;

			   EntityManager entityManager = HibernateUtilOpenempi.buildEntityManager();;

			   return entityManager.find(ReligionType.class, id) ;
		   }
		   catch (NumberFormatException e) {
			   log.info(e.getMessage(), e);
		   }
	   }
	   return null;
   }


}
