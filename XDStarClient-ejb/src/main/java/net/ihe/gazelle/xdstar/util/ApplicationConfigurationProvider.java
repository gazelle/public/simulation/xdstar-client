package net.ihe.gazelle.xdstar.util;

import java.io.Serializable;

import javax.ejb.Remove;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;

import net.ihe.gazelle.xdstar.menu.Pages;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used to store some of the preferences in the application state instead of querying several times the database
 * 
 * @author aberge
 * 
 */

@AutoCreate
@Name("applicationConfigurationProvider")
@Scope(ScopeType.APPLICATION)
public class ApplicationConfigurationProvider implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5004043953931007769L;

	private static Logger log = LoggerFactory.getLogger(ApplicationConfigurationProvider.class);

	private Boolean applicationWorksWithoutCas;
	private Boolean ipLogin;
	private String ipLoginAdmin;
	private String applicationUrl;
	private String casUrl;

	@Create
	public void init() {
		applicationWorksWithoutCas = null;
		ipLogin = null;
		ipLoginAdmin = null;
		casUrl = null;
		applicationUrl = null;
	}

	@Destroy
	@Remove
	public void destroy() {
		init();
	}

	public static ApplicationConfigurationProvider instance() {
		return (ApplicationConfigurationProvider) Component.getInstance("applicationConfigurationProvider");
	}

	public String loginByIP() {
		Identity identity = (Identity) Component.getInstance("org.jboss.seam.security.identity");
		if ("loggedIn".equals(identity.login())) {
			return "/home.xhtml";
		} else {
			return null;
		}
	}

	public String getApplicationUrl() {
		if (applicationUrl == null) {
			applicationUrl = ApplicationConfiguration.getValueOfVariable("application_url");
		}
		return applicationUrl;
	}

	public boolean isUserAllowedAsAdmin() {
		return Identity.instance().isLoggedIn() && Identity.instance().hasRole("admin_role");
	}

	public boolean isAdminPage(String currentLink){
		boolean adminAccess = false;
		String host = currentLink.substring(0, currentLink.lastIndexOf("XDStarClient/")+12);
		currentLink = currentLink.replace(host,"");
		currentLink = currentLink.replace("seam","xhtml");
		if(Pages.LIST_USAGES.getLink().contains(currentLink)){adminAccess = true;}
		else if(Pages.LIST_CLASSIFICATIONS.getLink().contains(currentLink)){adminAccess = true;}
		else if (Pages.LIST_EXTERNAL_IDENTIFIERS.getLink().contains(currentLink)){adminAccess = true;}
		else if (Pages.LIST_EXTRINSIC_OBJECTS.getLink().contains(currentLink)){adminAccess = true;}
		else if (Pages.LIST_REGISTRY_PACKAGES.getLink().contains(currentLink)){adminAccess = true;}
		else if (Pages.LIST_ADHOC_QUERIES.getLink().contains(currentLink)){adminAccess = true;}
		else if (Pages.LIST_REGISTRY_OBJECT_LIST.getLink().contains(currentLink)){adminAccess = true;}
		else if (Pages.VALIDATOR_PACK_ASSOCIATION.getLink().contains(currentLink)){adminAccess = true;}
		else if (Pages.BROWSE_UNIT_TESTS.getLink().contains(currentLink)){adminAccess = true;}
		else if (Pages.APPLICATION_CONFIGURATIONS.getLink().contains(currentLink)){adminAccess = true;}
		else if (Pages.XCPD_CONFIGURATIONS.getLink().contains(currentLink)){adminAccess = true;}
		else if (Pages.TRANSACTIONS_TYPES.getLink().contains(currentLink)){adminAccess = true;}
		else if (Pages.HOME_COMMUNITY_ID_LIST.getLink().contains(currentLink)){adminAccess = true;}
		else if (Pages.CONFIGURATIONS_TYPES_LIST.getLink().contains(currentLink)){adminAccess = true;}
		else if (Pages.TEST_PLANS_DEFINITION.getLink().contains(currentLink)){adminAccess = true;}
		else if (Pages.TRANSACTION_TEMPLATE.getLink().contains(currentLink)){adminAccess = true;}
		else if (Pages.CHECK_CONFIGURATION_SCHEDULER.getLink().contains(currentLink)){adminAccess = true;}
		else if (Pages.CONSTRAINTS_ADMIN.getLink().contains(currentLink)){adminAccess = true;}
		else if (Pages.VALIDATION_STATISTICS.getLink().contains(currentLink)){adminAccess = true;}
		else if (Pages.USAGE_EDITOR.getLink().contains(currentLink)){adminAccess = true;}
		else if (Pages.CLASSIFICATION_METADATA_EDITOR.getLink().contains(currentLink)){adminAccess = true;}
		else if (Pages.EXTERNAL_IDENTIFIER_METADATA_EDITOR.getLink().contains(currentLink)){adminAccess = true;}
		else if (Pages.EXTRINSIC_OBJECT_METADATA_EDITOR.getLink().contains(currentLink)) {adminAccess = true;}
		else if (Pages.REGISTRY_PACKAGE_METADATA_EDITOR.getLink().contains(currentLink)) {adminAccess = true;}
		else if (Pages.ADHOCQUERY_METADATA_EDITOR.getLink().contains(currentLink)) {adminAccess = true;}
		else if (Pages.REGISTRY_OBJECT_LIST_EDITOR.getLink().contains(currentLink)) {adminAccess = true;}
		else if (Pages.VALIDATOR_PACK_CLASS_EDITOR.getLink().contains(currentLink)) {adminAccess = true;}
		else if (Pages.UNIT_TEST_DETAILS.getLink().contains(currentLink)) {adminAccess = true;}
		else if (Pages.UNIT_TEST_LOGS.getLink().contains(currentLink)) {adminAccess = true;}
		else if (Pages.HOME_COMMUNITY_ID_EDITOR.getLink().contains(currentLink)) {adminAccess = true;}
		else if (Pages.HOME_COMMUNITY_ID_DETAILS.getLink().contains(currentLink)) {adminAccess = true;}
		else if (Pages.TEST_PLAN_TYPE_EDITOR.getLink().contains(currentLink)) {adminAccess = true;}
		else if (Pages.TEST_STEP_TYPE_EDITOR.getLink().contains(currentLink)) {adminAccess = true;}
		else if (Pages.INPUT_TYPE_EDITOR.getLink().contains(currentLink)) {adminAccess = true;}
		else if (Pages.RULE_TYPE_EDITOR.getLink().contains(currentLink)) {adminAccess = true;}
		else if (Pages.GLOBAL_INPUT_TYPE_EDITOR.getLink().contains(currentLink)) {adminAccess = true;}
		else if (Pages.IMPORT_XDSTOOLS2_KSA_ENDPOINT.getLink().contains(currentLink)) {adminAccess = true;}
		else if (Pages.HOME_COMMUNITY_SUMMARY.getLink().contains(currentLink)) {adminAccess = true;}
		return adminAccess;
	}
}
