package net.ihe.gazelle.metadata.action;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.metadata.model.RegistryObjectMetadata;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@Stateful
@Name("mETADATARegistryObjectMetadataEditorManager")
@Scope(ScopeType.SESSION)
@GenerateInterface("RegistryObjectMetadataEditorLocal")
public class RegistryObjectMetadataEditorManager implements RegistryObjectMetadataEditorLocal {

	@Logger
	private static Log log;
	
	public static <T extends RegistryObjectMetadata> List<T> filterMetadata(Class<T> classs, String name, String uuid){
		HQLQueryBuilder<T> hh = new HQLQueryBuilder<T>(classs);
		if (name != null) hh.addEq("name", name);
		if (uuid != null) hh.addEq("uuid", uuid);
		return hh.getList();
	}


	// --- destroy ---------------
	@Destroy
	@Remove
	public void destroy(){
		log.info("destroy mETADATARegistryObjectMetadataEditorManager..");
	}

}