package net.ihe.gazelle.metadata.action;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.metadata.model.*;
import net.ihe.gazelle.simulator.sut.model.Usage;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.*;

import static org.apache.commons.lang.CharEncoding.UTF_8;


@Stateful
@Name("metadataEditorManager")
@Scope(ScopeType.SESSION)
@GenerateInterface("MetadataEditorManagerLocal")
public class MetadataEditorManager implements MetadataEditorManagerLocal {


    private static Logger log = LoggerFactory.getLogger(MetadataEditorManager.class);

    public static String getAttributeAsStringStat(Object obj) {
        if (obj != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                MetadataEditorManager.save(baos, obj);
            } catch (JAXBException e) {
                log.info("JAXB exception", e);
                return null;
            }
            String res = null;
            try {
                res = baos.toString(UTF_8);
                if (res.indexOf("?>") > 0) {
                    res = res.substring(res.indexOf("?>") + 2);
                }
                return res.trim();
            } catch (UnsupportedEncodingException e) {
                log.error("" + e.getMessage());
            }
        }
        return null;
    }

    public String getAttributeAsString(Object obj) {
        return getAttributeAsStringStat(obj);
    }

    public static void save(OutputStream os, Object xdww) throws JAXBException {
        JAXBContext jc = null;
        jc = JAXBContext.newInstance("net.ihe.gazelle.metadata.model");
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        m.marshal(xdww, os);
    }

    public List<ClassificationMetaData> getAvailableClassifications() {
        HQLQueryBuilder<ClassificationMetaData> hh = new HQLQueryBuilder<ClassificationMetaData>(ClassificationMetaData.class);
        hh.addOrder("displayAttributeName", true);
        return hh.getList();
    }

    public List<ExternalIdentifierMetadata> getAvailableExts() {
        HQLQueryBuilder<ExternalIdentifierMetadata> hh = new HQLQueryBuilder<ExternalIdentifierMetadata>(ExternalIdentifierMetadata.class);
        hh.addOrder("displayAttributeName", true);
        return hh.getList();
    }

    public List<ExtrinsicObjectMetadata> getAvailableExtrinsicObject() {
        HQLQueryBuilder<ExtrinsicObjectMetadata> hh = new HQLQueryBuilder<ExtrinsicObjectMetadata>(ExtrinsicObjectMetadata.class);
        hh.addOrder("displayAttributeName", true);
        return hh.getList();
    }

    public List<RegistryPackageMetadata> getAvailableRegistryPackage() {
        HQLQueryBuilder<RegistryPackageMetadata> hh = new HQLQueryBuilder<RegistryPackageMetadata>(RegistryPackageMetadata.class);
        hh.addOrder("displayAttributeName", true);
        return hh.getList();
    }

    public List<CardinalityEnum> getAvailableCardinalityEnum() {

        return Arrays.asList(CardinalityEnum.values());

    }

    public CardinalityEnum[] getAvailableCardinalityArray() {
        return CardinalityEnum.values();
    }

    public List<SelectItem> getAvailableCardinality() {

        List<SelectItem> res = new ArrayList<SelectItem>();
        res.add(new SelectItem(CardinalityEnum._00, "00"));
        res.add(new SelectItem(CardinalityEnum._01, "01"));
        res.add(new SelectItem(CardinalityEnum._02, "02"));
        res.add(new SelectItem(CardinalityEnum._11, "11"));
        res.add(new SelectItem(CardinalityEnum._12, "12"));
        res.add(new SelectItem(CardinalityEnum._0star, "0star"));
        res.add(new SelectItem(CardinalityEnum._1star, "1star"));
        return res;

    }

    public List<String> getAvailableCardinalityString() {

        List<String> res = new ArrayList<String>();
        res.add(CardinalityEnum._00.toString());
        res.add(CardinalityEnum._01.toString());
        res.add(CardinalityEnum._02.toString());
        res.add(CardinalityEnum._11.toString());
        res.add(CardinalityEnum._12.toString());
        res.add(CardinalityEnum._0star.toString());
        res.add(CardinalityEnum._1star.toString());
        return res;

    }

    public List<Usage> getAvailableUsages() {
        HQLQueryBuilder<Usage> hh = new HQLQueryBuilder<Usage>(Usage.class);
        List<Usage> lu = hh.getList();
        Comparator<Usage> comp = new Comparator<Usage>() {

            @Override
            public int compare(Usage obj0, Usage obj1) {
                if (obj0 == obj1) {
                    return 0;
                }
                if (obj1 == null) {
                    return -1;
                }
                if (obj0.getAffinity() == null) {
                    if (obj1.getAffinity() != null) {
                        return -1;
                    }
                } else {
                    if (obj1.getAffinity() == null) {
                        return 1;
                    }
                    int i = obj0.getAffinity().getKeyword().compareTo(obj1.getAffinity().getKeyword());
                    if (i != 0) {
                        return i;
                    }
                }
                if (obj0.getTransaction() == null) {
                    if (obj1.getTransaction() != null) {
                        return -1;
                    }
                } else {
                    if (obj1.getTransaction() == null) {
                        return 1;
                    }
                    return obj0.getTransaction().getKeyword().compareTo(obj1.getTransaction().getKeyword());
                }
                return 0;
            }
        };
        Collections.sort(lu, comp);
        return lu;
    }

    @Destroy
    @Remove
    public void destroy() {
        log.info("destroy generatorManager..");
    }

}
