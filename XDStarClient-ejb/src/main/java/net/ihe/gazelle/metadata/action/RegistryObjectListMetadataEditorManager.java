package net.ihe.gazelle.metadata.action;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.metadata.model.ConstraintSpecification;
import net.ihe.gazelle.metadata.model.ExtrinsicObjectMetadata;
import net.ihe.gazelle.metadata.model.RegistryObjectListMetadata;
import net.ihe.gazelle.metadata.model.RegistryPackageMetadata;
import net.ihe.gazelle.metadata.model.describer.ExtrinsicObjectMetadataDescriber;
import net.ihe.gazelle.metadata.model.describer.RegistryPackageMetadataDescriber;
import net.ihe.gazelle.query.ReturnTypeType;
import net.ihe.gazelle.xdstar.core.DocumentFileUpload;
import net.ihe.gazelle.xdstar.validator.ws.ExtractExistingValidationObj;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.log.Log;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.zip.ZipOutputStream;

import static org.apache.commons.lang.CharEncoding.UTF_8;

@Stateful
@Name("registryObjectListMetadataEditorManager")
@Scope(ScopeType.SESSION)
@GenerateInterface("RegistryObjectListMetadataEditorLocal")
public class RegistryObjectListMetadataEditorManager extends CommonROManager implements RegistryObjectListMetadataEditorLocal {

    @Logger
    private static Log LOG;

    @PersistenceContext(unitName = "XDStarClient-PersistenceUnit")
    private EntityManager entityManager;

    // ---  attributes -------
    private RegistryObjectListMetadata selectedRegistryObjectListMetadata;

    private ConstraintSpecification selectedConstraintSpecification;

    private List<RegistryObjectListMetadata> listRegistryObjectListMetadatas;

    private File uploadedRegistryObjectListMetadataFile;
    private String uploadedFileContentType;

    public static void saveValidatorPackClassAsXML(RegistryObjectListMetadata rolmd, OutputStream os) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(RegistryObjectListMetadata.class);
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        m.marshal(rolmd, os);
    }

    public static RegistryObjectListMetadata loadValidatorPackClassAsXML(InputStream is) throws JAXBException, UnsupportedEncodingException {
        JAXBContext jc = JAXBContext.newInstance(RegistryObjectListMetadata.class);
        Unmarshaller u = jc.createUnmarshaller();
        Reader reader = new InputStreamReader(is, StandardCharsets.UTF_8);
        RegistryObjectListMetadata mimi = (RegistryObjectListMetadata) u.unmarshal(reader);
        return mimi;
    }

    public File getUploadedRegistryObjectListMetadataFile() {
        return uploadedRegistryObjectListMetadataFile;
    }

    public void setUploadedRegistryObjectListMetadataFile(
            File uploadedRegistryObjectListMetadataFile) {
        this.uploadedRegistryObjectListMetadataFile = uploadedRegistryObjectListMetadataFile;
    }

    public String getUploadedFileContentType() {
        return uploadedFileContentType;
    }

    public void setUploadedFileContentType(String uploadedFileContentType) {
        this.uploadedFileContentType = uploadedFileContentType;
    }

    public List<RegistryObjectListMetadata> getListRegistryObjectListMetadatas() {
        return listRegistryObjectListMetadatas;
    }

    public void setListRegistryObjectListMetadatas(
            List<RegistryObjectListMetadata> listRegistryObjectListMetadatas) {
        this.listRegistryObjectListMetadatas = listRegistryObjectListMetadatas;
    }

    public RegistryObjectListMetadata getSelectedRegistryObjectListMetadata() {

        return selectedRegistryObjectListMetadata;
    }

    public void setSelectedRegistryObjectListMetadata(RegistryObjectListMetadata selectedRegistryObjectListMetadata) {
        this.selectedRegistryObjectListMetadata = selectedRegistryObjectListMetadata;
    }

    public void exportToXml(RegistryObjectListMetadata selectedRegistryObjectListMetadata) {

        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        RegistryObjectListMetadata res = em.find(RegistryObjectListMetadata.class, selectedRegistryObjectListMetadata.getId());

        if (res != null) {

            JAXBContext jc;
            try {
                jc = JAXBContext.newInstance(RegistryObjectListMetadata.class);
                Marshaller jaxbMarshaller = jc.createMarshaller();
                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                jaxbMarshaller.marshal(res, baos);
                DocumentFileUpload.showFile(new ByteArrayInputStream(baos.toByteArray()), "output.xml", "xml", true);
            } catch (JAXBException e) {
                LOG.error("" + e.getMessage());
            }
        }

    }

    public void initSelectedRegistryObjectListMetadata() {
        this.selectedRegistryObjectListMetadata = new RegistryObjectListMetadata();
    }

    public void reloadSelectedRegistryObjectListMetaData() {
        if ((this.selectedRegistryObjectListMetadata != null) && (this.selectedRegistryObjectListMetadata.getId() != null)) {
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            this.selectedRegistryObjectListMetadata = em.find(RegistryObjectListMetadata.class, this.selectedRegistryObjectListMetadata.getId());
        }
    }

    public void deleteSelectedRegistryObjectListMetadata() {
        if (this.selectedRegistryObjectListMetadata != null) {
            RegistryObjectListMetadata uu = entityManager.find(RegistryObjectListMetadata.class, this.selectedRegistryObjectListMetadata.getId());
            entityManager.remove(uu);
            this.selectedRegistryObjectListMetadata = null;
        }
    }

    public void saveSelectedRegistryObjectListMetadata() {
        if (this.selectedRegistryObjectListMetadata != null) {
            if (this.selectedRegistryObjectListMetadata.getName() == null) {
                FacesMessages.instance().add(Severity.ERROR, "The RegistryObjectList shall contain a name !");
            } else {
                this.selectedRegistryObjectListMetadata = entityManager.merge(this.selectedRegistryObjectListMetadata);
                entityManager.flush();
                FacesMessages.instance().add(Severity.INFO, "The current RegistryObjectList was saved.");
            }
        }
    }

    public void initListRegistryObjectListMetadatas() {
        HQLQueryBuilder<RegistryObjectListMetadata> hql = new HQLQueryBuilder<RegistryObjectListMetadata>(RegistryObjectListMetadata.class);
        this.listRegistryObjectListMetadatas = hql.getList();
        Collections.sort(this.listRegistryObjectListMetadatas);
    }

    public int countListRegistryObjectListMetadatas() {
        return listRegistryObjectListMetadatas.size();
    }

    public void copyElements(RegistryObjectListMetadata original, RegistryObjectListMetadata cl) {

        cl.setName(original.getName());
        cl.setDisplayAttributeName(original.getDisplayAttributeName() + "_COPY");
        cl.getUsages().addAll(original.getUsages());
        cl.getRegistryPackage().addAll(original.getRegistryPackage());

        for (ExtrinsicObjectMetadataDescriber eid : original.getExtrinsicObject()) {
            ExtrinsicObjectMetadata ei = copyExtrinsicObject(eid.getEom());
            cl.addExtrinsicObject(ei, eid.getCardinality().toString());
        }

        for (RegistryPackageMetadataDescriber eid : original.getRegistryPackage()) {
            RegistryPackageMetadata rp = copyRegistryPackage(eid.getRpm());
            cl.addRegistryPackage(rp, eid.getCardinality().toString());
        }
    }

    private RegistryPackageMetadata copyRegistryPackage(RegistryPackageMetadata rp) {
        RegistryPackageMetadata ss = new RegistryPackageMetadata();
        ss.setDisplayAttributeName(rp.getDisplayAttributeName());
        ss.setName(rp.getName());
        ss.setUuid(rp.getUuid());
        ss.setValue(rp.getValue());
        return ss;
    }

    private ExtrinsicObjectMetadata copyExtrinsicObject(ExtrinsicObjectMetadata ei) {
        ExtrinsicObjectMetadata ss = new ExtrinsicObjectMetadata();
        ss.setDisplayAttributeName(ei.getDisplayAttributeName());
        ss.setName(ei.getName());
        ss.setUuid(ei.getUuid());
        ss.setValue(ei.getValue());
        return ss;
    }

    public void cloneAndPersistMetadata(RegistryObjectListMetadata meta) {

        RegistryObjectListMetadata cl = new RegistryObjectListMetadata();
        meta = entityManager.find(RegistryObjectListMetadata.class, meta.getId());
        this.copyElements(meta, cl);
        cl.setExtraConstraintSpecifications(extraConstraintCopyList(meta.getExtraConstraintSpecifications()));
        entityManager.merge(cl);
        entityManager.flush();

    }

    public List<SelectItem> getListReturnTypes() {
        List<SelectItem> res = new ArrayList<SelectItem>();
        res.add(new SelectItem(ReturnTypeType.LEAFCLASS, "LeafClass"));
        res.add(new SelectItem(ReturnTypeType.LEAFCLASSWITHREPOSITORYITEM, "LeafClassWithRepositoryItem"));
        res.add(new SelectItem(ReturnTypeType.OBJECTREF, "ObjectRef"));
        res.add(new SelectItem(ReturnTypeType.REGISTRY_OBJECT, "RegistryObject"));
        return res;
    }

    // --- destroy ---------------
    @Destroy
    @Remove
    public void destroy() {
        LOG.info("destroy mETADATARegistryObjectListMetadataEditorManager..");
    }

    private Map<Integer, String> getMapFilesContent() {
        Map<Integer, String> map = new HashMap<Integer, String>();
        for (RegistryObjectListMetadata rolmd : RegistryObjectListMetadata.getAllListRegistryObjectListMetadata()) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                saveValidatorPackClassAsXML(rolmd, baos);
                map.put(rolmd.getId(), baos.toString(UTF_8));
            } catch (UnsupportedEncodingException e) {
                LOG.error("" + e.getMessage());
            } catch (JAXBException e) {
                LOG.error("" + e.getMessage());
            }
        }
        return map;
    }

    public void downloadAll() {
        Map<Integer, String> mapFiles = getMapFilesContent();
        ZipOutputStream zos = null;
        try {
            File zipFile = File.createTempFile("rolmd_", ".zip");
            zos = new ZipOutputStream(new FileOutputStream(zipFile));
            DocumentFileUpload.zip(mapFiles, zos);
            zos.close();
            DocumentFileUpload.showFile(zipFile, "zip", true);
        } catch (FileNotFoundException e) {
            LOG.error("" + e.getMessage());
        } catch (IOException e) {
            LOG.error("" + e.getMessage());
        } finally {
            if (zos != null) {
                try {
                    zos.close();
                } catch (IOException e) {
                    LOG.error("" + e.getMessage());
                }
            }
        }
    }

    public void uploadListener(FileUploadEvent event) {
        UploadedFile item = event.getUploadedFile();
        String uploadedFilename = item.getName();
        uploadedFileContentType = item.getContentType();
        FileOutputStream fos = null;
        try {
            uploadedRegistryObjectListMetadataFile = File.createTempFile("adq", uploadedFilename);
            fos = new FileOutputStream(uploadedRegistryObjectListMetadataFile);
            fos.write(item.getData());
        } catch (FileNotFoundException e) {
            LOG.error("" + e.getMessage());
        } catch (IOException e) {
            LOG.error("" + e.getMessage());
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    LOG.error("" + e.getMessage());
                }
            }
        }
    }

    public void initSelectedExtraConstraint() {
        this.selectedConstraintSpecification = new ConstraintSpecification();
    }

    public void saveSelectedExtraConstraint() {

        if (this.selectedConstraintSpecification.getId() != null) {
            return;
        } else {
            if (this.selectedRegistryObjectListMetadata != null) {
                this.selectedRegistryObjectListMetadata.getExtraConstraintSpecifications().add(selectedConstraintSpecification);
            }
        }
    }

    public ConstraintSpecification getSelectedConstraintSpecification() {
        return selectedConstraintSpecification;
    }

    public void setSelectedConstraintSpecification(ConstraintSpecification selectedConstraintSpecification) {
        this.selectedConstraintSpecification = selectedConstraintSpecification;
    }

    public void importRegistryObjectListMetadata() {
        if (this.uploadedRegistryObjectListMetadataFile != null) {
            try {
                if (uploadedFileContentType.contains("zip")) {
                    List<String> listUploaded = DocumentFileUpload.unzip(this.uploadedRegistryObjectListMetadataFile);
                    for (String string : listUploaded) {
                        RegistryObjectListMetadata newRolmd = (RegistryObjectListMetadata) loadValidatorPackClassAsXML(new ByteArrayInputStream
                                (string.getBytes(StandardCharsets.UTF_8)));
                        newRolmd.setRegistryPackage(ExtractExistingValidationObj.getExistingRegistryPackage(newRolmd.getRegistryPackage()));
                        newRolmd.setExtrinsicObject(ExtractExistingValidationObj.getExistingExtrinsicObject(newRolmd.getExtrinsicObject()));
                        newRolmd.setUsages(ExtractExistingValidationObj.getExistingUsages(newRolmd.getUsages(), entityManager));

                        EntityManager em = (EntityManager) Component.getInstance("entityManager");
                        em.merge(newRolmd);
                        em.flush();
                    }
                    FacesMessages.instance().add(Severity.INFO, "The uploaded RegistryObjectListMetadatas are saved.");
                } else {
                    RegistryObjectListMetadata newRolmd = (RegistryObjectListMetadata) loadValidatorPackClassAsXML(new FileInputStream
                            (uploadedRegistryObjectListMetadataFile));
                    newRolmd.setRegistryPackage(ExtractExistingValidationObj.getExistingRegistryPackage(newRolmd.getRegistryPackage()));
                    newRolmd.setExtrinsicObject(ExtractExistingValidationObj.getExistingExtrinsicObject(newRolmd.getExtrinsicObject()));
                    newRolmd.setUsages(ExtractExistingValidationObj.getExistingUsages(newRolmd.getUsages(), entityManager));

                    EntityManager em = (EntityManager) Component.getInstance("entityManager");
                    em.merge(newRolmd);
                    em.flush();
                    FacesMessages.instance().add(Severity.INFO, "The uploaded RegistryObjectListMetadata is saved.");
                }
            } catch (UnsupportedEncodingException e) {
                FacesMessages.instance().add(Severity.ERROR, "Unsupported encoding of the document," + e.getMessage());
                LOG.info("unsupported encoding : ", e);
            } catch (JAXBException e) {
                FacesMessages.instance().add(Severity.ERROR, "Not able to parse the specified xml : " + e.getMessage());
                LOG.info("jaxb exception : ", e);
            } catch (Exception e) {
                FacesMessages.instance().add(Severity.ERROR, "Problem occurs when parsing the specified xml: " + e.getMessage());
                LOG.info("exception : ", e);
            } finally {
                this.uploadedRegistryObjectListMetadataFile = null;
            }
        }
    }

}