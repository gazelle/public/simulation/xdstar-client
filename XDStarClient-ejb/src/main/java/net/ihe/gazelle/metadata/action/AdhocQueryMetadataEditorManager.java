package net.ihe.gazelle.metadata.action;

import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.metadata.model.AdhocQueryMetadata;
import net.ihe.gazelle.metadata.model.AdhocQueryMetadataQuery;
import net.ihe.gazelle.metadata.model.ConstraintSpecification;
import net.ihe.gazelle.metadata.model.RegistryObjectMetadata;
import net.ihe.gazelle.metadata.model.describer.SlotMetadataDescriber;
import net.ihe.gazelle.query.ReturnTypeType;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.xdstar.core.DocumentFileUpload;
import net.ihe.gazelle.xdstar.util.IHERegistryObjectMetadataSpecification;
import net.ihe.gazelle.xdstar.validator.ws.ExtractExistingValidationObj;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.log.Log;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

import javax.ejb.AccessTimeout;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.bind.JAXBException;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.zip.ZipOutputStream;

import static org.apache.commons.lang.CharEncoding.UTF_8;

@Stateful
@Name("adhocQueryMetadataEditorManager")
@Scope(ScopeType.SESSION)
@AccessTimeout(value = 10000)
@GenerateInterface("AdhocQueryMetadataEditorLocal")
public class AdhocQueryMetadataEditorManager extends CommonROManager implements AdhocQueryMetadataEditorLocal {

    @Logger
    private static Log log;

    @PersistenceContext(unitName = "XDStarClient-PersistenceUnit")
    private EntityManager entityManager;

    // ---  attributes -------
    private net.ihe.gazelle.metadata.model.AdhocQueryMetadata selectedAdhocQueryMetadata;

    private List<AdhocQueryMetadata> listAdhocQueryMetadatas;

    private List<AdhocQueryMetadata> listAdhocQueryMetadatasDataModelList;

    private ConstraintSpecification selectedConstraintSpecification;

    private File uploadedAdhocQueryFile;

    private String uploadedFileContentType;

    public String getUploadedFileContentType() {
        return uploadedFileContentType;
    }

    public void setUploadedFileContentType(String uploadedFileContentType) {
        this.uploadedFileContentType = uploadedFileContentType;
    }

    public File getUploadedAdhocQueryFile() {
        return uploadedAdhocQueryFile;
    }

    public void setUploadedAdhocQueryFile(File uploadedAdhocQueryFile) {
        this.uploadedAdhocQueryFile = uploadedAdhocQueryFile;
    }

    public List<AdhocQueryMetadata> getListAdhocQueryMetadatas() {
        return listAdhocQueryMetadatas;
    }

    public void setListAdhocQueryMetadatas(
            List<AdhocQueryMetadata> listAdhocQueryMetadatas) {
        this.listAdhocQueryMetadatas = listAdhocQueryMetadatas;
    }

    public GazelleListDataModel<AdhocQueryMetadata> getListAdhocQueryMetadatasDataModelList() {
        List<AdhocQueryMetadata> res = getListAdhocQueryMetadatas();
        GazelleListDataModel<AdhocQueryMetadata> dm = new GazelleListDataModel<AdhocQueryMetadata>(res);
        return dm;
    }

    public AdhocQueryMetadata getSelectedAdhocQueryMetadata() {

        return selectedAdhocQueryMetadata;
    }

    public void setSelectedAdhocQueryMetadata(AdhocQueryMetadata selectedAdhocQueryMetadata) {
        this.selectedAdhocQueryMetadata = selectedAdhocQueryMetadata;
    }

    public void initSelectedAdhocQueryMetadata() {
        this.selectedAdhocQueryMetadata = new AdhocQueryMetadata();
    }

    public void reloadSelectedAdhocQueryMetaData() {
        if ((this.selectedAdhocQueryMetadata != null) && (this.selectedAdhocQueryMetadata.getId() != null)) {
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            this.selectedAdhocQueryMetadata = em.find(AdhocQueryMetadata.class, this.selectedAdhocQueryMetadata.getId());
        }
    }


    public void deleteSelectedAdhocQueryMetadata() {
        if (this.selectedAdhocQueryMetadata != null) {
            AdhocQueryMetadata uu = entityManager.find(AdhocQueryMetadata.class, this.selectedAdhocQueryMetadata.getId());
            entityManager.remove(uu);
            this.selectedAdhocQueryMetadata = null;
        }
    }

    public void saveSelectedAdhocQueryMetadata() {
        if (this.selectedAdhocQueryMetadata != null) {
            if (this.selectedAdhocQueryMetadata.getUuid() == null) {
                FacesMessages.instance().add(Severity.ERROR, "The AdhocQuery shall contain an UUID !");
            } else {
                this.selectedAdhocQueryMetadata = entityManager.merge(this.selectedAdhocQueryMetadata);
                entityManager.flush();
                FacesMessages.instance().add(Severity.INFO, "The current AdhocQuery was saved.");
            }
        }
    }

    public void initListAdhocQueryMetadatas() {
        HQLQueryBuilder<AdhocQueryMetadata> hql = new HQLQueryBuilder<AdhocQueryMetadata>(AdhocQueryMetadata.class);
        this.listAdhocQueryMetadatas = hql.getList();
        Collections.sort(this.listAdhocQueryMetadatas);
    }

    public int countListAdhocQueryMetadatas() {
        return listAdhocQueryMetadatas.size();
    }

    @SuppressWarnings("unused")
    public void cloneAndPersistMetadata(AdhocQueryMetadata meta) {
        AdhocQueryMetadata cl = new AdhocQueryMetadata();
        meta = entityManager.find(AdhocQueryMetadata.class, meta.getId());
        for (Usage us : meta.getUsages()) {
        }
        for (SlotMetadataDescriber slmdd : meta.getSlotDescriber()) {
        }
        this.copyElements(meta, cl);
        cl.setReturnTypes(new ArrayList<ReturnTypeType>(meta.getReturnTypes()));
        cl.setExtraConstraintSpecifications(extraConstraintCopyList(meta.getExtraConstraintSpecifications()));
        entityManager.merge(cl);
        entityManager.flush();
    }

    public List<SelectItem> getListReturnTypes() {
        List<SelectItem> res = new ArrayList<SelectItem>();
        res.add(new SelectItem(ReturnTypeType.LEAFCLASS, "LeafClass"));
        res.add(new SelectItem(ReturnTypeType.LEAFCLASSWITHREPOSITORYITEM, "LeafClassWithRepositoryItem"));
        res.add(new SelectItem(ReturnTypeType.OBJECTREF, "ObjectRef"));
        res.add(new SelectItem(ReturnTypeType.REGISTRY_OBJECT, "RegistryObject"));
        return res;
    }


    private Map<Integer, String> getMapFilesContent() {
        Map<Integer, String> map = new HashMap<Integer, String>();
        for (RegistryObjectMetadata rom : this.getListAdhocQueryMetadataSpecifications()) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                IHERegistryObjectMetadataSpecification.saveRegistryObjectMetadataSpecification(rom, baos);
                map.put(rom.getId(), baos.toString(UTF_8));
            } catch (UnsupportedEncodingException e) {
                log.error("" + e.getMessage());
            } catch (JAXBException e) {
                log.error("" + e.getMessage());
            }
        }
        return map;
    }


    public void downloadAll() {
        Map<Integer, String> mapFiles = getMapFilesContent();
        ZipOutputStream zos = null;
        try {
            File zipFile = File.createTempFile("ro_", ".zip");
            zos = new ZipOutputStream(new FileOutputStream(zipFile));
            DocumentFileUpload.zip(mapFiles, zos);
            zos.close();
            DocumentFileUpload.showFile(zipFile, "zip", true);
        } catch (FileNotFoundException e) {
            log.error("" + e.getMessage());
        } catch (IOException e) {
            log.error("" + e.getMessage());
        } finally {
            if (zos != null) {
                try {
                    zos.close();
                } catch (IOException e) {
                    log.error("" + e.getMessage());
                }
            }
        }
    }


    private List<AdhocQueryMetadata> getListAdhocQueryMetadataSpecifications() {
        AdhocQueryMetadataQuery qq = new AdhocQueryMetadataQuery();
        List<AdhocQueryMetadata> res = qq.getList();
        if (res != null) {
            Collections.sort(res);
        }
        return res;
    }


    public void importAdhocQuery() {
        if (this.uploadedAdhocQueryFile != null) {
            try {
                if (uploadedFileContentType.contains("zip")) {
                    List<String> listUploaded = DocumentFileUpload.unzip(this.uploadedAdhocQueryFile);
                    for (String string : listUploaded) {
                        AdhocQueryMetadata newadq = (AdhocQueryMetadata) IHERegistryObjectMetadataSpecification.load(new ByteArrayInputStream
                                (string.getBytes(StandardCharsets.UTF_8)));
                        EntityManager em = (EntityManager) Component.getInstance("entityManager");
                        if (newadq.getUsages() != null && newadq.getUsages().size() > 0) {
                            newadq.setUsages(ExtractExistingValidationObj.getExistingUsages(newadq.getUsages(), entityManager));
                        }

                        em.merge(newadq);
                        em.flush();
                    }
                    FacesMessages.instance().add(Severity.INFO, "The uploaded AdhocQueryMetadatas are saved.");
                } else {
                    AdhocQueryMetadata newams = (AdhocQueryMetadata) IHERegistryObjectMetadataSpecification.load(new FileInputStream
                            (uploadedAdhocQueryFile));
                    if (newams.getUsages() != null && newams.getUsages().size() > 0) {
                        newams.setUsages(ExtractExistingValidationObj.getExistingUsages(newams.getUsages(), entityManager));
                    }

                    EntityManager em = (EntityManager) Component.getInstance("entityManager");
                    em.merge(newams);
                    em.flush();
                    FacesMessages.instance().add(Severity.INFO, "The uploaded AdhocQueryMetadata is saved.");
                }
            } catch (UnsupportedEncodingException e) {
                FacesMessages.instance().add(Severity.ERROR, "Unsupported encoding of the document," + e.getMessage());
                log.info("unsupported encoding : ", e);
            } catch (JAXBException e) {
                FacesMessages.instance().add(Severity.ERROR, "Not able to parse the specified xml : " + e.getMessage());
                log.info("jaxb exception : ", e);
            } catch (Exception e) {
                FacesMessages.instance().add(Severity.ERROR, "Problem occurs when parsing the specified xml: " + e.getMessage());
                log.info("exception : ", e);
            } finally {
                this.uploadedAdhocQueryFile = null;
            }
        }
    }

    public void uploadListener(FileUploadEvent event) {
        UploadedFile item = event.getUploadedFile();
        String uploadedFilename = item.getName();
        uploadedFileContentType = item.getContentType();
        FileOutputStream fos = null;
        try {
            uploadedAdhocQueryFile = File.createTempFile("adq", uploadedFilename);
            fos = new FileOutputStream(uploadedAdhocQueryFile);
            fos.write(item.getData());
        } catch (FileNotFoundException e) {
            log.error("" + e.getMessage());
        } catch (IOException e) {
            log.error("" + e.getMessage());
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    log.error("" + e.getMessage());
                }
            }
        }
    }

    public void initSelectedExtraConstraint() {
        this.selectedConstraintSpecification = new ConstraintSpecification();
    }

    public void saveSelectedExtraConstraint() {

        if (this.selectedConstraintSpecification.getId() != null) {
            return;
        } else {
            if (this.selectedAdhocQueryMetadata != null) {
                this.selectedAdhocQueryMetadata.getExtraConstraintSpecifications().remove(selectedConstraintSpecification);
                this.selectedAdhocQueryMetadata.getExtraConstraintSpecifications().add(selectedConstraintSpecification);
            }
        }

    }

    public ConstraintSpecification getSelectedConstraintSpecification() {
        return selectedConstraintSpecification;
    }

    public void setSelectedConstraintSpecification(ConstraintSpecification selectedConstraintSpecification) {
        this.selectedConstraintSpecification = selectedConstraintSpecification;
    }


    // --- destroy ---------------
    @Destroy
    @Remove
    public void destroy() {
        log.info("destroy mETADATAAdhocQueryMetadataEditorManager..");
    }


}