package net.ihe.gazelle.metadata.action;

import java.io.Serializable;
import java.util.WeakHashMap;

import net.ihe.gazelle.metadata.model.ClassificationMetaData;
import net.ihe.gazelle.metadata.model.ExternalIdentifierMetadata;
import net.ihe.gazelle.metadata.model.RegistryObjectMetadata;
import net.ihe.gazelle.metadata.model.RegistryPackageMetadata;
import net.ihe.gazelle.metadata.model.SlotMetadata;
import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExternalIdentifierType;
import net.ihe.gazelle.rim.InternationalStringType;
import net.ihe.gazelle.rim.LocalizedStringType;
import net.ihe.gazelle.rim.RegistryObjectType;
import net.ihe.gazelle.rim.RegistryPackageType;
import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.rim.ValueListType;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("metadataGen")
@Scope(ScopeType.PAGE)
public class MetadataCommonGen implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static WeakHashMap<RegistryObjectType, RegistryObjectMetadata> listGenerations = new WeakHashMap<RegistryObjectType, RegistryObjectMetadata>();
	
	public static WeakHashMap<SlotType1, SlotMetadata> listSlots = new WeakHashMap<SlotType1, SlotMetadata>();
	
	public static RegistryPackageType generateRegistryPackageFromMetadata(RegistryPackageMetadata rpm){
		RegistryPackageType rp = new RegistryPackageType();
		MetadataCommonGen.updateRegistryObjectFromMetadata(rp, rpm);
		listGenerations.put(rp, rpm);
		return rp;
	}
	
	public static ClassificationType generateClassificationFromMetadata(ClassificationMetaData clm){
		ClassificationType cl = new ClassificationType();
		MetadataCommonGen.updateRegistryObjectFromMetadata(cl, clm);
		
		if (clm.getUuid() != null){
			cl.setClassificationScheme(clm.getUuid());
		}
		
		listGenerations.put(cl, clm);
		return cl;
	}
	
	public static ExternalIdentifierType generateExternalIdentifierFromMetadata(ExternalIdentifierMetadata extm){
		ExternalIdentifierType ext = new ExternalIdentifierType();
		MetadataCommonGen.updateRegistryObjectFromMetadata(ext, extm);
		
		if (extm.getUuid() != null){
			ext.setIdentificationScheme(extm.getUuid());
		}
		
		if (extm.getValue() != null){
			ext.setValue(extm.getValue());
		}
		
		listGenerations.put(ext, extm);
		return ext;
	}
	
	public static SlotType1 generateSlotFromMetadata(SlotMetadata slm){
		SlotType1 sl = new SlotType1();
		sl.setValueList(new ValueListType());
		if (slm.getDefaultValue() != null){
			sl.getValueList().getValue().add(slm.getDefaultValue());
		}
		if (slm.getName() != null){
			sl.setName(slm.getName());
		}
		listSlots.put(sl, slm);
		return sl;
	}
	
	public static void updateRegistryObjectFromMetadata(RegistryObjectType ro, RegistryObjectMetadata rom){
		for (ClassificationMetaData cl : rom.getClassification_required()){
				ro.addClassification(MetadataCommonGen.generateClassificationFromMetadata(cl));
		}
		
		for (ExternalIdentifierMetadata ei : rom.getExt_identifier_required()){
				ro.addExternalIdentifier(MetadataCommonGen.generateExternalIdentifierFromMetadata(ei));
		}
		
		for (SlotMetadata sl : rom.getSlot_required()){
				ro.addSlot(MetadataCommonGen.generateSlotFromMetadata(sl));
			
		}
		
		if (rom.getName() != null){
			ro.setName(new InternationalStringType());
			LocalizedStringType loc = new LocalizedStringType();
			loc.setValue(rom.getName());
			ro.getName().getLocalizedString().add(loc);
		}
		
	}

}
