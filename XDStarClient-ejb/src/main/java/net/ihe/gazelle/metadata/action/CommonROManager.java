package net.ihe.gazelle.metadata.action;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.metadata.model.*;
import net.ihe.gazelle.metadata.model.describer.*;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.xdstar.core.DocumentFileUpload;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

@Name("commonROManager")
@GenerateInterface("CommonROLocal")
public class CommonROManager implements CommonROLocal {

	private static final Logger LOG = LoggerFactory.getLogger(CommonROManager.class);

	private ClassificationMetaData selectedClassification;

	private ClassificationMetadataDescriber selectedClassificationDescriber;

	private ClassificationMetaData selectedClassificationOptional;

	private SlotMetadata selectedSlot;

	private SlotMetadataDescriber selectedSlotDescriber;

	private ExternalIdentifierMetadataDescriber selectedExternalIdentifierMetadataDescriber;

	private ExternalIdentifierMetadata selectedExt;

	private ExternalIdentifierMetadata selectedExtOptional;
	
	private ExtrinsicObjectMetadataDescriber selectedExtrinsicObjectDescriber;

	private ExtrinsicObjectMetadata selectedExtrinsicObject;
	
	private RegistryPackageMetadataDescriber selectedRegistryPackageDescriber;

	private RegistryPackageMetadata selectedRegistryPackage;
	
	private String selectedCardinalityEnum;

	private Usage selectedUsage;

	private Boolean editMode = false;
	
	public String getSelectedCardinalityEnum() {
		return selectedCardinalityEnum;
	}

	public void setSelectedCardinalityEnum(String selectedCardinalityEnum) {
		this.selectedCardinalityEnum = selectedCardinalityEnum;
	}

	public Usage getSelectedUsage() {
		return selectedUsage;
	}

	public void setSelectedUsage(Usage selectedUsage) {
		this.selectedUsage = selectedUsage;
	}

	public ExternalIdentifierMetadata getSelectedExtOptional() {
		return selectedExtOptional;
	}

	public void setSelectedExtOptional(
			ExternalIdentifierMetadata selectedExtOptional) {
		this.selectedExtOptional = selectedExtOptional;
	}

	public ExternalIdentifierMetadata getSelectedExt() {
		return selectedExt;
	}

	public void setSelectedExt(
			ExternalIdentifierMetadata selectedExtRequired) {
		this.selectedExt = selectedExtRequired;
	}

	public SlotMetadata getSelectedSlot() {
		return selectedSlot;
	}

	public void setSelectedSlot(SlotMetadata selectedSlot) {
		this.selectedSlot = selectedSlot;
	}

	public ClassificationMetaData getSelectedClassificationOptional() {
		return selectedClassificationOptional;
	}

	public void setSelectedClassificationOptional(
			ClassificationMetaData selectedClassificationOptional) {
		this.selectedClassificationOptional = selectedClassificationOptional;
	}

	public ClassificationMetaData getSelectedClassification() {
		return selectedClassification;
	}

	public void setSelectedClassification(
			ClassificationMetaData selectedClassificationRequired) {
		this.selectedClassification = selectedClassificationRequired;
	}

	public void initSelectedSlot(){
		this.selectedSlot = new SlotMetadata();
		this.selectedCardinalityEnum = null;
	}

	public void saveSelectedSlot(SlotMetadata slot, String cardinality){
		if (this.selectedSlotDescriber != null){
			CardinalityEnum cenum = CardinalityEnum.getCardinalityFromValue(cardinality);
			this.selectedSlotDescriber.setCardinality(cenum);
			this.selectedSlotDescriber.setSlotMetadata(selectedSlot);
			EntityManager em = (EntityManager)Component.getInstance("entityManager");
			SlotMetadataDescriber res = em.find(SlotMetadataDescriber.class, selectedSlotDescriber.getId());
			res.setSlotMetadata(slot);
			res.setCardinality(cenum);
			this.selectedSlotDescriber = em.merge(res);
			em.flush();
		}
	}

	public void exportToXml(RegistryObjectMetadata selectedRegistryObjectMetadata) {

		EntityManager em = (EntityManager)Component.getInstance("entityManager");
		RegistryObjectMetadata res = em.find(RegistryObjectMetadata.class, selectedRegistryObjectMetadata.getId());

		if (res != null){

			JAXBContext jc;
			try {
				jc = JAXBContext.newInstance("net.ihe.gazelle.metadata.model");
				Marshaller jaxbMarshaller = jc.createMarshaller();
				jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				jaxbMarshaller.marshal(res, baos);
				DocumentFileUpload.showFile(new ByteArrayInputStream(baos.toByteArray()), "output.xml","xml",true);		
			} catch (JAXBException e) {
				LOG.error("" + e.getMessage());
			}
		}

	}

    public void copyElements(RegistryObjectMetadata original, RegistryObjectMetadata cl) {

        cl.setName(original.getName());
        cl.setValue(original.getValue());
        cl.setUuid(original.getUuid());
        cl.setDisplayAttributeName(original.getDisplayAttributeName() + "_COPY");
        cl.setUsages(new ArrayList<Usage>(original.getUsages()));
        cl.getClassificationDescriber().addAll(classificationDescriberCopyList(original.getClassificationDescriber()));
        cl.getExt_identifierDescriber().addAll(externalIdentifierDescriberCopyList(original.getExt_identifierDescriber()));
        cl.getSlotDescriber().addAll(slotMetadataDescriberCopyList(original.getSlotDescriber()));
    }

    public List<ConstraintSpecification> extraConstraintCopyList(List<ConstraintSpecification> oldExtraConstraintList) {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        List<ConstraintSpecification> res = new ArrayList<ConstraintSpecification>();
        for (ConstraintSpecification oldExtraConstraint : oldExtraConstraintList) {
            ConstraintSpecification newExtraConstraint = new ConstraintSpecification();
            newExtraConstraint.setDescription(oldExtraConstraint.getDescription());
            newExtraConstraint.setKind(oldExtraConstraint.getKind());
            newExtraConstraint.setXpath(oldExtraConstraint.getXpath());
            res.add(newExtraConstraint);
            em.merge(newExtraConstraint);
        }
        return res;
    }

	private List<SlotMetadataDescriber> slotMetadataDescriberCopyList(List<SlotMetadataDescriber> oldSlotDescriberList) {
		List<SlotMetadataDescriber> res = new ArrayList<SlotMetadataDescriber>();
		for (SlotMetadataDescriber oldSlotMetadataDescriber : oldSlotDescriberList) {
			SlotMetadataDescriber newSlotMetadataDescriber = new SlotMetadataDescriber();
			newSlotMetadataDescriber.setCardinality(oldSlotMetadataDescriber.getCardinality());
			newSlotMetadataDescriber.setSlotMetadata(slotMetadataCopyList(oldSlotMetadataDescriber.getSlotMetadata()));
			res.add(newSlotMetadataDescriber);
		}
		return res;
	}

	private SlotMetadata slotMetadataCopyList(SlotMetadata oldSlotMetadata) {
		SlotMetadata res = new SlotMetadata();
		res.setAttribute(oldSlotMetadata.getAttribute());
		res.setDefaultValue(oldSlotMetadata.getDefaultValue());
		res.setIsNumber(oldSlotMetadata.getIsNumber());
		res.setMultiple(oldSlotMetadata.getMultiple());
		res.setName(oldSlotMetadata.getName());
		res.setRegex(oldSlotMetadata.getRegex());
		res.setSupportAndOr(oldSlotMetadata.getSupportAndOr());
		res.setValueset(oldSlotMetadata.getValueset());
		return res;
	}

	private List<ExternalIdentifierMetadataDescriber> externalIdentifierDescriberCopyList(List<ExternalIdentifierMetadataDescriber> oldExt_identifierDescriberList) {
		List<ExternalIdentifierMetadataDescriber> res = new ArrayList<ExternalIdentifierMetadataDescriber>();
		for (ExternalIdentifierMetadataDescriber oldExternalIdentifierDescriber : oldExt_identifierDescriberList) {
			ExternalIdentifierMetadataDescriber newExternalIdentifierDescriber = new ExternalIdentifierMetadataDescriber();
			newExternalIdentifierDescriber.setCardinality(oldExternalIdentifierDescriber.getCardinality());
			newExternalIdentifierDescriber.setEim(oldExternalIdentifierDescriber.getEim());
			res.add(newExternalIdentifierDescriber);
		}
		return res;
	}

	private List<ClassificationMetadataDescriber> classificationDescriberCopyList(List<ClassificationMetadataDescriber> oldClassificationMetadataDescriberList) {
		List<ClassificationMetadataDescriber> res = new ArrayList<ClassificationMetadataDescriber>();
		for (ClassificationMetadataDescriber oldClassificationMetadataDescriber : oldClassificationMetadataDescriberList) {
			ClassificationMetadataDescriber newClassificationMetadataDescriber = new ClassificationMetadataDescriber();
			newClassificationMetadataDescriber.setCardinality(oldClassificationMetadataDescriber.getCardinality());
			newClassificationMetadataDescriber.setCmd(oldClassificationMetadataDescriber.getCmd());
			res.add(newClassificationMetadataDescriber);
		}
		return res;
	}

	private List<Usage> usagesCopyList(List<Usage> oldUsages) {
		List<Usage> res = new ArrayList<Usage>();
		for (Usage oldUsage : oldUsages) {
			Usage newUsage = new Usage();
			newUsage.setAction(oldUsage.getAction());
			newUsage.setAffinity(oldUsage.getAffinity());
			newUsage.setKeyword(oldUsage.getKeyword());
			newUsage.setTransaction(oldUsage.getTransaction());
			res.add(newUsage);
		}
		return res;
	}
	
	public ClassificationMetadataDescriber getSelectedClassificationDescriber() {
		return selectedClassificationDescriber;
	}

	public void setSelectedClassificationDescriber(
			ClassificationMetadataDescriber selectedClassificationDescriber) {
		this.selectedClassificationDescriber = selectedClassificationDescriber;
	}
	
	public void editClassificationDescriber(ClassificationMetadataDescriber selectedClassificationDescriber) {
		this.selectedClassificationDescriber = selectedClassificationDescriber;
		this.selectedClassification = this.selectedClassificationDescriber.getCmd();
		this.selectedCardinalityEnum = this.selectedClassificationDescriber.getCardinality().toString();
	}

	public ExternalIdentifierMetadataDescriber getSelectedExternalIdentifierMetadataDescriber() {
		return selectedExternalIdentifierMetadataDescriber;
	}

	public void setSelectedExternalIdentifierMetadataDescriber(
			ExternalIdentifierMetadataDescriber selectedExternalIdentifierMetadataDescriber) {
		this.selectedExternalIdentifierMetadataDescriber = selectedExternalIdentifierMetadataDescriber;
	}

	public SlotMetadataDescriber getSelectedSlotDescriber() {
		return selectedSlotDescriber;
	}

	public void setSelectedSlotDescriber(SlotMetadataDescriber selectedSlotDescriber) {
		this.selectedSlotDescriber = selectedSlotDescriber;
	}
	
	public void initSelectedSlotDescriber(SlotMetadataDescriber selectedSlotDescriber) {
		this.selectedSlotDescriber = selectedSlotDescriber;
		this.selectedCardinalityEnum = this.selectedSlotDescriber.getCardinality().toString();
		this.selectedSlot = this.selectedSlotDescriber.getSlotMetadata();
	}

	public ExtrinsicObjectMetadata getSelectedExtrinsicObject() {
		return selectedExtrinsicObject;
	}

	public void setSelectedExtrinsicObject(ExtrinsicObjectMetadata selectedExtrinsicObject) {
		this.selectedExtrinsicObject = selectedExtrinsicObject;
	}

	public RegistryPackageMetadata getSelectedRegistryPackage() {
		return selectedRegistryPackage;
	}

	public void setSelectedRegistryPackage(RegistryPackageMetadata selectedRegistryPackage) {
		this.selectedRegistryPackage = selectedRegistryPackage;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	public ExtrinsicObjectMetadataDescriber getSelectedExtrinsicObjectDescriber() {
		return selectedExtrinsicObjectDescriber;
	}

	public void setSelectedExtrinsicObjectDescriber(
			ExtrinsicObjectMetadataDescriber selectedExtrinsicObjectDescriber) {
		this.selectedExtrinsicObjectDescriber = selectedExtrinsicObjectDescriber;
	}

	public RegistryPackageMetadataDescriber getSelectedRegistryPackageDescriber() {
		return selectedRegistryPackageDescriber;
	}

	public void setSelectedRegistryPackageDescriber(
			RegistryPackageMetadataDescriber selectedRegistryPackageDescriber) {
		this.selectedRegistryPackageDescriber = selectedRegistryPackageDescriber;
	}

}
