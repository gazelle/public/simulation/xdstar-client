package net.ihe.gazelle.metadata.action;

import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.metadata.model.ExternalIdentifierMetadata;
import net.ihe.gazelle.metadata.model.ExternalIdentifierMetadataQuery;
import net.ihe.gazelle.metadata.model.RegistryObjectMetadata;
import net.ihe.gazelle.metadata.model.describer.SlotMetadataDescriber;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.xdstar.core.DocumentFileUpload;
import net.ihe.gazelle.xdstar.util.IHERegistryObjectMetadataSpecification;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.bind.JAXBException;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

import static org.apache.commons.lang.CharEncoding.UTF_8;

@Stateful
@Name("externalIdentifierMetadataEditorManager")
@Scope(ScopeType.SESSION)
@GenerateInterface("ExternalIdentifierMetadataEditorLocal")
public class ExternalIdentifierMetadataEditorManager extends CommonROManager implements ExternalIdentifierMetadataEditorLocal {


    private static Logger log = LoggerFactory.getLogger(ExternalIdentifierMetadataEditorManager.class);

    @PersistenceContext(unitName = "XDStarClient-PersistenceUnit")
    private EntityManager entityManager;

    // ---  attributes -------

    private ExternalIdentifierMetadata selectedExternalIdentifierMetadata;

    private String selectedCardinalityEnum;

    private List<ExternalIdentifierMetadata> listExternalIdentifierMetadatas;

    private List<ExternalIdentifierMetadata> listExternalIdentifierMetadatasDataModelList;

    private File uploadedExternalIdentifierFile;

    private String uploadedFileContentType;

    public String getUploadedFileContentType() {
        return uploadedFileContentType;
    }

    public void setUploadedFileContentType(String uploadedFileContentType) {
        this.uploadedFileContentType = uploadedFileContentType;
    }

    public File getUploadedExterrnalIdentiferFile() {
        return uploadedExternalIdentifierFile;
    }

    public void setUploadedExternalIdentifierFile(File uploadedExternalIdentifierFile) {
        this.uploadedExternalIdentifierFile = uploadedExternalIdentifierFile;
    }

    public List<ExternalIdentifierMetadata> getListExternalIdentifierMetadatas() {
        return listExternalIdentifierMetadatas;
    }

    public void setListExternalIdentifierMetadatas(

            List<ExternalIdentifierMetadata> listExternalIdentifierMetadatas) {
        this.listExternalIdentifierMetadatas = listExternalIdentifierMetadatas;
    }

    public ExternalIdentifierMetadata getSelectedExternalIdentifierMetadata() {

        return selectedExternalIdentifierMetadata;
    }

    public void setSelectedExternalIdentifierMetadata(ExternalIdentifierMetadata selectedExternalIdentifierMetadata) {
        this.selectedExternalIdentifierMetadata = selectedExternalIdentifierMetadata;
    }


    public String getSelectedCardinalityEnum() {
        return selectedCardinalityEnum;
    }

    public void setSelectedCardinalityEnum(String selectedCardinalityEnum) {
        this.selectedCardinalityEnum = selectedCardinalityEnum;
    }

    public GazelleListDataModel<ExternalIdentifierMetadata> getListExternalIdentifierMetadatasDataModelList() {
        List<ExternalIdentifierMetadata> res = getListExternalIdentifierMetadatas();
        GazelleListDataModel<ExternalIdentifierMetadata> dm = new GazelleListDataModel<ExternalIdentifierMetadata>(res);
        return dm;
    }

    public void initSelectedExternalIdentifierMetadata() {
        this.selectedExternalIdentifierMetadata = new ExternalIdentifierMetadata();
    }


    public void reloadSelectedExternalIdentifierMetaData() {
        if ((this.selectedExternalIdentifierMetadata != null) && (this.selectedExternalIdentifierMetadata.getId() != null)) {
            this.selectedExternalIdentifierMetadata = entityManager.find(ExternalIdentifierMetadata.class, this.selectedExternalIdentifierMetadata
                    .getId());
        }
    }

    public void deleteSelectedExternalIdentifierMetadata() {
        if (this.selectedExternalIdentifierMetadata != null) {
            ExternalIdentifierMetadata uu = entityManager.find(ExternalIdentifierMetadata.class, this.selectedExternalIdentifierMetadata.getId());
            entityManager.remove(uu);
            this.selectedExternalIdentifierMetadata = null;
        }
    }

    public void saveSelectedExternalIdentifierMetadata() {

        if (this.selectedExternalIdentifierMetadata != null) {
            if (this.selectedExternalIdentifierMetadata.getUuid() == null) {
                FacesMessages.instance().add(Severity.ERROR, "The ExternalIdentifier shall contain an UUID !");
            } else {
                this.selectedExternalIdentifierMetadata = entityManager.merge(this.selectedExternalIdentifierMetadata);
                entityManager.flush();
                FacesMessages.instance().add(Severity.INFO, "The current ExternalIdentifier was saved.");
            }
        }

    }

    public int countListExternalIdentifierMetadatas() {
        return listExternalIdentifierMetadatas.size();
    }

    public void initListExternalIdentifierMetadatas() {
        HQLQueryBuilder<ExternalIdentifierMetadata> hql = new HQLQueryBuilder<ExternalIdentifierMetadata>(ExternalIdentifierMetadata.class);
        this.listExternalIdentifierMetadatas = hql.getList();
        Collections.sort(this.listExternalIdentifierMetadatas);
    }

    @SuppressWarnings("unused")
    public void cloneAndPersistMetadata(ExternalIdentifierMetadata meta) {
        ExternalIdentifierMetadata cl = new ExternalIdentifierMetadata();
        meta = entityManager.find(ExternalIdentifierMetadata.class, meta.getId());
        for (Usage us : meta.getUsages()) {
        }
        for (SlotMetadataDescriber slmdd : meta.getSlotDescriber()) {
        }
        this.copyElements(meta, cl);
        entityManager.merge(cl);
        entityManager.flush();
    }

    public void downloadAll() {
        Map<Integer, String> mapFiles = getMapFilesContent();
        ZipOutputStream zos = null;
        try {
            File zipFile = File.createTempFile("ro_", ".zip");
            zos = new ZipOutputStream(new FileOutputStream(zipFile));
            DocumentFileUpload.zip(mapFiles, zos);
            zos.close();
            DocumentFileUpload.showFile(zipFile, "zip", true);
        } catch (FileNotFoundException e) {
            log.error("" + e.getMessage());
        } catch (IOException e) {
            log.error("" + e.getMessage());
        } finally {
            if (zos != null) {
                try {
                    zos.close();
                } catch (IOException e) {
                    log.error("" + e.getMessage());
                }
            }
        }
    }

    private Map<Integer, String> getMapFilesContent() {
        Map<Integer, String> map = new HashMap<Integer, String>();
        for (RegistryObjectMetadata rom : this.getListExternalIdentifierMetadataSpecifications()) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                IHERegistryObjectMetadataSpecification.saveRegistryObjectMetadataSpecification(rom, baos);
                map.put(rom.getId(), baos.toString(UTF_8));
            } catch (UnsupportedEncodingException e) {
                log.error("" + e.getMessage());
            } catch (JAXBException e) {
                log.error("" + e.getMessage());
            }
        }
        return map;
    }

    private List<ExternalIdentifierMetadata> getListExternalIdentifierMetadataSpecifications() {
        ExternalIdentifierMetadataQuery qq = new ExternalIdentifierMetadataQuery();
        List<ExternalIdentifierMetadata> res = qq.getList();
        if (res != null) {
            Collections.sort(res);
        }
        return res;
    }


    public void importExternalIdentifier() {
        if (this.uploadedExternalIdentifierFile != null) {
            try {
                if (uploadedFileContentType.contains("zip")) {
                    List<String> listUploaded = DocumentFileUpload.unzip(this.uploadedExternalIdentifierFile);
                    for (String string : listUploaded) {
                        ExternalIdentifierMetadata newadq = (ExternalIdentifierMetadata) IHERegistryObjectMetadataSpecification.load(new
                                ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_8)));
                        EntityManager em = (EntityManager) Component.getInstance("entityManager");
                        em.merge(newadq);
                        em.flush();
                    }
                    FacesMessages.instance().add(Severity.INFO, "The uploaded ExternalIdentifierMetadatas are saved.");
                } else {
                    ExternalIdentifierMetadata newams = (ExternalIdentifierMetadata) IHERegistryObjectMetadataSpecification.load(new
                            FileInputStream(uploadedExternalIdentifierFile));

                    EntityManager em = (EntityManager) Component.getInstance("entityManager");
                    em.merge(newams);
                    em.flush();
                    FacesMessages.instance().add(Severity.INFO, "The uploaded ExternalIdentifierMetadata is saved.");
                }
            } catch (UnsupportedEncodingException e) {
                FacesMessages.instance().add(Severity.ERROR, "Unsupported encoding of the document," + e.getMessage());
                log.info("unsupported encoding : ", e);
            } catch (JAXBException e) {
                FacesMessages.instance().add(Severity.ERROR, "Not able to parse the specified xml : " + e.getMessage());
                log.info("jaxb exception : ", e);
            } catch (Exception e) {
                FacesMessages.instance().add(Severity.ERROR, "Problem occurs when parsing the specified xml: " + e.getMessage());
                log.info("exception : ", e);
            } finally {
                this.uploadedExternalIdentifierFile = null;
            }
        }
    }

    public void uploadListener(FileUploadEvent event) {
        UploadedFile item = event.getUploadedFile();
        String uploadedFilename = item.getName();
        uploadedFileContentType = item.getContentType();
        FileOutputStream fos = null;
        try {
            uploadedExternalIdentifierFile = File.createTempFile("eimd", uploadedFilename);
            fos = new FileOutputStream(uploadedExternalIdentifierFile);
            fos.write(item.getData());
        } catch (FileNotFoundException e) {
            log.error("" + e.getMessage());
        } catch (IOException e) {
            log.error("" + e.getMessage());
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    log.error("" + e.getMessage());
                }
            }
        }
    }

    // --- destroy ---------------
    @Destroy
    @Remove
    public void destroy() {
        log.info("destroy mETADATAExternalIdentifierMetadataEditorManager..");
    }


}