package net.ihe.gazelle.metadata.action;

import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.metadata.model.ClassificationMetaData;
import net.ihe.gazelle.metadata.model.ClassificationMetaDataQuery;
import net.ihe.gazelle.metadata.model.RegistryObjectMetadata;
import net.ihe.gazelle.metadata.model.describer.SlotMetadataDescriber;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.xdstar.core.DocumentFileUpload;
import net.ihe.gazelle.xdstar.util.IHERegistryObjectMetadataSpecification;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.log.Log;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.xml.bind.JAXBException;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

import static org.apache.commons.lang.CharEncoding.UTF_8;

@Stateful
@Name("classificationMetaDataEditorManager")
@Scope(ScopeType.SESSION)
@GenerateInterface("ClassificationMetaDataEditorLocal")
public class ClassificationMetaDataEditorManager extends CommonROManager implements ClassificationMetaDataEditorLocal {

    @Logger
    private static Log log;

    // ---  attributes -------
    private net.ihe.gazelle.metadata.model.ClassificationMetaData selectedClassificationMetaData;

    private List<ClassificationMetaData> listClassificationMetadatas;
    private List<ClassificationMetaData> listClassificationMetadatasDatamodelList;

    private File uploadedClassificationFile;

    private String uploadedFileContentType;

    public String getUploadedFileContentType() {
        return uploadedFileContentType;
    }

    public void setUploadedFileContentType(String uploadedFileContentType) {
        this.uploadedFileContentType = uploadedFileContentType;
    }

    public File getUploadedClassificationFile() {
        return uploadedClassificationFile;
    }

    public void setUploadedClassificationFile(File uploadedClassificationFile) {
        this.uploadedClassificationFile = uploadedClassificationFile;
    }

    public List<ClassificationMetaData> getListClassificationMetadatas() {
        return listClassificationMetadatas;
    }

    public void setListClassificationMetadatas(
            List<ClassificationMetaData> listClassificationMetadatas) {
        this.listClassificationMetadatas = listClassificationMetadatas;
    }

    public GazelleListDataModel<ClassificationMetaData> getListClassificationMetadatasDatamodelList() {
        List<ClassificationMetaData> res = getListClassificationMetadatas();
        GazelleListDataModel<ClassificationMetaData> dm = new GazelleListDataModel<ClassificationMetaData>(res);
        return dm;
    }

    public net.ihe.gazelle.metadata.model.ClassificationMetaData getSelectedClassificationMetaData() {
        return selectedClassificationMetaData;
    }

    public void setSelectedClassificationMetaData(net.ihe.gazelle.metadata.model.ClassificationMetaData selectedClassificationMetaData) {
        this.selectedClassificationMetaData = selectedClassificationMetaData;
    }

    public void initSelectedClassificationMetadata() {
        this.selectedClassificationMetaData = new ClassificationMetaData();
    }


    public void reloadSelectedClassificationMetaData() {
        if ((this.selectedClassificationMetaData != null) && (this.selectedClassificationMetaData.getId() != null)) {
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            this.selectedClassificationMetaData = em.find(ClassificationMetaData.class, this.selectedClassificationMetaData.getId());
        }
    }

    //	@RolesAllowed({"admin_role"})
    public void deleteSelectedClassificationMetadata() {
        if (this.selectedClassificationMetaData != null) {
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            ClassificationMetaData uu = em.find(ClassificationMetaData.class, this.selectedClassificationMetaData.getId());
            em.remove(uu);
            em.flush();
            this.selectedClassificationMetaData = null;
        }
    }

    //	@RolesAllowed({"admin_role"})
    public void saveSelectedClassificationMetaData() {
        if (this.selectedClassificationMetaData != null) {
            if (this.selectedClassificationMetaData.getUuid() == null) {
                FacesMessages.instance().add(Severity.ERROR, "The classification shall contain an UUID !");
            } else {
                EntityManager em = (EntityManager) Component.getInstance("entityManager");
                this.selectedClassificationMetaData = em.merge(this.selectedClassificationMetaData);
                em.flush();
                FacesMessages.instance().add(Severity.INFO, "The current classification was saved.");
            }
        }
    }

    public int countListClassificationMetadatas() {
        return listClassificationMetadatas.size();
    }

    public void initListClassificationMetadatas() {
        HQLQueryBuilder<ClassificationMetaData> hql = new HQLQueryBuilder<ClassificationMetaData>(ClassificationMetaData.class);
        this.listClassificationMetadatas = hql.getList();
        Collections.sort(this.listClassificationMetadatas);
    }

    @SuppressWarnings("unused")
    public void cloneAndPersistMetadata(ClassificationMetaData meta) {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        ClassificationMetaData cl = new ClassificationMetaData();
        meta = em.find(ClassificationMetaData.class, meta.getId());
        for (Usage us : meta.getUsages()) {
        }
        for (SlotMetadataDescriber slmdd : meta.getSlotDescriber()) {
        }
        this.copyElements(meta, cl);
        em.merge(cl);
        em.flush();
    }


    public void downloadAll() {
        Map<Integer, String> mapFiles = getMapFilesContent();
        ZipOutputStream zos = null;
        try {
            File zipFile = File.createTempFile("ro_", ".zip");
            zos = new ZipOutputStream(new FileOutputStream(zipFile));
            DocumentFileUpload.zip(mapFiles, zos);
            zos.close();
            DocumentFileUpload.showFile(zipFile, "zip", true);
        } catch (FileNotFoundException e) {
            log.error("" + e.getMessage());
        } catch (IOException e) {
            log.error("" + e.getMessage());
        } finally {
            if (zos != null) {
                try {
                    zos.close();
                } catch (IOException e) {
                    log.error("" + e.getMessage());
                }
            }
        }
    }

    private Map<Integer, String> getMapFilesContent() {
        Map<Integer, String> map = new HashMap<Integer, String>();
        for (RegistryObjectMetadata rom : this.getListClassificationMetadataSpecifications()) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                IHERegistryObjectMetadataSpecification.saveRegistryObjectMetadataSpecification(rom, baos);
                map.put(rom.getId(), baos.toString(UTF_8));
            } catch (UnsupportedEncodingException e) {
                log.error("" + e.getMessage());
            } catch (JAXBException e) {
                log.error("" + e.getMessage());
            }
        }
        return map;
    }

    private List<ClassificationMetaData> getListClassificationMetadataSpecifications() {
        ClassificationMetaDataQuery qq = new ClassificationMetaDataQuery();
        List<ClassificationMetaData> res = qq.getList();
        if (res != null) {
            Collections.sort(res);
        }
        return res;
    }


    public void importClassification() {
        if (this.uploadedClassificationFile != null) {
            try {
                if (uploadedFileContentType.contains("zip")) {
                    List<String> listUploaded = DocumentFileUpload.unzip(this.uploadedClassificationFile);
                    for (String string : listUploaded) {
                        ClassificationMetaData newadq = (ClassificationMetaData) IHERegistryObjectMetadataSpecification.load(new
                                ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_8)));
                        EntityManager em = (EntityManager) Component.getInstance("entityManager");
                        em.merge(newadq);
                        em.flush();
                    }
                    FacesMessages.instance().add(Severity.INFO, "The uploaded ClassificationMetaDatas are saved.");
                } else {
                    ClassificationMetaData newams = (ClassificationMetaData) IHERegistryObjectMetadataSpecification.load(new FileInputStream
                            (uploadedClassificationFile));

                    EntityManager em = (EntityManager) Component.getInstance("entityManager");
                    em.merge(newams);
                    em.flush();
                    FacesMessages.instance().add(Severity.INFO, "The uploaded ClassificationMetaData is saved.");
                }
            } catch (UnsupportedEncodingException e) {
                FacesMessages.instance().add(Severity.ERROR, "Unsupported encoding of the document," + e.getMessage());
                log.info("unsupported encoding : ", e);
            } catch (JAXBException e) {
                FacesMessages.instance().add(Severity.ERROR, "Not able to parse the specified xml : " + e.getMessage());
                log.info("jaxb exception : ", e);
            } catch (Exception e) {
                FacesMessages.instance().add(Severity.ERROR, "Problem occurs when parsing the specified xml: " + e.getMessage());
                log.info("exception : ", e);
            } finally {
                this.uploadedClassificationFile = null;
            }
        }
    }

    public void uploadListener(FileUploadEvent event) {
        UploadedFile item = event.getUploadedFile();
        String uploadedFilename = item.getName();
        uploadedFileContentType = item.getContentType();
        FileOutputStream fos = null;
        try {
            uploadedClassificationFile = File.createTempFile("clmd", uploadedFilename);
            fos = new FileOutputStream(uploadedClassificationFile);
            fos.write(item.getData());
        } catch (FileNotFoundException e) {
            log.error("" + e.getMessage());
        } catch (IOException e) {
            log.error("" + e.getMessage());
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    log.error("" + e.getMessage());
                }
            }
        }
    }


    @Destroy
    @Remove
    public void destroy() {
        log.info("destroy ClassificationMetaDataEditorManager..");
    }

}