package net.ihe.gazelle.metadata.action;

import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.metadata.model.*;
import net.ihe.gazelle.metadata.model.describer.ClassificationMetadataDescriber;
import net.ihe.gazelle.metadata.model.describer.ExternalIdentifierMetadataDescriber;
import net.ihe.gazelle.metadata.model.describer.SlotMetadataDescriber;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.xdstar.core.DocumentFileUpload;
import net.ihe.gazelle.xdstar.util.IHERegistryObjectMetadataSpecification;
import net.ihe.gazelle.xdstar.validator.ws.ExtractExistingValidationObj;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.log.Log;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.bind.JAXBException;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.zip.ZipOutputStream;

import static org.apache.commons.lang.CharEncoding.UTF_8;

@Stateful
@Name("extrinsicObjectMetadataEditorManager")
@Scope(ScopeType.SESSION)
@GenerateInterface("ExtrinsicObjectMetadataEditorLocal")
public class ExtrinsicObjectMetadataEditorManager extends CommonROManager implements ExtrinsicObjectMetadataEditorLocal {

    @Logger
    private static Log log;

    @PersistenceContext(unitName = "XDStarClient-PersistenceUnit")
    private EntityManager entityManager;

    // ---  attributes -------
    private net.ihe.gazelle.metadata.model.ExtrinsicObjectMetadata selectedExtrinsicObjectMetadata;

    private ConstraintSpecification selectedConstraintSpecification;

    private List<ExtrinsicObjectMetadata> listExtrinsicObjectMetadatas;

    private List<ExtrinsicObjectMetadata> listExtrinsicObjectMetadatasDataModelList;

    private File uploadedExtrinsicObjectFile;

    private String uploadedFileContentType;

    public String getUploadedFileContentType() {
        return uploadedFileContentType;
    }

    public void setUploadedFileContentType(String uploadedFileContentType) {
        this.uploadedFileContentType = uploadedFileContentType;
    }

    public File getUploadedExtrinsicObjectFile() {
        return uploadedExtrinsicObjectFile;
    }

    public void setUploadedExtrinsicObjectFile(File uploadedExtrinsicObjectFile) {
        this.uploadedExtrinsicObjectFile = uploadedExtrinsicObjectFile;
    }

    public List<ExtrinsicObjectMetadata> getListExtrinsicObjectMetadatas() {
        return listExtrinsicObjectMetadatas;
    }

    public void setListExtrinsicObjectMetadatas(
            List<ExtrinsicObjectMetadata> listExtrinsicObjectMetadatas) {
        this.listExtrinsicObjectMetadatas = listExtrinsicObjectMetadatas;
    }

    public GazelleListDataModel<ExtrinsicObjectMetadata> getListExtrinsicObjectMetadatasDataModelList() {
        List<ExtrinsicObjectMetadata> res = getListExtrinsicObjectMetadatas();
        GazelleListDataModel<ExtrinsicObjectMetadata> dm = new GazelleListDataModel<ExtrinsicObjectMetadata>(res);
        return dm;
    }

    public ExtrinsicObjectMetadata getSelectedExtrinsicObjectMetadata() {
        return selectedExtrinsicObjectMetadata;
    }

    public void setSelectedExtrinsicObjectMetadata(ExtrinsicObjectMetadata selectedExtrinsicObjectMetadata) {
        this.selectedExtrinsicObjectMetadata = selectedExtrinsicObjectMetadata;
    }

    public void initSelectedExtrinsicObjectMetadata() {
        this.selectedExtrinsicObjectMetadata = new ExtrinsicObjectMetadata();
    }

    public void reloadSelectedExtrinsicObjectMetaData() {
        if ((this.selectedExtrinsicObjectMetadata != null) && (this.selectedExtrinsicObjectMetadata.getId() != null)) {
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            this.selectedExtrinsicObjectMetadata = em.find(ExtrinsicObjectMetadata.class, this.selectedExtrinsicObjectMetadata.getId());
        }
    }

    public void deleteSelectedExtrinsicObjectMetadata() {
        if (this.selectedExtrinsicObjectMetadata != null) {
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            ExtrinsicObjectMetadata uu = em.find(ExtrinsicObjectMetadata.class, this.selectedExtrinsicObjectMetadata.getId());
            em.remove(uu);
            this.selectedExtrinsicObjectMetadata = null;
        }
    }

    public void saveSelectedExtrinsicObjectMetadata() {
        if (this.selectedExtrinsicObjectMetadata != null) {
            if (this.selectedExtrinsicObjectMetadata.getUuid() == null) {
                FacesMessages.instance().add(Severity.ERROR, "The ExtrinsicObject shall contain an UUID !");
            } else {
                EntityManager em = (EntityManager) Component.getInstance("entityManager");
                this.selectedExtrinsicObjectMetadata = em.merge(this.selectedExtrinsicObjectMetadata);
                em.flush();
                FacesMessages.instance().add(Severity.INFO, "The current ExtrinsicObject was saved.");
            }
        }
    }

    public int countListExtrinsicObjectMetadatas() {
        return listExtrinsicObjectMetadatas.size();
    }

    public void initListExtrinsicObjectMetadatas() {
        HQLQueryBuilder<ExtrinsicObjectMetadata> hql = new HQLQueryBuilder<ExtrinsicObjectMetadata>(ExtrinsicObjectMetadata.class);
        this.listExtrinsicObjectMetadatas = hql.getList();
        Collections.sort(this.listExtrinsicObjectMetadatas);
    }

    @SuppressWarnings("unused")
    public void cloneAndPersistMetadata(ExtrinsicObjectMetadata meta) {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        ExtrinsicObjectMetadata cl = new ExtrinsicObjectMetadata();
        meta = em.find(ExtrinsicObjectMetadata.class, meta.getId());
        for (Usage us : meta.getUsages()) {
        }
        for (SlotMetadataDescriber slmdd : meta.getSlotDescriber()) {
        }

        this.copyElements(meta, cl);
        cl.setExtraConstraintSpecifications(extraConstraintCopyList(meta.getExtraConstraintSpecifications()));
        log.info(cl);
        em.merge(cl);
        em.flush();
    }


    public void downloadAll() {
        Map<Integer, String> mapFiles = getMapFilesContent();
        ZipOutputStream zos = null;
        try {
            File zipFile = File.createTempFile("ro_", ".zip");
            zos = new ZipOutputStream(new FileOutputStream(zipFile));
            DocumentFileUpload.zip(mapFiles, zos);
            zos.close();
            DocumentFileUpload.showFile(zipFile, "zip", true);
        } catch (FileNotFoundException e) {
            log.error("" + e.getMessage());
        } catch (IOException e) {
            log.error("" + e.getMessage());
        } finally {
            if (zos != null) {
                try {
                    zos.close();
                } catch (IOException e) {
                    log.error("" + e.getMessage());
                }
            }
        }
    }

    private Map<Integer, String> getMapFilesContent() {
        Map<Integer, String> map = new HashMap<Integer, String>();
        for (RegistryObjectMetadata rom : this.getListExtrinsicObjectMetadataSpecifications()) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                IHERegistryObjectMetadataSpecification.saveRegistryObjectMetadataSpecification(rom, baos);
                map.put(rom.getId(), baos.toString(UTF_8));
            } catch (UnsupportedEncodingException e) {
                log.error("" + e.getMessage());
            } catch (JAXBException e) {
                log.error("" + e.getMessage());
            }
        }
        return map;
    }

    private List<ExtrinsicObjectMetadata> getListExtrinsicObjectMetadataSpecifications() {
        ExtrinsicObjectMetadataQuery qq = new ExtrinsicObjectMetadataQuery();
        List<ExtrinsicObjectMetadata> res = qq.getList();
        if (res != null) {
            Collections.sort(res);
        }
        return res;
    }

    public void importExtrinsicObject() {
        if (this.uploadedExtrinsicObjectFile != null) {
            try {
                if (uploadedFileContentType.contains("zip")) {
                    List<String> listUploaded = DocumentFileUpload.unzip(this.uploadedExtrinsicObjectFile);
                    for (String string : listUploaded) {
                        ExtrinsicObjectMetadata extrinsicObject = (ExtrinsicObjectMetadata) IHERegistryObjectMetadataSpecification.load(new
                                ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_8)));
                        extrinsicObject.setClassificationDescriber(getExistingClassifications(extrinsicObject.getClassificationDescriber()));
                        extrinsicObject.setExt_identifierDescriber(getExistingExternalIdentifier(extrinsicObject.getExt_identifierDescriber()));
                        extrinsicObject.setUsages(ExtractExistingValidationObj.getExistingUsages(extrinsicObject.getUsages(), entityManager));

                        EntityManager em = (EntityManager) Component.getInstance("entityManager");
                        em.merge(extrinsicObject);
                        em.flush();
                    }
                    FacesMessages.instance().add(Severity.INFO, "The uploaded ExtrinsicObjectMetadatas are saved.");
                } else {
                    ExtrinsicObjectMetadata extrinsicObject = (ExtrinsicObjectMetadata) IHERegistryObjectMetadataSpecification.load(new
                            FileInputStream(uploadedExtrinsicObjectFile));
                    extrinsicObject.setClassificationDescriber(getExistingClassifications(extrinsicObject.getClassificationDescriber()));
                    extrinsicObject.setExt_identifierDescriber(getExistingExternalIdentifier(extrinsicObject.getExt_identifierDescriber()));
                    extrinsicObject.setUsages(ExtractExistingValidationObj.getExistingUsages(extrinsicObject.getUsages(), entityManager));

                    EntityManager em = (EntityManager) Component.getInstance("entityManager");
                    em.merge(extrinsicObject);
                    em.flush();
                    FacesMessages.instance().add(Severity.INFO, "The uploaded ExtrinsicObjectMetadata is saved.");
                }
            } catch (UnsupportedEncodingException e) {
                FacesMessages.instance().add(Severity.ERROR, "Unsupported encoding of the document," + e.getMessage());
                log.info("unsupported encoding : ", e);
            } catch (JAXBException e) {
                FacesMessages.instance().add(Severity.ERROR, "Not able to parse the specified xml : " + e.getMessage());
                log.info("jaxb exception : ", e);
            } catch (Exception e) {
                FacesMessages.instance().add(Severity.ERROR, "Problem occurs when parsing the specified xml: " + e.getMessage());
                log.info("exception : ", e);
            } finally {
                this.uploadedExtrinsicObjectFile = null;
            }
        }
    }

    private List<ExternalIdentifierMetadataDescriber> getExistingExternalIdentifier(List<ExternalIdentifierMetadataDescriber>
                                                                                            oldExt_identifierDescriber) {
        List<ExternalIdentifierMetadataDescriber> res = new ArrayList<ExternalIdentifierMetadataDescriber>();
        for (ExternalIdentifierMetadataDescriber currentExternalIdentifier : oldExt_identifierDescriber) {

            ExternalIdentifierMetadataDescriber newExternalIdentifierDescriber = new ExternalIdentifierMetadataDescriber();

            ExternalIdentifierMetadataQuery cca = new ExternalIdentifierMetadataQuery();
            cca.uuid().eq(currentExternalIdentifier.getEim().getUuid());
            cca.name().eq(currentExternalIdentifier.getEim().getName());
            cca.displayAttributeName().eq(currentExternalIdentifier.getEim().getDisplayAttributeName());
            ExternalIdentifierMetadata tempExternalIdentifier = cca.getUniqueResult();
            if (tempExternalIdentifier != null) {
                newExternalIdentifierDescriber.setCardinality(currentExternalIdentifier.getCardinality());
                newExternalIdentifierDescriber.setEim(tempExternalIdentifier);
                res.add(newExternalIdentifierDescriber);
            } else {
                res.add(currentExternalIdentifier);
            }

        }
        return res;
    }

    private List<ClassificationMetadataDescriber> getExistingClassifications(List<ClassificationMetadataDescriber> oldClassificationDescriber) {
        List<ClassificationMetadataDescriber> res = new ArrayList<ClassificationMetadataDescriber>();
        for (ClassificationMetadataDescriber currentClassificationDescriber : oldClassificationDescriber) {

            ClassificationMetadataDescriber newClassificationDescriber = new ClassificationMetadataDescriber();

            ClassificationMetaDataQuery cca = new ClassificationMetaDataQuery();
            cca.uuid().eq(currentClassificationDescriber.getCmd().getUuid());
            cca.name().eq(currentClassificationDescriber.getCmd().getName());
            cca.displayAttributeName().eq(currentClassificationDescriber.getCmd().getDisplayAttributeName());
            ClassificationMetaData tempCmd = cca.getUniqueResult();
            if (tempCmd != null) {
                newClassificationDescriber.setCardinality(currentClassificationDescriber.getCardinality());
                newClassificationDescriber.setCmd(tempCmd);
                res.add(newClassificationDescriber);
            } else {
                res.add(currentClassificationDescriber);
            }

        }
        return res;
    }

    public void importExtrinsicObjectOld() {
        if (this.uploadedExtrinsicObjectFile != null) {
            try {
                if (uploadedFileContentType.contains("zip")) {
                    List<String> listUploaded = DocumentFileUpload.unzip(this.uploadedExtrinsicObjectFile);
                    for (String string : listUploaded) {
                        ExtrinsicObjectMetadata newadq = (ExtrinsicObjectMetadata) IHERegistryObjectMetadataSpecification.load(new
                                ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_8)));
                        EntityManager em = (EntityManager) Component.getInstance("entityManager");
                        em.merge(newadq);
                        em.flush();
                    }
                } else {
                    ExtrinsicObjectMetadata newams = (ExtrinsicObjectMetadata) IHERegistryObjectMetadataSpecification.load(new FileInputStream
                            (uploadedExtrinsicObjectFile));

                    EntityManager em = (EntityManager) Component.getInstance("entityManager");
                    em.merge(newams);
                    em.flush();
                }
            } catch (UnsupportedEncodingException e) {
                FacesMessages.instance().add(Severity.ERROR, "Unsupported encoding of the document," + e.getMessage());
                log.info("unsupported encoding : ", e);
            } catch (JAXBException e) {
                FacesMessages.instance().add(Severity.ERROR, "Not able to parse the specified xml : " + e.getMessage());
                log.info("jaxb exception : ", e);
            } catch (Exception e) {
                FacesMessages.instance().add(Severity.ERROR, "Problem occurs when parsing the specified xml: " + e.getMessage());
                log.info("exception : ", e);
            } finally {
                this.uploadedExtrinsicObjectFile = null;
            }
        }
    }

    public void uploadListener(FileUploadEvent event) {
        UploadedFile item = event.getUploadedFile();
        String uploadedFilename = item.getName();
        uploadedFileContentType = item.getContentType();
        FileOutputStream fos = null;
        try {
            uploadedExtrinsicObjectFile = File.createTempFile("eomd", uploadedFilename);
            fos = new FileOutputStream(uploadedExtrinsicObjectFile);
            fos.write(item.getData());
        } catch (FileNotFoundException e) {
            log.error("" + e.getMessage());
        } catch (IOException e) {
            log.error("" + e.getMessage());
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    log.error("" + e.getMessage());
                }
            }
        }
    }


    public void initSelectedExtraConstraint() {
        this.selectedConstraintSpecification = new ConstraintSpecification();
    }

    public void saveSelectedExtraConstraint() {

        if (this.selectedConstraintSpecification.getId() != null) {
            return;
        } else {
            if (this.selectedExtrinsicObjectMetadata != null) {
                this.selectedExtrinsicObjectMetadata.getExtraConstraintSpecifications().add(selectedConstraintSpecification);
            }
        }
    }

    public ConstraintSpecification getSelectedConstraintSpecification() {
        return selectedConstraintSpecification;
    }

    public void setSelectedConstraintSpecification(ConstraintSpecification selectedConstraintSpecification) {
        this.selectedConstraintSpecification = selectedConstraintSpecification;
    }


    // --- destroy ---------------
    @Destroy
    @Remove
    public void destroy() {
        log.info("destroy mETADATAExtrinsicObjectMetadataEditorManager..");
    }


}