package net.ihe.gazelle.metadata.action;

import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.metadata.model.*;
import net.ihe.gazelle.metadata.model.describer.ClassificationMetadataDescriber;
import net.ihe.gazelle.metadata.model.describer.ExternalIdentifierMetadataDescriber;
import net.ihe.gazelle.metadata.model.describer.SlotMetadataDescriber;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.xdstar.core.DocumentFileUpload;
import net.ihe.gazelle.xdstar.util.IHERegistryObjectMetadataSpecification;
import net.ihe.gazelle.xdstar.validator.ws.ExtractExistingValidationObj;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.log.Log;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.bind.JAXBException;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.zip.ZipOutputStream;

import static org.apache.commons.lang.CharEncoding.UTF_8;

@Stateful
@Name("registryPackageMetadataEditorManager")
@Scope(ScopeType.SESSION)
@GenerateInterface("RegistryPackageMetadataEditorLocal")
public class RegistryPackageMetadataEditorManager extends CommonROManager implements RegistryPackageMetadataEditorLocal {

    @Logger
    private static Log log;

    @PersistenceContext(unitName = "XDStarClient-PersistenceUnit")
    private EntityManager entityManager;

    // ---  attributes -------
    private net.ihe.gazelle.metadata.model.RegistryPackageMetadata selectedRegistryPackageMetadata;

    private List<RegistryPackageMetadata> listRegistryPackageMetadatas;

    private List<RegistryPackageMetadata> listRegistryPackageMetadatasDataModelList;

    private ConstraintSpecification selectedConstraintSpecification;

    private File uploadedRegistryPackageFile;

    private String uploadedFileContentType;

    public String getUploadedFileContentType() {
        return uploadedFileContentType;
    }

    public void setUploadedFileContentType(String uploadedFileContentType) {
        this.uploadedFileContentType = uploadedFileContentType;
    }

    public File getUploadedRegistryPackageFile() {
        return uploadedRegistryPackageFile;
    }

    public void setUploadedRegistryPackageFile(File uploadedRegistryPackageFile) {
        this.uploadedRegistryPackageFile = uploadedRegistryPackageFile;
    }

    public List<RegistryPackageMetadata> getListRegistryPackageMetadatas() {
        return listRegistryPackageMetadatas;
    }

    public void setListRegistryPackageMetadatas(
            List<RegistryPackageMetadata> listRegistryPackageMetadatas) {
        this.listRegistryPackageMetadatas = listRegistryPackageMetadatas;
    }

    public GazelleListDataModel<RegistryPackageMetadata> getListRegistryPackageMetadatasDataModelList() {
        List<RegistryPackageMetadata> res = getListRegistryPackageMetadatas();
        GazelleListDataModel<RegistryPackageMetadata> dm = new GazelleListDataModel<RegistryPackageMetadata>(res);
        return dm;
    }

    public RegistryPackageMetadata getSelectedRegistryPackageMetadata() {
        return selectedRegistryPackageMetadata;
    }

    public void setSelectedRegistryPackageMetadata(RegistryPackageMetadata selectedRegistryPackageMetadata) {
        this.selectedRegistryPackageMetadata = selectedRegistryPackageMetadata;
    }


    public void initSelectedRegistryPackageMetadata() {
        this.selectedRegistryPackageMetadata = new RegistryPackageMetadata();
    }

    public void reloadSelectedRegistryPackageMetaData() {
        if ((this.selectedRegistryPackageMetadata != null) && (this.selectedRegistryPackageMetadata.getId() != null)) {
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            this.selectedRegistryPackageMetadata = em.find(RegistryPackageMetadata.class, this.selectedRegistryPackageMetadata.getId());
        }
    }

    public void deleteSelectedRegistryPackageMetadata() {
        if (this.selectedRegistryPackageMetadata != null) {
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            RegistryPackageMetadata uu = em.find(RegistryPackageMetadata.class, this.selectedRegistryPackageMetadata.getId());
            em.remove(uu);
            this.selectedRegistryPackageMetadata = null;
        }
    }

    public void saveSelectedRegistryPackageMetadata() {
        if (this.selectedRegistryPackageMetadata != null) {
            if (this.selectedRegistryPackageMetadata.getUuid() == null) {
                FacesMessages.instance().add(Severity.ERROR, "The RegistryPackage shall contain an UUID !");
            } else {
                EntityManager em = (EntityManager) Component.getInstance("entityManager");
                this.selectedRegistryPackageMetadata = em.merge(this.selectedRegistryPackageMetadata);
                em.flush();
                FacesMessages.instance().add(Severity.INFO, "The current RegistryPackage was saved.");
            }
        }
    }

    public int countListRegistryPackageMetadatas() {
        return listRegistryPackageMetadatas.size();
    }

    public void initListRegistryPackageMetadatas() {
        HQLQueryBuilder<RegistryPackageMetadata> hql = new HQLQueryBuilder<RegistryPackageMetadata>(RegistryPackageMetadata.class);
        this.listRegistryPackageMetadatas = hql.getList();
        Collections.sort(this.listRegistryPackageMetadatas);
    }

    @SuppressWarnings("unused")
    public void cloneAndPersistMetadata(RegistryPackageMetadata meta) {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        RegistryPackageMetadata cl = new RegistryPackageMetadata();
        meta = em.find(RegistryPackageMetadata.class, meta.getId());
        for (Usage us : meta.getUsages()) {
        }
        for (SlotMetadataDescriber slmdd : meta.getSlotDescriber()) {
        }
        this.copyElements(meta, cl);
        cl.setExtraConstraintSpecifications(extraConstraintCopyList(meta.getExtraConstraintSpecifications()));
        em.merge(cl);
        em.flush();
    }

    public void downloadAll() {
        Map<Integer, String> mapFiles = getMapFilesContent();
        ZipOutputStream zos = null;
        try {
            File zipFile = File.createTempFile("ro_", ".zip");
            zos = new ZipOutputStream(new FileOutputStream(zipFile));
            DocumentFileUpload.zip(mapFiles, zos);
            zos.close();
            DocumentFileUpload.showFile(zipFile, "zip", true);
        } catch (FileNotFoundException e) {
            log.error("" + e.getMessage());
        } catch (IOException e) {
            log.error("" + e.getMessage());
        } finally {
            if (zos != null) {
                try {
                    zos.close();
                } catch (IOException e) {
                    log.error("" + e.getMessage());
                }
            }
        }
    }

    private Map<Integer, String> getMapFilesContent() {
        Map<Integer, String> map = new HashMap<Integer, String>();
        for (RegistryObjectMetadata rom : this.getListRegistryPackageMetadataSpecifications()) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                IHERegistryObjectMetadataSpecification.saveRegistryObjectMetadataSpecification(rom, baos);
                map.put(rom.getId(), baos.toString(UTF_8));
            } catch (UnsupportedEncodingException e) {
                log.error("" + e.getMessage());
            } catch (JAXBException e) {
                log.error("" + e.getMessage());
            }
        }
        return map;
    }

    private List<RegistryPackageMetadata> getListRegistryPackageMetadataSpecifications() {
        RegistryPackageMetadataQuery qq = new RegistryPackageMetadataQuery();
        List<RegistryPackageMetadata> res = qq.getList();
        if (res != null) {
            Collections.sort(res);
        }
        return res;
    }


    public void importRegistryPackage() {
        if (this.uploadedRegistryPackageFile != null) {
            try {
                if (uploadedFileContentType.contains("zip")) {
                    List<String> listUploaded = DocumentFileUpload.unzip(this.uploadedRegistryPackageFile);
                    for (String string : listUploaded) {
                        RegistryPackageMetadata rp = (RegistryPackageMetadata) IHERegistryObjectMetadataSpecification.load(new ByteArrayInputStream
                                (string.getBytes(StandardCharsets.UTF_8)));
                        rp.setClassificationDescriber(getExistingClassifications(rp.getClassificationDescriber()));
                        rp.setExt_identifierDescriber(getExistingExternalIdentifier(rp.getExt_identifierDescriber()));
                        rp.setUsages(ExtractExistingValidationObj.getExistingUsages(rp.getUsages(), entityManager));

                        entityManager.merge(rp);
                        entityManager.flush();
                    }
                    FacesMessages.instance().add(Severity.INFO, "The uploaded RegistryPackageMetadatas are saved.");
                } else {
                    RegistryPackageMetadata rp = (RegistryPackageMetadata) IHERegistryObjectMetadataSpecification.load(new FileInputStream
                            (uploadedRegistryPackageFile));
                    rp.setClassificationDescriber(getExistingClassifications(rp.getClassificationDescriber()));
                    rp.setExt_identifierDescriber(getExistingExternalIdentifier(rp.getExt_identifierDescriber()));
                    rp.setUsages(ExtractExistingValidationObj.getExistingUsages(rp.getUsages(), entityManager));

                    entityManager.merge(rp);
                    entityManager.flush();
                    FacesMessages.instance().add(Severity.INFO, "The uploaded RegistryPackageMetadata is saved.");
                }
            } catch (UnsupportedEncodingException e) {
                FacesMessages.instance().add(Severity.ERROR, "Unsupported encoding of the document," + e.getMessage());
                log.info("unsupported encoding : ", e);
            } catch (JAXBException e) {
                FacesMessages.instance().add(Severity.ERROR, "Not able to parse the specified xml : " + e.getMessage());
                log.info("jaxb exception : ", e);
            } catch (Exception e) {
                FacesMessages.instance().add(Severity.ERROR, "Problem occurs when parsing the specified xml: " + e.getMessage());
                log.info("exception : ", e);
            } finally {
                this.uploadedRegistryPackageFile = null;
            }
        }
    }

    public void importRegistryPackageOld() {
        if (this.uploadedRegistryPackageFile != null) {
            try {
                if (uploadedFileContentType.contains("zip")) {
                    List<String> listUploaded = DocumentFileUpload.unzip(this.uploadedRegistryPackageFile);
                    for (String string : listUploaded) {
                        RegistryPackageMetadata newadq = (RegistryPackageMetadata) IHERegistryObjectMetadataSpecification.load(new
                                ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_8)));
                        EntityManager em = (EntityManager) Component.getInstance("entityManager");
                        em.merge(newadq);
                        em.flush();
                    }
                } else {
                    RegistryPackageMetadata newams = (RegistryPackageMetadata) IHERegistryObjectMetadataSpecification.load(new FileInputStream
                            (uploadedRegistryPackageFile));

                    EntityManager em = (EntityManager) Component.getInstance("entityManager");
                    em.merge(newams);
                    em.flush();
                }
            } catch (UnsupportedEncodingException e) {
                FacesMessages.instance().add(Severity.ERROR, "Unsupported encoding of the document," + e.getMessage());
                log.info("unsupported encoding : ", e);
            } catch (JAXBException e) {
                FacesMessages.instance().add(Severity.ERROR, "Not able to parse the specified xml : " + e.getMessage());
                log.info("jaxb exception : ", e);
            } catch (Exception e) {
                FacesMessages.instance().add(Severity.ERROR, "Problem occurs when parsing the specified xml: " + e.getMessage());
                log.info("exception : ", e);
            } finally {
                this.uploadedRegistryPackageFile = null;
            }
        }
    }

    private List<ExternalIdentifierMetadataDescriber> getExistingExternalIdentifier(List<ExternalIdentifierMetadataDescriber>
                                                                                            oldExt_identifierDescriber) {
        List<ExternalIdentifierMetadataDescriber> res = new ArrayList<ExternalIdentifierMetadataDescriber>();
        for (ExternalIdentifierMetadataDescriber currentExternalIdentifier : oldExt_identifierDescriber) {

            ExternalIdentifierMetadataDescriber newExternalIdentifierDescriber = new ExternalIdentifierMetadataDescriber();

            ExternalIdentifierMetadataQuery cca = new ExternalIdentifierMetadataQuery();
            cca.uuid().eq(currentExternalIdentifier.getEim().getUuid());
            cca.name().eq(currentExternalIdentifier.getEim().getName());
            cca.displayAttributeName().eq(currentExternalIdentifier.getEim().getDisplayAttributeName());
            ExternalIdentifierMetadata tempExternalIdentifier = cca.getUniqueResult();
            if (tempExternalIdentifier != null) {
                newExternalIdentifierDescriber.setCardinality(currentExternalIdentifier.getCardinality());
                newExternalIdentifierDescriber.setEim(tempExternalIdentifier);
                res.add(newExternalIdentifierDescriber);
            } else {
                res.add(currentExternalIdentifier);
            }
        }
        return res;
    }

    private List<ClassificationMetadataDescriber> getExistingClassifications(List<ClassificationMetadataDescriber> oldClassificationDescriber) {
        List<ClassificationMetadataDescriber> res = new ArrayList<ClassificationMetadataDescriber>();
        for (ClassificationMetadataDescriber currentClassificationDescriber : oldClassificationDescriber) {

            ClassificationMetadataDescriber newClassificationDescriber = new ClassificationMetadataDescriber();

            ClassificationMetaDataQuery cca = new ClassificationMetaDataQuery();
            cca.uuid().eq(currentClassificationDescriber.getCmd().getUuid());
            cca.name().eq(currentClassificationDescriber.getCmd().getName());
            cca.displayAttributeName().eq(currentClassificationDescriber.getCmd().getDisplayAttributeName());
            ClassificationMetaData tempCmd = cca.getUniqueResult();
            if (tempCmd != null) {
                newClassificationDescriber.setCardinality(currentClassificationDescriber.getCardinality());
                newClassificationDescriber.setCmd(tempCmd);
                res.add(newClassificationDescriber);
            } else {
                res.add(currentClassificationDescriber);
            }

        }
        return res;
    }

    public void uploadListener(FileUploadEvent event) {
        UploadedFile item = event.getUploadedFile();
        String uploadedFilename = item.getName();
        uploadedFileContentType = item.getContentType();
        FileOutputStream fos = null;
        try {
            uploadedRegistryPackageFile = File.createTempFile("rpmd", uploadedFilename);
            fos = new FileOutputStream(uploadedRegistryPackageFile);
            fos.write(item.getData());
        } catch (FileNotFoundException e) {
            log.error("" + e.getMessage());
        } catch (IOException e) {
            log.error("" + e.getMessage());
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    log.error("" + e.getMessage());
                }
            }
        }
    }

    public void initSelectedExtraConstraint() {
        this.selectedConstraintSpecification = new ConstraintSpecification();
    }

    public void saveSelectedExtraConstraint() {

        if (this.selectedConstraintSpecification.getId() != null) {
            return;
        } else {
            if (this.selectedRegistryPackageMetadata != null) {
                this.selectedRegistryPackageMetadata.getExtraConstraintSpecifications().add(selectedConstraintSpecification);
            }
        }
    }

    public ConstraintSpecification getSelectedConstraintSpecification() {
        return selectedConstraintSpecification;
    }

    public void setSelectedConstraintSpecification(ConstraintSpecification selectedConstraintSpecification) {
        this.selectedConstraintSpecification = selectedConstraintSpecification;
    }


    // --- destroy ---------------
    @Destroy
    @Remove
    public void destroy() {
        log.info("destroy mETADATARegistryPackageMetadataEditorManager..");
    }


}