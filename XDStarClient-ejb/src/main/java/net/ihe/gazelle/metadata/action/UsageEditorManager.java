package net.ihe.gazelle.metadata.action;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.paths.HQLSafePathEntity;
import net.ihe.gazelle.metadata.model.RegistryObjectMetadataQuery;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.xdstar.common.model.AbstractMessageQuery;
import net.ihe.gazelle.xdstar.filter.UsageDataModel;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.xml.bind.JAXBException;
import java.math.BigInteger;
import java.util.List;

@Stateful
@Name("mETADATAUsageEditorManager")
@Scope(ScopeType.SESSION)
@GenerateInterface("UsageEditorLocal")
public class UsageEditorManager implements UsageEditorLocal {


    private static Logger log = LoggerFactory.getLogger(UsageEditorManager.class);

    // ---  attributes -------
    private net.ihe.gazelle.simulator.sut.model.Usage selectedMETADATAUsage;

    private UsageDataModel listUsages;


    public UsageDataModel getListUsages() {
        if (listUsages == null) {
            try {
                HQLSafePathEntity.filterLabelMethodInit.remove(Transaction.class);
                HQLSafePathEntity.filterLabelMethodInit.put(Transaction.class, true);
                HQLSafePathEntity.filterLabelMethods.put(Transaction.class, Transaction.class.getMethod("getKeyword", null));
            } catch (SecurityException e) {
                log.info("", e);
            } catch (NoSuchMethodException e) {
                log.info("", e);
            }
            listUsages = new UsageDataModel();
        }
        return listUsages;
    }

    public void setListUsages(UsageDataModel listUsages) {
        this.listUsages = listUsages;
    }

    public net.ihe.gazelle.simulator.sut.model.Usage getSelectedMETADATAUsage() {
        return selectedMETADATAUsage;
    }

    public void setSelectedMETADATAUsage(net.ihe.gazelle.simulator.sut.model.Usage selectedMETADATAUsage) {
        this.selectedMETADATAUsage = selectedMETADATAUsage;
    }


    // --- editors method -----------
    public String editUsage(net.ihe.gazelle.simulator.sut.model.Usage usage, String back) {
        this.selectedMETADATAUsage = usage;
        return "/editor/metadata/Usage.xhtml";
    }

    // --- initiators method -----------


    // --- initiators for edit method -----------


    // --- adds method -----------

    public List<Transaction> getAvailableTransactions() {
        HQLQueryBuilder<Transaction> hh = new HQLQueryBuilder<Transaction>(Transaction.class);
        hh.addOrder("keyword", true);
        return hh.getList();
    }

    public List<AffinityDomain> getAvailableAffinities() {
        HQLQueryBuilder<AffinityDomain> hh = new HQLQueryBuilder<AffinityDomain>(AffinityDomain.class);
        hh.addOrder("keyword", true);
        return hh.getList();
    }

    public void saveSelectedUsage() {
        if (this.selectedMETADATAUsage != null) {
            if (this.selectedMETADATAUsage.getAffinity() == null && this.selectedMETADATAUsage.getTransaction() == null) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The usage shall contain a transaction and affinity domain !");
            } else if (this.selectedMETADATAUsage.getAffinity() == null) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The usage shall contain an affinity domain !");
            } else if (this.selectedMETADATAUsage.getTransaction() == null) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The usage shall contain a transaction !");
            } else {
                EntityManager em = (EntityManager) Component.getInstance("entityManager");
                this.selectedMETADATAUsage = em.merge(this.selectedMETADATAUsage);
                em.flush();
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "The current usage was saved.");
            }
        }
    }

    public String addNewUsage(List<net.ihe.gazelle.simulator.sut.model.Usage> list_Usage, String back) {
        net.ihe.gazelle.simulator.sut.model.Usage usage_el = new net.ihe.gazelle.simulator.sut.model.Usage();
        list_Usage.add(usage_el);
        this.selectedMETADATAUsage = usage_el;
        return "/editor/metadata/Usage.xhtml";
    }

    public void initSelectedMETADATAUsage() {
        this.selectedMETADATAUsage = new Usage();
    }

    public void deleteSelectedUsage() {
        if (this.selectedMETADATAUsage != null) {
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            Usage uu = em.find(Usage.class, this.selectedMETADATAUsage.getId());
            // Empty UsageMetadata case
            deleteEmptyUsageMetadata(uu);
            // Verify if references exists in other tables
            boolean hasReferences = checkReferencesInValidatorPackClassUsage(Long.valueOf(uu.getId()));
            if (!hasReferences) {
                em.remove(uu);
                this.selectedMETADATAUsage = null;
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "Usage deleted");
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.WARN, "Unable to delete this usage because it is already used!");
            }
        }
    }

    private void deleteEmptyUsageMetadata(Usage uu) {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        if (uu.getTransaction() == null && uu.getAffinity() == null) {
            Long usageId = Long.valueOf(uu.getId());
            Query query = em.createNativeQuery("DELETE FROM validator_pack_class_usage WHERE usages_id = :id");
            query.setParameter("id", usageId);
            query.executeUpdate();
            query = em.createNativeQuery("DELETE FROM registry_object_metadata_usage WHERE usages_id = :id");
            query.setParameter("id", usageId);
            query.executeUpdate();
            query = em.createNativeQuery("DELETE FROM sys_conf_type_usages WHERE listusages_id = :id");
            query.setParameter("id", usageId);
            query.executeUpdate();
            query = em.createNativeQuery("DELETE FROM registry_object_list_metadata_usage WHERE usages_id = :id");
            query.setParameter("id", usageId);
            query.executeUpdate();
            query = em.createNativeQuery("DELETE FROM conf_type_usages WHERE listusages_id = :id");
            query.setParameter("id", usageId);
            query.executeUpdate();
            em.remove(uu);
        }
    }

    private boolean checkReferencesInValidatorPackClassUsage(Long usageId) {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        Query query = em.createNativeQuery("SELECT COUNT(*) FROM validator_pack_class_usage WHERE usages_id = :id");
        query.setParameter("id", usageId);
        int count = ((BigInteger) query.getSingleResult()).intValue();
        query = em.createNativeQuery("SELECT COUNT(*) FROM registry_object_metadata_usage WHERE usages_id = :id");
        query.setParameter("id", usageId);
        count += ((BigInteger) query.getSingleResult()).intValue();
        query = em.createNativeQuery("SELECT COUNT(*) FROM sys_conf_type_usages WHERE listusages_id = :id");
        query.setParameter("id", usageId);
        count += ((BigInteger) query.getSingleResult()).intValue();
        query = em.createNativeQuery("SELECT COUNT(*) FROM registry_object_list_metadata_usage WHERE usages_id = :id");
        query.setParameter("id", usageId);
        count += ((BigInteger) query.getSingleResult()).intValue();
        query = em.createNativeQuery("SELECT COUNT(*) FROM conf_type_usages WHERE listusages_id = :id");
        query.setParameter("id", usageId);
        count += ((BigInteger) query.getSingleResult()).intValue();
        return count > 0;
    }

    // --- search for elements method -----------
    public String genStringValue(java.io.Serializable obj) throws JAXBException {

        if (obj instanceof String) {
            return (String) obj;
        }
        return null;
    }

    public void resetFilter() {
        this.getListUsages().getFilter().clear();
        this.getListUsages().resetCache();
    }

    // --- destroy ---------------
    @Destroy
    @Remove
    public void destroy() {
        log.info("destroy mETADATAUsageEditorManager..");
    }

}