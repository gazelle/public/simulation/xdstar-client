package net.ihe.gazelle.metadata.action;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.xml.bind.JAXBException;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@Stateful
@Name("mETADATASlotMetadataEditorManager")
@Scope(ScopeType.SESSION)
@GenerateInterface("SlotMetadataEditorLocal")
public class SlotMetadataEditorManager implements SlotMetadataEditorLocal {

	@Logger
	private static Log log;

	// ---  attributes -------
	private net.ihe.gazelle.metadata.model.SlotMetadata selectedMETADATASlotMetadata;
	
	private String selectedCardinalityEnum;

	
	// --- accessors ----------
	public net.ihe.gazelle.metadata.model.SlotMetadata getSelectedMETADATASlotMetadata() {
		return selectedMETADATASlotMetadata;
	}
	
	public void setSelectedMETADATASlotMetadata(net.ihe.gazelle.metadata.model.SlotMetadata selectedMETADATASlotMetadata) {
		this.selectedMETADATASlotMetadata = selectedMETADATASlotMetadata;
	}

	public String getSelectedCardinalityEnum() {
		return selectedCardinalityEnum;
	}

	public void setSelectedCardinalityEnum(String selectedCardinalityEnum) {
		this.selectedCardinalityEnum = selectedCardinalityEnum;
	}

	// --- editors method -----------
	public String editSlotMetadata(net.ihe.gazelle.metadata.model.SlotMetadata slotmetadata, String back){
		this.selectedMETADATASlotMetadata = slotmetadata;
		return "/editor/metadata/SlotMetadata.xhtml";
	}

	// --- initiators method -----------









	// --- initiators for edit method -----------


	// --- adds method -----------
	public String addNewSlotMetadata(List<net.ihe.gazelle.metadata.model.SlotMetadata> list_SlotMetadata, String back){
		net.ihe.gazelle.metadata.model.SlotMetadata slotMetadata_el = new net.ihe.gazelle.metadata.model.SlotMetadata();
		list_SlotMetadata.add(slotMetadata_el);
		this.selectedMETADATASlotMetadata = slotMetadata_el;
		return "/editor/metadata/SlotMetadata.xhtml";
	}

	// --- search for elements method -----------
	public String genStringValue(java.io.Serializable obj) throws JAXBException{

		if (obj instanceof String){
			return (String)obj;
		} 
		return null;
	}

	// --- destroy ---------------
	@Destroy
	@Remove
	public void destroy(){
		log.info("destroy mETADATASlotMetadataEditorManager..");
	}

}