package net.ihe.gazelle.xdsar.dsub.ws;

import net.ihe.gazelle.dsub.b2.NotifyType;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.xdstar.common.model.ContentType;
import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingType;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.soap.Addressing;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import static org.apache.commons.lang.CharEncoding.UTF_8;

/**
 * @author aboufahj
 */
@Stateless
@Name("DSUBRecipientWS")
@WebService(name = "DSUBRecipientWS",
        serviceName = "DSUBRecipientWSService",
        portName = "DSUBRecipientWSPort",
        targetNamespace = "http://docs.oasis-open.org/wsn/bw-2")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT,
        parameterStyle = SOAPBinding.ParameterStyle.BARE)
@BindingType(javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
@Addressing(enabled = true)
public class DSUBRecipientWS implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(DSUBRecipientWS.class);

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Resource
    WebServiceContext wsContext;

    @WebMethod(action = "http://docs.oasis-open.org/wsn/bw-2/NotificationConsumer/Notify")
    @Oneway
    public void notify(
            @WebParam(name = "Notify", targetNamespace = "http://docs.oasis-open.org/wsn/b-2") NotifyType notify) {
        DSUBNotifyMessage ds = new DSUBNotifyMessage();
        ds.setReceivedMessageContent(new MetadataMessage());
        ds.getReceivedMessageContent().setHttpHeader("Content-Type: application/soap+xml");
        ds.getReceivedMessageContent().setMessageType(MetadataMessageType.NOTIFY);
        ds.getReceivedMessageContent().setMessageContent(getNotifyTypeAsString(notify));
        updateDSUBNotifyMessage(ds);
        DSUBNotifyMessage.storeMessage(ds);
    }

    private String getNotifyTypeAsString(NotifyType nt) {
        if (nt != null) {
            try {
                JAXBContext jj = JAXBContext.newInstance(NotifyType.class);
                Marshaller m = jj.createMarshaller();
                m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                m.marshal(nt, baos);
                return baos.toString(UTF_8);
            } catch (PropertyException e) {
                LOG.error("" + e.getMessage());
            } catch (UnsupportedEncodingException e) {
                LOG.error("" + e.getMessage());
            } catch (JAXBException e) {
                LOG.error("" + e.getMessage());
            }
        }
        return null;
    }

    private void updateDSUBNotifyMessage(DSUBNotifyMessage ds) {
        ds.setAffinityDomain(AffinityDomain.getAffinityDomainByKeyword("IHE_DSUB"));
        ds.setContentType(ContentType.SOAP_MESSAGE);
        ds.setIp(getIpAddress());
        ds.setTimeStamp(new Date());
        ds.setSentMessageContent(null);
        ds.setTransaction(Transaction.GetTransactionByKeyword("ITI-53"));
        ds.setWsdlInterface(((QName) wsContext.getMessageContext().get(MessageContext.WSDL_INTERFACE)).toString());
        ds.setWsdlOperation(((QName) wsContext.getMessageContext().get(MessageContext.WSDL_OPERATION)).toString());
        ds.setWsdlService(((QName) wsContext.getMessageContext().get(MessageContext.WSDL_SERVICE)).toString());
    }

    private String getIpAddress() {
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc
                .get(MessageContext.SERVLET_REQUEST);
        return req.getRemoteAddr();
    }
}


