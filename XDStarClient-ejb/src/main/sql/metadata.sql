--
-- TOC entry 2769 (class 0 OID 92271)
-- Dependencies: 209
-- Data for Name: usage_metadata; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO usage_metadata (id, action, affinity_id, transaction_id) VALUES (1, NULL, 1, 1);

--
-- TOC entry 2757 (class 0 OID 92168)
-- Dependencies: 197
-- Data for Name: slot_metadata; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (1, 'authorInstitution', '', true, 'authorInstitution', false, '');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (2, 'authorRole', '', true, 'authorRole', false, '');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (3, 'authorPerson', '', false, 'authorPerson', false, '');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (4, 'authorSpecialty', '', true, 'authorSpecialty', false, '');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (5, 'codingScheme', '', false, 'codingScheme', false, '1.3.6.1.4.1.12559.11.4.3.2');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (6, 'codingScheme', '', true, 'codingScheme', false, '1.3.6.1.4.1.12559.11.4.3.7');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (7, 'codingScheme', '', false, 'codingScheme', false, '1.3.6.1.4.1.12559.11.4.3.3');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (8, 'codingScheme', '', false, 'codingScheme', false, '1.3.6.1.4.1.12559.11.4.3.4');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (9, 'hash', '', false, 'hash', false, '');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (10, 'codingScheme', '', false, 'codingScheme', false, '1.3.6.1.4.1.12559.11.4.3.5');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (11, 'languageCode', 'en-us', false, 'languageCode', false, '');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (12, 'legalAuthenticator', '', false, 'legalAuthenticator', false, '');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (13, 'codingScheme', '', false, 'codingScheme', false, '1.3.6.1.4.1.12559.11.4.3.6');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (14, 'repositoryUniqueId', '', false, 'repositoryUniqueId', false, '');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (15, 'serviceStartTime', '', false, 'serviceStartTime', false, '');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (16, 'serviceStopTime', '', false, 'serviceStopTime', false, '');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (17, 'size', '', false, 'size', false, '');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (18, 'sourcePatientInfo', '', false, 'sourcePatientInfo', false, '');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (19, 'codingScheme', '', false, 'codingScheme', false, '1.3.6.1.4.1.12559.11.4.3.8');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (20, 'URI', '', false, 'URI', false, '');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (21, 'authorPerson', '', false, 'authorPerson', false, '');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (22, 'authorInstitution', '', true, 'authorInstitution', false, '');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (23, 'authorRole', '', true, 'authorRole', false, '');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (24, 'authorSpecialty', '', true, 'authorSpecialty', false, '');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (25, 'codingScheme', '', false, 'codingScheme', false, '1.3.6.1.4.1.12559.11.4.3.1');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (26, 'intendedRecipient', '', false, 'intendedRecipient', false, '');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (27, 'codingScheme', '', false, 'codingScheme', false, '1.3.6.1.4.1.12559.11.4.3.10');
INSERT INTO slot_metadata (id, attribute, defaultvalue, multiple, name, supportandor, valueset) VALUES (28, 'lastUpdateTime', '', false, 'lastUpdateTime', false, '');

--
-- TOC entry 2756 (class 0 OID 92160)
-- Dependencies: 196
-- Data for Name: registry_object_metadata; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO registry_object_metadata (dtype, id, displayattributename, name, uuid, value) VALUES ('Classification', 1, 'XDSDocumentEntry.author', 'XDSDocumentEntry.author', 'urn:uuid:93606bcf-9494-43ec-9b4e-a7748d1a838d', NULL);
INSERT INTO registry_object_metadata (dtype, id, displayattributename, name, uuid, value) VALUES ('Classification', 2, 'XDSDocumentEntry.classCode', '', 'urn:uuid:41a5887f-8865-4c09-adf7-e362475b143a', NULL);
INSERT INTO registry_object_metadata (dtype, id, displayattributename, name, uuid, value) VALUES ('ExtrinsicObject', 3, 'XDSDocument Entry', 'XDSDocument Entry', 'urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1', NULL);
INSERT INTO registry_object_metadata (dtype, id, displayattributename, name, uuid, value) VALUES ('Classification', 4, 'XDSDocumentEntry.eventCodeList', '', 'urn:uuid:2c6b8cb7-8b2a-4051-b291-b1ae6a575ef4', NULL);
INSERT INTO registry_object_metadata (dtype, id, displayattributename, name, uuid, value) VALUES ('Classification', 5, 'XDSDocumentEntry.confidentialityCode', '', 'urn:uuid:f4f85eac-e6cb-4883-b524-f2705394840f', NULL);
INSERT INTO registry_object_metadata (dtype, id, displayattributename, name, uuid, value) VALUES ('Classification', 6, 'XDSDocumentEntry.formatCode', NULL, 'urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d', NULL);
INSERT INTO registry_object_metadata (dtype, id, displayattributename, name, uuid, value) VALUES ('Classification', 7, 'XDSDocumentEntry.healthcareFacilityTypeCode', NULL, 'urn:uuid:f33fb8ac-18af-42cc-ae0e-ed0b0bdb91e1', NULL);
INSERT INTO registry_object_metadata (dtype, id, displayattributename, name, uuid, value) VALUES ('Classification', 8, 'XDSDocumentEntry.practiceSettingCode', NULL, 'urn:uuid:cccf5598-8b07-4b77-a05e-ae952c785ead', NULL);
INSERT INTO registry_object_metadata (dtype, id, displayattributename, name, uuid, value) VALUES ('Classification', 9, 'XDSDocumentEntry.typeCode', NULL, 'urn:uuid:f0306f51-975f-434e-a61c-c59651d33983', NULL);
INSERT INTO registry_object_metadata (dtype, id, displayattributename, name, uuid, value) VALUES ('ExternalIdentifier', 10, 'XDSDocumentEntry.uniqueId', 'XDSDocumentEntry.uniqueId', 'urn:uuid:2e82c1f6-a085-4c72-9da3-8640a32e42ab', NULL);
INSERT INTO registry_object_metadata (dtype, id, displayattributename, name, uuid, value) VALUES ('Classification', 11, 'XDSSubmissionSet.author', 'XDSSubmissionSet.author', 'urn:uuid:a7058bb9-b4e4-4307-ba5b-e3f0ab85e12d', NULL);
INSERT INTO registry_object_metadata (dtype, id, displayattributename, name, uuid, value) VALUES ('Classification', 12, 'XDSSubmissionSet.contentTypeCode', NULL, 'urn:uuid:aa543740-bdda-424e-8c96-df4873be8500', NULL);
INSERT INTO registry_object_metadata (dtype, id, displayattributename, name, uuid, value) VALUES ('ExternalIdentifier', 13, 'XDSSubmissionSet.sourceId', 'XDSSubmissionSet.sourceId', 'urn:uuid:554ac39e-e3fe-47fe-b233-965d2a147832', NULL);
INSERT INTO registry_object_metadata (dtype, id, displayattributename, name, uuid, value) VALUES ('ExternalIdentifier', 14, 'XDSSubmissionSet.uniqueId', 'XDSSubmissionSet.uniqueId', 'urn:uuid:96fdda7c-d067-4183-912e-bf5ee74998a8', NULL);
INSERT INTO registry_object_metadata (dtype, id, displayattributename, name, uuid, value) VALUES ('RegistryPackage', 15, 'XDSSubmissionSet', 'XDS Submission Set', 'urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd', NULL);
INSERT INTO registry_object_metadata (dtype, id, displayattributename, name, uuid, value) VALUES ('Classification', 17, 'XDSFolder.codeList', NULL, 'urn:uuid:1ba97051-7806-41a8-a48b-8fce7af683c5', NULL);
INSERT INTO registry_object_metadata (dtype, id, displayattributename, name, uuid, value) VALUES ('ExternalIdentifier', 18, 'XDSFolder.uniqueId', 'XDSFolder.uniqueId', 'urn:uuid:75df8f67-9973-4fbe-a900-df66cefecc5a', NULL);
INSERT INTO registry_object_metadata (dtype, id, displayattributename, name, uuid, value) VALUES ('RegistryPackage', 16, 'XDSFolder', 'XDS Folder', 'urn:uuid:d9d542f3-6cc4-48b6-8870-ea235fbc94c2', NULL);


--
-- TOC entry 2771 (class 0 OID 92281)
-- Dependencies: 211
-- Data for Name: registry_object_metadata_classification_optional; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO registry_object_metadata_classification_optional (registry_object_metadata_id, classification_optional_id) VALUES (3, 1);
INSERT INTO registry_object_metadata_classification_optional (registry_object_metadata_id, classification_optional_id) VALUES (3, 4);
INSERT INTO registry_object_metadata_classification_optional (registry_object_metadata_id, classification_optional_id) VALUES (15, 11);


--
-- TOC entry 2772 (class 0 OID 92284)
-- Dependencies: 212
-- Data for Name: registry_object_metadata_classification_required; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO registry_object_metadata_classification_required (registry_object_metadata_id, classification_required_id) VALUES (3, 2);
INSERT INTO registry_object_metadata_classification_required (registry_object_metadata_id, classification_required_id) VALUES (3, 5);
INSERT INTO registry_object_metadata_classification_required (registry_object_metadata_id, classification_required_id) VALUES (3, 6);
INSERT INTO registry_object_metadata_classification_required (registry_object_metadata_id, classification_required_id) VALUES (3, 7);
INSERT INTO registry_object_metadata_classification_required (registry_object_metadata_id, classification_required_id) VALUES (3, 8);
INSERT INTO registry_object_metadata_classification_required (registry_object_metadata_id, classification_required_id) VALUES (3, 9);
INSERT INTO registry_object_metadata_classification_required (registry_object_metadata_id, classification_required_id) VALUES (15, 12);
INSERT INTO registry_object_metadata_classification_required (registry_object_metadata_id, classification_required_id) VALUES (16, 17);


--
-- TOC entry 2773 (class 0 OID 92287)
-- Dependencies: 213
-- Data for Name: registry_object_metadata_ext_identifier_optional; Type: TABLE DATA; Schema: public; Owner: gazelle
--



--
-- TOC entry 2774 (class 0 OID 92290)
-- Dependencies: 214
-- Data for Name: registry_object_metadata_ext_identifier_required; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO registry_object_metadata_ext_identifier_required (registry_object_metadata_id, ext_identifier_required_id) VALUES (3, 10);
INSERT INTO registry_object_metadata_ext_identifier_required (registry_object_metadata_id, ext_identifier_required_id) VALUES (15, 13);
INSERT INTO registry_object_metadata_ext_identifier_required (registry_object_metadata_id, ext_identifier_required_id) VALUES (15, 14);
INSERT INTO registry_object_metadata_ext_identifier_required (registry_object_metadata_id, ext_identifier_required_id) VALUES (16, 18);


--
-- TOC entry 2775 (class 0 OID 92293)
-- Dependencies: 215
-- Data for Name: registry_object_metadata_slot_optional; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO registry_object_metadata_slot_optional (registry_object_metadata_id, slot_optional_id) VALUES (1, 1);
INSERT INTO registry_object_metadata_slot_optional (registry_object_metadata_id, slot_optional_id) VALUES (1, 2);
INSERT INTO registry_object_metadata_slot_optional (registry_object_metadata_id, slot_optional_id) VALUES (1, 3);
INSERT INTO registry_object_metadata_slot_optional (registry_object_metadata_id, slot_optional_id) VALUES (1, 4);
INSERT INTO registry_object_metadata_slot_optional (registry_object_metadata_id, slot_optional_id) VALUES (3, 9);
INSERT INTO registry_object_metadata_slot_optional (registry_object_metadata_id, slot_optional_id) VALUES (3, 12);
INSERT INTO registry_object_metadata_slot_optional (registry_object_metadata_id, slot_optional_id) VALUES (3, 14);
INSERT INTO registry_object_metadata_slot_optional (registry_object_metadata_id, slot_optional_id) VALUES (3, 15);
INSERT INTO registry_object_metadata_slot_optional (registry_object_metadata_id, slot_optional_id) VALUES (3, 16);
INSERT INTO registry_object_metadata_slot_optional (registry_object_metadata_id, slot_optional_id) VALUES (3, 17);
INSERT INTO registry_object_metadata_slot_optional (registry_object_metadata_id, slot_optional_id) VALUES (3, 18);
INSERT INTO registry_object_metadata_slot_optional (registry_object_metadata_id, slot_optional_id) VALUES (3, 20);
INSERT INTO registry_object_metadata_slot_optional (registry_object_metadata_id, slot_optional_id) VALUES (11, 21);
INSERT INTO registry_object_metadata_slot_optional (registry_object_metadata_id, slot_optional_id) VALUES (11, 22);
INSERT INTO registry_object_metadata_slot_optional (registry_object_metadata_id, slot_optional_id) VALUES (11, 23);
INSERT INTO registry_object_metadata_slot_optional (registry_object_metadata_id, slot_optional_id) VALUES (11, 24);
INSERT INTO registry_object_metadata_slot_optional (registry_object_metadata_id, slot_optional_id) VALUES (15, 26);
INSERT INTO registry_object_metadata_slot_optional (registry_object_metadata_id, slot_optional_id) VALUES (16, 28);


--
-- TOC entry 2776 (class 0 OID 92298)
-- Dependencies: 216
-- Data for Name: registry_object_metadata_slot_required; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO registry_object_metadata_slot_required (registry_object_metadata_id, slot_required_id) VALUES (2, 5);
INSERT INTO registry_object_metadata_slot_required (registry_object_metadata_id, slot_required_id) VALUES (4, 6);
INSERT INTO registry_object_metadata_slot_required (registry_object_metadata_id, slot_required_id) VALUES (5, 7);
INSERT INTO registry_object_metadata_slot_required (registry_object_metadata_id, slot_required_id) VALUES (6, 8);
INSERT INTO registry_object_metadata_slot_required (registry_object_metadata_id, slot_required_id) VALUES (7, 10);
INSERT INTO registry_object_metadata_slot_required (registry_object_metadata_id, slot_required_id) VALUES (3, 11);
INSERT INTO registry_object_metadata_slot_required (registry_object_metadata_id, slot_required_id) VALUES (8, 13);
INSERT INTO registry_object_metadata_slot_required (registry_object_metadata_id, slot_required_id) VALUES (9, 19);
INSERT INTO registry_object_metadata_slot_required (registry_object_metadata_id, slot_required_id) VALUES (12, 25);
INSERT INTO registry_object_metadata_slot_required (registry_object_metadata_id, slot_required_id) VALUES (17, 27);


--
-- TOC entry 2777 (class 0 OID 92303)
-- Dependencies: 217
-- Data for Name: registry_object_metadata_usage; Type: TABLE DATA; Schema: public; Owner: gazelle
--

INSERT INTO registry_object_metadata_usage (registry_object_metadata_id, usages_id) VALUES (1, 1);
INSERT INTO registry_object_metadata_usage (registry_object_metadata_id, usages_id) VALUES (2, 1);
INSERT INTO registry_object_metadata_usage (registry_object_metadata_id, usages_id) VALUES (4, 1);
INSERT INTO registry_object_metadata_usage (registry_object_metadata_id, usages_id) VALUES (10, 1);
INSERT INTO registry_object_metadata_usage (registry_object_metadata_id, usages_id) VALUES (3, 1);
INSERT INTO registry_object_metadata_usage (registry_object_metadata_id, usages_id) VALUES (12, 1);
INSERT INTO registry_object_metadata_usage (registry_object_metadata_id, usages_id) VALUES (18, 1);



