INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('1', 'FindDocuments', 'urn:uuid:14d4debf-8f97-4251-9a74-a90016b0af0d', '4', 'urn:ihe:iti:2007:CrossGatewayQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('2', 'FindSubmissionSets', 'urn:uuid:f26abbcb-ac74-4422-8a30-edb644bbc1a9', '4', 'urn:ihe:iti:2007:CrossGatewayQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('3', 'FindFolders', 'urn:uuid:958f3006-baad-4929-a4de-ff1114824431', '4', 'urn:ihe:iti:2007:CrossGatewayQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('4', 'GetAll', 'urn:uuid:10b545ea-725c-446d-9b95-8aeb444eddf3', '4', 'urn:ihe:iti:2007:CrossGatewayQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('5', 'GetDocuments', 'urn:uuid:5c4f972b-d56b-40ac-a5fc-c8ca9b40b9d4', '4', 'urn:ihe:iti:2007:CrossGatewayQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('6', 'GetFolders', 'urn:uuid:5737b14c-8a1a-4539-b659-e03a34a5e1e4', '4', 'urn:ihe:iti:2007:CrossGatewayQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('7', 'GetAssociations', 'urn:uuid:a7ae438b-4bc2-4642-93e9-be891f7bb155', '4', 'urn:ihe:iti:2007:CrossGatewayQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('8', 'GetDocumentsAndAssociations', 'urn:uuid:bab9529a-4a10-40b3-a01f-f68a615d247a', '4', 'urn:ihe:iti:2007:CrossGatewayQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('9', 'GetSubmissionSets', 'urn:uuid:51224314-5390-4169-9b91-b1980040715a', '4', 'urn:ihe:iti:2007:CrossGatewayQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('10','GetSubmissionSetAndContents', 'urn:uuid:e8e3cb2c-e39c-46b9-99e4-c12f57260b83', '4', 'urn:ihe:iti:2007:CrossGatewayQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('11','GetFoldersAndContents', 'urn:uuid:b909a503-523d-4517-8acf-8e5834dfc4c7', '4', 'urn:ihe:iti:2007:CrossGatewayQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('12','GetFoldersForDocument', 'urn:uuid:10cae35a-c7f9-4cf5-b61e-fc3278ffb578', '4', 'urn:ihe:iti:2007:CrossGatewayQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('13','GetRelatedDocuments', 'urn:uuid:d90e5407-b356-4d91-a89f-873917b4b0e6', '4', 'urn:ihe:iti:2007:CrossGatewayQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('14','CrossGatewayRetrieve', null, '5', 'urn:ihe:iti:2007:CrossGatewayRetrieve');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('15','PatientService:list', 'urn:uuid:14d4debf-8f97-4251-9a74-a90016b0af0d', '6', 'urn:ihe:iti:2007:CrossGatewayQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('16','OrderService:list', 'urn:uuid:14d4debf-8f97-4251-9a74-a90016b0af0d', '6', 'urn:ihe:iti:2007:CrossGatewayQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('17','PatientService:retrieve', null, '6', 'urn:ihe:iti:2007:CrossGatewayRetrieve');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('18','OrderService:retrieve', null, '6', 'urn:ihe:iti:2007:CrossGatewayRetrieve');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('19', 'FindDocuments', 'urn:uuid:14d4debf-8f97-4251-9a74-a90016b0af0d', '7', 'urn:ihe:iti:2007:RegistryStoredQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('20', 'FindSubmissionSets', 'urn:uuid:f26abbcb-ac74-4422-8a30-edb644bbc1a9', '7', 'urn:ihe:iti:2007:RegistryStoredQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('21', 'FindFolders', 'urn:uuid:958f3006-baad-4929-a4de-ff1114824431', '7', 'urn:ihe:iti:2007:RegistryStoredQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('22', 'GetAll', 'urn:uuid:10b545ea-725c-446d-9b95-8aeb444eddf3', '7', 'urn:ihe:iti:2007:RegistryStoredQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('23', 'GetDocuments', 'urn:uuid:5c4f972b-d56b-40ac-a5fc-c8ca9b40b9d4', '7', 'urn:ihe:iti:2007:RegistryStoredQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('24', 'GetFolders', 'urn:uuid:5737b14c-8a1a-4539-b659-e03a34a5e1e4', '7', 'urn:ihe:iti:2007:RegistryStoredQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('25', 'GetAssociations', 'urn:uuid:a7ae438b-4bc2-4642-93e9-be891f7bb155', '7', 'urn:ihe:iti:2007:RegistryStoredQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('26', 'GetDocumentsAndAssociations', 'urn:uuid:bab9529a-4a10-40b3-a01f-f68a615d247a', '7', 'urn:ihe:iti:2007:RegistryStoredQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('27', 'GetSubmissionSets', 'urn:uuid:51224314-5390-4169-9b91-b1980040715a', '7', 'urn:ihe:iti:2007:RegistryStoredQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('28','GetSubmissionSetAndContents', 'urn:uuid:e8e3cb2c-e39c-46b9-99e4-c12f57260b83', '7', 'urn:ihe:iti:2007:RegistryStoredQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('29','GetFoldersAndContents', 'urn:uuid:b909a503-523d-4517-8acf-8e5834dfc4c7', '7', 'urn:ihe:iti:2007:RegistryStoredQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('30','GetFoldersForDocument', 'urn:uuid:10cae35a-c7f9-4cf5-b61e-fc3278ffb578', '7', 'urn:ihe:iti:2007:RegistryStoredQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('31','GetRelatedDocuments', 'urn:uuid:d90e5407-b356-4d91-a89f-873917b4b0e6', '7', 'urn:ihe:iti:2007:RegistryStoredQuery');
INSERT INTO "message_type" (id, name, uuid, transaction_id, action) VALUES ('32','RetrieveDocumentSet', null, '8', 'urn:ihe:iti:2007:RetrieveDocumentSet');

SELECT pg_catalog.setval('message_type_id_seq', 32 , true);

INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('1', '$XDSDocumentEntryPatientId', 'urn:uuid:58a6f841-87b3-4a3e-92fd-a8ffeff98427','XDSDocumentEntry.patientId', false, false, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('2', '$XDSDocumentEntryClassCode', 'urn:uuid:41a5887f-8865-4c09-adf7-e362475b143a', 'XDSDocumentEntry.classCode', false, true, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('3', '$XDSDocumentEntryTypeCode', 'urn:uuid:f0306f51-975f-434e-a61c-c59651d33983', 'XDSDocumentEntry.typeCode', false, true, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('4', '$XDSDocumentEntryPracticeSettingCode', 'urn:uuid:cccf5598-8b07-4b77-a05e-ae952c785ead', 'XDSDocumentEntry.practiceSettingCode', false, true, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('5', '$XDSDocumentEntryCreationTimeFrom', null, 'Lower value of XDSDocumentEntry. creationTime', false, false, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('6', '$XDSDocumentEntryCreationTimeTo', null, 'Lower value of XDSDocumentEntry.creationTime', false, false, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('7', '$XDSDocumentEntryServiceStartTimeFrom', null, 'Lower value of XDSDocumentEntry.serviceStartTime', false, false, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('8', '$XDSDocumentEntryServiceStartTimeTo', null, 'Upper value of XDSDocumentEntry.serviceStartTime', false, false, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('10', '$XDSDocumentEntryServiceStopTimeFrom', null, 'Lower value of XDSDocumentEntry.serviceStopTime', false, false, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('11', '$XDSDocumentEntryServiceStopTimeTo', null, 'Upper value of XDSDocumentEntry.serviceStopTime', false, false, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('12', '$XDSDocumentEntryHealthcareFacilityTypeCode', 'urn:uuid:f33fb8ac-18af-42cc-ae0e-ed0b0bdb91e1', 'XDSDocumentEntry.healthcareFacilityTypeCode', false, true, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('13', '$XDSDocumentEntryEventCodeList', 'urn:uuid:2c6b8cb7-8b2a-4051-b291-b1ae6a575ef4', 'XDSDocumentEntry.eventCodeList', true, true, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('14', '$XDSDocumentEntryConfidentialityCode', 'urn:uuid:f4f85eac-e6cb-4883-b524-f2705394840f', 'XDSDocumentEntry.confidentialityCode', true, true, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('15', '$XDSDocumentEntryAuthorPerson', 'urn:uuid:93606bcf-9494-43ec-9b4e-a7748d1a838d', 'XDSDocumentEntry.author', false, true, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('16', '$XDSDocumentEntryFormatCode', 'urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d', 'XDSDocumentEntry.formatCode', false, true, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('17', '$XDSDocumentEntryStatus', null, 'XDSDocumentEntry.status', false, true, 'urn:oasis:names:tc:ebxml-regrep:StatusType:Approved');
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('18', '$XDSSubmissionSetPatientId', 'urn:uuid:6b5aea1a-874d-4603-a4bc-96a0a7b38446', 'XDSSubmissionSet.patientId', false, false, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('19', '$XDSSubmissionSetSourceId', 'urn:uuid:554ac39e-e3fe-47fe-b233-965d2a147832', 'XDSSubmissionSet.sourceId', false, true, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('20', '$XDSSubmissionSetSubmissionTimeFrom', null, 'Lower value of XDSSubmissionSet.submissionTime', false, false, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('21', '$XDSSubmissionSetSubmissionTimeTo', null, 'Upper value of XDSSubmissionSet.serviceStopTime', false, false, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('22', '$XDSSubmissionSetAuthorPerson', 'urn:uuid:a7058bb9-b4e4-4307-ba5b-e3f0ab85e12d', 'XDSSubmissionSet.authorPerson', false, false, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('23', '$XDSSubmissionSetContentType', 'urn:uuid:aa543740-bdda-424e-8c96-df4873be8500', 'XDSSubmissionSet.contentTypeCode', false, true, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('24', '$XDSSubmissionSetStatus', null, 'XDSSubmissionSet.status', false, true, 'urn:oasis:names:tc:ebxml-regrep:StatusType:Approved');
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('25', '$XDSFolderPatientId', 'urn:uuid:f64ffdf0-4b97-4e06-b79f-a52b38ec2f8a', 'XDSFolder.patientId', false, false, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('26', '$XDSFolderLastUpdateTimeFrom', null, 'Lower value of XDSFolder.lastUpdateTime', false, false, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('27', '$XDSFolderLastUpdateTimeTo', null, 'Upper value of XDSFolder.lastUpdateTime', false, false, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('28', '$XDSFolderCodeList', 'urn:uuid:1ba97051-7806-41a8-a48b-8fce7af683c5', 'XDSFolder.codeList', true, true, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('29', '$XDSFolderStatus', null, 'XDSFolder.status', false, true, 'urn:oasis:names:tc:ebxml-regrep:StatusType:Approved');
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('30', '$patientId', 'urn:uuid:f64ffdf0-4b97-4e06-b79f-a52b38ec2f8a', 'XDSFolder.patientId, XDSSubmissionSet.patientId, XDSDocumentEntry.patientId', false, false, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('31', '$XDSDocumentEntryEntryUUID', null, 'XDSDocumentEntry.entryUUID', false, true, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('32', '$XDSDocumentEntryUniqueId', 'urn:uuid:2e82c1f6-a085-4c72-9da3-8640a32e42ab', 'XDSDocumentEntry. uniqueId', false, true, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('33', '$homeCommunityId', null, 'None', false, false, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('34', '$XDSFolderEntryUUID', null, 'XDSFolder.entryUUID', false, true, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('35', '$XDSFolderUniqueId', 'urn:uuid:75df8f67-9973-4fbe-a900-df66cefecc5a', 'XDSFolder. uniqueId', false, true, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('36', '$uuid', null, 'None', false, true, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('37', '$XDSSubmissionSetEntryUUID', null, 'XDSSubmissionSet.entryUUID', false, false, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('38', '$XDSSubmissionSetUniqueId', 'urn:uuid:2e82c1f6-a085-4c72-9da3-8640a32e42ab', 'XDSSubmissionSet.uniqueId', false, false, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('39', '$AssociationTypes', null, 'Not a named attribute', false, true, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('40', 'HomeCommunityId', null, null, false, true, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('41', 'RepositoryUniqueId', null, null, false, true, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('42', 'DocumentUniqueId', null,null, false, true, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('43', '$XDSDocumentEntryEventCodeList', 'urn:uuid:f0306f51-975f-434e-a61c-c59651d33983', 'XDSDocumentEntry.typeCode', false, true, '57833-6^^2.16.840.1.113883.6.1'); -- order service --
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('44', '$XDSDocumentEntryEventCodeList', 'urn:uuid:f0306f51-975f-434e-a61c-c59651d33983 ', 'XDSDocumentEntry.typeCode', false, true, '34133-9^^2.16.840.1.113883.6.1'); -- patient service --
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('45', 'HomeCommunityId', null, null, false, false, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('46', 'RepositoryUniqueId', null, null, false, false, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('47', 'DocumentUniqueId', null, null, false, false, null);
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('48', '$XDSDocumentEntryClassCode', 'urn:uuid:41a5887f-8865-4c09-adf7-e362475b143a', 'XDSDocumentEntry.classCode', false, false, '60591-5^^2.16.840.1.113883.6.1');
INSERT INTO "parameter" (id, name, uuid, attribute, supports_and_or, multiple, default_value) VALUES ('49', '$XDSDocumentEntryClassCode', 'urn:uuid:41a5887f-8865-4c09-adf7-e362475b143a', 'XDSDocumentEntry.classCode', false, false, '57833-6^^2.16.840.1.113883.6.1');


SELECT pg_catalog.setval('parameter_id_seq', 49 , true);



INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('1', '1');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('1', '17');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('2', '18');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('2', '24');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('3', '25');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('3', '29');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('4', '30');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('4', '17');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('4', '24');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('4', '29');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('7', '36');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('9', '36');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('13', '39');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('14', '40');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('14', '41');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('14', '42');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('15', '1');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('15', '48');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('15', '17');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('16', '1');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('16', '17');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('16', '49');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('17', '40');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('17', '41');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('17', '42');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('18', '40');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('18', '41');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('18', '42');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('19', '1');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('19', '17');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('20', '18');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('20', '24');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('21', '25');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('21', '29');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('22', '30');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('22', '17');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('22', '24');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('22', '29');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('25', '36');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('27', '36');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('31', '39');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('32', '40');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('32', '41');
INSERT INTO "message_type_required_parameters" (message_type_id, parameter_id) VALUES ('32', '42');


INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('1', '2');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('1', '3');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('1', '4');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('1', '5');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('1', '6');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('1', '7');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('1', '8');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('1', '10');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('1', '11');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('1', '12');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('1', '13');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('1', '14');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('1', '15');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('1', '16');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('2', '19');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('2', '20');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('2', '21');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('2', '22');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('2', '23');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('3', '26');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('3', '27');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('3', '28');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('4', '16');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('4', '14');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('5', '31');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('5', '32');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('5', '33');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('6', '34');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('6', '35');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('6', '33');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('7', '33');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('8', '31');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('8', '32');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('8', '33');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('9', '33');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('10', '33');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('10', '37');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('10', '38');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('10', '14');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('10', '16');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('11', '33');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('11', '34');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('11', '35');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('11', '14');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('11', '16');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('12', '33');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('12', '31');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('12', '32');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('13', '33');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('13', '31');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('13', '32');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('19', '2');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('19', '3');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('19', '4');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('19', '5');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('19', '6');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('19', '7');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('19', '8');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('19', '10');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('19', '11');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('19', '12');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('19', '13');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('19', '14');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('19', '15');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('19', '16');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('20', '19');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('20', '20');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('20', '21');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('20', '22');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('20', '23');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('21', '26');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('21', '27');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('21', '28');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('22', '16');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('22', '14');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('23', '31');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('23', '32');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('23', '33');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('24', '34');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('24', '35');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('24', '33');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('25', '33');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('26', '31');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('26', '32');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('26', '33');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('27', '33');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('28', '33');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('28', '37');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('28', '38');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('28', '14');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('28', '16');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('29', '33');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('29', '34');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('29', '35');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('29', '14');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('29', '16');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('30', '33');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('30', '31');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('30', '32');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('31', '33');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('31', '31');
INSERT INTO "message_type_optional_parameters" (message_type_id, parameter_id) VALUES ('31', '32');
