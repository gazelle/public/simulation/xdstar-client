INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(1,'1.2.840.10008.1.2','Implicit VR Endian: Default Transfer Syntax for DICOM','');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(2,'1.2.840.10008.1.2.1','Explicit VR Little Endian','');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(3,'1.2.840.10008.1.2.1.99','Deflated Explicit VR Big Endian','');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(4,'1.2.840.10008.1.2.2','Explicit VR Big Endian','');

INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(5,'1.2.840.10008.1.2.4.50','JPEG Baseline (Process 1)','');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(6,'1.2.840.10008.1.2.4.51','JPEG Baseline (Processes 2 & 4)','');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(7,'1.2.840.10008.1.2.4.52','JPEG Extended (Processes 3 & 5)','retired');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(8,'1.2.840.10008.1.2.4.53','JPEG Spectral Selection, Nonhierarchical (Processes 6 & 8)','retired');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(9,'1.2.840.10008.1.2.4.54','JPEG Spectral Selection, Nonhierarchical (Processes 7 & 9)','retired');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(10,'1.2.840.10008.1.2.4.55','JPEG Full Progression, Nonhierarchical (Processes 10 & 12)','retired');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(11,'1.2.840.10008.1.2.4.56','JPEG Full Progression, Nonhierarchical (Processes 11 & 13)','retired');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(12,'1.2.840.10008.1.2.4.57','JPEG Lossless, Nonhierarchical (Processes 14)','');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(13,'1.2.840.10008.1.2.4.58','JPEG Lossless, Nonhierarchical (Processes 15)','retired');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(14,'1.2.840.10008.1.2.4.59','JPEG Extended, Hierarchical (Processes 16 & 18)','retired');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(15,'1.2.840.10008.1.2.4.60','JPEG Extended, Hierarchical (Processes 17 & 19)','retired');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(16,'1.2.840.10008.1.2.4.61','JPEG Spectral Selection, Hierarchical (Processes 20 & 22)','retired');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(17,'1.2.840.10008.1.2.4.62','JPEG Spectral Selection, Hierarchical (Processes 21 & 23)','retired');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(18,'1.2.840.10008.1.2.4.63','JPEG Full Progression, Hierarchical (Processes 24 & 26)','retired');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(19,'1.2.840.10008.1.2.4.64','JPEG Full Progression, Hierarchical (Processes 25 & 27)','retired');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(20,'1.2.840.10008.1.2.4.65','JPEG Lossless, Nonhierarchical (Process 28)','retired');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(21,'1.2.840.10008.1.2.4.66','JPEG Lossless, Nonhierarchical (Process 29)','retired');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(22,'1.2.840.10008.1.2.4.70','JPEG Lossless, Nonhierarchical, First- Order Prediction','');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(23,'1.2.840.10008.1.2.4.80','JPEG-LS Lossless Image Compression','');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(24,'1.2.840.10008.1.2.4.81','JPEG-LS Lossy (Near- Lossless) Image Compression','');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(25,'1.2.840.10008.1.2.4.90','JPEG 2000 Image Compression (Lossless Only)','');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(26,'1.2.840.10008.1.2.4.91','JPEG 2000 Image Compression','');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(27,'1.2.840.10008.1.2.4.92','JPEG 2000 Part 2 Multicomponent Image Compression (Lossless Only)','');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(28,'1.2.840.10008.1.2.4.93','JPEG 2000 Part 2 Multicomponent Image Compression','');

INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(29,'1.2.840.10008.1.2.4.94','JPIP Referenced','');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(30,'1.2.840.10008.1.2.4.95','JPIP Referenced Deflate','');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(31,'1.2.840.10008.1.2.5','RLE Lossless','');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(32,'1.2.840.10008.1.2.6.1','RFC 2557 MIME Encapsulation','');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(33,'1.2.840.10008.1.2.4.100','MPEG2 Main Profile Main Level','');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(34,'1.2.840.10008.1.2.4.102','MPEG-4 AVC/H.264 High Profile / Level 4.1','');
INSERT INTO transfer_syntax_uid (id,uid,name,comment)VALUES(35,'1.2.840.10008.1.2.4.103','MPEG-4 AVC/H.264 BD-compatible High Profile / Level 4.1','');


SELECT pg_catalog.setval('transfer_syntax_uid_id_seq', 35 , true);
