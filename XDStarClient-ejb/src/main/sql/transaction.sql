INSERT INTO tf_transaction (id, name, keyword, description) VALUES (1, 'Provide and Register Document Set b', 'ITI-41', 'ITI-41');
INSERT INTO tf_transaction (id, name, keyword, description) VALUES (2, 'DispensationService:initialize()', 'DispensationService:initialize()', 'epSOS : DispensationService:initialize()');
INSERT INTO tf_transaction (id, name, keyword, description) VALUES (3, 'ConsentService:put()', 'ConsentService:put()', 'epSOS : ConsentService:put()');

INSERT INTO affinity_domain (id, label_to_display, profile, keyword) VALUES ('1',	'IHE', 'XDR',	'IHE_XDR');
INSERT INTO affinity_domain (id, label_to_display, profile, keyword) VALUES ('2',	'epSOS','XDR', 'epSOS_XDR');

INSERT INTO affinity_domain_transactions (affinity_domain_id, transaction_id) VALUES ('1', '1');
INSERT INTO affinity_domain_transactions (affinity_domain_id, transaction_id) VALUES ('2', '2');
INSERT INTO affinity_domain_transactions (affinity_domain_id, transaction_id) VALUES ('2', '3');


--------- RSQ / RDS -----------
INSERT INTO tf_transaction (id, description, keyword, name) VALUES ('4',	'Cross Gateway Query',	'ITI-38',	'Cross Gateway Query');
INSERT INTO tf_transaction (id, description, keyword, name) VALUES ('5',	'Cross Gateway Retrieve',	'ITI-39',	'Cross Gateway Retrieve');
INSERT INTO tf_transaction (id, description, keyword, name) VALUES ('6',	'ITI-38 + ITI-39',	'epSOS-1',	null);
INSERT INTO tf_transaction (id, description, keyword, name) VALUES ('7',	'Registry Stored Query',	'ITI-18',	'Registry Stored Query');
INSERT INTO tf_transaction (id, description, keyword, name) VALUES ('8',	'Retrieve Document Set',	'ITI-43',	'Retrieve Document Set');

SELECT pg_catalog.setval('tf_transaction_id_seq', 8 , true);

INSERT INTO affinity_domain (id, label_to_display, profile, keyword) VALUES ('3',	'IHE', 'XCA',	'IHE_XCA');
INSERT INTO affinity_domain (id, label_to_display, profile, keyword) VALUES ('4',	'epSOS','XCA', 'epSOS_XCA');
INSERT INTO affinity_domain (id, label_to_display, profile, keyword) VALUES ('5',	'IHE','XDS-b', 'IHE_XDS-b');

SELECT pg_catalog.setval('affinity_domain_id_seq', 5 , true);

INSERT INTO affinity_domain_transactions (affinity_domain_id, transaction_id) VALUES ('3', '4');
INSERT INTO affinity_domain_transactions (affinity_domain_id, transaction_id) VALUES ('3', '5');
INSERT INTO affinity_domain_transactions (affinity_domain_id, transaction_id) VALUES ('4', '6');
INSERT INTO affinity_domain_transactions (affinity_domain_id, transaction_id) VALUES ('4', '7');
INSERT INTO affinity_domain_transactions (affinity_domain_id, transaction_id) VALUES ('5', '8');

