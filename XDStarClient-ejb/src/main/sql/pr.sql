INSERT INTO system_configuration (id,name,system_name,url, dtype)VALUES(1,'Epsos Secured', 'Jumbo Repository','https://131.254.209.14:9085/epsos/services/xdsrepositoryb', 'Repository');
INSERT INTO system_configuration (id,name,system_name,url, dtype)VALUES(2,'Epsos Unsecured', 'Jumbo Repository', 'http://131.254.209.14:9080/epsos/services/xdsrepositoryb', 'Repository');

SELECT pg_catalog.setval('system_configuration_id_seq', 4, true);

-- XDS Document Entry
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (1, 'urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1', 'XDSDocumentEntry', 'ClassificationNode', null, false);
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (2, 'urn:uuid:93606bcf-9494-43ec-9b4e-a7748d1a838d', 'XDSDocumentEntry.author', 'External Classification Scheme', false, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (3, 'urn:uuid:41a5887f-8865-4c09-adf7-e362475b143a', 'XDSDocumentEntry.classCode', 'External Classification Scheme', true, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (4, 'urn:uuid:f4f85eac-e6cb-4883-b524-f2705394840f', 'XDSDocumentEntry.confidentialityCode', 'External Classification Scheme', true, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (5, 'urn:uuid:2c6b8cb7-8b2a-4051-b291-b1ae6a575ef4', 'XDSDocumentEntry.eventCodeList', 'External Classification Scheme', false, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (6, 'urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d', 'XDSDocumentEntry.formatCode', 'External Classification Scheme', true, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (7, 'urn:uuid:f33fb8ac-18af-42cc-ae0e-ed0b0bdb91e1', 'XDSDocumentEntry.healthCareFacilityTypeCode', 'External Classification Scheme', true, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (8, 'urn:uuid:58a6f841-87b3-4a3e-92fd-a8ffeff98427', 'XDSDocumentEntry.patientId', 'External Identifier', true, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (9, 'urn:uuid:cccf5598-8b07-4b77-a05e-ae952c785ead', 'XDSDocumentEntry.practiceSettingCode', 'External Classification Scheme', true, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (10, 'urn:uuid:f0306f51-975f-434e-a61c-c59651d33983', 'XDSDocumentEntry.typeCode', 'External Classification Scheme', true, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (11, 'urn:uuid:2e82c1f6-a085-4c72-9da3-8640a32e42ab', 'XDSDocumentEntry.uniqueId', 'External Identifier', true, true );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (12, null, 'authorDepartment', 'Slot', null, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (13, null, 'authorInstitution', 'Slot', null, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (14, null, 'authorPerson', 'Slot', null, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (15, null, 'creationTime', 'Slot', true, true );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (16, null, 'hash', 'Slot', null, true );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (17, null, 'healthcareFacilityTypeCodeDisplayName', 'Slot', null, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (18, null, 'languageCode', 'Slot', true, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (19, null, 'legalAuthenticator', 'Slot', null, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (20, null, 'serviceStartTime', 'Slot', null, true );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (21, null, 'serviceStopTime', 'Slot', null, true );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (22, null, 'sourcePatientInfo', 'Slot', null, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (23, null, 'size', 'Slot', null, true );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (24, null, 'sourcePatientId', 'Slot', true, true );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (25, 'urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd', 'XDSSubmissionSet', 'ClassificationNode', null, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (26, 'urn:uuid:a7058bb9-b4e4-4307-ba5b-e3f0ab85e12d', 'XDSSubmissionSet.author', 'External Classification Scheme', false, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (27, 'urn:uuid:aa543740-bdda-424e-8c96-df4873be8500', 'XDSSubmissionSet.contentTypeCode', 'External Classification Scheme', false, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (28, 'urn:uuid:6b5aea1a-874d-4603-a4bc-96a0a7b38446', 'XDSSubmissionSet.patientId', 'External Identifier', true, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (29, 'urn:uuid:554ac39e-e3fe-47fe-b233-965d2a147832', 'XDSSubmissionSet.sourceId', 'External Identifier', true, true );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (30, 'urn:uuid:96fdda7c-d067-4183-912e-bf5ee74998a8', 'XDSSubmissionSet.uniqueId', 'External Identifier', true, true );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (31, 'urn:uuid:d9d542f3-6cc4-48b6-8870-ea235fbc94c2', 'XDSFolder', 'ClassificationNode', null, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (32, 'urn:uuid:1ba97051-7806-41a8-a48b-8fce7af683c5', 'XDSFolder.codeList', 'External Classification Scheme', false, false );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (33, 'urn:uuid:f64ffdf0-4b97-4e06-b79f-a52b38ec2f8a', 'XDSFolder.patientId', 'External Classification Scheme', true, true );
INSERT INTO metadata (id, uuid, name, type, required, generated) VALUES (34, 'urn:uuid:75df8f67-9973-4fbe-a900-df66cefecc5a', 'XDSFolder.uniqueId', 'External Classification Scheme', true, true );


SELECT pg_catalog.setval('metadata_id_seq', 34 , true);


INSERT INTO code (id, class_scheme, code, coding_scheme, display, transaction_id) VALUES (4, 'urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d', 'urn:epSOS:ep:dis:2010', 'epSOS formatCodes', 'urn:epSOS:ep:dis:2010', 2);
INSERT INTO code (id, class_scheme, code, coding_scheme, display, transaction_id) VALUES (5, 'urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d', 'urn:ihe:iti:xds-sd:pdf:2008', 'epSOS formatCodes', 'urn:ihe:iti:xds-sd:pdf:2008', 2);
INSERT INTO code (id, class_scheme, code, coding_scheme, display, transaction_id) VALUES (8, 'urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d', 'urn:ihe:iti:bppc-sd:2007', '1.3.6.1.4.1.19376.1.2.3', 'Consent (Scanned Document)', 3);
INSERT INTO code (id, class_scheme, code, coding_scheme, display, transaction_id) VALUES (15, 'urn:uuid:2c6b8cb7-8b2a-4051-b291-b1ae6a575ef4', 'urn:oid:1.3.6.1.4.1.12559.11.10.1.3.2.4.1.1', 'epSOS Consent Code', 'Opt-in', 3);
INSERT INTO code (id, class_scheme, code, coding_scheme, display, transaction_id) VALUES (16, 'urn:uuid:2c6b8cb7-8b2a-4051-b291-b1ae6a575ef4', 'urn:oid:1.3.6.1.4.1.12559.11.10.1.3.2.4.1.2', 'epSOS Consent Code', 'Opt-out', 3);
INSERT INTO code (id, class_scheme, code, coding_scheme, display, transaction_id) VALUES (13, 'urn:uuid:f0306f51-975f-434e-a61c-c59651d33983', '60593-1', '2.16.840.1.113883.6.1', 'eDispensation', 2);
INSERT INTO code (id, class_scheme, code, coding_scheme, display, transaction_id) VALUES (9, 'urn:uuid:f33fb8ac-18af-42cc-ae0e-ed0b0bdb91e1', 'Not Used', 'epSOS Healthcare Facility Type Codes-Not Used', 'Not Used', 2);
INSERT INTO code (id, class_scheme, code, coding_scheme, display, transaction_id) VALUES (1, 'urn:uuid:41a5887f-8865-4c09-adf7-e362475b143a', '60593-1', '2.16.840.1.113883.6.1', 'eDispensation', 2);
INSERT INTO code (id, class_scheme, code, coding_scheme, display, transaction_id) VALUES (3, 'urn:uuid:f4f85eac-e6cb-4883-b524-f2705394840f', 'N', '2.16.840.1.113883.5.25', 'Normal', 2);
INSERT INTO code (id, class_scheme, code, coding_scheme, display, transaction_id) VALUES (11, 'urn:uuid:cccf5598-8b07-4b77-a05e-ae952c785ead', 'Not Used', 'epSOS Practice Setting Codes-Not Used', 'Not Used', 2);
INSERT INTO code (id, class_scheme, code, coding_scheme, display, transaction_id) VALUES (14, 'urn:uuid:aa543740-bdda-424e-8c96-df4873be8500', '60593-1', '2.16.840.1.113883.6.1', 'eDispensation', 2);
INSERT INTO code (id, class_scheme, code, coding_scheme, display, transaction_id) VALUES (2, 'urn:uuid:41a5887f-8865-4c09-adf7-e362475b143a', '57016-8', 'LOINC', 'Privacy Policy Acknowledgement Document', 3);
INSERT INTO code (id, class_scheme, code, coding_scheme, display, transaction_id) VALUES (12, 'urn:uuid:cccf5598-8b07-4b77-a05e-ae952c785ead', 'Not Used', 'epSOS Practice Setting Codes-Not Used', 'Not Used', 3);
INSERT INTO code (id, class_scheme, code, coding_scheme, display, transaction_id) VALUES (10, 'urn:uuid:f33fb8ac-18af-42cc-ae0e-ed0b0bdb91e1', 'Not Used', 'epSOS Healthcare Facility Type Codes-Not Used', 'Not Used', 3);
INSERT INTO code (id, class_scheme, code, coding_scheme, display, transaction_id) VALUES (7, 'urn:uuid:f4f85eac-e6cb-4883-b524-f2705394840f', 'N', '2.16.840.1.113883.5.25', 'Normal', 3);
INSERT INTO code (id, class_scheme, code, coding_scheme, display, transaction_id) VALUES (6, 'urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d', 'urn:ihe:iti:bppc:2007', 'IHE PCC', 'Consent', 3);
INSERT INTO code (id, class_scheme, code, coding_scheme, display, transaction_id) VALUES (18, 'urn:uuid:f0306f51-975f-434e-a61c-c59651d33983', '57016-8', '2.16.840.1.113883.6.1', 'Privacy Policy Acknowledgement Document', 3);
INSERT INTO code (id, class_scheme, code, coding_scheme, display, transaction_id) VALUES (17, 'urn:uuid:aa543740-bdda-424e-8c96-df4873be8500', '57016-8', '2.16.840.1.113883.6.1', 'Privacy Policy Acknowledgement Document', 3);



SELECT pg_catalog.setval('code_id_seq', 18, true);
