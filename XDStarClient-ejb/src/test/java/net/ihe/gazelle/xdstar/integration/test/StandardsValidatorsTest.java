package net.ihe.gazelle.xdstar.integration.test;

import net.ihe.gazelle.xdstar.validator.ws.ExternalValidators;
import net.ihe.gazelle.xdstar.validator.ws.XDSMetadataValidator;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.soap.SOAPException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import static org.junit.Assert.fail;

public class StandardsValidatorsTest {

    private static final Logger LOG = LoggerFactory.getLogger(StandardsValidatorsTest.class);

    private static final String pathDoc = "src/test/resources/samples/";
/*
    @Test
	public void validateIHE_PNR_REQ() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_PNR_REQ;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_PNR_RESP() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_PNR_RESP;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_RSQ_REQ() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_RSQ_REQ;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_RSQ_RESP() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_RSQ_RESP;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_RDS_REQ() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_RDS_REQ;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_RDS_RESP() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_RDS_RESP;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_XGQ_REQ() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_XGQ_REQ;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_XGQ_RESP() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_XGQ_RESP;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_XGR_REQ() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_XGR_REQ;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_XGR_RESP() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_XGR_RESP;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_RegDS_REQ() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_RegDS_REQ;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_RegDS_RESP() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_RegDS_RESP;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_PHARM1_REQ() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_PHARM1_REQ;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_PHARM1_RESP() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_PHARM1_RESP;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_XCF_REQ() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_XCF_REQ;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_XCF_RESP() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_XCF_RESP;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_DDS_REQ() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_DDS_REQ;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_DDS_RESP() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_DDS_RESP;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_MPQ_REQ() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_MPQ_REQ;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_MPQ_RESP() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_MPQ_RESP;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_RAD_68_REQ() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_RAD_68_REQ;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_RAD_68_RESP() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_RAD_68_RESP;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_RAD_69_XDSI_REQ() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_RAD_69_XDSI_REQ;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_RAD_69_XDSI_RESP() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_RAD_69_XDSI_RESP;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_RAD_69_XCAI_REQ() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_RAD_69_XCAI_REQ;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_RAD_69_XCAI_RESP() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_RAD_69_XCAI_RESP;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_RAD_75_XCAI_REQ() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_RAD_75_XCAI_REQ;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_RAD_75_XCAI_RESP() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_RAD_75_XCAI_RESP;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_ITI_61_XDSB_REQ() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_ITI_61_XDSB_REQ;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_DDS_MEDIA() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_DDS_MEDIA;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_XDRDS() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_XDRDS;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_XDRDS_ML() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_XDRDS_ML;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_XCA_RSQ_REQ() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_XCA_RSQ_REQ;
		testValidatorType(val);
	}
	
	@Test
	public void validateIHE_XCA_RSQ_RESP() throws SOAPException {
		ExternalValidators val = ExternalValidators.IHE_XCA_RSQ_RESP;
		testValidatorType(val);
	}
	*/


    private void testValidatorType(ExternalValidators val) throws SOAPException {
        File file = new File(pathDoc);
        if (file.isDirectory()) {
            File dir = null;
            for (File fis : file.listFiles()) {
                if (fis.getName().equals(val.toString())) {
                    dir = fis;
                }
            }
            if (dir == null || !dir.isDirectory()) {
                fail("The standard was not found : " + val.toString());
                return;
            }
            for (File fis : dir.listFiles()) {
                if (fis.getName().equals("1.xml")) {
                    String valRes = validateStandards(val, readDoc(fis.getAbsolutePath()));
                    Assert.assertFalse(valRes.contains("<ValidationTestResult>FAILED</ValidationTestResult>"));
                } else if (fis.getName().equals("2.xml")) {
                    String valRes = validateStandards(val, readDoc(fis.getAbsolutePath()));
                    Assert.assertTrue(valRes.contains("<ValidationTestResult>FAILED</ValidationTestResult>"));
                }
            }
        } else {
            fail("The standards specified has no tests !");
        }
    }

    private String validateStandards(ExternalValidators val, String doc) throws SOAPException {
        return XDSMetadataValidator.validateDocument(doc, val.getValue());
    }

    private static String readDoc(String name) {
        BufferedReader scanner = null;
        StringBuilder res = new StringBuilder();
        try {
            scanner = new BufferedReader(new FileReader(name));
            String line = scanner.readLine();
            while (line != null) {
                res.append(line + "\n");
                line = scanner.readLine();
            }
        } catch (Exception e) {
            LOG.error("" + e.getMessage());
        } finally {
            try {
                if (scanner != null) {
                    scanner.close();
                }
            } catch (Exception e) {
                LOG.error("" + e.getMessage());
            }
        }
        return res.toString();
    }

}
