package net.ihe.gazelle.xdstar.patient;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;

import net.ihe.gazelle.openempi.hb.PersonInfo;
import net.ihe.gazelle.openempi.hb.dao.DocumentHeaderDAO;
import net.ihe.gazelle.openempi.hb.dao.HibernateUtilOpenempi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PatientTest {

    private static final Logger LOG = LoggerFactory.getLogger(PatientTest.class);

    public static void main(String[] args) throws IOException {
        LOG.info(System.getProperty("ee" + JAXBContext.JAXB_CONTEXT_FACTORY));
//		PersonInfo selectedPersonInfo = new PersonInfo();
//		selectedPersonInfo.setPersonInfoId(2469776);
//		deletePersonInfo(selectedPersonInfo);
	}
	
	public static void deletePersonInfo(PersonInfo pi){
        if (pi != null){
            if (pi.getPersonInfoId() != null){
                EntityManager em = HibernateUtilOpenempi.buildEntityManager();
                pi = em.find(PersonInfo.class, pi.getPersonInfoId());
                em.remove(pi);
                em.flush();
                DocumentHeaderDAO.deleteDocumentHeader(pi.getDocumentHeader(), em);
            }
        }
    }

}
