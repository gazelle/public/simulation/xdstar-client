package net.ihe.gazelle.xdstar.test;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.ihe.gazelle.edit.FileReadWrite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FlattenDBSchema {

	private static final Logger LOG = LoggerFactory.getLogger(FlattenDBSchema.class);

	public static void main(String[] args) throws IOException {
	String res = flattenDB("/home/aboufahj/dump_xds_0109/dump_new_schema_3.sql", "/home/aboufahj/dump_xds_0109/dump_old_schema.sql");
	FileReadWrite.printDoc(res, "/home/aboufahj/dump_xds_0109/dump_new_schema_3_modified.sql");
    }

    private static String flattenDB(String tobemodified, String reference) throws IOException {
	String content = FileReadWrite.readDoc(tobemodified);
	String res = content;
	String ref = FileReadWrite.readDoc(reference);
	Pattern pat = Pattern.compile("^ALTER\\s*TABLE\\s*ONLY\\s*(.*)?\\s*ADD\\s*CONSTRAINT\\s*(.*)?\\s*UNIQUE\\s*\\((.*)?\\);", Pattern.MULTILINE);
	Matcher mat = pat.matcher(content);
	while(mat.find()) {
	    String research = "^ALTER\\s*TABLE\\s*ONLY\\s*" + mat.group(1) + 
		    "\\s*ADD\\s*CONSTRAINT\\s*\\s*(.*)?\\s*UNIQUE\\s*\\(" + mat.group(3) + "\\);";
	    Pattern pat2 = Pattern.compile(research, Pattern.MULTILINE);
	    Matcher mat2 = pat2.matcher(ref);
	    if (mat2.find()) {
			LOG.info(mat.group(2) +  " == " + mat2.group(1));
		 if (!res.contains(" " + mat.group(2))) {
			 LOG.info("problem with '" + mat.group(2) + "'");
		 }
		res =  res.replace(" " + mat.group(2), " " + mat2.group(1));
	    }
	}
	return res;
    }

}
