package net.ihe.gazelle.xdstar.integration.test;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({XDSToolsAccessTest.class, WADOValidatorTest.class})
public class AllTests {

}
