package net.ihe.gazelle.xdstar.xdstools2;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import net.ihe.gazelle.common.Concept;
import net.ihe.gazelle.xdstar.comon.util.SVSConsumerNew;

public class CodesGenerationEPR {
	
	static String contentTypeCode_oid = "1.3.6.1.4.1.12559.11.25.1.15.1";
	
	static String classCode_oid = "2.16.756.5.30.1.127.3.10.1.3";
	
	static String confidentialityCode_oid = "2.16.756.5.30.1.127.3.10.1.5";
	
	static String formatCode_oid = "2.16.756.5.30.1.127.3.10.1.9";
	
	static String healthcareFacilityTypeCode_oid = "2.16.756.5.30.1.127.3.10.1.11";
	
	static String practiceSettingCode_oid = "2.16.756.5.30.1.127.3.10.1.18";
	
	static String eventCodeList_oid = "";
	
	static String typeCode_oid = "2.16.756.5.30.1.127.3.10.1.27";
	
	static String mimeType_oid = "2.16.756.5.30.1.127.3.10.1.16";
	
	static String folderCodeList_oid = "";
	
	static String associationDocumentation_oid = "1.3.6.1.4.1.12559.11.4.3.11";
	
	static String[] listAssigningAuthority = {"&1.3.6.1.4.1.21367.2005.13.20.1000&ISO", "&1.3.6.1.4.1.21367.2005.13.20.2000&ISO", "&1.3.6.1.4.1.21367.2005.13.20.3000&ISO"};
	
	public static void main(String[] args) throws FileNotFoundException, JAXBException {
		
		Codes codes = new Codes();
		addCodeType(contentTypeCode_oid, "contentTypeCode", "urn:uuid:aa543740-bdda-424e-8c96-df4873be8500", codes);
		addCodeType(classCode_oid, "EprDocumentClassCode", "urn:uuid:41a5887f-8865-4c09-adf7-e362475b143a", codes);
		addCodeType(confidentialityCode_oid, "EprDocumentConfidentialityCode", "urn:uuid:f4f85eac-e6cb-4883-b524-f2705394840f", codes);
		addCodeType(formatCode_oid, "EprDocumentFormatCode", "urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d", codes);
		addCodeType(healthcareFacilityTypeCode_oid, "EprHealthcareFacilityTypeCode", "urn:uuid:f33fb8ac-18af-42cc-ae0e-ed0b0bdb91e1", codes);
		addCodeType(practiceSettingCode_oid, "EprDocumentPracticeSettingCode", "urn:uuid:cccf5598-8b07-4b77-a05e-ae952c785ead", codes);
		addCodeType(eventCodeList_oid, "eventCodeList", "urn:uuid:2c6b8cb7-8b2a-4051-b291-b1ae6a575ef4", codes);
		addCodeType(typeCode_oid, "EprDocumentTypeCode", "urn:uuid:f0306f51-975f-434e-a61c-c59651d33983", codes);
		addCodeType(folderCodeList_oid, "folderCodeList", "urn:uuid:1ba97051-7806-41a8-a48b-8fce7af683c5", codes, 
				"http://gazelle.ihe.net/SVSSimulator/rest/RetrieveValueSetForSimulator");
		addCodeTypeMimeType(mimeType_oid, codes);
		addCodeType(associationDocumentation_oid, "associationDocumentation", "urn:uuid:abd807a3-4432-4053-87b4-fd82c643d1f3", codes, 
				"http://gazelle.ihe.net/SVSSimulator/rest/RetrieveValueSetForSimulator");
		
		for (String string : listAssigningAuthority) {
			AssigningAuthority as = new AssigningAuthority();
			as.setId(string);
			codes.getAssigningAuthority().add(as);
		}
		
		marsh(codes);
	}
	
	private static void addCodeTypeMimeType(String mimeType_oid2, Codes codes) {
		CodeType ct = new CodeType();
		codes.getCodeType().add(ct);
		ct.setName("mimeType");
		SVSConsumerNew svs  = new SVSConsumerNew(){
			public String getSVSRepositoryUrl(EntityManager entityManager){
				return "https://ehealthsuisse.ihe-europe.net/SVSSimulator/rest/RetrieveValueSetForSimulator";
			}
		};
		List<Concept> lcons = svs.getConceptsListFromValueSet(mimeType_oid2, null);
		if (lcons != null){
			for (Concept concept : lcons) {
				Code cod = new Code();
				if (concept.getCode().equals("application/pdf")){
					cod.setCode("application/pdf");
					cod.setExt("pdf");
				}
				else if (concept.getCode().equals("text/xml")){
					cod.setCode("text/xml");
					cod.setExt("xml");
				}
				else if (concept.getCode().equals("application/x-hl7")){
					cod.setCode("application/x-hl7");
					cod.setExt("hl7");
				}
				else if (concept.getCode().equals("multipart/x-hl7-cda-level1")){
					cod.setCode("multipart/x-hl7-cda-level1");
					cod.setExt("hl7");
				}
				else if (concept.getCode().equals("application/dicom")){
					cod.setCode("application/dicom");
					cod.setExt("dicom");
				}
				else if (concept.getCode().equals("text/plain")){
					cod.setCode("text/plain");
					cod.setExt("txt");
				}
				else if (concept.getCode().equals("multipart/related")){
					cod.setCode("multipart/related");
					cod.setExt("mpr");
				}
				else if (concept.getCode().equals("image/tiff")){
					cod.setCode("image/tiff");
					cod.setExt("tiff");
				}
				else if (concept.getCode().equals("audio/mpeg")){
					cod.setCode("audio/mpeg");
					cod.setExt("mpeg");
				}
				else if (concept.getCode().equals("video/mpeg")){
					cod.setCode("video/mpeg");
					cod.setExt("mpeg");
				}
				else if (concept.getCode().equals("image/png")){
					cod.setCode("image/png");
					cod.setExt("png");
				}
				else if (concept.getCode().equals("image/jpeg")){
					cod.setCode("image/jpeg");
					cod.setExt("jpeg");
				}
				else if (concept.getCode().equals("image/gif")){
					cod.setCode("image/gif");
					cod.setExt("gif");
				}
				else if (concept.getCode().equals("text/goofy")){
					cod.setCode("text/goofy");
					cod.setExt("gfy");
				}
				ct.getCode().add(cod);
			}
		}
	}
	
	private static void addCodeType(String oid, String name, String classScheme, Codes codes){
		addCodeType(oid, name, classScheme, codes, "https://ehealthsuisse.ihe-europe.net/SVSSimulator/rest/RetrieveValueSetForSimulator");
	}

	private static void addCodeType(String oid, String name, String classScheme, Codes codes, final String svsurl){
		String[] oids = oid.split(",");
		SVSConsumerNew svs  = new SVSConsumerNew(){
			public String getSVSRepositoryUrl(EntityManager entityManager){
				return svsurl;
			}
		};
		CodeType ct = new CodeType();
		codes.getCodeType().add(ct);
		ct.setName(name);
		ct.setClassScheme(classScheme);
		for (String string : oids) {
			List<Concept> lcons = svs.getConceptsListFromValueSet(string, null);
			if (lcons != null) {
				for (Concept concept : lcons) {
					Code code = new Code();
					code.setCode(removeInvalidXMLCharacters(concept.getCode()));
					code.setCodingScheme(removeInvalidXMLCharacters(concept.getCodeSystem()));
					if (code.getCodingScheme().equals("1.3.6.1.4.1.12559.11.4.9")){
						code.setCodingScheme(removeInvalidXMLCharacters(concept.getCodeSystemName()));
					}
					code.setDisplay(removeInvalidXMLCharacters(concept.getDisplayName()));
					ct.getCode().add(code);
				}
			}
		}
	}
	
	static void marsh(Codes cod) throws JAXBException, FileNotFoundException{
		JAXBContext jj = JAXBContext.newInstance(Codes.class);
		Marshaller mm = jj.createMarshaller();
		mm.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		mm.marshal(cod, new FileOutputStream("codes_new_eheathsuisse.xml"));
	}
	
	public static String removeInvalidXMLCharacters(String s)
    {
        StringBuilder out = new StringBuilder();

        int codePoint;
        int i = 0;

        while (i < s.length())
        {
            // This is the unicode code of the character.
            codePoint = s.codePointAt(i);
            if ((codePoint == 0x9) ||
                    (codePoint == 0xA) ||
                    (codePoint == 0xD) ||
                    ((codePoint >= 0x20) && (codePoint <= 0xD7FF)) ||
                    ((codePoint >= 0xE000) && (codePoint <= 0xFFFD)) ||
                    ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF)))
            {
                out.append(Character.toChars(codePoint));
            }
            i += Character.charCount(codePoint);
        }
        String res = out.toString();
        res = cleanInvalidXmlChars(res, "");
        return out.toString();
    }
	
	public static String cleanInvalidXmlChars(String text, String replacement) {
	    String re = "[^\\x09\\x0A\\x0D\\x20-\\xD7FF\\xE000-\\xFFFD\\x10000-x10FFFF]";
	    return text.replaceAll(re, replacement);
	}

}
