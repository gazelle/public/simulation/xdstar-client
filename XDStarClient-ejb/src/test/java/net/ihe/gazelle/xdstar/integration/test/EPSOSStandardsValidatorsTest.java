package net.ihe.gazelle.xdstar.integration.test;

import net.ihe.gazelle.xdstar.validator.ws.ExternalValidators;
import net.ihe.gazelle.xdstar.validator.ws.XDSMetadataValidator;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.soap.SOAPException;
import java.io.*;

import static org.junit.Assert.fail;

public class EPSOSStandardsValidatorsTest {

    private static final Logger LOG = LoggerFactory.getLogger(EPSOSStandardsValidatorsTest.class);

    private static final String pathDoc = "src/test/resources/samples/";

    /*
        @Test
        public void validateEPSOS_DS_INIT_REQ() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS_DS_INIT_REQ;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS_DS_INIT_RESP() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS_DS_INIT_RESP;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS_DS_DISC_REQ() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS_DS_DISC_REQ;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS_DS_DISC_RESP() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS_DS_DISC_RESP;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS_CS_PUT_REQ() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS_CS_PUT_REQ;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS_CS_PUT_RESP() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS_CS_PUT_RESP;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS_CS_DISC_REQ() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS_CS_DISC_REQ;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS_CS_DISC_RESP() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS_CS_DISC_RESP;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS_OS_LIST_REQ_V2() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS_OS_LIST_REQ_V2;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS_OS_LIST_RESP_V2() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS_OS_LIST_RESP_V2;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS_PS_LIST_REQ_V2() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS_PS_LIST_REQ_V2;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS_PS_LIST_RESP_V2() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS_PS_LIST_RESP_V2;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS_OS_LIST_REQ_V1() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS_OS_LIST_REQ_V1;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS_OS_LIST_RESP_V1() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS_OS_LIST_RESP_V1;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS_PS_LIST_REQ_V1() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS_PS_LIST_REQ_V1;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS_PS_LIST_RESP_V1() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS_PS_LIST_RESP_V1;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS_OS_RET_REQ_V1() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS_OS_RET_REQ_V1;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS_OS_RET_RESP_V1() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS_OS_RET_RESP_V1;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS_PS_RET_REQ_V1() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS_PS_RET_REQ_V1;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS_PS_RET_RESP_V1() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS_PS_RET_RESP_V1;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS2_XCA_LIST_REQ() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS2_XCA_LIST_REQ;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS2_XCA_LIST_RESP() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS2_XCA_LIST_RESP;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS2_XCA_RET_REQ() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS2_XCA_RET_REQ;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS2_XCA_RET_RESP() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS2_XCA_RET_RESP;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS2_XDR_REQ() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS2_XDR_REQ;
            testValidatorType(val);
        }

        @Test
        public void validateEPSOS2_XDR_RESP() throws SOAPException {
            ExternalValidators val = ExternalValidators.EPSOS2_XDR_RESP;
            testValidatorType(val);
        }
        */
    private void testValidatorType(ExternalValidators val) throws SOAPException {
        File file = new File(pathDoc);
        if (file.isDirectory()) {
            File dir = null;
            for (File fis : file.listFiles()) {
                if (fis.getName().equals(val.toString())) {
                    dir = fis;
                }
            }
            if (dir == null || !dir.isDirectory()) {
                fail("The standard was not found : " + val.toString());
                return;
            }
            for (File fis : dir.listFiles()) {
                if (fis.getName().equals("1.xml")) {
                    String valRes = validateStandards(val, readDoc(fis.getAbsolutePath()));
                    Assert.assertFalse(valRes.contains("<ValidationTestResult>FAILED</ValidationTestResult>"));
                } else if (fis.getName().equals("2.xml")) {
                    String valRes = validateStandards(val, readDoc(fis.getAbsolutePath()));
                    Assert.assertTrue(valRes.contains("<ValidationTestResult>FAILED</ValidationTestResult>"));
                }
            }
        } else {
            fail("The standards specified has no tests !");
        }
    }

    private String validateStandards(ExternalValidators val, String doc) throws SOAPException {
        return XDSMetadataValidator.validateDocument(doc, val.getValue());
    }

    private static String readDoc(String name) {
        BufferedReader scanner = null;
        StringBuilder res = new StringBuilder();
        try {
            scanner = new BufferedReader(new FileReader(name));
            String line = scanner.readLine();
            while (line != null) {
                res.append(line + "\n");
                line = scanner.readLine();
            }
        } catch (FileNotFoundException e) {
            LOG.error("" + e.getMessage());
        } catch (IOException e) {
            LOG.error("" + e.getMessage());
        } finally {
            try {
                if (scanner != null) {
                    scanner.close();
                }
            } catch (Exception e) {
                LOG.error("" + e.getMessage());
            }
        }
        return res.toString();
    }

}
