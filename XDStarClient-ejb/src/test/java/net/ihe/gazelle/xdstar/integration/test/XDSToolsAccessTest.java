package net.ihe.gazelle.xdstar.integration.test;

import static org.junit.Assert.assertTrue;
import net.ihe.gazelle.xdstar.comon.util.URLCheck;
import net.ihe.gazelle.xdstar.validator.NistValidator;

import org.junit.Ignore;
import org.junit.Test;

/**
 * Test the possibility to access external tool of validation of metadatas (epSOS and nist tools)
 * @author abderrazek boufahja
 *
 */
@Ignore("All tools tested here are down. The class should be removed")
public class XDSToolsAccessTest {

	/**
	 * Test the access to the nist tool
	 */
	@Ignore("the Nist tool is down. The test should be removed")
	@Test
	public void testNistXDSToolsCodesAccess() {
		assertTrue(URLCheck.checkURL("http://ihexds.nist.gov:12090/xdsref/codes/codes.xml"));
	}
	
	/**
	 * Test the access to the nist tool
	 */
	@Ignore("the jumbo server is down. The test should be removed")
	@Test
	public void testEbRIMXSDAccess() {
		assertTrue(URLCheck.checkURL("http://jumbo.irisa.fr:9080//xdsref/schema/"));
	}
	
	/**
	 * Test the access to the epSOS tool
	 */
	@Ignore("the jumbo server is down. The test should be removed")
	@Test
	public void testEpSOSXDSToolsCodesAccess() {
		assertTrue(URLCheck.checkURL("http://jumbo.irisa.fr:9080/xdsref/codes/codes.xml"));
	}

}
