package net.ihe.gazelle.xdstar.integration.test;

import static org.junit.Assert.assertTrue;

import javax.xml.soap.SOAPException;

import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.xdstar.wado.validator.WADOValidator;

import org.junit.Test;

public class WADOValidatorTest {
	/**
	 * Test the possibility to validate a wado request with IHE WADO validator.
	 */
	@Test
	public void testWADOValidatorValidateRightDocument() throws SOAPException {
		DetailedResult rlt ;
		WADOValidator wadoValidator = new WADOValidator() ;
		String url = "https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=%2B2.5E3" ;
		rlt = wadoValidator.validateMessageByValidator(url, "IHE - WADO") ;
		assertTrue("The result must be PASSED", rlt.getValidationResultsOverview().getValidationTestResult().equals("PASSED"));
	}
	
	/**
	 * Test the possibility to reject a wado request with IHE WADO validator.
	 */
	@Test
	public void testWADOValidatorValidateWrongDocument() throws SOAPException {
		DetailedResult rlt ;
		WADOValidator wadoValidator = new WADOValidator() ;
		String url = "https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&presentationUID=1.2.250.1.F0.40211.111A13.1D1C16"
					+ "&presentationSeriesUID=1.2.250.1.59.40211.222120.191817" ;
		rlt = wadoValidator.validateMessageByValidator(url, "IHE - WADO") ;
		assertTrue("The result must be FAILED", rlt.getValidationResultsOverview().getValidationTestResult().equals("FAILED"));
	}
	
}
