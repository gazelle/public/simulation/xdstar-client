package net.ihe.gazelle.xdstar.xdstools2;

public enum CodesValueSet {
	
	eventCodeList("eventCodeList", "1.3.6.1.4.1.12559.11.4.3.7"), mimeType("mimeType", "1.3.6.1.4.1.12559.11.4.3.9"), 
	contentTypeCode("contentTypeCode", "1.3.6.1.4.1.12559.11.4.3.1"), confidentialityCode("confidentialityCode", "1.3.6.1.4.1.12559.11.4.3.3"),
	formatCode("formatCode", "1.3.6.1.4.1.12559.11.4.3.4"), healthcareFacilityTypeCode("healthcareFacilityTypeCode", "1.3.6.1.4.1.12559.11.4.3.5"),
	practiceSettingCode("practiceSettingCode", "1.3.6.1.4.1.12559.11.4.3.6"), typeCode("typeCode", "1.3.6.1.4.1.12559.11.4.3.8"), 
	folderCodeList("folderCodeList", "1.3.6.1.4.1.12559.11.4.3.10"), classCode("classCode", "1.3.6.1.4.1.12559.11.4.3.2")
	;
	
	private String name;
	
	private String oid;
	
	public String getName() {
		return name;
	}


	public String getOid() {
		return oid;
	}

	private CodesValueSet(String name, String oid) {
		this.name = name;
		this.oid = oid;
	}
	
	public static CodesValueSet extractCVS(String name) {
		for (CodesValueSet cvs : CodesValueSet.values()) {
			if (cvs.getName().equals(name)) {
				return cvs;
			}
		}
		return null;
	}

}
