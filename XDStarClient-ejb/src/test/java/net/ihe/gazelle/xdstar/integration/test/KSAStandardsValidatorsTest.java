package net.ihe.gazelle.xdstar.integration.test;

import net.ihe.gazelle.common.StringMatcher;
import net.ihe.gazelle.xdstar.comon.util.SVSConsumerNew;
import net.ihe.gazelle.xdstar.validator.ws.ExternalValidators;
import net.ihe.gazelle.xdstar.validator.ws.XDSMetadataValidator;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.xml.soap.SOAPException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import static org.junit.Assert.fail;

public class KSAStandardsValidatorsTest {

    private static final Logger LOG = LoggerFactory.getLogger(KSAStandardsValidatorsTest.class);

    private static final String pathDoc = "src/test/resources/samples/";

    {
        try {
            XDSMetadataValidator.validateDocument(null, null);
        } catch (SOAPException e) {

        } finally {
            StringMatcher.setValueSetProvider(new SVSConsumerNew() {
                @Override
                protected String getSVSRepositoryUrl(EntityManager entityManager) {
                    return "http://k-project.ihe-europe.net:8180/SVSSimulator/rest/RetrieveValueSetForSimulator";
                }
            });
        }

    }

    /*
        @Test
        public void validateKSA_XDS_PNR_REQ() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_XDS_PNR_REQ;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_CLO_PNR_OC_REQ() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_CLO_PNR_OC_REQ;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_CLO_PNR_OC_RESP() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_CLO_PNR_OC_RESP;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_CLO_PNR_OF_REQ() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_CLO_PNR_OF_REQ;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_CLO_PNR_OF_RESP() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_CLO_PNR_OF_RESP;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_CLR_PNR_REQ() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_CLR_PNR_REQ;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_CLR_PNR_RESP() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_CLR_PNR_RESP;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_DDS_REQ() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_DDS_REQ;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_DDS_RESP() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_DDS_RESP;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_SIIR_RAD_69_REQ() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_SIR_RAD_69_REQ;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_SIIR_RAD_69_RESP() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_SIR_RAD_69_RESP;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_SIR_PNR_BSRR_REQ() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_SIR_PNR_BSRR_REQ;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_SIR_PNR_BSRR_RESP() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_SIR_PNR_BSRR_RESP;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_SIR_PNR_DRR_REQ() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_SIR_PNR_DRR_REQ;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_SIR_PNR_DRR_RESP() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_SIR_PNR_DRR_RESP;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_SIR_PNR_IM_REQ() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_SIR_PNR_IM_REQ;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_SIR_PNR_IM_RESP() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_SIR_PNR_IM_RESP;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_XDS_PNR_RESP() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_XDS_PNR_RESP;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_XDS_RDS_REQ() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_XDS_RDS_REQ;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_XDS_RDS_RESP() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_XDS_RDS_RESP;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_XDS_RSQ_REQ() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_XDS_RSQ_REQ;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_XDS_RSQ_RESP() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_XDS_RSQ_RESP;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_BPPC_REQ() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_BPPC_REQ;
            testValidatorType(val);
        }

        @Test
        public void validateKSA_BPPC_RESP() throws SOAPException {
            ExternalValidators val = ExternalValidators.KSA_BPPC_RESP;
            testValidatorType(val);
        }
        */
    private void testValidatorType(ExternalValidators val) throws SOAPException {
        File file = new File(pathDoc);
        if (file.isDirectory()) {
            File dir = null;
            for (File fis : file.listFiles()) {
                if (fis.getName().equals(val.toString())) {
                    dir = fis;
                }
            }
            if (dir == null || !dir.isDirectory()) {
                fail("The standard was not found : " + val.toString());
                return;
            }
            for (File fis : dir.listFiles()) {
                if (fis.getName().equals("1.xml")) {
                    String valRes = validateStandards(val, readDoc(fis.getAbsolutePath()));
                    boolean ss = valRes.contains("<ValidationTestResult>FAILED</ValidationTestResult>");
                    if (ss) {
                        LOG.info(valRes);
                    }
                    Assert.assertFalse(valRes.contains("<ValidationTestResult>FAILED</ValidationTestResult>"));
                } else if (fis.getName().equals("2.xml")) {
                    String valRes = validateStandards(val, readDoc(fis.getAbsolutePath()));
                    Assert.assertTrue(valRes.contains("<ValidationTestResult>FAILED</ValidationTestResult>"));
                }
            }
        } else {
            fail("The standards specified has no tests !");
        }
    }

    private String validateStandards(ExternalValidators val, String doc) throws SOAPException {
        return XDSMetadataValidator.validateDocument(doc, val.getValue());
    }

    private static String readDoc(String name) {
        BufferedReader scanner = null;
        StringBuilder res = new StringBuilder();
        try {
            scanner = new BufferedReader(new FileReader(name));
            String line = scanner.readLine();
            while (line != null) {
                res.append(line + "\n");
                line = scanner.readLine();
            }
        } catch (Exception e) {
            LOG.error("" + e.getMessage());
        } finally {
            try {
                if (scanner != null) {
                    scanner.close();
                }
            } catch (Exception e) {
                LOG.error("" + e.getMessage());
            }
        }
        return res.toString();
    }

}
