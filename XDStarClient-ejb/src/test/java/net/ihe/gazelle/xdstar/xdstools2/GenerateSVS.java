package net.ihe.gazelle.xdstar.xdstools2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import net.ihe.gazelle.datatypes.CE;
import net.ihe.gazelle.svs.ConceptListType;
import net.ihe.gazelle.svs.RetrieveValueSetResponseType;
import net.ihe.gazelle.svs.ValueSetResponseType;

public class GenerateSVS {
	
	public static void main(String[] args) throws JAXBException, FileNotFoundException {
		
		String outputFile = "res";
		File fil = new File(outputFile);
		fil.mkdir();
		
		String codesPath = "/home/aboufahj/Téléchargements/codes_2018.xml";
		
		JAXBContext jax = JAXBContext.newInstance(Codes.class);
		Unmarshaller um = jax.createUnmarshaller();
		Codes codes = (Codes) um.unmarshal(new File(codesPath));
		
		for (CodeType code : codes.getCodeType()) {
			extractSVS(code, fil);
		}
		
	}

	private static void extractSVS(CodeType code, File out) throws FileNotFoundException, JAXBException {
		CodesValueSet cvs = CodesValueSet.extractCVS(code.getName());
		if (cvs != null) {
			RetrieveValueSetResponseType rvsr = new RetrieveValueSetResponseType();
			rvsr.setValueSet(new ValueSetResponseType());
			rvsr.getValueSet().setDisplayName(cvs.getName());
			rvsr.getValueSet().setVersion("20180403");
			rvsr.getValueSet().setId(cvs.getOid());
			rvsr.getValueSet().getConceptList().add(new ConceptListType());
			rvsr.getValueSet().getConceptList().get(0).setLang("en-US");
			for (Code cons : code.getCode()) {
				CE ce = new CE();
				ce.setCode(cons.getCode());
				if (cons.getCodingScheme() != null) {
					if (cons.getCodingScheme().matches("(\\d|\\.)*")) { 
						ce.setCodeSystem(cons.getCodingScheme());
					}
					else {
						ce.setCodeSystem("1.3.6.1.4.1.12559.11.4.9");
						ce.setCodeSystemName(cons.getCodingScheme());
					}
				}
				ce.setDisplayName(cons.getDisplay());
				rvsr.getValueSet().getConceptList().get(0).getConcept().add(ce);
			}
			createFile(rvsr);
		}
		 
	}
	
	private static void createFile(RetrieveValueSetResponseType rvsr) throws JAXBException, FileNotFoundException {
		if (rvsr != null) {
			File output = new File("res/" + rvsr.getValueSet().getId() + ".xml");
			JAXBContext jx = JAXBContext.newInstance(RetrieveValueSetResponseType.class);
			Marshaller m = jx.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(rvsr, new FileOutputStream(output));
		}
	}

}
