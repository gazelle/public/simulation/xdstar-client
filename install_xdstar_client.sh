#!/bin/bash
source common.sh 

function show_help() {
    echo "install_xdstar_client install XDStarClient into the server (with the database, ...)"
    echo " -hap (host and port - default :  localhost:8380)"
}

function initResources() {
	wget https://gazelle.ihe.net/jenkins/job/Installer_script/ws/_install.sh -O _install.sh --no-check-certificate
	wget https://gazelle.ihe.net/jenkins/job/XDStarClient/ws/configuration/jboss7/standalone/configuration/standalone.xml --no-check-certificate
	wget https://gazelle.ihe.net/jenkins/job/XDStarClient/ws/configuration/sql/dump_conf_2_0_12.sql --no-check-certificate
	wget https://gazelle.ihe.net/jenkins/job/XDStarClient/ws/configuration/opt_XDStarClient.zip --no-check-certificate
}

function installStandaloneXML() {
	if ! $INSTALL_STANDALONE ; then
		read -p "Do you want to install jboss server? [y,n]"
		if [ "$REPLY" = "y" ] || [ "$REPLY" = "Y" ] || [ "$REPLY" = "yes" ] || [ "$REPLY" = "YES" ] || [ "$REPLY" = "Yes" ]; then
		  INSTALL_STANDALONE=true
		fi
	fi
	if $INSTALL_STANDALONE ; then 
		doCommand "cp standalone.xml /usr/local/jboss-as-7.2.0.Final/standalone/configuration/" "not able to copy standalone.xml"
	fi
}

checkScriptRunAsRoot

DATABASE="xdstar-client"
PACKAGE="XDStarClient.ear"
JENKINS_JOB="XDStarClient"
EAR_FOLDER="XDStarClient-ear"
DEPLOY_ROOT="/usr/local/jboss-as-7.2.0.Final/standalone/deployments/"
hap=localhost:8380
INSTALL_STANDALONE=false

while [ "$1" != "" ]; do
    case $1 in
        "-h")
            cecho -green "showing help"
            show_help
            exit 0
            ;;
        *)
            name1=$1
            name=${name1:1}
            shift
            value=$1
            eval "$name=$value"
            ;;
    esac
    shift
done

## init working folder ##
initResources
checkFolderExists $DEPLOY_ROOT
checkFolderExists "/opt"
checkFileExists "_install.sh"
checkFileExists "standalone.xml"
checkFileExists "opt_XDStarClient.zip"
checkFileExists "dump_conf_2_0_12.sql"
doCommand "chmod +x _install.sh" "chmod does not work on _install.sh"

sed sed s/_HOSTANDPORT/"$hap"/ dump_conf_2_0_12.sql > dump_conf_2_0_12.sql


echo "install xdstar-client database and EAR"
./_install.sh "$DATABASE" "$PACKAGE" "$JENKINS_JOB" "$EAR_FOLDER" "$DEPLOY_ROOT"

if [ "`psql -U gazelle -W gazelle --list | grep xdstar-client`"="" ]; then
		cecho -red "xdstar-client was not created !"
        exit 1
fi

## configure app_configuration table ##
RESETDB="psql -U gazelle -W gazelle -d \"$DATABASE\" -f dump_conf_2_0_12.sql"
doCommand "$RESETDB" "Failed to reset database $DATABASE"

## configure /opt/XDStarClient/ ##
UNZIPOPT="unzip opt_XDStarClient.zip /opt/"
doCommand "$UNZIPOPT" "Failed to upzip opt_XDStarClient.zip in /opt/"

## copy standalone.xml to the jboss7 ##
installStandaloneXML
